﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.IO;
using RFP;
using MethodClasses;

namespace DAL.CommonClasses
{
    public static class Functions
    {
        public static bool ImportReplenishList(string fileName, int Retailer)
        {
            List<string[]> csvData = new List<string[]>();
            string line;
            string[] row;
            try
            {
                using (StreamReader r = new StreamReader(fileName))
                {
                    while ((line = r.ReadLine()) != null)
                    {
                        row = line.Split(',');
                        csvData.Add(row);
                    }
                }
                //Check imported data is valid: first row should contain headings "Component" and "Quantity"
                if (csvData[0][0].ToString() != "Component" || csvData[0][1].ToString() != "Quantity")
                {
                    ImportError ie = new ImportError();
                    ie.Date = DateTime.Now;
                    ie.FileName = fileName;
                    ie.Retailer = Retailer;
                    ie.Description = "Import data is not of the correct type. Please ensure that there are only 2 columns in the file with headings " +
                        (char)34 + "Component" + (char)34 + " and " + (char)34 + "Quantity" + (char)34;
                    ImportErrorHandler.Add(ie);
                    return false;
                }
                else
                {
                    //check all rows only have 2 columns, that the item in the first column is a valid stock code for the retailer and the 
                    //second column is an integer
                    bool OK = true;
                    int qty;
                    for (int i = 1; i < csvData.Count(); i++) //don't check first row
                    {
                        if ((csvData[i][0].ToString() ?? string.Empty) != string.Empty)
                        {
                            if (csvData[i].Length != 2)
                            {
                                ImportError ie = new ImportError();
                                ie.Date = DateTime.Now;
                                ie.FileName = fileName;
                                ie.Retailer = Retailer;
                                ie.Description = "Row " + i.ToString() + " has the wrong number of entries. Should only be 2";
                                ImportErrorHandler.Add(ie);
                                OK = false;
                            }
                            else 
                            {
                                //Valid component code?
                                if (ComponentHandler.GetComponentIdFromCode(csvData[i][0].ToString(), Retailer) == 0)
                                {
                                    ImportError ie = new ImportError();
                                    ie.Date = DateTime.Now;
                                    ie.FileName = fileName;
                                    ie.Retailer = Retailer;
                                    ie.Description = "Row " + i.ToString() + " has invalid stock code: " + csvData[i][0].ToString();
                                    ImportErrorHandler.Add(ie);
                                    OK = false;
                                }
                                else
                                {
                                    if (!int.TryParse(csvData[i][1].ToString(), out qty))
                                    {
                                        ImportError ie = new ImportError();
                                        ie.Date = DateTime.Now;
                                        ie.FileName = fileName;
                                        ie.Retailer = Retailer;
                                        ie.Description = "Row " + i.ToString() + " has invalid quantity. Must be an integer";
                                        ImportErrorHandler.Add(ie);
                                        OK = false;
                                    }
                                }
                            }
                        }
                    }
                    if (OK)
                    {
                        //Import data
                        //Create replenish list
                        ReplenishList rpl = new ReplenishList();
                        ReplenishListDetail rpldet;
                        rpl.ReplenishDate = DateTime.Now;
                        rpl.Retailer = Retailer;
                        rpl.Status = (int)Globals.ReplenishListStatus.New;
                        rpl.FileName = Path.GetFileNameWithoutExtension(fileName);
                        int rplId = ReplenishListHandler.Add(rpl);
                        for (int j = 1; j < csvData.Count(); j++) //don't check first row
                        {
                            rpldet = new ReplenishListDetail();
                            rpldet.Component = ComponentHandler.GetComponentIdFromCode(csvData[j][0].ToString(), Retailer);
                            rpldet.Quantity = int.Parse(csvData[j][1].ToString());
                            rpldet.ReplenishList = rplId;
                            ReplenishListHandler.AddDetail(rpldet);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ImportError ie = new ImportError();
                ie.Date = DateTime.Now;
                ie.FileName = fileName;
                ie.Retailer = Retailer;
                ie.Description = ex.Message;
                ImportErrorHandler.Add(ie);
                return false;
            }
        }

        public static bool ExportStockList(Retailer r, string outputFile)
        {
            CSVExport exportList = new CSVExport();

            try
            {
                List<StockLevel> stockList = StockLevelHandler.GetRetailerStockExportList(r.Id);
                foreach (StockLevel sl in stockList)
                {
                    exportList.AddRow();
                    exportList["Component"] = sl.Batch1.Component1.RetailerStockCode;
                    exportList["Quantity"] = sl.Quantity;
                }
                exportList.ExportToFile(outputFile);
                return true;
            }
            catch (Exception ex)
            {
                Exceptions.HandleException(ex, "ExportStockList", false);
                return false;
            }
        }
    }
}
