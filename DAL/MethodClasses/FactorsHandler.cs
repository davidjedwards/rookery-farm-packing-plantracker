﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;

namespace MethodClasses
{
    public static class FactorsHandler
    {
        public static string GetFTPDownloadFolder(int environment)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    return (from f in rfc.Factors
                            where f.Environment == environment
                            select f.FTPDownloadFolder).SingleOrDefault();

                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_GetFTPDownloadFolder", true);
                    return null;
                }
            }
        }
        public static string GetFTPUploadFolder(int environment)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    return (from f in rfc.Factors
                            where f.Environment == environment
                            select f.FTPUploadFolder).SingleOrDefault();

                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_GetFTPUploadFolder", true);
                    return null;
                }
            }
        }

        public static bool ShowUploadError(int environment)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    return (from f in rfc.Factors
                            where f.Environment == environment
                            select f.ShowUploadError).SingleOrDefault();

                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_ShowUploadError", true);
                    return false;
                }
            }
        }

        public static bool ShowDownloadError(int environment)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    return (from f in rfc.Factors
                            where f.Environment == environment
                            select f.ShowDownloadError).SingleOrDefault();

                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_ShowDownloadError", true);
                    return false;
                }
            }
        }

        public static bool ShowDownloadsAvailable(int environment)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    return (from f in rfc.Factors
                            where f.Environment == environment
                            select f.ShowDownloadsAvailable).SingleOrDefault();

                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_ShowDownloadsAvailable", true);
                    return false;
                }
            }
        }

        public static void Update(Factor f)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Factors.Attach(f);
                    rfc.Entry(f).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_Update", true);
                }
            }
        }

        public static Factor GetFactorByEnvironment(int environment)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    return (from f in rfc.Factors where f.Environment == environment select f).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "FactorsHandler_GetFactorByEnvironment", true);
                    return null;
                }
            }
        }

    }
}
