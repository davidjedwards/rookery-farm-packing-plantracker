﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;

namespace MethodClasses
{
    public static class ErrorsHandler
    {
        public static void Add(Error err)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Errors.Add(err);
                    rfc.SaveChanges();
                }
                catch
                {
                }
            }
        }
    }
}
