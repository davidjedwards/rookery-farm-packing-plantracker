﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;


namespace MethodClasses
{
    public static class ReplenishListHandler
    {
        /// <summary>
        /// Add Replenish List
        /// </summary>
        /// <param name="newBatch"></param>
        /// <returns></returns>
        public static int Add(ReplenishList newReplenList)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.ReplenishLists.Add(newReplenList);
                    rfc.SaveChanges();
                    return newReplenList.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_Add", true);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Delete Replenish List
        /// </summary>
        /// <param name="delBatch"></param>
        /// <returns></returns>
        public static bool Delete(ReplenishList delReplenList)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(delReplenList).State = System.Data.Entity.EntityState.Deleted;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_Delete", true);
                    return false;
                }
            }
        }

        public static int AddDetail(ReplenishListDetail newReplenListDetail)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.ReplenishListDetails.Add(newReplenListDetail);
                    rfc.SaveChanges();
                    return newReplenListDetail.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_AddDetail", true);
                    return 0;
                }
            }
        }

        public static ReplenishList GetReplenishListById(int rplId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    ReplenishList rpl = (from r in rfc.ReplenishLists
                                         where r.Id == rplId
                                         select r).SingleOrDefault();
                    return rpl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_GetReplenishListById", true);
                    return null;
                }
            }
        }

        public static ReplenishListDetail GetReplenishListDetailById(int rpldId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    ReplenishListDetail rpld = (from r in rfc.ReplenishListDetails
                                                where r.Id == rpldId
                                                select r).SingleOrDefault();
                    return rpld;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_GetReplenishListDetailById", true);
                    return null;
                }
            }
        }



        public static bool Update(ReplenishList updReplenList)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(updReplenList).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_Update", true);
                    return false;
                }
            }
        }

        public static BindingSource GetReplenishLists(int retailerId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var replens = from r in rfc.ReplenishLists
                                  where (r.Retailer == retailerId || retailerId == 0)
                                  orderby r.ReplenishDate descending
                                  select new
                                  {
                                      r.Id,
                                      r.Retailer1.Name,
                                      r.ReplenishDate,
                                      r.Status,
                                      r.FileName
                                  };

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(ReplenListOutput);
                    foreach (var n in replens)
                    {
                        ReplenListOutput rplOut = new ReplenListOutput(n.Id,
                                                                       n.Name,
                                                                       (DateTime)n.ReplenishDate,
                                                                       n.Status,
                                                                       Globals.ReplenListStatusScreenDesc((Globals.ReplenishListStatus)n.Status),
                                                                       n.FileName);
                        bs.Add(rplOut);
                    }
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_GetReplenishLists", true);
                    return null;
                }
            }
        }

        public static void UpdateReplenishListStatus(int replenList)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    //Determine status as follows:
                    //For each replenishment list line determine if the line has been fully picked. If it has set value of 1 against line otherwise
                    //set value of 0. Sum all these values and if they equal the count of replenishment list lines then it's fully picked, otherwise
                    //it's partially picked
                    var replenSum = from r in rfc.ReplenishListDetails
                                    where r.ReplenishList == replenList
                                    select new
                                    {
                                        PickedQuantity = (int)(r.BatchHistories.Where(n => n.Action == (int)Globals.Action.Picked).Sum(n => n.Quantity) ?? 0),
                                        SetToPick = (int)(r.StockLevels.Where(n => n.Quality == (int)Globals.CropQuality.Pick).Sum(n => n.Quantity) ?? 0)
                                    };
                    int PickedLines = replenSum.Count(n => n.PickedQuantity > 0 && n.PickedQuantity >= n.SetToPick); 
                    int PickLines = replenSum.Count();
                    ReplenishList rpl = (from r in rfc.ReplenishLists
                                         where r.Id == replenList
                                         select r).SingleOrDefault();

                    rfc.Entry(rpl).State = System.Data.Entity.EntityState.Unchanged;

                    if (PickedLines == 0)
                    {
                        rpl.Status = (int)Globals.ReplenishListStatus.Applied;
                    }
                    else if (PickedLines >= PickLines)
                    {
                        rpl.Status = (int)Globals.ReplenishListStatus.Picked;
                    }
                    else
                    {
                        rpl.Status = (int)Globals.ReplenishListStatus.PartPicked;
                    }
                    rfc.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_UpdateReplenishListStatus", true);
                }
            }
        }

        public static BindingSource GetReplenishListDetails(int replenList)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var replenDets = from r in rfc.ReplenishListDetails
                                     where r.ReplenishList == replenList
                                     select new
                                     {
                                         r.Id,
                                         r.Component,
                                         ComponentDesc = r.Component1.RetailerStockCode + " (" + r.Component1.Description + ")",
                                         r.Quantity,
                                         PickedQuantity = (r.BatchHistories.Where(n => n.Action == (int)Globals.Action.Picked).Sum(n => n.Quantity) ?? 0),
                                         ShowAdd = (bool)((r.ReplenishList1.Status > (int)Globals.ReplenishListStatus.New) && //replenish list has been applied
                                                         (((Nullable<int>)r.BatchHistories.Where(n => n.Action == (int)Globals.Action.Picked).Sum(n => n.Quantity) ?? 0) + 
                                                         ((Nullable<int>)r.StockLevels.Where(n => n.Quality == (int)Globals.CropQuality.Pick).Sum(n => n.Quantity) ?? 0) < r.Quantity))  //Picked quantity + Set to pick quantity != quantity to pick
                                     };
                    BindingSource bs = new BindingSource();
                    replenDets.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_GetReplenishListDetails", true);
                    return null;
                }
            }
        }

        public static List<ReplenishListDetail> GetReplenishListDetailsList(int replenList)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<ReplenishListDetail> rplList = (from r in rfc.ReplenishListDetails
                                                         where r.ReplenishList == replenList
                                                         select r).ToList();
                    return rplList;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_GetReplenishListDetailsList", true);
                    return null;
                }
            }
        }

        public static bool CheckShippableQtyForReplenDetailLine(int replenDetail, out string returnMsg)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var replenDetailLine = (from r in rfc.ReplenishListDetails
                                            where r.Id == replenDetail
                                            select r).SingleOrDefault();

                    if (replenDetailLine == null)
                    {
                        returnMsg = "Error retrieving replenishment line details. Please contact support";
                        return false;
                    }
                    else
                    {
                        int? shippableQty = (int?)(rfc.StockLevels.Where(s => (s.Batch1.Component == replenDetailLine.Component) &&
                                                                            (s.Quality == (int)Globals.CropQuality.Shippable)).Sum(s => s.Quantity));
                        if ((shippableQty ?? 0) < replenDetailLine.Quantity)
                        {
                            returnMsg = "There is not enough shippable quantity to create picking instructions for this line";
                            return false;
                        }
                        else
                        {
                            returnMsg = string.Empty;
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_CheckShippableQtyForReplenDetailLine", true);
                    returnMsg = string.Empty;
                    return false;
                }
            }

        }

        public static bool CheckShippableQuantities(int replenId, out string returnMsg)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    Nullable<int> retailer = (from r in rfc.ReplenishLists where r.Id == replenId select r.Retailer).SingleOrDefault();
                    if (retailer == null)
                    {
                        returnMsg = "Error getting retailer related to the replenishment list. Please contact support";
                        return false;
                    }
                    else
                    {
                        //retrieve shippable quantities for the retailers components
                        var shippableQuantities = (from s in rfc.StockLevels
                                                   where s.Batch1.Component1.Retailer == retailer &&
                                                         s.Quality == (int)Globals.CropQuality.Shippable
                                                   group s by s.Batch1.Component into g
                                                   select new { Component = g.Key, TotalQuantity = g.Sum(s => s.Quantity) });

                        Dictionary<int, int> shipQtiesDict = new Dictionary<int, int>();
                        shippableQuantities.ToList().ForEach(n => shipQtiesDict.Add(n.Component, (int)n.TotalQuantity));

                        //get replenish details (use group in case they've put the same component on more than one row
                        var replenReqs = (from r in rfc.ReplenishListDetails
                                          where r.ReplenishList == replenId
                                          group r by r.Component into g
                                          select new { Component = g.Key, ReqQuantity = g.Sum(r => r.Quantity) });

                        //loop through requirements and ensure we have enough shippable quantity to meet them
                        int availQty;
                        returnMsg = string.Empty;
                        foreach (var r in replenReqs)
                        {
                            if (shipQtiesDict.ContainsKey((int)r.Component))
                            {
                                availQty = shipQtiesDict[(int)r.Component];
                            }
                            else
                            {
                                availQty = 0;
                            }
                            if (r.ReqQuantity > availQty)
                            {
                                Component comp = ComponentHandler.GetComponentById((int)r.Component);
                                returnMsg += "\n" + comp.RetailerStockCode + " (" + comp.Description + ")";
                            }
                        }
                        if (returnMsg != string.Empty)
                        {
                            returnMsg = "There is not enough shippable quantity of the following components to meet this replenish list:" + returnMsg;
                            return false;
                        }
                        else
                            return true;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ReplenishListHander_CheckShippableQuantities", true);
                    returnMsg = string.Empty;
                    return false;
                }
            }
        }


    }

    public class ReplenListOutput
    {
        public int Id { get; set; }
        public string RetailerName { get; set; }
        public DateTime ReplenishDate { get; set; }
        public int Status { get; set; }
        public string StatusDesc { get; set; }
        public string FileName { get; set; }

        public ReplenListOutput()
        {
        }

        public ReplenListOutput(int id, string retailerName, DateTime replenishDate, int status, string statusDesc, string fileName)
        {
            Id = id;
            RetailerName = retailerName;
            ReplenishDate = replenishDate;
            Status = status;
            StatusDesc = statusDesc;
            FileName = fileName;
        }
    }

}
