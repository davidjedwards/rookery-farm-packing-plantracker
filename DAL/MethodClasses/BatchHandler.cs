﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class BatchHandler
    {
        /// <summary>
        /// Add new batch to the database
        /// </summary>
        /// <param name="newBatch"></param>
        /// <returns></returns>
        public static int Add(Batch newBatch)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Batches.Add(newBatch);
                    rfc.SaveChanges();
                    return newBatch.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHander_Add", true);
                    return 0;
                }
            }
        }

        public static bool Delete(Batch delBatch)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(delBatch).State = System.Data.Entity.EntityState.Deleted;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_Delete", true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Update batch changes to database
        /// </summary>
        /// <param name="updBatch"></param>
        /// <returns></returns>
        public static bool Update(Batch updBatch)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(updBatch).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHander_Update", true);
                    return false;
                }
            }
        }

        public static BindingSource GetBatchList(int componentId, Nullable<DateTime> startDate, Nullable<DateTime> endDate)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var batches =
                        from b in rfc.Batches
                        where (b.Component == componentId || componentId == 0) &&
                              (b.BatchDate >= startDate || startDate == null) &&
                              (b.BatchDate <= endDate || endDate == null)
                        orderby b.Component1.RetailerStockCode ascending, b.BatchDate descending
                        select new { b.Id, 
                                     b.Component, 
                                     b.BatchDate, 
                                     b.BatchNo, 
                                     b.CreatedBy, 
                                     b.CreatedDate, 
                                     b.LastModifiedBy, 
                                     b.LastModifiedDate, 
                                     b.Component1.RetailerStockCode, 
                                     b.Component1.Description 
                                    };

                    BindingSource bs = new BindingSource();
                    batches.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHander_GetBatchList", true);
                    return null;
                }
            }
        }

        public static Batch GetBatchById(int batchId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    Batch batch = (from b in rfc.Batches where b.Id == batchId select b).Single();
                    return batch;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static bool IsBatchNoAlreadyUsed(Batch batch)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    DateTime dt = (DateTime)batch.BatchDate;
                    DateTime startYear = DateTime.Parse("01/01/" + dt.Year.ToString());
                    DateTime endYear = DateTime.Parse("31/12/" + dt.Year.ToString());
                    var dups =
                        from b in rfc.Batches
                        where b.BatchDate >= startYear &&
                              b.BatchDate <= endYear &&
                              b.Id != batch.Id &&
                              b.Component == batch.Component && 
                              b.BatchNo == batch.BatchNo
                        select b;

                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;

                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static BindingSource GetBatchHistoryList(int batchId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var hist =
                        from bh in rfc.BatchHistories
                        where bh.Batch == batchId &&
                              bh.Action != (int)Globals.Action.MovedOut
                        orderby bh.ActionDate descending
                        select new
                        {
                            bh.Id,
                            bh.Batch,
                            bh.ActionDate,
                            bh.Location1.Code,
                            bh.Quantity,
                            bh.Quality,
                            bh.Action,
                            bh.ActionedBy,
                            bh.Reason
                        };

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(BHOutput);
                    BHOutput bhOut;
                    foreach (var n in hist)
                    {
                        bhOut = new BHOutput();
                        bhOut.BatchHistoryId = n.Id;
                        bhOut.BatchId = (int)n.Batch;
                        bhOut.ActionDate = (DateTime)n.ActionDate;
                        bhOut.LocationCode = n.Code;
                        bhOut.Quantity = (int)n.Quantity;
                        bhOut.Quality = Globals.CropQualityScreenDesc((Globals.CropQuality)n.Quality);
                        bhOut.Action = Globals.ActionScreenDesc((Globals.Action)n.Action);
                        bhOut.ActionedBy = n.ActionedBy;
                        bhOut.Reason = n.Reason;
                        bs.Add(bhOut);
                    }

                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_GetBatchHistory", true);
                    return null;
                }
            }
        }

        public static List<BatchHistory> GetLatestBatchHistoriesToId(int batchId, int batchHistoryId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<BatchHistory> hist = (from bh in rfc.BatchHistories
                                               where bh.Batch == batchId &&
                                                     bh.Id >= batchHistoryId &&
                                                     bh.Action != (int)Globals.Action.MovedOut
                                               orderby bh.Id descending
                                               select bh).ToList();

                    return hist;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_GetBatchHistory", true);
                    return null;
                }
            }
        }

        public static BatchHistory GetBatchHistoryRecordFromId(int id)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    BatchHistory batchHistory = (from bh in rfc.BatchHistories
                                                 where bh.Id == id select bh).Single();
                    return batchHistory;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static bool UndoBatchHistory(BatchHistory bh, int userId, out string returnMsg)
        {
            //Action to take depends on the transaction we're undoing
            Batch batch = GetBatchById((int)bh.Batch);

            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.BatchHistories.Attach(bh);

                    switch ((Globals.Action)bh.Action)
                    {
                        case Globals.Action.Added:
                            //Need to remove stock level(s) (should only be one) associated with this batch 
                            List<StockLevel> ToDelete = (from s in rfc.StockLevels
                                                         where s.Batch == batch.Id
                                                         select s).ToList();
                           
                            for (int i = 0; i < ToDelete.Count(); i++)
                            {
                                rfc.Entry((StockLevel)ToDelete[i]).State = System.Data.Entity.EntityState.Deleted;
                            }
                            //plus remove the batch as well as the batch
                            //history record
                            rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                            rfc.Entry(batch).State = System.Data.Entity.EntityState.Deleted;
                            rfc.SaveChanges();
                            returnMsg = string.Empty;
                            return true;
                        case Globals.Action.Disposed:
                            //Restore prevQuantity back to quantity.
                            //See if a stock level record exists with the right values. If one does then we'll just change the 
                            //quantity on it, if not, we'll have to create a new stock level record
                            StockLevel dispStockLevel = StockLevelHandler.GetMergeableStockLevel((int)bh.Batch, (Globals.CropQuality)bh.PrevQuality, 
                                                            (int)bh.PrevShippableWk, (int)bh.PrevShippableYr, (int)bh.Location, 0, bh.ReplenDetailId) ;
                            if (dispStockLevel == null)
                            {
                                //Create new stock level record
                                dispStockLevel = new StockLevel();
                                dispStockLevel.Location = bh.Location;
                                dispStockLevel.Batch = bh.Batch;
                                dispStockLevel.Quality = bh.PrevQuality;
                                dispStockLevel.Quantity = bh.PrevQuantity;
                                dispStockLevel.Query = false;
                                dispStockLevel.ShippableWeek = bh.PrevShippableWk;
                                dispStockLevel.ShippableYear = bh.PrevShippableYr;
                                dispStockLevel.ReplenDetailId = bh.ReplenDetailId;
                                StockLevelHandler.AddStockLevel(dispStockLevel, Globals.Action.Other, userId);
                            }
                            else
                            {
                                //Update existing stock level
                                rfc.Entry(dispStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                dispStockLevel.Quantity += bh.Quantity;
                            }
                            rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                            rfc.SaveChanges();
                            returnMsg = string.Empty;
                            return true;
                        case Globals.Action.Picked:
                            //Restore prevQuantity back to quantity.
                            //See if a stock level record exists with the right values. If one does then we'll just change the 
                            //quantity on it, if not, we'll have to create a new stock level record
                            StockLevel pickStockLevel = StockLevelHandler.GetMergeableStockLevel((int)bh.Batch, (Globals.CropQuality)bh.PrevQuality, 
                                                            (int)bh.PrevShippableWk, (int)bh.PrevShippableYr, (int)bh.Location, 0, bh.ReplenDetailId);
                            if (pickStockLevel == null)
                            {
                                //Create new stock level record
                                pickStockLevel = new StockLevel();
                                pickStockLevel.Location = bh.Location;
                                pickStockLevel.Batch = bh.Batch;
                                pickStockLevel.Quality = bh.PrevQuality;
                                pickStockLevel.Quantity = bh.PrevQuantity;
                                pickStockLevel.Query = false;
                                pickStockLevel.ShippableWeek = bh.PrevShippableWk;
                                pickStockLevel.ShippableYear = bh.PrevShippableYr;
                                pickStockLevel.ReplenDetailId = bh.ReplenDetailId;
                                StockLevelHandler.AddStockLevel(pickStockLevel, Globals.Action.Other, userId);
                            }
                            else
                            {
                                //Update existing stock level
                                rfc.Entry(pickStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                pickStockLevel.Quantity += bh.Quantity;
                                pickStockLevel.ReplenDetailId = bh.ReplenDetailId;
                            }
                            rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                            rfc.SaveChanges();
                            returnMsg = string.Empty;
                            //Update status on replenish list if present
                            if (pickStockLevel.ReplenDetailId != null)
                            {
                                ReplenishListDetail rpld = ReplenishListHandler.GetReplenishListDetailById((int)pickStockLevel.ReplenDetailId);
                                ReplenishListHandler.UpdateReplenishListStatus((int)rpld.ReplenishList);
                            }
                            return true;
                        case Globals.Action.MovedIn:
                            //Undo both moved in and associated moved out transactions (moved out transactions not shown on the batch history list
                            //Get Moved Out details as these will determine the (possibly new) stock level that gets updated
                            if (bh.MovedOutId == null)
                            {
                                returnMsg = "Unable to perform undo. Please do the change manually. Batch history record will be deleted";
                                rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                rfc.SaveChanges();
                                return false;
                            }
                            else
                            {
                                BatchHistory bhMovedOut = BatchHandler.GetBatchHistoryRecordFromId((int)bh.MovedOutId);
                                if (bhMovedOut == null)
                                {
                                    returnMsg = "Unable to perform undo. Please do the change manually. Batch history record will be deleted";
                                    rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                    rfc.Entry(bhMovedOut).State = System.Data.Entity.EntityState.Deleted;
                                    rfc.SaveChanges();
                                    return false;
                                }
                                else
                                {
                                    //Add quantity on "moved in" movement to original stock level (may need to recreate it).
                                    //Determine if original exists, if not then need to create
                                    StockLevel movedFromStockLevel = StockLevelHandler.GetStockLevelForUndo((int)bhMovedOut.Batch, (Globals.CropQuality)bhMovedOut.Quality,
                                                                    (int)bhMovedOut.Location);
                                    if (movedFromStockLevel == null)
                                    {
                                        //Create new stock level record
                                        movedFromStockLevel = new StockLevel();
                                        movedFromStockLevel.Location = bhMovedOut.Location;
                                        movedFromStockLevel.Batch = bhMovedOut.Batch;
                                        movedFromStockLevel.Quality = bhMovedOut.Quality;
                                        movedFromStockLevel.Quantity = bhMovedOut.Quantity;
                                        movedFromStockLevel.Query = false;
                                        movedFromStockLevel.ShippableWeek = bhMovedOut.PrevShippableWk;
                                        movedFromStockLevel.ShippableYear = bhMovedOut.PrevShippableYr;
                                        movedFromStockLevel.ReplenDetailId = bhMovedOut.ReplenDetailId;
                                        StockLevelHandler.AddStockLevel(movedFromStockLevel, Globals.Action.Other, userId);
                                    }
                                    else
                                    {
                                        //Update existing stock level 
                                        rfc.Entry(movedFromStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                        movedFromStockLevel.Quantity += bhMovedOut.Quantity;
                                        movedFromStockLevel.ReplenDetailId = bhMovedOut.ReplenDetailId;
                                    }
                                    //Deal with "moved in" movement. If the whole quantity on the stock level is being moved back then delete the stock level
                                    //otherwise adjust it.
                                    StockLevel movedToStockLevel = StockLevelHandler.GetStockLevelForUndo((int)bh.Batch, (Globals.CropQuality)bh.Quality, (int)bh.Location);
                                    if (movedToStockLevel == null)
                                    {
                                        returnMsg = "Unable to perform undo. Please do the change manually. Batch history record will be deleted";
                                        rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                        rfc.Entry(bhMovedOut).State = System.Data.Entity.EntityState.Deleted;
                                        rfc.SaveChanges();
                                        return false;
                                    }
                                    else
                                    {
                                        if (movedToStockLevel.Quantity == bh.Quantity)
                                            rfc.Entry(movedToStockLevel).State = System.Data.Entity.EntityState.Deleted;
                                        else
                                        {
                                            rfc.Entry(movedToStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                            movedToStockLevel.Quantity -= bh.Quantity;
                                        }
                                    }
                                    
                                    
                                    rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                    rfc.Entry(bhMovedOut).State = System.Data.Entity.EntityState.Deleted;
                                    rfc.SaveChanges();
                                    returnMsg = string.Empty;
                                    return true;
                                }
                            }
                        case Globals.Action.QualityChange:
                            //if the quality of the full quantity was changed then just change the quality on the stock level otherwise we need to put the 
                            //quantity associated with the quality change back onto the old stock level and remove the stock level for the current quality;
                            StockLevel qualOrigStockLevel = StockLevelHandler.GetStockLevelForUndo((int)bh.Batch, (Globals.CropQuality)bh.PrevQuality, (int)bh.Location);
                            StockLevel qualNewStockLevel = StockLevelHandler.GetStockLevelForUndo((int)bh.Batch, (Globals.CropQuality)bh.Quality, (int)bh.Location);
                            int replenId = 0;
                            if (qualNewStockLevel == null)
                            {
                                returnMsg = "Unable to perform undo. Please do the change manually. Batch history record will be deleted";
                                rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                rfc.SaveChanges();
                                return false;
                            }
                            else
                            {
                                if (qualOrigStockLevel == null)
                                {
                                    //All of the quantity was changed. Just change it back
                                    rfc.Entry(qualNewStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                    if (qualNewStockLevel.Quality == (int)Globals.CropQuality.Pick)
                                        replenId = (int)qualNewStockLevel.ReplenDetailId;
                                    qualNewStockLevel.Quality = bh.PrevQuality;
                                    qualNewStockLevel.ReplenDetailId = null;
                                }
                                else
                                {
                                    //Increase quantity on orig stock level with quantity on new stock level and delete new stock level
                                    rfc.Entry(qualOrigStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                    qualOrigStockLevel.Quantity += bh.Quantity;
                                    if (qualNewStockLevel.Quality == (int)Globals.CropQuality.Pick && qualNewStockLevel.ReplenDetailId != null)
                                        replenId = (int)qualNewStockLevel.ReplenDetailId;
                                    //if the batch history quantity is less than the quantity on the "new" stock level then the change we're undoing had
                                    //resulted in the stock levels being merged. Update the quantity on the "new" stock level (that we merged into) rather
                                    //than deleting it!
                                    if (bh.Quantity < qualNewStockLevel.Quantity)
                                    {
                                        qualNewStockLevel.Quantity -= bh.Quantity;
                                        rfc.Entry(qualNewStockLevel).State = System.Data.Entity.EntityState.Modified;
                                    }
                                    else
                                    {
                                        rfc.Entry(qualNewStockLevel).State = System.Data.Entity.EntityState.Deleted;
                                    }
                                    qualNewStockLevel.ReplenDetailId = null;
                                }
                                if (replenId != 0)
                                {
                                    ReplenishListDetail rpld = ReplenishListHandler.GetReplenishListDetailById(replenId);
                                    ReplenishListHandler.UpdateReplenishListStatus((int)rpld.ReplenishList);
                                }
                                rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                rfc.SaveChanges();
                                returnMsg = string.Empty;
                                return true;
                            }
                        case Globals.Action.QuantityChange:
                            //Just put old quantity back
                            StockLevel quanStockLevel = StockLevelHandler.GetStockLevelForUndo((int)bh.Batch, (Globals.CropQuality)bh.PrevQuality, (int)bh.Location);
                            if (quanStockLevel == null)
                            {
                                returnMsg = "Unable to perform undo. Please do the change manually. Batch history record will be deleted";
                                rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                rfc.SaveChanges();
                                return false;
                            }
                            else
                            {
                                rfc.Entry(quanStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                                quanStockLevel.Quantity = bh.PrevQuantity;
                                rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                                rfc.SaveChanges();
                                returnMsg = string.Empty;
                                return true;
                            }
                        default:
                            returnMsg = "Unable to perform undo. Please do the change manually. Batch history record will be deleted";
                            rfc.Entry(bh).State = System.Data.Entity.EntityState.Deleted;
                            rfc.SaveChanges();
                            return false;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_UndoBatchHistory", true);
                    returnMsg = string.Empty;
                    return false;
                }
            }
        }

        public static List<string> ChangeReasonList()
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var reasonList = (from bh in rfc.BatchHistories
                                      where bh.Action == (int)Globals.Action.QuantityChange &&
                                            bh.Reason != null
                                      orderby bh.Reason
                                      select bh.Reason).Distinct();

                    List<string> reasons = reasonList.ToList();
                    reasons.Insert(0, string.Empty);
                    return reasons;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_ChangeReasonList", true);
                    return null;
                }
            }
        }

        public static List<BatchHistory> GetBatchHistoryListByReplenDetId(int rplDetId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<BatchHistory> bhList = (from b in rfc.BatchHistories
                                                 where b.ReplenDetailId == rplDetId &&
                                                       b.Action == (int)Globals.Action.QualityChange &&
                                                       b.Quality == (int)Globals.CropQuality.Pick
                                                 select b).ToList();

                    return bhList;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "BatchHandler_ChangeReasonList", true);
                    return null;
                }
            }
        }

    }

    public class BHOutput
    {
        public BHOutput()
        {
        }

        public int BatchHistoryId { get; set; }
        public int BatchId { get; set; }
        public DateTime ActionDate { get; set; }
        public string LocationCode { get; set; }
        public int Quantity { get; set; }
        public string Quality { get; set; }
        public string Action { get; set; }
        public string ActionedBy { get; set; }
        public string Reason { get; set; }
    }
}
