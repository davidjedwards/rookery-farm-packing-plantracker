﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class ConfigurationHandler
    {

        public static List<ConfigurationGroup> GetConfigGroups()
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var configGroups =
                        from c in rfc.ConfigurationGroups
                        orderby c.GroupName
                        select c;

                    return configGroups.ToList();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ConfigurationHandler_GetConfigGroups", true);
                    return null;
                }
            }
        }


        public static BindingSource GetConfigurationItemsAsBindingSource(ConfigGroup cfgId, bool includeInActive = false)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(ConfigurationItem);
                    if (includeInActive)
                    {
                        rfc.ConfigurationItems
                            .Where(c => c.ConfigurationGroup1.Id == (int)cfgId)
                            .OrderBy(c => c.ItemValue)
                            .ToList().ForEach(n => bs.Add(n));
                    }
                    else
                    {
                        rfc.ConfigurationItems
                            .Where(c => c.Active == true && c.ConfigurationGroup1.Id == (int)cfgId)
                            .OrderBy(c => c.ItemValue).ToList().ForEach(n => bs.Add(n));
                    }
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ConfigurationHandler_GetConfigurationItemsAsBindingSource", true);
                    return null;
                }
            }
        }

        public static List<ConfigurationItem> GetConfigurationItemsAsList(ConfigGroup cfgId, bool includeInActive = false)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<ConfigurationItem> thisList = (from c in rfc.ConfigurationItems
                                                        where (c.Active == true || includeInActive == true) &&
                                                               c.ConfigurationGroup == (int)cfgId
                                                        orderby c.ItemValue
                                                        select c).ToList();
                    ConfigurationItem nullValue = new ConfigurationItem { Id = 0, ItemValue = string.Empty };
                    thisList.Insert(0, nullValue);

                    return thisList;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ConfigurationHandler_GetConfigurationItemsAsList", true);
                    return null;
                }
            }
        }



        /// <summary>
        /// Add configuration item
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns></returns>
        public static int Add(ConfigurationItem newItem)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.ConfigurationItems.Add(newItem);
                    rfc.SaveChanges();
                    return newItem.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ConfigurationHandler_Add", true);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Update configuration item
        /// </summary>
        /// <param name="updItem"></param>
        /// <returns></returns>
        public static bool Update(ConfigurationItem updItem)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.ConfigurationItems.Attach(updItem);
                    rfc.Entry(updItem).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ConfigurationHandler_Update", true);
                    return false;
                }
            }
        }

        public static bool IsDuplicate(ConfigurationItem chkItem)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var dups = (from c in rfc.ConfigurationItems where c.Id != chkItem.Id && c.ConfigurationGroup == chkItem.ConfigurationGroup && c.ItemValue == chkItem.ItemValue select c);
                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static ConfigurationItem GetItemById(int cfiId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    ConfigurationItem cfi = (from c in rfc.ConfigurationItems where c.Id == cfiId select c).Single();
                    return cfi;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

    }
}
