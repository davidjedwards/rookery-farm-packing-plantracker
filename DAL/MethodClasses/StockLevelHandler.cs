﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class StockLevelHandler
    {
        public static int AddStockLevel(StockLevel newStockLevel, Globals.Action act, int userId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.StockLevels.Add(newStockLevel);

                    if (act != Globals.Action.Other)
                    {
                        //create batch history record
                        BatchHistory bh = new BatchHistory();
                        bh.Batch = newStockLevel.Batch;
                        bh.ActionDate = DateTime.Now;
                        bh.Location = newStockLevel.Location;
                        bh.Quantity = newStockLevel.Quantity;
                        bh.Quality = newStockLevel.Quality;
                        bh.Action = (int)act;
                        bh.ActionedBy = UserHandler.GetUserName(userId);
                        bh.PrevShippableWk = newStockLevel.ShippableWeek;
                        bh.PrevShippableYr = newStockLevel.ShippableYear;
                        rfc.BatchHistories.Add(bh);
                    }
                    rfc.SaveChanges();
                    return newStockLevel.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_AddStockLevel", true);
                    return 0;
                }
            }
        }

        public static bool UpdateStockLevel(StockLevel updStockLevel, Globals.Action act, int userId, int prevQuantity, int prevQuality, int prevShipWk, int prevShipYr,  string chgReason = "")
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                    if (act != Globals.Action.Other)
                    {
                        //create batch history record
                        BatchHistory bh = new BatchHistory();
                        bh.Batch = updStockLevel.Batch;
                        bh.ActionDate = DateTime.Now;
                        bh.Location = updStockLevel.Location;
                        bh.Quantity = updStockLevel.Quantity;
                        bh.Quality = updStockLevel.Quality;
                        bh.Action = (int)act;
                        bh.ActionedBy = UserHandler.GetUserName(userId);
                        bh.Reason = chgReason;
                        bh.PrevQuality = prevQuality;
                        bh.PrevQuantity = prevQuantity;
                        bh.PrevShippableWk = prevShipWk;
                        bh.PrevShippableYr = prevShipYr;
                        rfc.BatchHistories.Add(bh);
                    }
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_UpdateStockLevel", true);
                    return false;
                }
            }
        }

        public static StockLevel SetNewQuality(StockLevel updStockLevel, Globals.CropQuality cq, int userId, int qtyToProcess, Nullable<int> replenDetailId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    StockLevel newStockLevel;
                    Nullable<int> prevQuality;
                    Nullable<int> prevQuantity;

                    if (updStockLevel.Quantity == qtyToProcess)
                    {
                        //Just an update of the quality on the stock level record and return the passed in record
                        //Before updating the stock level record, check to see if one exists that has the same component,
                        //batch, (new) quality and shippable detals. If there is, then just make the adjustment to this stock level record
                        //and remove the current one.
                        newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, cq, (int)updStockLevel.ShippableWeek, 
                            (int)updStockLevel.ShippableYear, (int)updStockLevel.Location, (int)updStockLevel.Id, updStockLevel.ReplenDetailId);

                        if (newStockLevel == null)
                        {
                            prevQuality = updStockLevel.Quality;
                            updStockLevel.Quality = (int)cq;
                            updStockLevel.ReplenDetailId = replenDetailId;
                            rfc.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                            //create batch history record
                            BatchHistory bh = new BatchHistory();
                            bh.Batch = updStockLevel.Batch;
                            bh.ActionDate = DateTime.Now;
                            bh.Location = updStockLevel.Location;
                            bh.Quantity = updStockLevel.Quantity;
                            bh.Quality = updStockLevel.Quality;
                            bh.Action = (int)Globals.Action.QualityChange;
                            bh.ActionedBy = UserHandler.GetUserName(userId);
                            bh.PrevQuantity = updStockLevel.Quantity;
                            bh.PrevQuality = prevQuality;
                            bh.PrevShippableWk = updStockLevel.ShippableWeek;
                            bh.PrevShippableYr = updStockLevel.ShippableYear;
                            bh.ReplenDetailId = replenDetailId;
                            rfc.BatchHistories.Add(bh);
                        }
                        else
                        {
                            newStockLevel.Quantity += qtyToProcess;
                            newStockLevel.ReplenDetailId = replenDetailId;
                            rfc.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;

                            //create batch history record
                            BatchHistory bh = new BatchHistory();
                            bh.Batch = newStockLevel.Batch;
                            bh.ActionDate = DateTime.Now;
                            bh.Location = newStockLevel.Location;
                            bh.Quantity = qtyToProcess;
                            bh.Quality = newStockLevel.Quality;
                            bh.Action = (int)Globals.Action.QualityChange;
                            bh.ActionedBy = UserHandler.GetUserName(userId);
                            bh.PrevQuality = updStockLevel.Quality;
                            bh.PrevQuantity = updStockLevel.Quantity;
                            bh.PrevShippableWk = updStockLevel.ShippableWeek;
                            bh.PrevShippableYr = updStockLevel.ShippableYear;
                            bh.ReplenDetailId = replenDetailId;
                            rfc.BatchHistories.Add(bh);

                            //remove current stock level record
                            rfc.StockLevels.Attach(updStockLevel);
                            rfc.StockLevels.Remove(updStockLevel);
                        }
 
                        rfc.SaveChanges();

                        if (newStockLevel == null)
                            return updStockLevel;
                        else
                            return newStockLevel;
                    }
                    else
                    {
                        //qty to process is less than the stock quantity so reduce the quantity on the current stock level record
                        //and create a new stock level record for the qty to process
                        prevQuantity = updStockLevel.Quantity;
                        updStockLevel.Quantity -= qtyToProcess;
                        rfc.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                        //Before creating a new stock level record, check to see if one exists that has the same component,
                        //batch, quality and shippable detals. If there is, then just make the adjustment to this stock level record
                        //and return that.
                        newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, cq, (int)updStockLevel.ShippableWeek, 
                            (int)updStockLevel.ShippableYear, (int)updStockLevel.Location, (int)updStockLevel.Id, updStockLevel.ReplenDetailId);
                        if (newStockLevel == null)
                        {
                            newStockLevel = new StockLevel();
                            newStockLevel.Location = updStockLevel.Location;
                            newStockLevel.Quality = (int)cq;
                            newStockLevel.Quantity = qtyToProcess;
                            newStockLevel.Batch = updStockLevel.Batch;
                            newStockLevel.ShippableWeek = updStockLevel.ShippableWeek;
                            newStockLevel.ShippableYear = updStockLevel.ShippableYear;
                            newStockLevel.ReplenDetailId = replenDetailId;
                            rfc.StockLevels.Add(newStockLevel);
                        }
                        else
                        {
                            newStockLevel.Quantity += qtyToProcess;
                            newStockLevel.ReplenDetailId = replenDetailId;
                            rfc.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;
                        }

                        //create batch history record
                        BatchHistory bh = new BatchHistory();
                        bh.Batch = newStockLevel.Batch;
                        bh.ActionDate = DateTime.Now;
                        bh.Location = newStockLevel.Location;
                        bh.Quantity = qtyToProcess;
                        bh.Quality = newStockLevel.Quality;
                        bh.Action = (int)Globals.Action.QualityChange;
                        bh.ActionedBy = UserHandler.GetUserName(userId);
                        bh.PrevQuantity = 0;
                        bh.PrevQuality = updStockLevel.Quality;
                        bh.PrevShippableWk = updStockLevel.ShippableWeek;
                        bh.PrevShippableYr = updStockLevel.ShippableYear;
                        bh.ReplenDetailId = replenDetailId;

                        rfc.BatchHistories.Add(bh);
                        rfc.SaveChanges();
                        return newStockLevel;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_SetNewQuality", true);
                    return updStockLevel;
                }
            }
        }

        public static StockLevel GetMergeableStockLevel(int batchId, Globals.CropQuality cq, Nullable<int> shipWeek, 
                                        Nullable<int> shipYear, Nullable<int> location, int currId, int? replenDetailid)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    StockLevel sl = (from s in rfc.StockLevels
                                     where s.Batch == batchId &&
                                           s.Quality == (int)cq &&
                                           s.ShippableWeek == shipWeek &&
                                           s.ShippableYear == shipYear &&
                                           s.Location == location &&
                                           s.Id != currId &&
                                           (s.ReplenDetailId == null || s.ReplenDetailId == replenDetailid)
                                     select s).SingleOrDefault();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetMergeableStockLevel", true);
                    return null;
                }
            }
        }

        public static StockLevel GetStockLevelForUndo(int batchId, Globals.CropQuality cq, int location)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    StockLevel sl = (from s in rfc.StockLevels
                                     where s.Batch == batchId &&
                                           s.Quality == (int)cq &&
                                           s.Location == location 
                                     select s).FirstOrDefault();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetStockLevelForUndo", true);
                    return null;
                }
            }
        }

        public static bool Delete(int stockLevelId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    StockLevel sl = (from s in rfc.StockLevels.Include("Location1").Include("Batch1").Include("Batch1.Component1")
                                     where s.Id == stockLevelId
                                     select s).SingleOrDefault();
                    rfc.Entry(sl).State = System.Data.Entity.EntityState.Deleted;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_Delete", true);
                    return false;
                }
            }
        }

        public static StockLevel GetStockLevelFromId(int stockLevelId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    StockLevel sl = (from s in rfc.StockLevels.Include("Location1").Include("Batch1").Include("Batch1.Component1")
                                     where s.Id == stockLevelId
                                     select s).SingleOrDefault();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetStockLevelFromId", true);
                    return null;
                }
            }
        }

        public static List<StockLevel> GetRetailerStockExportList(int retailer)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<StockLevel> sl = (from s in rfc.StockLevels.Include("Batch1.Component1")
                                           where s.Quality == (int)Globals.CropQuality.Shippable &&
                                                 s.Batch1.Component1.Retailer == retailer
                                           select s).ToList();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetRetailerStockExportList", true);
                    return null;
                }
            }
        }

        public static BindingSource GetStockLevelList(Nullable<int> compId, Nullable<int> batchNo, Nullable<int> quality, bool ShowQueryOnly, int Retailer)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var stockLevels =
                        from sl in rfc.StockLevels
                        where (sl.Batch1.Component == compId || compId == null) &&
                              (sl.Batch == batchNo || (batchNo ?? 0) == 0) &&
                              (sl.Quality == quality || quality == -1) &&
                              (sl.Query == true || ShowQueryOnly == false) &&
                              (sl.Batch1.Component1.Retailer == Retailer || Retailer == 0)
                        orderby sl.Location1.SortOrder, sl.Batch1.Component1.RetailerStockCode
                        select new
                        {
                            sl.Id,
                            sl.Location1.Code,
                            Component = sl.Batch1.Component1.RetailerStockCode + " (" + sl.Batch1.Component1.Description + ")",
                            sl.Batch1.BatchNo,
                            sl.Batch1.BatchDate,
                            sl.Quantity,
                            sl.Quality,
                            ShippableWeek = sl.ShippableWeek.ToString() + "/" + sl.ShippableYear.ToString(),
                            sl.Query,
                            QueryType = sl.ConfigurationItem.ItemValue,
                            sl.ReplenishListDetail.ReplenishList1.FileName,
                            PickFaceLocation = sl.Batch1.Component1.ConfigurationItem.ItemValue.ToString()
                        };

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(StockLevelRow);
                    foreach (var n in stockLevels)
                    {
                        bs.Add(new StockLevelRow(n.Id, n.Code, n.Component, (int)n.BatchNo, (DateTime)n.BatchDate, (int)n.Quantity,
                            (int)n.Quality, n.ShippableWeek, (bool)n.Query, n.QueryType, n.FileName, n.PickFaceLocation));
                    }

                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetStockLevelList", true);
                    return null;
                }
            }
        }

        public static BindingSource GetStockLevelsForStockCheck()
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var stockLevels =
                        from sl in rfc.StockLevels
                        where sl.StockTake == true
                        orderby sl.Location1.SortOrder, sl.Batch1.Component1.RetailerStockCode
                        select new
                        {
                            sl.Id,
                            sl.Location1.Code,
                            Component = sl.Batch1.Component1.RetailerStockCode + " (" + sl.Batch1.Component1.Description + ")",
                            sl.Quantity,
                            sl.Quality,
                        };

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(StockCheckLevelRow);
                    foreach (var n in stockLevels)
                    {
                        bs.Add(new StockCheckLevelRow(n.Id, n.Code, n.Component, (int)n.Quantity, (int)n.Quality));
                    }

                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetStockLevelsForStockCheck", true);
                    return null;
                }
            }
        }
        
        public static bool PickOrDispose(StockLevel updStockLevel, Globals.Action act, int userId, int qtyToProcess)
        {
            //Should only be here for picks and disposals
            if (act != Globals.Action.Disposed && act != Globals.Action.Picked)
                return false;

            using (RFContext rfc = new RFContext())
            {
                try
                {
                    BatchHistory bh;
                    Nullable<int> replenId = updStockLevel.ReplenDetailId;

                    //create batch history record for pick or dispose
                    bh = new BatchHistory();
                    bh.Batch = updStockLevel.Batch;
                    bh.ActionDate = DateTime.Now;
                    bh.Location = updStockLevel.Location;
                    bh.Quantity = qtyToProcess;
                    bh.Quality = updStockLevel.Quality;
                    bh.Action = (int)act;
                    bh.ActionedBy = UserHandler.GetUserName(userId);
                    bh.PrevQuality = updStockLevel.Quality;
                    bh.PrevQuantity = updStockLevel.Quantity;
                    bh.PrevShippableWk = updStockLevel.ShippableWeek;
                    bh.PrevShippableYr = updStockLevel.ShippableYear;
                    bh.ReplenDetailId = updStockLevel.ReplenDetailId;
                    rfc.BatchHistories.Add(bh);

                    if (updStockLevel.Quantity == qtyToProcess)
                    {
                        //Remove stock level record
                        rfc.StockLevels.Attach(updStockLevel);
                        rfc.StockLevels.Remove(updStockLevel);
                    }
                    else
                    {
                        //Reduce quantity on stock level
                        rfc.Entry(updStockLevel).State = System.Data.Entity.EntityState.Unchanged;
                        updStockLevel.Quantity -= qtyToProcess;
                    }

                    rfc.SaveChanges();

                    //Update status on replenish list if present
                    if (act == Globals.Action.Picked && replenId != null)
                    {
                        ReplenishListDetail rpld = ReplenishListHandler.GetReplenishListDetailById((int)replenId);
                        ReplenishListHandler.UpdateReplenishListStatus((int)rpld.ReplenishList);
                    }

                    return true;

                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_PickOrDispose", true);
                    return false;
                }
            }
        }

        public static StockLevel PerformMove(StockLevel updStockLevel, int newLocation, int userId, int qtyToProcess)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    StockLevel newStockLevel;
                    BatchHistory bh;
                    int movedOutId;
                    Globals.CropQuality newQuality;

                    if ((Globals.CropQuality)updStockLevel.Quality == Globals.CropQuality.MoveOn)
                        newQuality = Globals.CropQuality.GrowingOn;
                    else
                        newQuality = (Globals.CropQuality)updStockLevel.Quality;


                    if (updStockLevel.Quantity == qtyToProcess)
                    {
                        //Just update the location on the stock level and adjust the quality back to growing on (only if quality is currently "Move On")
                        //Before updating the stock level record, check to see if one exists that has the same component,
                        //batch, (new) quality and shippable detals. If there is, then just make the adjustment to this stock level record
                        //and remove the current one.
                        newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, newQuality, (int)updStockLevel.ShippableWeek,
                           (int)updStockLevel.ShippableYear, newLocation, (int)updStockLevel.Id, updStockLevel.ReplenDetailId);

                        //create batch history record for outward move
                        bh = new BatchHistory();
                        bh.Batch = updStockLevel.Batch;
                        bh.ActionDate = DateTime.Now;
                        bh.Location = updStockLevel.Location;
                        bh.Quantity = updStockLevel.Quantity;
                        bh.Quality = updStockLevel.Quality;
                        bh.Action = (int)Globals.Action.MovedOut;
                        bh.ActionedBy = UserHandler.GetUserName(userId);
                        bh.PrevShippableWk = updStockLevel.ShippableWeek;
                        bh.PrevShippableYr = updStockLevel.ShippableYear;
                        rfc.BatchHistories.Add(bh);

                        rfc.SaveChanges();
                        movedOutId = bh.Id;

                        if (newStockLevel == null)
                        {
                            updStockLevel.Location = newLocation;
                            updStockLevel.Location1 = LocationHandler.GetLocationById(newLocation);
                            updStockLevel.Quality = (int)newQuality;
                            rfc.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                            //create batch history record for inward move
                            bh = new BatchHistory();
                            bh.Batch = updStockLevel.Batch;
                            bh.ActionDate = DateTime.Now;
                            bh.Location = newLocation;
                            bh.Quantity = updStockLevel.Quantity;
                            bh.Quality = updStockLevel.Quality;
                            bh.Action = (int)Globals.Action.MovedIn;
                            bh.ActionedBy = UserHandler.GetUserName(userId);
                            bh.MovedOutId = movedOutId;
                            rfc.BatchHistories.Add(bh);
                        }
                        else
                        {
                            newStockLevel.Quantity += qtyToProcess;
                            rfc.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;
                            //remove current stock level record
                            rfc.StockLevels.Attach(updStockLevel);
                            rfc.StockLevels.Remove(updStockLevel);

                            //create batch history record
                            bh = new BatchHistory();
                            bh.Batch = newStockLevel.Batch;
                            bh.ActionDate = DateTime.Now;
                            bh.Location = newStockLevel.Location;
                            bh.Quantity = qtyToProcess;
                            bh.Quality = newStockLevel.Quality;
                            bh.Action = (int)Globals.Action.MovedIn;
                            bh.ActionedBy = UserHandler.GetUserName(userId);
                            bh.MovedOutId = movedOutId;
                            rfc.BatchHistories.Add(bh);
                        }

                        rfc.SaveChanges();

                        if (newStockLevel == null)
                            return updStockLevel;
                        else
                            return newStockLevel;
                    }
                    else
                    {
                        //qty to process is less than the stock quantity so reduce the quantity on the current stock level record
                        //and create a new stock level record for the qty to process
                        updStockLevel.Quantity -= qtyToProcess;
                        rfc.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                        //create batch history record for outward move
                        bh = new BatchHistory();
                        bh.Batch = updStockLevel.Batch;
                        bh.ActionDate = DateTime.Now;
                        bh.Location = updStockLevel.Location;
                        bh.Quantity = qtyToProcess;
                        bh.Quality = updStockLevel.Quality;
                        bh.Action = (int)Globals.Action.MovedOut;
                        bh.ActionedBy = UserHandler.GetUserName(userId);
                        bh.PrevShippableWk = updStockLevel.ShippableWeek;
                        bh.PrevShippableYr = updStockLevel.ShippableYear;
                        rfc.BatchHistories.Add(bh);

                        rfc.SaveChanges();
                        movedOutId = bh.Id;

                        //Before creating a new stock level record, check to see if one exists that has the same component,
                        //batch, quality and shippable detals. If there is, then just make the adjustment to this stock level record
                        //and return that.
                        newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, Globals.CropQuality.GrowingOn, (int)updStockLevel.ShippableWeek,
                            (int)updStockLevel.ShippableYear, newLocation, (int)updStockLevel.Id, updStockLevel.ReplenDetailId);
                        if (newStockLevel == null)
                        {
                            newStockLevel = new StockLevel();
                            newStockLevel.Location = newLocation;
                            newStockLevel.Quality = (int)newQuality;
                            newStockLevel.Quantity = qtyToProcess;
                            newStockLevel.Batch = updStockLevel.Batch;
                            newStockLevel.ShippableWeek = updStockLevel.ShippableWeek;
                            newStockLevel.ShippableYear = updStockLevel.ShippableYear;
                            rfc.StockLevels.Add(newStockLevel);
                        }
                        else
                        {
                            newStockLevel.Quantity += qtyToProcess;
                            rfc.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;
                        }
                        //create batch history record for inward move
                        bh = new BatchHistory();
                        bh.Batch = newStockLevel.Batch;
                        bh.ActionDate = DateTime.Now;
                        bh.Location = newStockLevel.Location;
                        bh.Quantity = qtyToProcess;
                        bh.Quality = newStockLevel.Quality;
                        bh.Action = (int)Globals.Action.MovedIn;
                        bh.ActionedBy = UserHandler.GetUserName(userId);
                        bh.MovedOutId = movedOutId;
                        rfc.BatchHistories.Add(bh);
                        rfc.SaveChanges();
                        return newStockLevel;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_PerformMove", true);
                    return updStockLevel;
                }
            }
        }

        public static List<StockLevel> GetShippableStockLevels(int componentId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    //Return shippable stock levels in location sort order
                    List<StockLevel> sl = (from s in rfc.StockLevels
                                           where s.Batch1.Component == componentId &&
                                                 s.Quality == (int)Globals.CropQuality.Shippable
                                           orderby s.Batch1.BatchDate, s.Batch1.BatchNo, s.Location1.SortOrder
                                           select s).ToList();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetShippableStockLevels", true);
                    return null;
                }
            }
        }

        public static bool AddStockLevelsForStockCheck(int componentId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<StockLevel> slList = (from s in rfc.StockLevels
                                               where s.Batch1.Component == componentId
                                               select s).ToList();

                    foreach (StockLevel sl in slList)
                    {
                        rfc.StockLevels.Attach(sl);
                        sl.StockTake = true;
                    }
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_GetShippableStockLevels", true);
                    return false;
                }
            }
        }
    }

    public class StockLevelRow
    {
        public int StockLevelId { get; set; }
        public string LocationCode { get; set; }
        public string ComponentDesc { get; set; }
        public int BatchNo { get; set; }
        public DateTime BatchDate { get; set; }
        public int Quantity { get; set; }
        public string QualityDesc { get; set; }
        public string ShippableWeek { get; set; }
        public bool Query { get; set; }
        public string QueryTypeDesc { get; set; }
        public string FileName { get; set; }
        public string PickFaceLocation { get; set; }

        public StockLevelRow()
        {
        }

        public StockLevelRow(int id, string code, string compDesc, int batchNo, DateTime batchDate, int quantity, int quality,
            string shippableWeek, bool query, string queryType, string fileName, string pickFaceLocation)
        {
            StockLevelId = id;
            LocationCode = code;
            ComponentDesc = compDesc;
            BatchNo = batchNo;
            BatchDate = batchDate;
            Quantity = quantity;
            QualityDesc = Globals.CropQualityScreenDesc((Globals.CropQuality)quality);
            ShippableWeek = shippableWeek;
            Query = query;
            QueryTypeDesc = queryType;
            FileName = fileName;
            PickFaceLocation = pickFaceLocation;
        }
    }

    public class StockCheckLevelRow
    {
        public int StockLevelId { get; set; }
        public string LocationCode { get; set; }
        public string ComponentDesc { get; set; }
        public int Quantity { get; set; }
        public string QualityDesc { get; set; }

        public StockCheckLevelRow()
        {
        }

        public StockCheckLevelRow(int id, string code, string compDesc, int quantity, int quality)
        {
            StockLevelId = id;
            LocationCode = code;
            ComponentDesc = compDesc;
            Quantity = quantity;
            QualityDesc = Globals.CropQualityScreenDesc((Globals.CropQuality)quality);
        }
    }
}
