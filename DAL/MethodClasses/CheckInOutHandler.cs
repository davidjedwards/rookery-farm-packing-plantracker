﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;

namespace MethodClasses
{
    public static class CheckInOutHander
    {
        /// <summary>
        /// Creates an entry in the lock table
        /// </summary>
        /// <param name="reclock">Lock instance to add</param>
        /// <returns>True if sucessfuly added</returns>
        public static bool Add(Lock reclock)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Locks.Add(reclock);
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "CheckInOutHander_Add", true);
                    return false;
                }
            }
        }

        public static bool Delete(Lock recLock)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    int numRecs = 0;
                    numRecs = (from l in rfc.Locks
                               where l.LockRecId == recLock.LockRecId && l.LockTable == recLock.LockTable && l.LockUserId == recLock.LockUserId
                               select l).Count();
                    if (numRecs > 0)
                    {
                        rfc.Locks.Attach(recLock);
                        rfc.Locks.Remove(recLock);
                        rfc.SaveChanges();
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "CheckInOutHander_Delete", true);
                    return false;
                }
            }
        }


        /// <summary>
        /// Checks if record is locked
        /// </summary>
        /// <param name="entity">Name of table to check</param>
        /// <param name="recordId">Id of record on table to check</param>
        /// <param name="currUserId">Id of current user</param>
        /// <returns>Full name of user who has record locked. Empty if not locked</returns>
        public static bool IsRecordCheckedOut(Lock newlock, out Nullable<int> lockingUser)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var lockedBy = (from l in rfc.Locks
                                    where l.LockTable == newlock.LockTable && l.LockRecId == newlock.LockRecId 
                                    select new { l.LockUserId }).FirstOrDefault();

                    if (lockedBy == null)
                    {
                        lockingUser = null;
                        return false;
                    }
                    else
                    {
                        lockingUser = lockedBy.LockUserId;
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocksHandler_IsRecordLocked", true);
                    lockingUser = null;
                    return true;

                }
            }
        }

        public static bool ClearUserLocks(int userId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<Lock> userLockList = (from l in rfc.Locks where l.LockUserId == (int)userId select l).ToList();
                    foreach (Lock ulock in userLockList)
                    {
                        rfc.Locks.Attach(ulock);
                        rfc.Locks.Remove(ulock);
                    }
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocksHandler_IsRecordLocked", true);
                    return false;
                }
            }
        }

    }
}
