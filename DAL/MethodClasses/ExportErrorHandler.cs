﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class ExportErrorHandler
    {
        public static void Add(ExportError ee)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.ExportErrors.Add(ee);
                    rfc.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ExportErrorHandler_Add", false);
                }
            }
        }

        public static bool Delete(ExportError ee)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(ee).State = System.Data.Entity.EntityState.Deleted;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ExportErrorHandler_Delete", true);
                    return false;
                }
            }
        }

        public static ExportError GetExportErrorById(int Id)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    ExportError ee = (from e in rfc.ExportErrors
                                      where e.Id == Id
                                      select e).SingleOrDefault();
                    return ee;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ExportErrorHandler_GetExportErrorById", true);
                    return null;
                }
            }
        }

        public static BindingSource GetExportErrorList()
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var eeList = from ee in rfc.ExportErrors
                                 orderby ee.Date descending
                                 select new
                                 {
                                     ee.Id,
                                     ee.Date,
                                     ee.FileName,
                                     ee.Retailer1.Name,
                                     ee.Description
                                 };

                    BindingSource bs = new BindingSource();
                    eeList.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ExportErrorHandler_GetExportErrorList", true);
                    return null;
                }
            }
        }
    }
}
