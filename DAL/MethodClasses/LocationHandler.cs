﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;

namespace MethodClasses
{
    public static class LocationHandler
    {
        /// <summary>
        /// Add new location to the database
        /// </summary>
        /// <param name="newLoc"></param>
        /// <returns></returns>
        public static int Add(Location newLoc)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    newLoc.Active = true;
                    rfc.Locations.Add(newLoc);
                    rfc.SaveChanges();
                    return newLoc.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocationHandler_Add", true);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Update changes to location to database
        /// </summary>
        /// <param name="updLoc"></param>
        /// <returns></returns>
        public static bool Update(Location updLoc)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(updLoc).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocationHandler_Update", true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Determine if location is a duplicate of another location in the database.
        /// </summary>
        /// <param name="chkLoc"></param>
        /// <returns></returns>
        public static bool IsDuplicate(Location chkLoc)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var dups = (from l in rfc.Locations
                                where l.Id != chkLoc.Id &&
                                      l.Code == chkLoc.Code
                                orderby l.SortOrder
                                select l);

                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static List<Location> GetLocationList(bool includeInActive)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var locations =
                        from l in rfc.Locations
                        where (l.Active == true || includeInActive == true) 
                        orderby l.SortOrder
                        select l;

                    return locations.ToList();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocationHandler_GetLocationList", true);
                    return null;
                }
            }
        }

        public static List<Location> GetLocationListForDropDown()
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    List<Location> locations =
                        (from l in rfc.Locations
                         where (l.Active == true)
                         orderby l.Code
                         select l).ToList();

                    Location nullLoc = new Location();
                    nullLoc.Id = 0;
                    nullLoc.Code = string.Empty;

                    locations.Insert(0, nullLoc);

                    return locations.ToList();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocationHandler_GetLocationList", true);
                    return null;
                }
            }
        }

        public static Location GetLocationById(int locId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    Location loc = (from l in rfc.Locations where l.Id == locId select l).Single();
                    return loc;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static bool IsUsed(int locationId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    int count = (from s in rfc.StockLevels
                                 where s.Location == locationId
                                 select s).Count();
                    if (count > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "LocationHandler_IsUsed", true);
                    return true;
                }
            }
        }
                    
    }
}
