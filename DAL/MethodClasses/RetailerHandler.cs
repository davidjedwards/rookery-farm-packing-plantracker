﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class RetailerHandler
    {
        /// <summary>
        /// Return retailer object for given retailer id.
        /// </summary>
        /// <param name="rtlId"></param>
        /// <returns></returns>
        public static Retailer GetRetailerById(int rtlId)
        {
            if (rtlId == 0)
                return null;
            else
            {
                Retailer rtl;
                using (RFContext rfc = new RFContext())
                {
                    try
                    {
                        rtl = (from r in rfc.Retailers where r.Id == rtlId select r).Single();
                        return rtl;
                    }
                    catch (Exception)
                    {
                        //No retailer found for the Id. Return null
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Get list of retailers
        /// </summary>
        /// <param name="includeInActive">Set to true if inactive retailers are required</param>
        /// <returns>Retailer list</returns>
        public static List<Retailer> GetRetailerList(bool includeInActive)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var retailers =
                        from r in rfc.Retailers
                        where (r.Active == true | includeInActive == true)
                        orderby r.Name
                        select r;

                    return retailers.ToList();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "RetailerHandler_GetRetailerList", true);
                    return null;
                }
            }
        }

        public static BindingSource GetRetailerContacts(Retailer rtl)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Retailers.Attach(rtl);
                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(RetailerContact);
                    rtl.RetailerContacts.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "RetailerHandler_GetRetailerContacts", true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Add retailer object
        /// </summary>
        /// <param name="newRetailer"></param>
        /// <returns></returns>
        public static bool Add(Retailer newRetailer)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Retailers.Add(newRetailer);
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "RetailerHandler_Add", true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Update existing retailer object
        /// </summary>
        /// <param name="updRetailer"></param>
        /// <returns></returns>
        public static bool Update(Retailer updRetailer)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Retailers.Attach(updRetailer);
                    rfc.Entry(updRetailer).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "RetailerHandler_Add", true);
                    return false;
                }
            }
        }

        public static bool IsDuplicate(Retailer chkRetailer)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var dups = (from r in rfc.Retailers where r.Id != chkRetailer.Id && r.Name == chkRetailer.Name select r);
                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }
                                

    }
}
