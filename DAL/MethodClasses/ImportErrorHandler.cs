﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class ImportErrorHandler
    {
        public static void Add(ImportError ie)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.ImportErrors.Add(ie);
                    rfc.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ImportErrorHander_Add", false);
                }
            }
        }

        public static bool Delete(ImportError ie)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(ie).State = System.Data.Entity.EntityState.Deleted;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ImportErrorHandler_Delete", true);
                    return false;
                }
            }
        }

        public static ImportError GetImportErrorById(int Id)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    ImportError ie = (from i in rfc.ImportErrors 
                                      where i.Id == Id 
                                      select i).SingleOrDefault();
                    return ie;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ImportErrorHandler_GetImportErrorById", true);
                    return null;
                }
            }
        }

        public static BindingSource GetImportErrorList()
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var ieList = from ie in rfc.ImportErrors
                                 orderby ie.Date descending
                                 select new
                                 {
                                     ie.Id,
                                     ie.Date,
                                     ie.FileName,
                                     ie.Retailer1.Name,
                                     ie.Description
                                 };

                    BindingSource bs = new BindingSource();
                    ieList.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ImportErrorHandler_GetImportErrorList", true);
                    return null;
                }
            }
        }
    }
}
