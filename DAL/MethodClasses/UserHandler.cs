﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class UserHandler
    {
        /// <summary>
        /// Return full name of user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string GetUserName(int userId)
        {
            string userName;
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    userName = (from u in rfc.Users where u.Id == userId select u.FullName).SingleOrDefault();
                    if (userName.ToString() == string.Empty)
                    {
                        userName = "Unknown";
                    }
                }
                catch
                {
                    userName = "Unknown";
                }
                return userName;
            }
        }

        public static User GetUser(int userId)
        {
            using (RFContext rfc = new RFContext())
            {
                User thisUser = (from u in rfc.Users where u.Id == userId select u).SingleOrDefault();
                return thisUser;
            }
        }

        public static User GetUserFromLogon(string logon)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    User thisUser = (from u in rfc.Users where u.Logon == logon select u).Single();
                    return thisUser;
                }
                catch
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Commit new user to database
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns>Id of newly added record</returns>
        public static int Add(User newUser)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    newUser.Active = true;
                    rfc.Users.Add(newUser);
                    rfc.SaveChanges();
                    return newUser.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "UserHandler_Add", true);
                    return 0;
                }
            }
        }
        /// <summary>
        /// Commit changes to user to database
        /// </summary>
        /// <param name="updUser"></param>
        /// <returns>True if successful</returns>
        public static bool Update(User updUser)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    //rfc.Users.Attach(updUser);
                    rfc.Entry(updUser).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "UserHandler_Update", true);
                    return false;
                }
            }
        }
        /// <summary>
        /// Determine if user has a duplicate name to another user in the database.
        /// </summary>
        /// <param name="chkUser"></param>
        /// <returns></returns>
        public static bool IsDuplicate(User chkUser)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var dups = (from u in rfc.Users where u.Id != chkUser.Id && u.Logon == chkUser.Logon select u);
                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static BindingSource GetUserList(bool includeInActive)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var users =
                        from u in rfc.Users
                        where (u.Active == true | includeInActive == true)
                        orderby u.FullName
                        select u;

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(User);
                    users.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "UserHandler_GetUserList", true);
                    return null;
                }
            }
        }
    }
}
