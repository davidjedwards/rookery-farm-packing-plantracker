﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DAL.CommonClasses;
using RFP;
using System.Windows.Forms;

namespace MethodClasses
{
    public static class ComponentHandler
    {
        /// <summary>
        /// Add new component to the database
        /// </summary>
        /// <param name="newComp"></param>
        /// <returns>Id of newly added record (or 0 if failed to add)</returns>
        public static int Add(Component newComp)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    newComp.Active = true;
                    rfc.Components.Add(newComp);
                    rfc.SaveChanges();
                    return newComp.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_Add", true);
                    return 0;
                }
            }
        }
        /// <summary>
        /// Update component
        /// </summary>
        /// <param name="updComp"></param>
        /// <returns>Returns true if successful</returns>
        public static bool Update(Component updComp)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Entry(updComp).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_Update", true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Determine if component is a duplicate of another component in the database.
        /// </summary>
        /// <param name="chkComp"></param>
        /// <returns></returns>
        public static bool IsDuplicate(Component chkComp)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var dups = (from c in rfc.Components
                                where c.Id != chkComp.Id &&
                                      c.Description == chkComp.Description &&
                                      c.Retailer == chkComp.Retailer
                                select c);

                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static int CurrentQuantity(int ComponentId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    Nullable<int> TotalQty = (from n in rfc.StockLevels
                                    where n.Batch1.Component == ComponentId
                                    select n.Quantity).Sum();
                    return TotalQty ?? 0;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_CurrentQuantity", true);
                    return 0;
                }
            }
        }

        public static BindingSource GetComponentList(string Desc, int Retailer, string Code, int PickFace, bool includeInActive)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    var comps =
                        from c in rfc.Components
                        where (c.Active == true || includeInActive == true) &&
                              (Desc == string.Empty || c.Description.Contains(Desc)) &&
                              (Retailer == 0 || c.Retailer == Retailer) &&
                              (PickFace == 0 || c.PickFaceLocation == PickFace) &&
                              (Code == string.Empty || c.RetailerStockCode.Contains(Code))
                        select c;

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(Component);
                    comps.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_GetComponentList", true);
                    return null;
                }
            }
        }

        public static BindingSource GetBatchListFromComponent(Component comp)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    rfc.Components.Attach(comp);
                    BindingSource bs = new BindingSource();
                    var batches =
                        from btch in rfc.Batches
                        where btch.Component == comp.Id
                        orderby btch.Id descending
                        select btch;

                    bs.Add(new BatchDDEntry(0));
                    foreach (Batch b in batches)
                    {
                        bs.Add(new BatchDDEntry(b.Id, (int)b.BatchNo, (DateTime)b.BatchDate));
                    }
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_GetBatchListFromComponent", true);
                    return null;
                }
            }
        }

        public static int GetComponentIdFromCode(string Code, int Retailer)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    Nullable<int> comp = (from c in rfc.Components
                                where c.RetailerStockCode == Code && c.Retailer == Retailer && c.Active == true
                                select c.Id).SingleOrDefault();
                    if (comp == null)
                        return 0;
                    else
                        return (int)comp;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_GetComponentIdFromCode", true);
                    return 0;
                }
            }
        }

        public static Component GetComponentById(int compId)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    Component comp = (from c in rfc.Components
                                      where c.Id == compId
                                      select c).SingleOrDefault();
                    return comp;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "ComponentHandler_GetComponentById", true);
                    return null;
                }
            }
        }

    }

    public class BatchDDEntry
    {
        public int BatchId { get; set; }
        public string Description { get; set; }

        public BatchDDEntry()
        {
        }

        public BatchDDEntry(int Id)
        {
            BatchId = Id;
            Description = string.Empty;
        }

        public BatchDDEntry(int Id, int batchNumber, DateTime batchDate )
        {
            BatchId = Id;
            Description = string.Format("{0} - {1:dd/MM/yyyy}", batchNumber.ToString(), batchDate);
        }
    }
}
