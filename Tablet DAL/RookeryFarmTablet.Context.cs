﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tablet_DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using RFP;
    
    public partial class RFCTabletContext : DbContext
    {
        public RFCTabletContext()
            : base("name=RFCTabletContext")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<TabletComponent> TabletComponents { get; set; }
        public DbSet<TabletLocation> TabletLocations { get; set; }
        public DbSet<TabletConfigurationItem> TabletConfigurationItems { get; set; }
        public DbSet<TabletUser> TabletUsers { get; set; }
        public DbSet<TabletTransaction> TabletTransactions { get; set; }
        public DbSet<TabletReasonsForChange> TabletReasonsForChanges { get; set; }
        public DbSet<TabletStockLevelOrig> TabletStockLevelOrigs { get; set; }
        public DbSet<TabletBatch> TabletBatches { get; set; }
        public DbSet<TabletStockLevel> TabletStockLevels { get; set; }
        public DbSet<TabletFactor> TabletFactors { get; set; }
    }
}
