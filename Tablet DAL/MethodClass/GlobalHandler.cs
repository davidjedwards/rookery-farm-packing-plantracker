﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tablet_DAL.CommonClasses;
using DAL.CommonClasses;
using MethodClasses;
using RFP;
using DAL;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace Tablet_DAL.MethodClass
{
    public static class GlobalHandler
    {
        /// <summary>
        /// Clear tables
        /// </summary>
        /// <param name="updBatch"></param>
        /// <returns></returns>
        public static bool ClearTables()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletTransaction");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletStockLevelOrig");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletStockLevel");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletBatch");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletLocation");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletComponent");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletConfigurationItem");
                    rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletReasonsForChange");
                    return true;
                }
                catch (Exception ex)
                {
                    Tablet_DAL.CommonClasses.Exceptions.HandleException(ex, "GlobalHandler_ClearTables", true);
                    return false;
                }
            }
        }

        public static void UploadUserPWChange()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    //If the last modified date of a user on the tablet is later than the last modified date of a user on the desktop then update the desktop user (it will only be the password)
                    List<TabletUser> tabletUserList = (from u in rfcTablet.TabletUsers select u).ToList();
                    User desktopUser;
                    foreach (TabletUser u in tabletUserList)
                    {
                        desktopUser = UserHandler.GetUser(u.Id);
                        if (u.LastModifiedDate > desktopUser.LastModifiedDate)
                        {
                            desktopUser.Password = u.Password;
                            UserHandler.Update(desktopUser);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Tablet_DAL.CommonClasses.Exceptions.HandleException(ex, "GlobalHandler_UploadUserPWChange", true);
                }
            }
        }

        public static bool DownloadData(BackgroundWorker worker)
        {
            using (RFContext rfc = new RFContext())
            {
                try
                {
                    int numRecsToDownload = 0;
                    int downloadRec = 0;
                    int percentComplete = 0;

                    List<ConfigurationItem> configItemList = (from c in rfc.ConfigurationItems where c.Active == true select c).ToList();
                    numRecsToDownload += configItemList.Count();
                    List<RFP.Component> compList = (from c in rfc.Components where c.Active == true select c).ToList();
                    numRecsToDownload += compList.Count();
                    List<Location> locList = (from l in rfc.Locations where l.Active == true select l).ToList();
                    numRecsToDownload += locList.Count();
                    List<Batch> batchList = (from b in rfc.Batches where b.Component1.Active == true select b).ToList();
                    numRecsToDownload += batchList.Count();
                    List<StockLevel> slList = (from s in rfc.StockLevels
                                               where s.Batch1.Component1.Active == true && s.Location1.Active == true
                                               select s).ToList();
                    numRecsToDownload += slList.Count();
                    List<string> reasons = (from bh in rfc.BatchHistories
                                            where bh.Action == (int)Globals.Action.QuantityChange &&
                                            bh.Reason != null
                                            orderby bh.Reason
                                            select bh.Reason).Distinct().ToList();
                    numRecsToDownload += reasons.Count();

                    TabletConfigurationItem tConf;
                    List<TabletConfigurationItem> tcList = new List<TabletConfigurationItem>();
                    foreach (ConfigurationItem c in configItemList)
                    {
                        tConf = new TabletConfigurationItem();
                        tConf.Id = c.Id;
                        tConf.ItemValue = c.ItemValue;
                        tConf.ConfigurationGroup = c.ConfigurationGroup;
                        tcList.Add(tConf);
                        downloadRec += 1;
                        percentComplete = (int)(((float)downloadRec / (float)numRecsToDownload) * 100);
                        worker.ReportProgress(percentComplete);
                    }

                    using (RFCTabletContext rfcTablet = new RFCTabletContext())
                    {
                        rfcTablet.TabletConfigurationItems.AddRange(tcList);
                        rfcTablet.SaveChanges();
                    }

                    TabletComponent tComp;
                    List<TabletComponent> tcompList = new List<TabletComponent>();
                    foreach (RFP.Component c in compList)
                    {
                        tComp = new TabletComponent();
                        tComp.Id = c.Id;
                        tComp.Description = c.Description;
                        tComp.RetailerStockCode = c.RetailerStockCode;
                        tcompList.Add(tComp);
                        downloadRec += 1;
                        percentComplete = (int)(((float)downloadRec / (float)numRecsToDownload) * 100);
                        worker.ReportProgress(percentComplete);
                    }

                    using (RFCTabletContext rfcTablet = new RFCTabletContext())
                    {
                        rfcTablet.TabletComponents.AddRange(tcompList);
                        rfcTablet.SaveChanges();
                    }

                    TabletLocation tLoc;
                    List<TabletLocation> tlocList = new List<TabletLocation>();
                    foreach (Location l in locList)
                    {
                        tLoc = new TabletLocation();
                        tLoc.Id = l.Id;
                        tLoc.Code = l.Code;
                        tLoc.Description = l.Description;
                        tLoc.SortOrder = l.SortOrder;
                        tlocList.Add(tLoc);
                        downloadRec += 1;
                        percentComplete = (int)(((float)downloadRec / (float)numRecsToDownload) * 100);
                        worker.ReportProgress(percentComplete);
                    }

                    using (RFCTabletContext rfcTablet = new RFCTabletContext())
                    {
                        rfcTablet.TabletLocations.AddRange(tlocList);
                        rfcTablet.SaveChanges();
                    }

                    TabletBatch tBatch;
                    List<TabletBatch> tbatchList = new List<TabletBatch>();
                    foreach (Batch b in batchList)
                    {
                        tBatch = new TabletBatch();
                        tBatch.Id = b.Id;
                        tBatch.BatchDate = b.BatchDate;
                        tBatch.BatchNo = b.BatchNo;
                        tBatch.Component = b.Component;
                        tbatchList.Add(tBatch);
                        downloadRec += 1;
                        percentComplete = (int)(((float)downloadRec / (float)numRecsToDownload) * 100);
                        worker.ReportProgress(percentComplete);
                    }

                    using (RFCTabletContext rfcTablet = new RFCTabletContext())
                    {
                        rfcTablet.TabletBatches.AddRange(tbatchList);
                        rfcTablet.SaveChanges();
                    }

                    TabletStockLevel tStockLevel;
                    List<TabletStockLevel> tStockLevelList = new List<TabletStockLevel>();
                    foreach (StockLevel sl in slList)
                    {
                        tStockLevel = new TabletStockLevel();
                        tStockLevel.Id = sl.Id;
                        tStockLevel.Batch = sl.Batch;
                        tStockLevel.Location = sl.Location;
                        tStockLevel.Quality = sl.Quality;
                        tStockLevel.Quantity = sl.Quantity;
                        tStockLevel.Query = sl.Query;
                        tStockLevel.QueryType = sl.QueryType;
                        tStockLevel.ShippableWeek = sl.ShippableWeek;
                        tStockLevel.ShippableYear = sl.ShippableYear;
                        tStockLevel.StockTake = sl.StockTake;
                        tStockLevel.ReplenDetailId = sl.ReplenDetailId;
                        tStockLevel.Status = (int)TransactionStatus.Unchanged;
                        if (sl.ReplenishListDetail != null)
                            tStockLevel.FileName = sl.ReplenishListDetail.ReplenishList1.FileName;
                        tStockLevelList.Add(tStockLevel);
                        downloadRec += 1;
                        percentComplete = (int)(((float)downloadRec / (float)numRecsToDownload) * 100);
                        worker.ReportProgress(percentComplete);
                    }

                    //Reset all those stock level records that have stock take set now we've downloaded them
                    rfc.Database.ExecuteSqlCommand("UPDATE StockLevel SET StockTake = 0");

                    using (RFCTabletContext rfcTablet = new RFCTabletContext())
                    {
                        rfcTablet.TabletStockLevels.AddRange(tStockLevelList);
                        rfcTablet.SaveChanges();
                    }

                    TabletReasonsForChange tReason;
                    List<TabletReasonsForChange> tReasonsList = new List<TabletReasonsForChange>();
                    foreach (string r in reasons)
                    {
                        tReason = new TabletReasonsForChange();
                        tReason.Reason = r;
                        tReasonsList.Add(tReason);
                        downloadRec += 1;
                        percentComplete = (int)(((float)downloadRec / (float)numRecsToDownload) * 100);
                        worker.ReportProgress(percentComplete);
                    }

                    using (RFCTabletContext rfcTablet = new RFCTabletContext())
                    {
                        rfcTablet.TabletReasonsForChanges.AddRange(tReasonsList);
                        rfcTablet.SaveChanges();
                    }

                    
                    return true;
                }
                catch (Exception ex)
                {
                    Tablet_DAL.CommonClasses.Exceptions.HandleException(ex, "GlobalHandler_DownloadData", true);
                    return false;
                }
            }
        }

        public static bool UploadData(BackgroundWorker worker)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                //Local datatables to hold cross reference of DesktopIds and TabletIds for use when we're adding new records from the tablet
                DataTable ConfigXRef = new DataTable();
                ConfigXRef.Columns.Add("DesktopId", typeof(int));
                ConfigXRef.Columns.Add("TabletId", typeof(int));
                DataRow drConfigXRef;

                DataTable BatchXRef = new DataTable();
                BatchXRef.Columns.Add("DesktopId", typeof(int));
                BatchXRef.Columns.Add("TabletId", typeof(int));
                DataRow drBatchXRef;

                DataTable BatchHistoryXRef = new DataTable();
                BatchHistoryXRef.Columns.Add("DesktopId", typeof(int));
                BatchHistoryXRef.Columns.Add("TabletId", typeof(int));
                DataRow drBatchHistoryXRef;
           
                //Prepopulate with not new tablet data
                List<int> ConfigIds = (from c in rfcTablet.TabletConfigurationItems where c.New == false select c.Id).ToList();
                foreach (int i in ConfigIds)
                {
                    drConfigXRef = ConfigXRef.NewRow();
                    drConfigXRef["DesktopId"] = i;
                    drConfigXRef["TabletId"] = i;
                    ConfigXRef.Rows.Add(drConfigXRef);
                }

                List<int> BatchIds = (from b in rfcTablet.TabletBatches where b.New == false select b.Id).ToList();
                foreach (int i in BatchIds)
                {
                    drBatchXRef = BatchXRef.NewRow();
                    drBatchXRef["DesktopId"] = i;
                    drBatchXRef["TabletId"] = i;
                    BatchXRef.Rows.Add(drBatchXRef);
                }

                int numRecsToUpload = 0;
                int uploadRec = 0;
                int percentComplete = 0;
                    
                List<TabletStockLevelOrig> stockLevelOrig = (from sl in rfcTablet.TabletStockLevelOrigs.Include("TabletBatch").Include("TabletBatch.TabletComponent") select sl).ToList();
                numRecsToUpload += stockLevelOrig.Count();
                List<TabletConfigurationItem> configItemList = (from c in rfcTablet.TabletConfigurationItems where c.New == true select c).ToList();
                numRecsToUpload += configItemList.Count();
                List<TabletBatch> batchList = (from b in rfcTablet.TabletBatches where b.New == true select b).ToList();
                numRecsToUpload += batchList.Count();
                List<TabletStockLevel> stockLevelListNew = (from s in rfcTablet.TabletStockLevels where s.Status == (int)TransactionStatus.New select s).ToList();
                numRecsToUpload += stockLevelListNew.Count();
                List<TabletStockLevel> stockLevelListChanged = (from s in rfcTablet.TabletStockLevels where s.Status == (int)TransactionStatus.Changed select s).ToList();
                numRecsToUpload += stockLevelListChanged.Count();
                List<TabletStockLevel> stockLevelListDeleted = (from s in rfcTablet.TabletStockLevels where s.Status == (int)TransactionStatus.Deleted select s).ToList();
                numRecsToUpload += stockLevelListDeleted.Count();
                List<TabletTransaction> transactionList = (from t in rfcTablet.TabletTransactions orderby t.Id ascending select t).ToList();
                numRecsToUpload += transactionList.Count();

                if (numRecsToUpload == 0)
                {
                    percentComplete = 100;
                    worker.ReportProgress(percentComplete);
                    return true;
                }
                //First thing to check is whether the stock level records on the tablet that we're about to upload to the desktop have changed
                //on the desktop since we did a download. The user will then need to decide whether to keep the desktop change or overwrite with the tablet
                //change
                List<int> BatchesToIgnore = new List<int>();
                StockLevel sLevel;
                string msg;
                string transMsg;
                using (RFContext rfc = new RFContext())
                {
                    foreach (TabletStockLevelOrig slo in stockLevelOrig)
                    {
                        if (!BatchesToIgnore.Contains((int)slo.Batch))
                        {
                            sLevel = StockLevelHandler.GetStockLevelFromId(slo.Id);
                            if (sLevel == null ||
                                sLevel.Location != slo.Location ||
                                sLevel.Quantity != slo.Quantity ||
                                sLevel.Quality != slo.Quality ||
                                sLevel.ShippableWeek != slo.ShippableWeek ||
                                sLevel.ShippableYear != slo.ShippableYear ||
                                sLevel.Query != slo.Query ||
                                sLevel.QueryType != slo.QueryType)
                            {
                                msg = "Changes have been made on the desktop system to stock lines for " + slo.TabletBatch.TabletComponent.RetailerStockCode + " (" +
                                        slo.TabletBatch.TabletComponent.Description + "). Unable to upload changes made for this stock line on the tablet. \n" + 
                                        "Tablet transactions for this batch are as follows:\n";
                                List<TabletTransaction> trans = TabletTransactionHandler.GetTransactionListForBatch((int)slo.Batch);
                                BatchesToIgnore.Add((int)slo.Batch);
                                if (trans != null)
                                {
                                    foreach (TabletTransaction t in trans)
                                    {
                                        transMsg = Globals.ActionScreenDesc((Globals.Action)t.Action);
                                        switch ((Globals.Action)t.Action)
                                        {
                                            case Globals.Action.Disposed:
                                                transMsg += ": Location: " + t.TabletLocation.Code + "; Quantity: " + t.Quantity.ToString();
                                                break;
                                            case Globals.Action.MovedIn:
                                                transMsg += ": Quantity: " + t.Quantity.ToString() + " to location: " + t.TabletLocation.Code;
                                                break;
                                            case Globals.Action.Picked:
                                                transMsg += ": Location: " + t.TabletLocation.Code + "; Quantity: " + t.Quantity.ToString();
                                                break;
                                            case Globals.Action.QuantityChange:
                                                transMsg += ": Location: " + t.TabletLocation.Code + "; Quantity changed from : " + t.PrevQuantity.ToString() + " to " + t.Quantity.ToString();
                                                break;
                                            case Globals.Action.QualityChange:
                                                transMsg += ": Location: " + t.TabletLocation.Code + "; Quantity: " + t.Quantity.ToString() + "; Quality changed from " +
                                                    Globals.CropQualityScreenDesc((Globals.CropQuality)t.PrevQuality) + " to " +
                                                    Globals.CropQualityScreenDesc((Globals.CropQuality)t.Quality);
                                                break;
                                        }
                                        msg += transMsg + "\n";
                                    }
                                }
                                MessageBox.Show(msg, "Upload conflict", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        uploadRec += 1;
                    }
                    // update status
                    percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                    worker.ReportProgress(percentComplete);
                }

                using (RFContext rfc = new RFContext())
                {
                    using (var rfcTrans = rfc.Database.BeginTransaction())
                    {
                        try
                        {
                            //Upload new configuration items
                            ConfigurationItem conf;
                            foreach (TabletConfigurationItem tconf in configItemList)
                            {
                                //Before uploading check to see if a duplicate already exists. If it does, then use this rather than create
                                //a new record
                                conf = (from c in rfc.ConfigurationItems
                                        where c.ConfigurationGroup == (int)tconf.ConfigurationGroup &&
                                                c.ItemValue == tconf.ItemValue
                                        select c).SingleOrDefault();

                                if (conf == null)
                                {
                                    conf = new ConfigurationItem();
                                    conf.ConfigurationGroup = (int)tconf.ConfigurationGroup;
                                    conf.ItemValue = tconf.ItemValue;
                                    conf.Active = true;
                                    rfc.ConfigurationItems.Add(conf);
                                    rfc.SaveChanges();
                                }
                                //Add to local xref table
                                drConfigXRef = ConfigXRef.NewRow();
                                drConfigXRef["DesktopId"] = conf.Id;
                                drConfigXRef["TabletId"] = tconf.Id;
                                ConfigXRef.Rows.Add(drConfigXRef);
                                // update status
                                uploadRec += 1;
                                percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                                worker.ReportProgress(percentComplete);
                            }

                            //Upload new batch details
                            Batch btch;
                            foreach (TabletBatch tbtch in batchList)
                            {
                                btch = new Batch();
                                btch.Component = (int)tbtch.Component;
                                btch.BatchDate = tbtch.BatchDate;
                                btch.BatchNo = tbtch.BatchNo;
                                btch.CreatedBy = tbtch.CreatedBy;
                                btch.CreatedDate = DateTime.Now;
                                rfc.Batches.Add(btch);
                                rfc.SaveChanges();

                                //Add to local xref table
                                drBatchXRef = BatchXRef.NewRow();
                                drBatchXRef["DesktopId"] = btch.Id;
                                drBatchXRef["TabletId"] = tbtch.Id;
                                BatchXRef.Rows.Add(drBatchXRef);
                                // update status
                                uploadRec += 1;
                                percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                                worker.ReportProgress(percentComplete);
                            }

                            //Upload new stock levels
                            StockLevel stockLvl;
                            List<StockLevel> stockLvlList = new List<StockLevel>();
                            foreach (TabletStockLevel tsl in stockLevelListNew)
                            {
                                if (!BatchesToIgnore.Contains((int)tsl.Batch))
                                {
                                    stockLvl = new StockLevel();
                                    stockLvl.Location = tsl.Location;
                                    stockLvl.Batch = (int)BatchXRef.Select("TabletId=" + tsl.Batch.ToString())[0]["DesktopId"];
                                    stockLvl.Quantity = tsl.Quantity;
                                    stockLvl.Quality = tsl.Quality;
                                    stockLvl.ShippableWeek = tsl.ShippableWeek;
                                    stockLvl.ShippableYear = tsl.ShippableYear;
                                    stockLvl.Query = tsl.Query;
                                    if ((tsl.QueryType ?? 0) != 0)
                                        stockLvl.QueryType = (int)ConfigXRef.Select("TabletId=" + tsl.QueryType.ToString())[0]["DesktopId"];
                                    stockLvlList.Add(stockLvl);
                                }
                                // update status
                                uploadRec += 1;
                                percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                                worker.ReportProgress(percentComplete);
                            }

                            rfc.StockLevels.AddRange(stockLvlList);
                            rfc.SaveChanges();
                            stockLvlList.Clear();

                            //Upload changes to stock levels
                            foreach (TabletStockLevel tsl in stockLevelListChanged)
                            {
                                if (!BatchesToIgnore.Contains((int)tsl.Batch))
                                {
                                    stockLvl = rfc.StockLevels.Find(tsl.Id);
                                    stockLvl.Location = tsl.Location;
                                    Location loc = rfc.Locations.Find((int)tsl.Location);
                                    if (loc == null)
                                        loc = LocationHandler.GetLocationById((int)tsl.Location);
                                    stockLvl.Location1 = loc;
                                    stockLvl.Batch = (int)BatchXRef.Select("TabletId=" + tsl.Batch.ToString())[0]["DesktopId"];
                                    stockLvl.Quantity = tsl.Quantity;
                                    stockLvl.Quality = tsl.Quality;
                                    stockLvl.ShippableWeek = tsl.ShippableWeek;
                                    stockLvl.ShippableYear = tsl.ShippableYear;
                                    stockLvl.Query = tsl.Query;
                                    if ((tsl.QueryType ?? 0) != 0)
                                        stockLvl.QueryType = (int)ConfigXRef.Select("TabletId=" + tsl.QueryType.ToString())[0]["DesktopId"];
                                    rfc.StockLevels.Attach(stockLvl);
                                    rfc.Entry(stockLvl).State = System.Data.Entity.EntityState.Modified;
                                    rfc.SaveChanges();
                                }
                                // update status
                                uploadRec += 1;
                                percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                                worker.ReportProgress(percentComplete);
                            }

                            //Upload deleted stock levels
                            foreach (TabletStockLevel tsl in stockLevelListDeleted)
                            {
                                if (!BatchesToIgnore.Contains((int)tsl.Batch))
                                {
                                    stockLvl = rfc.StockLevels.Find(tsl.Id);
                                    rfc.Entry(stockLvl).State = System.Data.Entity.EntityState.Deleted;
                                    rfc.SaveChanges();
                                }
                                // update status
                                uploadRec += 1;
                                percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                                worker.ReportProgress(percentComplete);
                            }

                            //Now for the transactions - need to turn them into batch histories
                            BatchHistory bh;
                            List<BatchHistory> bhList = new List<BatchHistory>();
                            foreach (TabletTransaction tt in transactionList)
                            {
                                bh = new BatchHistory();
                                bh.Batch = (int)BatchXRef.Select("TabletId=" + tt.Batch.ToString())[0]["DesktopId"];
                                bh.ActionDate = tt.ModifiedDate;
                                bh.Location = tt.Location;
                                bh.Quantity = tt.Quantity;
                                bh.Quality = tt.Quality;
                                bh.Action = tt.Action;
                                bh.ActionedBy = tt.ActionedBy;
                                bh.Reason = tt.Reason;
                                bh.PrevQuantity = tt.PrevQuantity;
                                bh.PrevQuality = tt.PrevQuality;
                                if (tt.MovedOutId != null)
                                    bh.MovedOutId = (int)BatchHistoryXRef.Select("TabletId=" + tt.MovedOutId.ToString())[0]["DesktopId"];
                                bh.PrevShippableWk = tt.PrevShippableWk;
                                bh.PrevShippableYr = tt.PrevShippableYr;
                                bh.ReplenDetailId = tt.ReplenDetailId;
                                bhList.Add(bh);

                                rfc.BatchHistories.Add(bh);
                                rfc.SaveChanges();
                                //Add to local xref table
                                drBatchHistoryXRef = BatchHistoryXRef.NewRow();
                                drBatchHistoryXRef["DesktopId"] = bh.Id;
                                drBatchHistoryXRef["TabletId"] = tt.Id;
                                BatchHistoryXRef.Rows.Add(drBatchHistoryXRef);

                                // update status
                                uploadRec += 1;
                                percentComplete = (int)(((float)uploadRec / (float)numRecsToUpload) * 100);
                                worker.ReportProgress(percentComplete);
                            }

                            //Where the action is "picked" we need to update the replenishment list
                            List<int> replenDetailIds = (from b in bhList
                                                            where b.Action == (int)Globals.Action.Picked && b.ReplenDetailId != null
                                                            select (int)b.ReplenDetailId).Distinct().ToList();
                            int replenId;
                            ReplenishList rpl;
                            foreach (int r in replenDetailIds)
                            {
                                replenId = (int)(from rld in rfc.ReplenishListDetails where rld.Id == r select rld.ReplenishList).Single();
                                rpl = rfc.ReplenishLists.Find(replenId);
                                //Determine status as follows:
                                //For each replenishment list line determine if the line has been fully picked. If it has set value of 1 against line otherwise
                                //set value of 0. Sum all these values and if they equal the count of replenishment list lines then it's fully picked, otherwise
                                //it's partially picked
                                var replenSum = from r1 in rfc.ReplenishListDetails
                                                where r1.ReplenishList == replenId
                                                select new
                                                {
                                                    PickedQuantity = (int)(r1.BatchHistories.Where(n => n.Action == (int)Globals.Action.Picked).Sum(n => n.Quantity) ?? 0),
                                                    SetToPick = (int)(r1.StockLevels.Where(n => n.Quality == (int)Globals.CropQuality.Pick).Sum(n => n.Quantity) ?? 0)
                                                };
                                int PickedLines = replenSum.Count(n => n.PickedQuantity > 0 && n.PickedQuantity >= n.SetToPick);
                                int PickLines = replenSum.Count();

                                if (PickedLines == 0)
                                {
                                    rpl.Status =  (int)Globals.ReplenishListStatus.Applied;
                                }
                                else if (PickedLines >= PickLines)
                                {
                                    rpl.Status = (int)Globals.ReplenishListStatus.Picked;
                                }
                                else
                                {
                                    rpl.Status = (int)Globals.ReplenishListStatus.PartPicked;
                                }
                            }
                            rfc.SaveChanges();

                            //Remove data from tablet transactions table
                            rfcTablet.Database.ExecuteSqlCommand("DELETE FROM TabletTransaction");

                            rfcTrans.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            rfcTrans.Rollback();
                            Tablet_DAL.CommonClasses.Exceptions.HandleException(ex, "GlobalHandler_UploadData", true);
                            return false;
                        }
                    }
                }
            }
        }
    }
}
