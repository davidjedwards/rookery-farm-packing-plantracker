﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using Tablet_DAL.CommonClasses;

namespace Tablet_DAL.MethodClass
{
    public static class TabletUserHandler
    {
        public static void RefreshUserData()
        {
            try
            {
                using (RFContext rfc = new RFContext())
                {
                    using (RFCTabletContext lrfc = new RFCTabletContext())
                    {
                            lrfc.Database.ExecuteSqlCommand("DELETE FROM TabletUser");

                            TabletUser tu;
                            List<User> users = (from u in rfc.Users where u.Active == true && u.TabletUser == true select u).ToList();
                            foreach (User u in users)
                            {
                                tu = new TabletUser();
                                tu.Id = u.Id;
                                tu.Logon = u.Logon;
                                tu.FullName = u.FullName;
                                tu.Password = u.Password;
                                lrfc.TabletUsers.Add(tu);
                            }
                            lrfc.SaveChanges();
                      }
                }
            }
            catch (Exception ex)
            {
                Exceptions.HandleException(ex, "TabletUserHandler_RefreshUserData", true);
            }
        }

        public static TabletUser GetUser(int userId)
        {
            using (RFCTabletContext rfc = new RFCTabletContext())
            {
                TabletUser thisUser = (from u in rfc.TabletUsers where u.Id == userId select u).SingleOrDefault();
                return thisUser;
            }
        }

        /// <summary>
        /// Commit changes to user to database
        /// </summary>
        /// <param name="updUser"></param>
        /// <returns>True if successful</returns>
        public static bool Update(TabletUser updUser)
        {
            using (RFCTabletContext rfc = new RFCTabletContext())
            {
                try
                {
                    //rfc.Users.Attach(updUser);
                    rfc.Entry(updUser).State = System.Data.Entity.EntityState.Modified;
                    rfc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletUserHandler_Update", true);
                    return false;
                }
            }
        }

        public static TabletUser GetUserFromLogon(string logon)
        {
            using (RFCTabletContext rfc = new RFCTabletContext())
            {
                try
                {
                    TabletUser thisUser = (from u in rfc.TabletUsers where u.Logon == logon select u).Single();
                    return thisUser;
                }
                catch
                {
                    return null;
                }
            }
        }

        public static string GetUserName(int userId)
        {
            string userName;
            using (RFCTabletContext rfc = new RFCTabletContext())
            {
                try
                {
                    userName = (from u in rfc.TabletUsers where u.Id == userId select u.FullName).SingleOrDefault();
                    if (userName.ToString() == string.Empty)
                    {
                        userName = "Unknown";
                    }
                }
                catch
                {
                    userName = "Unknown";
                }
                return userName;
            }
        }
    }
}
