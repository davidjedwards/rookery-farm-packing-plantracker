﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using System.Windows.Forms;
using Tablet_DAL.CommonClasses;

namespace Tablet_DAL.MethodClass
{

    public static class TabletStockLevelHandler
    {
        public static int GetMaxId()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    int maxId = rfcTablet.TabletStockLevels.Max(n => n.Id);
                    return maxId;
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static bool StockLinesToProcess(Globals.CropQuality cq)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    int NumLines = rfcTablet.TabletStockLevels.Count(n => n.Quality == (int)cq);
                    if (NumLines > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_StockLinesToProcess", true);
                    return false;
                }
            }
        }

        public static bool StockCheckLinesToProcess()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    int NumLines = rfcTablet.TabletStockLevels.Count(n => n.StockTake == true);
                    if (NumLines > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_AnyStockCheckLinesToProcess", true);
                    return false;
                }
            }
        }

        public static bool CheckLinesToProcess()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    int NumLines = rfcTablet.TabletStockLevels.Count();
                    if (NumLines > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_CheckLinesToProcess", true);
                    return false;
                }
            }
        }

        public static List<TabletStockLevelRow> GetStockLevelsForLocation(int locationId, bool stockCheck)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    var stockLevels =
                        from sl in rfcTablet.TabletStockLevels
                        where sl.Location == locationId && sl.Status != (int)TransactionStatus.Deleted &&
                                             (sl.StockTake == true || stockCheck == false)
                        orderby sl.TabletBatch.TabletComponent.RetailerStockCode
                        select new
                        {
                            sl.Id,
                            Component = sl.TabletBatch.TabletComponent.RetailerStockCode + " (" + sl.TabletBatch.TabletComponent.Description + ")",
                            sl.TabletBatch.BatchNo,
                            sl.Quantity,
                            sl.Quality,
                            sl.ShippableWeek,
                            sl.ShippableYear,
                            sl.Query,
                            sl.QueryType,
                            sl.FileName
                        };

                    List<TabletStockLevelRow> slr = new List<TabletStockLevelRow>();
                    foreach (var s in stockLevels)
                    {
                        slr.Add(new TabletStockLevelRow(s.Id, s.Component, (int)s.BatchNo, (int)s.Quantity, (int)s.Quality, (int?)s.ShippableWeek,
                            (int?)s.ShippableYear, s.Query, (int?)s.QueryType, s.FileName));
                    }
                    return slr;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_GetStockLevelsForLocation", true);
                    return null;
                }
            }
        }

        public static List<TabletStockLevelRow> GetStockLevelsForLocationForProcess(int locationId, Globals.CropQuality cq)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    var stockLevels =
                        from sl in rfcTablet.TabletStockLevels
                        where sl.Location == locationId && sl.Status != (int)TransactionStatus.Deleted &&
                                             sl.Quality == (int)cq
                        orderby sl.TabletBatch.TabletComponent.RetailerStockCode
                        select new
                        {
                            sl.Id,
                            Component = sl.TabletBatch.TabletComponent.RetailerStockCode + " (" + sl.TabletBatch.TabletComponent.Description + ")",
                            sl.TabletBatch.BatchNo,
                            sl.Quantity,
                            sl.Quality,
                            sl.ShippableWeek,
                            sl.ShippableYear,
                            sl.Query,
                            sl.QueryType,
                            sl.FileName
                        };

                    List<TabletStockLevelRow> slr = new List<TabletStockLevelRow>();
                    foreach (var s in stockLevels)
                    {
                        slr.Add(new TabletStockLevelRow(s.Id, s.Component, (int)s.BatchNo, (int)s.Quantity, (int)s.Quality, (int?)s.ShippableWeek,
                            (int?)s.ShippableYear, s.Query, (int?)s.QueryType, s.FileName));
                    }
                    return slr;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_GetStockLevelsForLocation", true);
                    return null;
                }
            }
        }

        public static TabletStockLevel GetStockLevelFromId(int stockLevelId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletStockLevel sl = (from s in rfcTablet.TabletStockLevels
                                     where s.Id == stockLevelId
                                     select s).Single();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_GetStockLevelFromId", true);
                    return null;
                }
            }
        }

        public static bool UpdateStockLevel(TabletStockLevel updStockLevel, Globals.Action act, int userId, int prevQuantity, 
                                                int prevQuality, int? prevShipWk, int? prevShipYr, string chgReason = "")
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                    //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                    if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                    {
                        //Get original details (updStockLevel will contain updated details)
                        TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                        TabletStockLevelOrig slo = new TabletStockLevelOrig();
                        slo.Id = orig.Id;
                        slo.Location = orig.Location;
                        slo.Batch = orig.Batch;
                        slo.Quantity = orig.Quantity;
                        slo.Quality = orig.Quality;
                        slo.ShippableWeek = orig.ShippableWeek;
                        slo.ShippableYear = orig.ShippableYear;
                        slo.Query = orig.Query;
                        slo.QueryType = orig.QueryType;
                        slo.QueryTypeDesc = orig.QueryTypeDesc;
                        rfcTablet.TabletStockLevelOrigs.Add(slo);
                    }
                    //Only set to changed if it is not a new stock level. New stock levels need to remain new for the 
                    //upload process to work correctly
                    if (updStockLevel.Status != (int)TransactionStatus.New)
                    {
                        updStockLevel.Status = (int)TransactionStatus.Changed;
                        rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;
                    }

                    if (act != Globals.Action.Other)
                    {
                        //create batch history record
                        TabletTransaction tt = new TabletTransaction();
                        tt.StockLevelId = updStockLevel.Id;
                        tt.Batch = updStockLevel.Batch;
                        tt.ModifiedDate = DateTime.Now;
                        tt.Location = updStockLevel.Location;
                        tt.Quantity = updStockLevel.Quantity;
                        tt.Quality = updStockLevel.Quality;
                        tt.Action = (int)act;
                        tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                        tt.Reason = chgReason;
                        tt.PrevQuality = prevQuality;
                        tt.PrevQuantity = prevQuantity;
                        tt.PrevShippableWk = prevShipWk;
                        tt.PrevShippableYr = prevShipYr; 
                        rfcTablet.TabletTransactions.Add(tt);
                    }
                    rfcTablet.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_UpdateStockLevel", true);
                    return false;
                }
            }
        }

        public static int AddStockLevel(TabletStockLevel newStockLevel, Globals.Action act, int userId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    newStockLevel.Id = GetMaxId() + 1;
                    newStockLevel.Status = (int)TransactionStatus.New;
                    rfcTablet.TabletStockLevels.Add(newStockLevel);

                    if (act != Globals.Action.Other)
                    {
                        //create batch history record
                        TabletTransaction tt = new TabletTransaction();
                        tt.StockLevelId = newStockLevel.Id;
                        tt.Batch = newStockLevel.Batch;
                        tt.ModifiedDate = DateTime.Now;
                        tt.Location = newStockLevel.Location;
                        tt.Quantity = newStockLevel.Quantity;
                        tt.Quality = newStockLevel.Quality;
                        tt.Action = (int)act;
                        tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                        tt.PrevShippableWk = newStockLevel.ShippableWeek;
                        tt.PrevShippableYr = newStockLevel.ShippableYear; 
                        rfcTablet.TabletTransactions.Add(tt);
                    }
                    rfcTablet.SaveChanges();
                    return newStockLevel.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_AddStockLevel", true);
                    return 0;
                }
            }
        }

        public static TabletStockLevel SetNewQuality(TabletStockLevel updStockLevel, Globals.CropQuality cq, int userId, int qtyToProcess)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    Nullable<int> prevQuality;
                    Nullable<int> prevQuantity;
                    TabletStockLevel newStockLevel;

                    if (updStockLevel.Quantity == qtyToProcess)
                    {
                        //Just an update of the quality on the stock level record and return the passed in record
                        //Before updating the stock level record, check to see if one exists that has the same component,
                        //batch, (new) quality and shippable detals. If there is, then just make the adjustment to this stock level record
                        //and remove the current one.
                        newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, cq, (int)updStockLevel.ShippableWeek,
                            (int)updStockLevel.ShippableYear, (int)updStockLevel.Location, (int)updStockLevel.Id);

                        if (newStockLevel == null)
                        {
                            //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                            //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                            if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                            {
                                //Get original details (updStockLevel will contain updated details)
                                TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                                TabletStockLevelOrig slo = new TabletStockLevelOrig();
                                slo.Id = orig.Id;
                                slo.Location = orig.Location;
                                slo.Batch = orig.Batch;
                                slo.Quantity = orig.Quantity;
                                slo.Quality = orig.Quality;
                                slo.ShippableWeek = orig.ShippableWeek;
                                slo.ShippableYear = orig.ShippableYear;
                                slo.Query = orig.Query;
                                slo.QueryType = orig.QueryType;
                                slo.QueryTypeDesc = orig.QueryTypeDesc;
                                rfcTablet.TabletStockLevelOrigs.Add(slo);
                            }

                            prevQuality = updStockLevel.Quality;
                            updStockLevel.Quality = (int)cq;
                            if (updStockLevel.Status != (int)TransactionStatus.New)
                                updStockLevel.Status = (int)TransactionStatus.Changed;
                            rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                            //create transaction record
                            TabletTransaction tt = new TabletTransaction();
                            tt.StockLevelId = updStockLevel.Id;
                            tt.Batch = updStockLevel.Batch;
                            tt.ModifiedDate = DateTime.Now;
                            tt.Location = updStockLevel.Location;
                            tt.Quantity = updStockLevel.Quantity;
                            tt.Quality = updStockLevel.Quality;
                            tt.Action = (int)Globals.Action.QualityChange;
                            tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                            tt.PrevQuantity = updStockLevel.Quantity;
                            tt.PrevQuality = prevQuality;
                            tt.PrevShippableWk = updStockLevel.ShippableWeek;
                            tt.PrevShippableYr = updStockLevel.ShippableYear;
                            tt.ReplenDetailId = updStockLevel.ReplenDetailId;
                            rfcTablet.TabletTransactions.Add(tt);
                        }
                        else
                        {
                            newStockLevel.Quantity += qtyToProcess;
                            newStockLevel.ReplenDetailId = updStockLevel.ReplenDetailId;
                            rfcTablet.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;

                            //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                            //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                            //Do this for both newStockLevel and updStockLevel
                            if (newStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == newStockLevel.Id))
                            {
                                //Get original details (updStockLevel will contain updated details)
                                TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                                TabletStockLevelOrig slo = new TabletStockLevelOrig();
                                slo.Id = orig.Id;
                                slo.Location = orig.Location;
                                slo.Batch = orig.Batch;
                                slo.Quantity = orig.Quantity;
                                slo.Quality = orig.Quality;
                                slo.ShippableWeek = orig.ShippableWeek;
                                slo.ShippableYear = orig.ShippableYear;
                                slo.Query = orig.Query;
                                slo.QueryType = orig.QueryType;
                                slo.QueryTypeDesc = orig.QueryTypeDesc;
                                rfcTablet.TabletStockLevelOrigs.Add(slo);
                            }
                            if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                            {
                                //Get original details (updStockLevel will contain updated details)
                                TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                                TabletStockLevelOrig slo = new TabletStockLevelOrig();
                                slo.Id = orig.Id;
                                slo.Location = orig.Location;
                                slo.Batch = orig.Batch;
                                slo.Quantity = orig.Quantity;
                                slo.Quality = orig.Quality;
                                slo.ShippableWeek = orig.ShippableWeek;
                                slo.ShippableYear = orig.ShippableYear;
                                slo.Query = orig.Query;
                                slo.QueryType = orig.QueryType;
                                slo.QueryTypeDesc = orig.QueryTypeDesc;
                                rfcTablet.TabletStockLevelOrigs.Add(slo);
                            }


                            //create transaction record
                            if (updStockLevel.Status != (int)TransactionStatus.New)
                            {
                                TabletTransaction tt = new TabletTransaction();
                                tt.StockLevelId = newStockLevel.Id;
                                tt.Batch = newStockLevel.Batch;
                                tt.ModifiedDate = DateTime.Now;
                                tt.Location = newStockLevel.Location;
                                tt.Quantity = qtyToProcess;
                                tt.Quality = newStockLevel.Quality;
                                tt.Action = (int)Globals.Action.QualityChange;
                                tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                                tt.PrevQuality = updStockLevel.Quality;
                                tt.PrevQuantity = updStockLevel.Quantity;
                                tt.PrevShippableWk = updStockLevel.ShippableWeek;
                                tt.PrevShippableYr = updStockLevel.ShippableYear;
                                tt.ReplenDetailId = updStockLevel.ReplenDetailId;
                                rfcTablet.TabletTransactions.Add(tt);
                            }
                            //remove current stock level record - if it was created by the tablet then just delete, otherwise set status to deleted
                            if (updStockLevel.Status == (int)TransactionStatus.New)
                                rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Deleted;
                            else
                            {
                                updStockLevel.Status = (int)TransactionStatus.Deleted;
                                rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;
                            }
                        }

                        rfcTablet.SaveChanges();

                        if (newStockLevel == null)
                            return updStockLevel;
                        else
                            return newStockLevel;
                    }
                    else
                    {
                        //qty to process is less than the stock quantity so reduce the quantity on the current stock level record
                        //and create a new stock level record for the qty to process
                        prevQuantity = updStockLevel.Quantity;
                        updStockLevel.Quantity -= qtyToProcess;

                        //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                        //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                        if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                        {
                            //Get original details (updStockLevel will contain updated details)
                            TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                            TabletStockLevelOrig slo = new TabletStockLevelOrig();
                            slo.Id = orig.Id;
                            slo.Location = orig.Location;
                            slo.Batch = orig.Batch;
                            slo.Quantity = orig.Quantity;
                            slo.Quality = orig.Quality;
                            slo.ShippableWeek = orig.ShippableWeek;
                            slo.ShippableYear = orig.ShippableYear;
                            slo.Query = orig.Query;
                            slo.QueryType = orig.QueryType;
                            slo.QueryTypeDesc = orig.QueryTypeDesc;
                            rfcTablet.TabletStockLevelOrigs.Add(slo);
                        }
                        
                        if (updStockLevel.Status != (int)TransactionStatus.New)
                            updStockLevel.Status = (int)TransactionStatus.Changed;
                        rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;

                        //Before creating a new stock level record, check to see if one exists that has the same component,
                        //batch, quality and shippable detals. If there is, then just make the adjustment to this stock level record
                        //and return that.
                        newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, cq, (int)updStockLevel.ShippableWeek,
                            (int)updStockLevel.ShippableYear, (int)updStockLevel.Location, (int)updStockLevel.Id);
                        if (newStockLevel == null)
                        {
                            newStockLevel = new TabletStockLevel();
                            newStockLevel.Id = GetMaxId() + 1;
                            newStockLevel.Location = updStockLevel.Location;
                            newStockLevel.Quality = (int)cq;
                            newStockLevel.Quantity = qtyToProcess;
                            newStockLevel.Batch = updStockLevel.Batch;
                            newStockLevel.ShippableWeek = updStockLevel.ShippableWeek;
                            newStockLevel.ShippableYear = updStockLevel.ShippableYear;
                            newStockLevel.ReplenDetailId = updStockLevel.ReplenDetailId;
                            newStockLevel.Status = (int)TransactionStatus.New;
                            rfcTablet.TabletStockLevels.Add(newStockLevel);
                        }
                        else
                        {
                            newStockLevel.Quantity += qtyToProcess;
                            newStockLevel.ReplenDetailId = updStockLevel.ReplenDetailId;
                            if (newStockLevel.Status != (int)TransactionStatus.New)
                                newStockLevel.Status = (int)TransactionStatus.Changed;
                            rfcTablet.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;
                        }

                        //create transaction record
                        TabletTransaction tt = new TabletTransaction();
                        tt.StockLevelId = newStockLevel.Id;
                        tt.Batch = newStockLevel.Batch;
                        tt.ModifiedDate = DateTime.Now;
                        tt.Location = newStockLevel.Location;
                        tt.Quantity = qtyToProcess;
                        tt.Quality = newStockLevel.Quality;
                        tt.Action = (int)Globals.Action.QualityChange;
                        tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                        tt.PrevQuantity = 0;
                        tt.PrevQuality = updStockLevel.Quality;
                        tt.PrevShippableWk = updStockLevel.ShippableWeek;
                        tt.PrevShippableYr = updStockLevel.ShippableYear;
                        tt.ReplenDetailId = updStockLevel.ReplenDetailId;

                        rfcTablet.TabletTransactions.Add(tt);
                        rfcTablet.SaveChanges();
                        return newStockLevel;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "StockLevelHandler_SetNewQuality", true);
                    return updStockLevel;
                }
            }
        }

        public static TabletStockLevel GetMergeableStockLevel(int batchId, Globals.CropQuality cq, Nullable<int> shipWeek, Nullable<int> shipYear, Nullable<int> location, int currId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletStockLevel sl = (from s in rfcTablet.TabletStockLevels
                                           where s.Batch == batchId &&
                                                 s.Quality == (int)cq &&
                                                 s.ShippableWeek == shipWeek &&
                                                 s.ShippableYear == shipYear &&
                                                 s.Location == location &&
                                                 s.Id != currId
                                           select s).SingleOrDefault();
                    return sl;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_GetMergeableStockLevel", true);
                    return null;
                }
            }
        }

        public static bool PickOrDispose(TabletStockLevel updStockLevel, Globals.Action act, int userId, int qtyToProcess)
        {
            //Should only be here for picks and disposals
            if (act != Globals.Action.Disposed && act != Globals.Action.Picked)
                return false;

            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    //create transaction record for pick or dispose
                    TabletTransaction tt = new TabletTransaction();
                    tt.StockLevelId = updStockLevel.Id;
                    tt.Batch = updStockLevel.Batch;
                    tt.ModifiedDate = DateTime.Now;
                    tt.Location = updStockLevel.Location;
                    tt.Quantity = qtyToProcess;
                    tt.Quality = updStockLevel.Quality;
                    tt.Action = (int)act;
                    tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                    tt.PrevQuantity = updStockLevel.Quantity;
                    tt.PrevQuality = updStockLevel.Quality;
                    tt.PrevShippableWk = updStockLevel.ShippableWeek;
                    tt.PrevShippableYr = updStockLevel.ShippableYear;
                    tt.ReplenDetailId = updStockLevel.ReplenDetailId;

                    rfcTablet.TabletTransactions.Add(tt);

                    //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                    //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                    if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                    {
                        //Get original details (updStockLevel will contain updated details)
                        TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                        TabletStockLevelOrig slo = new TabletStockLevelOrig();
                        slo.Id = orig.Id;
                        slo.Location = orig.Location;
                        slo.Batch = orig.Batch;
                        slo.Quantity = orig.Quantity;
                        slo.Quality = orig.Quality;
                        slo.ShippableWeek = orig.ShippableWeek;
                        slo.ShippableYear = orig.ShippableYear;
                        slo.Query = orig.Query;
                        slo.QueryType = orig.QueryType;
                        slo.QueryTypeDesc = orig.QueryTypeDesc;
                        rfcTablet.TabletStockLevelOrigs.Add(slo);
                    }

                    if (updStockLevel.Quantity == qtyToProcess)
                    {
                        //Set stock level to delete
                        if (updStockLevel.Status == (int)TransactionStatus.New)
                        {
                            //This stock level item was set to dispose on the tablet and the item is being disposed
                            //in the same session (i.e. prior to upload). Therefore delete the stock level record
                            //rather than setting its status
                            rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Deleted;
                        }
                        else
                        {
                            updStockLevel.Status = (int)TransactionStatus.Deleted;
                            rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    else
                    {
                        //Reduce quantity on stock level
                        updStockLevel.Quantity -= qtyToProcess;
                        rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;
                    }
                    
                    rfcTablet.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_PickOrDispose", true);
                    return false;
                }
            }
        }

        public static bool PerformMove(TabletStockLevel updStockLevel, int newLocation, int? newQuality, int userId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletStockLevel newStockLevel;
                    TabletTransaction tt;
                    int movedOutId;

                    //The quantity to process will always be the quantity on the stock level so no need to do quite as much as the desktop version of this function
                    //Just update the location on the stock level and adjust the quality to the newQuality parameter
                    //Before updating the stock level record, check to see if one exists that has the same component,
                    //batch, (new) quality and shippable details. If there is, then just make the adjustment to this stock level record
                    //and remove the current one.
                    newStockLevel = GetMergeableStockLevel((int)updStockLevel.Batch, (Globals.CropQuality)newQuality, (int)updStockLevel.ShippableWeek,
                       (int)updStockLevel.ShippableYear, newLocation, (int)updStockLevel.Id);

                    //create transaction record for outward move
                    tt = new TabletTransaction();
                    tt.StockLevelId = updStockLevel.Id;
                    tt.Batch = updStockLevel.Batch;
                    tt.ModifiedDate = DateTime.Now;
                    tt.Location = updStockLevel.Location;
                    tt.Quantity = updStockLevel.Quantity;
                    tt.Quality = updStockLevel.Quality;
                    tt.Action = (int)Globals.Action.MovedOut;
                    tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                    tt.PrevShippableWk = updStockLevel.ShippableWeek;
                    tt.PrevShippableYr = updStockLevel.ShippableYear;

                    rfcTablet.TabletTransactions.Add(tt);
                    rfcTablet.SaveChanges();
                    movedOutId = tt.Id; //will need to be translated to a batch history id when we do the upload

                    if (newStockLevel == null)
                    {
                        //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                        //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                        if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                        {
                            //Get original details (updStockLevel will contain updated details)
                            TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                            TabletStockLevelOrig slo = new TabletStockLevelOrig();
                            slo.Id = orig.Id;
                            slo.Location = orig.Location;
                            slo.Batch = orig.Batch;
                            slo.Quantity = orig.Quantity;
                            slo.Quality = orig.Quality;
                            slo.ShippableWeek = orig.ShippableWeek;
                            slo.ShippableYear = orig.ShippableYear;
                            slo.Query = orig.Query;
                            slo.QueryType = orig.QueryType;
                            slo.QueryTypeDesc = orig.QueryTypeDesc;
                            rfcTablet.TabletStockLevelOrigs.Add(slo);
                            //To delete
                            rfcTablet.SaveChanges();
                        }

                        updStockLevel.Location = newLocation;
                        updStockLevel.TabletLocation = TabletLocationHandler.GetLocationById(newLocation);
                        updStockLevel.Quality = newQuality;
                        //If a portion of a stock level was set to Move On in the tablet session then it will have a status of New. If this is the
                        //case then we need to keep the status as new (otherwise it won't get onto the main database)
                        if (updStockLevel.Status != (int)TransactionStatus.New)
                            updStockLevel.Status = (int)TransactionStatus.Changed;
                        rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;
                        //To delete
                        rfcTablet.SaveChanges();

                        //create transaction record for inward move
                        tt = new TabletTransaction();
                        tt.StockLevelId = updStockLevel.Id;
                        tt.Batch = updStockLevel.Batch;
                        tt.ModifiedDate = DateTime.Now;
                        tt.Location = newLocation;
                        tt.Quantity = updStockLevel.Quantity;
                        tt.Quality = updStockLevel.Quality;
                        tt.Action = (int)Globals.Action.MovedIn;
                        tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                        tt.MovedOutId = movedOutId;

                        rfcTablet.TabletTransactions.Add(tt);
                        //To delete
                        rfcTablet.SaveChanges();
                    }
                    else
                    {
                        //Before updating the stock level, add the original into the StockLevelOrig table (used in the upload process). First
                        //check that there isn't already a record in the orig table for this stock level (if there is then don't overwrite)
                        //Do this for both newStockLevel and updStockLevel
                        if (newStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == newStockLevel.Id))
                        {
                            //Get original details (updStockLevel will contain updated details)
                            TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                            TabletStockLevelOrig slo = new TabletStockLevelOrig();
                            slo.Id = orig.Id;
                            slo.Location = orig.Location;
                            slo.Batch = orig.Batch;
                            slo.Quantity = orig.Quantity;
                            slo.Quality = orig.Quality;
                            slo.ShippableWeek = orig.ShippableWeek;
                            slo.ShippableYear = orig.ShippableYear;
                            slo.Query = orig.Query;
                            slo.QueryType = orig.QueryType;
                            slo.QueryTypeDesc = orig.QueryTypeDesc;
                            rfcTablet.TabletStockLevelOrigs.Add(slo);
                            //To delete
                            rfcTablet.SaveChanges();
                        }
                        if (updStockLevel.Status != (int)TransactionStatus.New && !rfcTablet.TabletStockLevelOrigs.Any(s => s.Id == updStockLevel.Id))
                        {
                            //Get original details (updStockLevel will contain updated details)
                            TabletStockLevel orig = GetStockLevelFromId(updStockLevel.Id);
                            TabletStockLevelOrig slo = new TabletStockLevelOrig();
                            slo.Id = orig.Id;
                            slo.Location = orig.Location;
                            slo.Batch = orig.Batch;
                            slo.Quantity = orig.Quantity;
                            slo.Quality = orig.Quality;
                            slo.ShippableWeek = orig.ShippableWeek;
                            slo.ShippableYear = orig.ShippableYear;
                            slo.Query = orig.Query;
                            slo.QueryType = orig.QueryType;
                            slo.QueryTypeDesc = orig.QueryTypeDesc;
                            rfcTablet.TabletStockLevelOrigs.Add(slo);
                            //To delete
                            rfcTablet.SaveChanges();
                        } 
                        
                        newStockLevel.Quantity += updStockLevel.Quantity;
                        if (newStockLevel.Status != (int)TransactionStatus.New)
                            newStockLevel.Status = (int)TransactionStatus.Changed;
                        rfcTablet.Entry(newStockLevel).State = System.Data.Entity.EntityState.Modified;

                        //create transaction record for inward move
                        tt = new TabletTransaction();
                        tt.StockLevelId = null;
                        tt.Batch = updStockLevel.Batch;
                        tt.ModifiedDate = DateTime.Now;
                        tt.Location = newStockLevel.Location;
                        tt.Quantity = updStockLevel.Quantity;
                        tt.Quality = newStockLevel.Quality;
                        tt.Action = (int)Globals.Action.MovedIn;
                        tt.ActionedBy = TabletUserHandler.GetUserName(userId);
                        tt.MovedOutId = movedOutId;
                        
                        if (updStockLevel.Status == (int)TransactionStatus.New)
                        {
                            //This stock level item was set to move on on the tablet and the item is being moved
                            //in the same session (i.e. prior to upload). Therefore delete the stock level record
                            //rather than setting its status
                            rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Deleted;
                            //To delete
                            rfcTablet.SaveChanges();
                        }
                        else
                        {
                            updStockLevel.Status = (int)TransactionStatus.Deleted;
                            rfcTablet.Entry(updStockLevel).State = System.Data.Entity.EntityState.Modified;
                            //To delete
                            rfcTablet.SaveChanges();
                        }


                        rfcTablet.TabletTransactions.Add(tt);
                    }

                    rfcTablet.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletStockLevelHandler_PerformMove", true);
                    return false;
                }
            }
        }
    }
}
