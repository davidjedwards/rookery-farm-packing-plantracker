﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL.CommonClasses;

namespace Tablet_DAL.MethodClass
{
    public static class TabletFactorHandler
    {
        public static void SetUploadDoneFlag()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletFactor f = (from fac in rfcTablet.TabletFactors where fac.Id == 1 select fac).SingleOrDefault();
                    f.UploadDone = true;
                    rfcTablet.Entry(f).State = System.Data.Entity.EntityState.Modified;
                    rfcTablet.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletFactorHandler_SetUploadDoneFlag", true);
                }
            }
        }

        public static void ResetUploadDoneFlag()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletFactor f = (from fac in rfcTablet.TabletFactors where fac.Id == 1 select fac).SingleOrDefault();
                    f.UploadDone = false;
                    rfcTablet.Entry(f).State = System.Data.Entity.EntityState.Modified;
                    rfcTablet.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletFactorHandler_ResetUploadDoneFlag", true);
                }
            }
        }

        public static bool ReturnUploadDoneFlag()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    bool ud = (from fac in rfcTablet.TabletFactors where fac.Id == 1 select fac.UploadDone).SingleOrDefault();
                    return ud;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletFactorHandler_ResetUploadDoneFlag", true);
                    return true;
                }
            }
        }
    }
}
