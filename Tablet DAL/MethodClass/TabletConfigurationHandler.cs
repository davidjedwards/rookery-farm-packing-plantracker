﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using Tablet_DAL.CommonClasses;

namespace Tablet_DAL.MethodClass
{
    public static class TabletConfigurationHandler
    {
        public static List<TabletConfigurationItem> GetConfigurationItemsAsList(ConfigGroup cfgId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    List<TabletConfigurationItem> thisList = (from c in rfcTablet.TabletConfigurationItems
                                                        where c.ConfigurationGroup == (int)cfgId
                                                        orderby c.ItemValue
                                                        select c).ToList();
                    TabletConfigurationItem nullValue = new TabletConfigurationItem { Id = -1, ItemValue = string.Empty };
                    thisList.Insert(0, nullValue);

                    return thisList;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletConfigurationHandler_GetConfigurationItemsAsList", true);
                    return null;
                }
            }
        }



        /// <summary>
        /// Add configuration item
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns></returns>
        public static int Add(TabletConfigurationItem newItem)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    newItem.Id = rfcTablet.TabletConfigurationItems.Max(n => n.Id) + 1;
                    newItem.New = true;
                    rfcTablet.TabletConfigurationItems.Add(newItem);
                    rfcTablet.SaveChanges();
                    return newItem.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletConfigurationHandler_Add", true);
                    return 0;
                }
            }
        }


        public static bool IsDuplicate(TabletConfigurationItem chkItem)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    var dups = (from c in rfcTablet.TabletConfigurationItems 
                                where c.Id != chkItem.Id && c.ConfigurationGroup == chkItem.ConfigurationGroup && c.ItemValue == chkItem.ItemValue select c);
                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
