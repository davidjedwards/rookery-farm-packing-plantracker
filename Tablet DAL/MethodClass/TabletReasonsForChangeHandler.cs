﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;

namespace Tablet_DAL.MethodClass
{
    public static class TabletReasonsForChangeHandler
    {
        public static List<string> GetReasonsForChangeList()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    List<string> reasonList = (from r in rfcTablet.TabletReasonsForChanges
                                               orderby r.Reason
                                               select r.Reason).Distinct().ToList();
                    reasonList.Insert(0, string.Empty);
                    return reasonList;
                }
                catch (Exception ex)
                {
                    Tablet_DAL.CommonClasses.Exceptions.HandleException(ex, "TabletReasonsForChangeHandler_GetReasonsForChangeList", true);
                    return null;
                }
            }
        }

        public static bool Add(TabletReasonsForChange trc)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    rfcTablet.TabletReasonsForChanges.Add(trc);
                    rfcTablet.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Tablet_DAL.CommonClasses.Exceptions.HandleException(ex, "TabletReasonsForChangeHandler_Add", true);
                    return false;
                }
            }
        }
    }

}
