﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;

namespace Tablet_DAL.MethodClass
{
    public static class TabletTransactionHandler
    {
        public static bool TransactionsExist()
        {
            using (RFCTabletContext rfc = new RFCTabletContext())
            {
                try
                {
                    int recCount = (from t in rfc.TabletTransactions select t).Count();
                    if (recCount > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static List<TabletTransaction> GetTransactionListForBatch(int batchId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    return (from t in rfcTablet.TabletTransactions.Include("TabletLocation")
                            where t.Batch == batchId select t).ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

    }
}
