﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL.CommonClasses;

namespace Tablet_DAL.MethodClass
{
    public static class TabletBatchHandler
    {
        public static int Add(TabletBatch newBatch)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    rfcTablet.TabletBatches.Add(newBatch);
                    rfcTablet.SaveChanges();
                    return newBatch.Id;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletBatchHander_Add", true);
                    return 0;
                }
            }
        }

        public static int GetMaxId()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    int maxId = rfcTablet.TabletBatches.Max(n => n.Id);
                    return maxId;
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static bool IsBatchNoAlreadyUsed(TabletBatch batch)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    DateTime dt = (DateTime)batch.BatchDate;
                    DateTime startYear = DateTime.Parse("01/01/" + dt.Year.ToString());
                    DateTime endYear = DateTime.Parse("31/12/" + dt.Year.ToString());
                    var dups =
                        from b in rfcTablet.TabletBatches
                        where b.BatchDate >= startYear &&
                              b.BatchDate <= endYear &&
                              b.Id != batch.Id &&
                              b.Component == batch.Component &&
                              b.BatchNo == batch.BatchNo
                        select b;

                    if (dups.Count() > 0)
                        return true;
                    else
                        return false;

                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
