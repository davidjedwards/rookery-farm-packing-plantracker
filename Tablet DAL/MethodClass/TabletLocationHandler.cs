﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using Tablet_DAL.CommonClasses;

namespace Tablet_DAL.MethodClass
{
    public static class TabletLocationHandler
    {
        public static LinkedList<TabletLocation> ListLocationsInOrder(bool stockTake = false)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    List<TabletLocation> locList;

                    if (stockTake)
                    {
                        locList = (from l in rfcTablet.TabletLocations
                                   where l.TabletStockLevels.Any(n => n.StockTake == true)
                                   orderby l.SortOrder ascending
                                   select l).ToList();
                    }
                    else
                    {
                        locList = (from l in rfcTablet.TabletLocations
                                   orderby l.SortOrder ascending
                                   select l).ToList();
                    }
                    LinkedListNode<TabletLocation> node = new LinkedListNode<TabletLocation>(null);
                    LinkedList<TabletLocation> locLinkedList = new LinkedList<TabletLocation>();
                    bool firstRec = true;
                    foreach (TabletLocation tl in locList)
                    {
                        if (firstRec)
                        {
                            node = locLinkedList.AddFirst(tl);
                            firstRec = false;
                        }
                        else
                        {
                            locLinkedList.AddAfter(node, new LinkedListNode<TabletLocation>(tl));
                            node = node.Next;
                        }
                    }
                    return locLinkedList;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletLocationHandler_ListLocationsInOrder", true);
                    return null;
                }
            }
        }

        public static LinkedList<TabletLocation> ListLocationsInOrderForProcess(Globals.CropQuality cq)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    List<TabletLocation> locList;

                    locList = (from l in rfcTablet.TabletLocations
                                where l.TabletStockLevels.Any(n => n.Quality == (int)cq)
                                orderby l.SortOrder ascending
                                select l).ToList();

                    LinkedListNode<TabletLocation> node = new LinkedListNode<TabletLocation>(null);
                    LinkedList<TabletLocation> locLinkedList = new LinkedList<TabletLocation>();
                    bool firstRec = true;
                    foreach (TabletLocation tl in locList)
                    {
                        if (firstRec)
                        {
                            node = locLinkedList.AddFirst(tl);
                            firstRec = false;
                        }
                        else
                        {
                            locLinkedList.AddAfter(node, new LinkedListNode<TabletLocation>(tl));
                            node = node.Next;
                        }
                    }
                    return locLinkedList;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletLocationHandler_ListLocationsInOrderForProcess", true);
                    return null;
                }
            }
        }

        public static TabletLocation GetLocationById(int locId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletLocation loc = (from l in rfcTablet.TabletLocations where l.Id == locId select l).Single();
                    return loc;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static List<TabletLocation> GetLocationList()
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    var locations =
                        from l in rfcTablet.TabletLocations
                        orderby l.SortOrder
                        select l;

                    return locations.ToList();
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletLocationHandler_GetLocationList", true);
                    return null;
                }
            }
        }
    }
}
