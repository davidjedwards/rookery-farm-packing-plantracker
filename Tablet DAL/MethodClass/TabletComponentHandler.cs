﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using System.Windows.Forms;
using Tablet_DAL.CommonClasses;


namespace Tablet_DAL.MethodClass
{
    public static class TabletComponentHandler
    {
        public static BindingSource GetComponentList(string Desc, string Code)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    var comps =
                        from c in rfcTablet.TabletComponents
                        where (Desc == string.Empty || c.Description.Contains(Desc)) &&
                              (Code == string.Empty || c.RetailerStockCode.Contains(Code))
                        select c;

                    BindingSource bs = new BindingSource();
                    bs.DataSource = typeof(TabletComponent);
                    comps.ToList().ForEach(n => bs.Add(n));
                    return bs;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletComponentHandler_GetComponentList", true);
                    return null;
                }
            }
        }

        public static TabletComponent GetComponentById(int compId)
        {
            using (RFCTabletContext rfcTablet = new RFCTabletContext())
            {
                try
                {
                    TabletComponent comp =
                        (from c in rfcTablet.TabletComponents
                         where c.Id == compId
                         select c).SingleOrDefault();

                    return comp;
                }
                catch (Exception ex)
                {
                    Exceptions.HandleException(ex, "TabletComponentHandler_GetComponentById", true);
                    return null;
                }
            }
        }

    }
}
