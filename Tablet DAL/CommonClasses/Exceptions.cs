﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using RFP;

namespace Tablet_DAL.CommonClasses
{
    public static class Exceptions
    {
        public static void HandleException(Exception ex1, string location, bool showMessage)
        {
            WriteToEventLog(ex1.Message + "\n\n" + "Location: " + location + "\n\n" + ex1.InnerException, Application.ProductName,
                EventLogEntryType.Error, Globals.LogType.Application);
            //Error error = new Error();
            //if (SystemInformation.UserName.Length > 150)
            //{
            //    error.errAppUser = SystemInformation.UserName.Substring(0, 150);
            //}
            //else
            //{
            //    error.errAppUser = SystemInformation.UserName;
            //}
            //if (Environment.UserName.Length > 150)
            //{
            //    error.errComputerUser = Environment.UserName.Substring(0, 150);
            //}
            //else
            //{
            //    error.errComputerUser = Environment.UserName;
            //}
            //if (Environment.MachineName.Length > 150)
            //{
            //    error.errComputer = Environment.MachineName.Substring(0, 150);
            //}
            //else
            //{
            //    error.errComputer = Environment.MachineName;
            //}
            //if (ex1.Message.Length > 250)
            //{
            //    error.errDescription = ex1.Message.Substring(0, 250);
            //}
            //else
            //{
            //    error.errDescription = ex1.Message;
            //}
            //if (Application.ProductVersion.Length > 250)
            //{
            //    error.errVersion = Application.ProductVersion.Substring(0, 250);
            //}
            //else
            //{
            //    error.errVersion = Application.ProductVersion;
            //}
            //if (Environment.OSVersion.ToString().Length > 250)
            //{
            //    error.errOSVersion = Environment.OSVersion.ToString().Substring(0, 250);
            //}
            //else
            //{
            //    error.errOSVersion = Environment.OSVersion.ToString();
            //}
            //if (ex1.StackTrace.ToString().Length > 500)
            //{
            //    error.errStack = ex1.StackTrace.ToString().Substring(0, 500);
            //}
            //else
            //{
            //    error.errStack = ex1.StackTrace.ToString();
            //}
            //error.errLocation = location;

            //ErrorsHandler.Add(error);

            if (showMessage)
            {
                MessageBox.Show(ex1.Message + "\nLocation: " + location, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
        }

        /// <summary>
        /// Write Event Log
        /// </summary>
        /// <param name="entry">Entry to be displayed in Event Log</param>
        /// <param name="appName">Application Name</param>
        /// <param name="eventType">What type of event you wish to log</param>
        /// <param name="logLocation">Place event in what location</param>
        public static void WriteToEventLog(string entry, string appName, EventLogEntryType eventType, Globals.LogType logLocation)
        {
            EventLog eventLog = new EventLog();
            string logName;

            switch (logLocation)
            {
                case Globals.LogType.Application:
                    logName = "Application";
                    break;
                case Globals.LogType.System:
                    logName = "System";
                    break;
                default:
                    logName = "Application";
                    break;
            }

            try
            {
                if (!EventLog.SourceExists(appName))
                {
                    EventLog.CreateEventSource(appName, logName);
                }
                eventLog.Source = appName;
                eventLog.WriteEntry(entry, eventType);
            }
            catch
            {
            }
        }

    }
}
