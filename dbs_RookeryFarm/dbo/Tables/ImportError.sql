﻿CREATE TABLE [dbo].[ImportError] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Date]        DATETIME       NULL,
    [Retailer]    INT            NULL,
    [FileName]    NVARCHAR (255) NULL,
    [Description] NVARCHAR (255) NULL,
    CONSTRAINT [PK_ImportError] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ImportError_Retailer] FOREIGN KEY ([Retailer]) REFERENCES [dbo].[Retailer] ([Id])
);

