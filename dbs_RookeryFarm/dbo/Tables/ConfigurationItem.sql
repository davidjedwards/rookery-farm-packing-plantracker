﻿CREATE TABLE [dbo].[ConfigurationItem] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [ConfigurationGroup] INT           NOT NULL,
    [ItemValue]          NVARCHAR (50) NULL,
    [Active]             BIT           CONSTRAINT [DF_ConfigurationItem_Active] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ConfigurationItem] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ConfigurationItem_ConfigurationGroup] FOREIGN KEY ([ConfigurationGroup]) REFERENCES [dbo].[ConfigurationGroup] ([Id])
);

