﻿CREATE TABLE [dbo].[Batch] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Component]        INT           NOT NULL,
    [BatchDate]        DATETIME      NULL,
    [BatchNo]          INT           NULL,
    [CreatedBy]        NVARCHAR (15) NULL,
    [CreatedDate]      DATETIME      NULL,
    [LastModifiedBy]   NVARCHAR (15) NULL,
    [LastModifiedDate] DATETIME      NULL,
    CONSTRAINT [PK_Batch] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Batch_Component] FOREIGN KEY ([Component]) REFERENCES [dbo].[Component] ([Id])
);

