﻿CREATE TABLE [dbo].[ExportError] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Date]        DATETIME       NULL,
    [Retailer]    INT            NULL,
    [FileName]    NVARCHAR (255) NULL,
    [Description] NTEXT          NULL,
    CONSTRAINT [PK_ExportError] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ExportError_Retailer] FOREIGN KEY ([Retailer]) REFERENCES [dbo].[Retailer] ([Id])
);

