﻿CREATE TABLE [dbo].[ConfigurationGroup] (
    [Id]        INT           NOT NULL,
    [GroupName] NVARCHAR (50) NULL,
    CONSTRAINT [PK_ConfigurationGroup] PRIMARY KEY CLUSTERED ([Id] ASC)
);

