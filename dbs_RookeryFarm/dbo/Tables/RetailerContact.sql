﻿CREATE TABLE [dbo].[RetailerContact] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Retailer]         INT            NOT NULL,
    [Name]             NVARCHAR (128) NULL,
    [Telephone]        NVARCHAR (50)  NULL,
    [Mobile]           NVARCHAR (50)  NULL,
    [Email]            NVARCHAR (128) NULL,
    [Active]           BIT            CONSTRAINT [DF_RetailerContact_Active] DEFAULT ((1)) NOT NULL,
    [CreatedBy]        NVARCHAR (15)  NULL,
    [CreatedDate]      DATETIME       NULL,
    [LastModifiedBy]   NVARCHAR (15)  NULL,
    [LastModifiedDate] DATETIME       NULL,
    CONSTRAINT [PK_RetailerContact] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RetailerContact_Retailer] FOREIGN KEY ([Retailer]) REFERENCES [dbo].[Retailer] ([Id]) ON DELETE CASCADE
);

