﻿CREATE TABLE [dbo].[ReplenishList] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Retailer]      INT            NULL,
    [ReplenishDate] DATETIME       NULL,
    [Status]        SMALLINT       CONSTRAINT [DF_ReplenishList_Applied] DEFAULT ((0)) NOT NULL,
    [FileName]      NVARCHAR (255) NULL,
    CONSTRAINT [PK_ReplenishList] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReplenishList_Retailer] FOREIGN KEY ([Retailer]) REFERENCES [dbo].[Retailer] ([Id])
);







