﻿CREATE TABLE [dbo].[StockLevel] (
    [Id]             INT IDENTITY (1, 1) NOT NULL,
    [Location]       INT NULL,
    [Batch]          INT NULL,
    [Quantity]       INT NULL,
    [Quality]        INT NULL,
    [ShippableWeek]  INT NULL,
    [ShippableYear]  INT NULL,
    [Query]          BIT CONSTRAINT [DF_StockLevel_Query] DEFAULT ((0)) NOT NULL,
    [QueryType]      INT NULL,
    [ReplenDetailId] INT NULL,
    [StockTake]      BIT CONSTRAINT [DF_StockLevel_StockTake] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_StockLevel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StockLevel_Batch] FOREIGN KEY ([Batch]) REFERENCES [dbo].[Batch] ([Id]),
    CONSTRAINT [FK_StockLevel_ConfigurationItem] FOREIGN KEY ([QueryType]) REFERENCES [dbo].[ConfigurationItem] ([Id]),
    CONSTRAINT [FK_StockLevel_Location] FOREIGN KEY ([Location]) REFERENCES [dbo].[Location] ([Id]),
    CONSTRAINT [FK_StockLevel_ReplenishListDetail] FOREIGN KEY ([ReplenDetailId]) REFERENCES [dbo].[ReplenishListDetail] ([Id]) ON DELETE SET NULL
);









