﻿CREATE TABLE [dbo].[BatchHistory] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Batch]           INT           NULL,
    [ActionDate]      DATETIME      NULL,
    [Location]        INT           NULL,
    [Quantity]        INT           NULL,
    [Quality]         INT           NULL,
    [Action]          INT           NULL,
    [ActionedBy]      NVARCHAR (15) NULL,
    [Reason]          VARCHAR (255) NULL,
    [PrevQuantity]    INT           NULL,
    [PrevQuality]     INT           NULL,
    [MovedOutId]      INT           NULL,
    [PrevShippableYr] INT           NULL,
    [PrevShippableWk] INT           NULL,
    [ReplenDetailId]  INT           NULL,
    CONSTRAINT [PK_BatchHistory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BatchHistory_Batch] FOREIGN KEY ([Batch]) REFERENCES [dbo].[Batch] ([Id]),
    CONSTRAINT [FK_BatchHistory_Location] FOREIGN KEY ([Location]) REFERENCES [dbo].[Location] ([Id]),
    CONSTRAINT [FK_BatchHistory_ReplenishListDetail] FOREIGN KEY ([ReplenDetailId]) REFERENCES [dbo].[ReplenishListDetail] ([Id]) ON DELETE SET NULL
);









