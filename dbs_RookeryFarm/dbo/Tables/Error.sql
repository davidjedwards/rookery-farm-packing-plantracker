﻿CREATE TABLE [dbo].[Error] (
    [errId]           INT            NOT NULL,
    [errLogDate]      DATETIME       NULL,
    [errAppUser]      NVARCHAR (150) NULL,
    [errComputerUser] NVARCHAR (150) NULL,
    [errComputer]     NVARCHAR (150) NULL,
    [errNumber]       NVARCHAR (50)  NULL,
    [errDescription]  NVARCHAR (255) NULL,
    [errVersion]      NVARCHAR (255) NULL,
    [errOSVersion]    NVARCHAR (255) NULL,
    [errStack]        NVARCHAR (500) NULL,
    [errLocation]     NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Error] PRIMARY KEY CLUSTERED ([errId] ASC)
);

