﻿CREATE TABLE [dbo].[Retailer] (
    [Id]                       INT            IDENTITY (1, 1) NOT NULL,
    [Name]                     NVARCHAR (128) NULL,
    [AddressLine1]             NVARCHAR (128) NULL,
    [AddressLine2]             NVARCHAR (128) NULL,
    [Town]                     NVARCHAR (128) NULL,
    [County]                   NVARCHAR (128) NULL,
    [PostCode]                 NVARCHAR (50)  NULL,
    [FTPHost]                  NVARCHAR (50)  NULL,
    [FTPUserName]              NVARCHAR (50)  NULL,
    [FTPPassword]              NVARCHAR (50)  NULL,
    [FTPInFolder]              NVARCHAR (255) NULL,
    [FTPOutFolder]             NVARCHAR (255) NULL,
    [Active]                   BIT            CONSTRAINT [DF_Retailer_Active] DEFAULT ((1)) NULL,
    [CreatedBy]                NVARCHAR (15)  NULL,
    [CreatedDate]              DATETIME       NULL,
    [LastModifiedBy]           NVARCHAR (15)  NULL,
    [LastModifiedDate]         DATETIME       NULL,
    [FTPProtocol]              SMALLINT       NULL,
    [FTPSshHostKeyFingerprint] NVARCHAR (60)  NULL,
    [FTPPort]                  INT            NULL,
    CONSTRAINT [PK_Retailer] PRIMARY KEY CLUSTERED ([Id] ASC)
);





