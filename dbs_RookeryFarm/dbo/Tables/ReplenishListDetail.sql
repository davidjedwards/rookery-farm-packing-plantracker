﻿CREATE TABLE [dbo].[ReplenishListDetail] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [ReplenishList] INT NULL,
    [Component]     INT NULL,
    [Quantity]      INT NULL,
    CONSTRAINT [PK_ReplenishListDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReplenishListDetail_Component] FOREIGN KEY ([Component]) REFERENCES [dbo].[Component] ([Id]),
    CONSTRAINT [FK_ReplenishListDetail_ReplenishList] FOREIGN KEY ([ReplenishList]) REFERENCES [dbo].[ReplenishList] ([Id]) ON DELETE CASCADE
);



