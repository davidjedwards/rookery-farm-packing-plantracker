﻿CREATE TABLE [dbo].[Lock] (
    [LockTable]  NVARCHAR (50) NOT NULL,
    [LockRecId]  INT           NOT NULL,
    [LockUserId] INT           NOT NULL,
    CONSTRAINT [PK_Lock] PRIMARY KEY CLUSTERED ([LockTable] ASC, [LockRecId] ASC, [LockUserId] ASC),
    CONSTRAINT [FK_Lock_User] FOREIGN KEY ([LockUserId]) REFERENCES [dbo].[User] ([Id])
);

