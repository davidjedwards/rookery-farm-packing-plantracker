﻿CREATE TABLE [dbo].[User] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Logon]            NVARCHAR (15)  NULL,
    [FullName]         NVARCHAR (128) NULL,
    [Password]         NVARCHAR (64)  NULL,
    [Permission]       INT            CONSTRAINT [DF_User_Permission] DEFAULT ((0)) NULL,
    [Retailer]         INT            NULL,
    [Active]           BIT            CONSTRAINT [DF_User_Active] DEFAULT ((1)) NOT NULL,
    [DesktopUser]      BIT            CONSTRAINT [DF_User_DesktopUser] DEFAULT ((0)) NULL,
    [TabletUser]       BIT            CONSTRAINT [DF_User_TabletUser] DEFAULT ((0)) NULL,
    [CreatedBy]        NVARCHAR (15)  NULL,
    [CreatedDate]      DATETIME       NULL,
    [LastModifiedBy]   NVARCHAR (15)  NULL,
    [LastModifiedDate] DATETIME       NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_User_Retailer] FOREIGN KEY ([Retailer]) REFERENCES [dbo].[Retailer] ([Id])
);

