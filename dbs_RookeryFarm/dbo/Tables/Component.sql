﻿CREATE TABLE [dbo].[Component] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Description]       NVARCHAR (128) NULL,
    [Retailer]          INT            NULL,
    [RetailerStockCode] NVARCHAR (50)  NULL,
    [PickFaceLocation]  INT            NULL,
    [Active]            BIT            CONSTRAINT [DF_Component_Active] DEFAULT ((1)) NOT NULL,
    [CreatedBy]         NVARCHAR (15)  NULL,
    [CreatedDate]       DATETIME       NULL,
    [LastModifiedBy]    NVARCHAR (15)  NULL,
    [LastModifiedDate]  DATETIME       NULL,
    CONSTRAINT [PK_Component] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Component_ConfigurationItem] FOREIGN KEY ([PickFaceLocation]) REFERENCES [dbo].[ConfigurationItem] ([Id]),
    CONSTRAINT [FK_Component_Retailer] FOREIGN KEY ([Retailer]) REFERENCES [dbo].[Retailer] ([Id]) ON DELETE SET NULL
);

