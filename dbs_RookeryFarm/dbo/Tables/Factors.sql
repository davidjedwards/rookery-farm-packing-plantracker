﻿CREATE TABLE [dbo].[Factors] (
    [Environment]            INT            NOT NULL,
    [FTPDownloadFolder]      NVARCHAR (255) NULL,
    [FTPUploadFolder]        NVARCHAR (255) NULL,
    [ShowUploadError]        BIT            CONSTRAINT [DF_Factors_ShowUploadError] DEFAULT ((0)) NOT NULL,
    [ShowDownloadError]      BIT            CONSTRAINT [DF_Factors_ShowDownloadError] DEFAULT ((0)) NOT NULL,
    [ShowDownloadsAvailable] BIT            CONSTRAINT [DF_Factors_ShowDownloadsAvailable] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Factors] PRIMARY KEY CLUSTERED ([Environment] ASC)
);

