﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTP_Service.Classes
{
    public static class WSGlobals
    {
        public const string logSource = "Plantracker FTP Service";
        public const string logName = "Application";

        public static void WriteEvent(string message, System.Diagnostics.EventLogEntryType logEntryType)
        {
            System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(logSource))
            {
                System.Diagnostics.EventLog.CreateEventSource(logSource, logName);
                log.Log = logSource;
            }
            else
                log.Source = logSource;

            log.WriteEntry(message, logEntryType);
        }
    }
}
