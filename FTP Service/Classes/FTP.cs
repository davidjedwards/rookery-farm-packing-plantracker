﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.ServiceProcess;
using BLL;
using RFP;
using System.Net;
using System.IO;
using WinSCP;
using System.Collections;

namespace FTP_Service.Classes
{
    public static class FTP
    {
        public static void PerformDownloadAndImport(Retailer r)
        {
            //Where to put downloaded file depends on the environment so get environment first
            int environment = Properties.Settings.Default.Environment;
            //Get location to put downloaded files
            string downloadFolder = FactorLogic.FTPDownloadFolder(environment);
            List<string> fileList = GetFileList(r);
            string localPath;
            ImportError ie;
            bool success;

            if (fileList != null && fileList.Count() > 0)
            {
                foreach (string file in fileList) 
                {
                    if (DownloadFile(file, downloadFolder, r))
                    {
                        //File successfully downloaded, now import it
                        localPath = Path.Combine(downloadFolder, file);
                        success = BLLFunctions.ImportReplenList(localPath, (int)r.Id);
                        Factor f = FactorLogic.RetrieveFactor(environment);
                        if (success)
                            f.ShowDownloadsAvailable = true;
                        else
                            f.ShowDownloadError = true;

                        FactorLogic.Update(f);
                    }
                    else
                    {
                        ie = new ImportError();
                        ie.Date = DateTime.Now;
                        ie.FileName = file;
                        ie.Retailer = r.Id;
                        ie.Description = "Download from FTP server failed";
                        ImportErrorLogic.AddImportError(ie);
                    }
                }
            }
        }

        public static bool DownloadFile(string fileName, string downloadFolder, Retailer r)
        {
            int environment = Properties.Settings.Default.Environment;
            try
            {
                //Specify full path of where the file will go (including file name)
                //string filePath = Path.Combine(downloadFolder, fileName);
                string filePath = downloadFolder + "\\";
                string remoteFile;
                if (string.IsNullOrEmpty(r.FTPOutFolder))
                    remoteFile = fileName;
                else
                    remoteFile = r.FTPOutFolder + "/" + fileName;

                Protocol p;
                if (r.FTPProtocol == (short?)Globals.FTPProtocol.FTP)
                    p = Protocol.Ftp;
                else
                    p = Protocol.Sftp;

                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = p,
                    HostName = r.FTPHost,
                    UserName = r.FTPUserName,
                    Password = r.FTPPassword,
                    PortNumber = (int)r.FTPPort,
                    SshHostKeyFingerprint = r.FTPSshHostKeyFingerprint
                };

                using (Session session = new Session())
                {
                    session.Open(sessionOptions);
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    transferOptions.FilePermissions = null;
                    transferOptions.PreserveTimestamp = true;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(remoteFile, filePath, true, transferOptions);
                    transferResult.Check();
                    WSGlobals.WriteEvent("Downloaded " + fileName + " to " + downloadFolder, System.Diagnostics.EventLogEntryType.Information);

                    return true;
                }
	        }
	        catch (Exception ex)
	        {
                WSGlobals.WriteEvent("Download File: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                Factor f = FactorLogic.RetrieveFactor(environment);
                f.ShowDownloadError = true;
                FactorLogic.Update(f);
                return false;
            }

        }

        private static List<string> GetFileList(Retailer r)
        {
            List<string> result = new List<string>();

            try
            {
               // Setup session options
                Protocol p;
                if (r.FTPProtocol == (short?)Globals.FTPProtocol.FTP)
                    p = Protocol.Ftp;
                else
                    p = Protocol.Sftp;

                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = p,
                    HostName = r.FTPHost,
                    UserName = r.FTPUserName,
                    Password = r.FTPPassword,
                    PortNumber = (int)r.FTPPort,
                    SshHostKeyFingerprint = r.FTPSshHostKeyFingerprint
                };
 
                using (Session session = new Session())
                {
                    session.Open(sessionOptions);

                    RemoteDirectoryInfo dir = session.ListDirectory(r.FTPOutFolder);
                    foreach (RemoteFileInfo fileInfo in dir.Files)
                    {
                        if (fileInfo.FileType == '-') //regular file, i.e. not symlink or directory
                            result.Add(fileInfo.Name);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                WSGlobals.WriteEvent("GetFileList: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                int environment = Properties.Settings.Default.Environment;
                Factor f = FactorLogic.RetrieveFactor(environment);
                f.ShowDownloadError = true;
                FactorLogic.Update(f);
                return null;
            }
        }

        public static void CreateStockLists()
        {

            //Where to create file for upload depends on the environment so get environment first
            int environment = Properties.Settings.Default.Environment;
            //Get location to create files
            string uploadFolder = FactorLogic.FTPUploadFolder(environment);
            string fileName;
            try
            {
                foreach (Retailer r in BLL.RetailerLogic.ListRetailers(false))
                {
                    if (!string.IsNullOrEmpty(r.FTPHost))
                    {
                        //There should be a subdirectory under uploadFolder for each retailer who has stock lists FTP'd to them.
                        //If no subdirectory exists create it
                        if (!Directory.Exists(Path.Combine(uploadFolder, r.Name)))
                            Directory.CreateDirectory(Path.Combine(uploadFolder, r.Name));

                        fileName = Path.Combine(uploadFolder, r.Name, "RFPStockList_" + DateTime.Today.ToString("yyyyMMdd") + ".csv");
                        BLLFunctions.ExportStockList(r, fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                WSGlobals.WriteEvent("CreateStockLists: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                Factor f = FactorLogic.RetrieveFactor(environment);
                f.ShowUploadError = true;
                FactorLogic.Update(f);
            }
        }

        public static void PerformUpload(Retailer r)
        {
            //Upload any files sitting in the retailers upload folder
            //Where to put downloaded file depends on the environment so get environment first
            int environment = Properties.Settings.Default.Environment;
            //Get location to put downloaded files
            string uploadFolder = FactorLogic.FTPUploadFolder(environment);
            string remoteFolder;
            string doneFilePath;
            string[] uploadFiles;

            if (string.IsNullOrEmpty(r.FTPInFolder))
                remoteFolder = string.Empty;
            else
                remoteFolder = r.FTPInFolder + "/";

            try
            {
                //There should be a subdirectory under uploadFolder for each retailer who has stock lists FTP'd to them.
                //If no subdirectory exists then no file will be there to upload!
                if (Directory.Exists(Path.Combine(uploadFolder, r.Name)))
                {
                    // Setup session options
                    Protocol p;
                    if (r.FTPProtocol == (short?)Globals.FTPProtocol.FTP)
                        p = Protocol.Ftp;
                    else
                        p = Protocol.Sftp;

                    SessionOptions sessionOptions = new SessionOptions
                    {
                        Protocol = p,
                        HostName = r.FTPHost,
                        UserName = r.FTPUserName,
                        Password = r.FTPPassword,
                        PortNumber = (int)r.FTPPort,
                        SshHostKeyFingerprint = r.FTPSshHostKeyFingerprint
                    };

                    using (Session session = new Session())
                    {
                        session.Open(sessionOptions);
                        TransferOptions transferOptions = new TransferOptions();
                        transferOptions.TransferMode = TransferMode.Binary;
                        TransferOperationResult transferResult = null;

                        uploadFiles = Directory.GetFiles(Path.Combine(uploadFolder, r.Name));
                        foreach (string fileName in uploadFiles)
                        {
                            try
                            {
                                FileInfo fileInf = new FileInfo(fileName);
                                transferResult = session.PutFiles(fileName, remoteFolder, false, transferOptions);
                                transferResult.Check(); //Will throw an exception if there was an error in the transfer

                                WSGlobals.WriteEvent("Sent " + fileInf.Name + " to " + remoteFolder, System.Diagnostics.EventLogEntryType.Information);

                                //Move file to "Done" folder
                                if (!Directory.Exists(Path.Combine(uploadFolder, r.Name, "Done")))
                                    Directory.CreateDirectory(Path.Combine(uploadFolder, r.Name, "Done"));

                                doneFilePath = Path.Combine(uploadFolder, r.Name, "Done", fileInf.Name);
                                //If a file of the same name exists delete it first
                                if (File.Exists(doneFilePath))
                                    File.Delete(doneFilePath);
                                //Now do the move
                                File.Move(fileName, doneFilePath);

                            }
                            catch (Exception)
                            {
                                foreach (TransferEventArgs transfer in transferResult.Transfers)
                                {
                                    if (transfer.Error != null)
                                    {
                                        //Write to Error Log
                                        ExportError ee = new ExportError();
                                        ee.Date = DateTime.Now;
                                        ee.FileName = transfer.FileName;
                                        ee.Retailer = r.Id;
                                        ee.Description = transfer.Error.Message;
                                        ExportErrorLogic.AddExportError(ee);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WSGlobals.WriteEvent("PerformUpload: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                Factor f = FactorLogic.RetrieveFactor(environment);
                f.ShowUploadError = true;
                FactorLogic.Update(f);
            }
        }
    }
}
