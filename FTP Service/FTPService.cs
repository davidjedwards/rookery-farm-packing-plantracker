﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using FTP_Service.Classes;
using System.Timers;
using BLL;
using RFP;

namespace FTP_Service
{
    public partial class FTPService : ServiceBase
    {
        private System.Timers.Timer timer1;

        public FTPService()
        {
            //InitializeComponent();
            this.ServiceName = "Plantracker FTP Service";
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        public static void Main()
        {
            System.ServiceProcess.ServiceBase.Run(new FTPService());
        }

        protected override void OnStart(string[] args)
        {
            WSGlobals.WriteEvent("FTP Service started at " + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), EventLogEntryType.Information);
            timer1 = new System.Timers.Timer(Properties.Settings.Default.TimerInterval);
            timer1.Enabled = true;
            // Hook up elapsed event for the timer
            timer1.Elapsed += OnTimedEvent;
        }

        protected override void OnPause()
        {
            WSGlobals.WriteEvent("FTP Service paused at " + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), EventLogEntryType.Information);
            timer1.Enabled = false;
        }

        protected override void OnContinue()
        {
            WSGlobals.WriteEvent("FTP Service continued at " + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), EventLogEntryType.Information);
            timer1.Enabled = true;
        }

        protected override void OnStop()
        {
            WSGlobals.WriteEvent("FTP Service stopped at " + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), EventLogEntryType.Information);
            timer1.Enabled = false;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            //Stock list to be created only once a day. First time through for the day we create the stock list
            //and attempt to upload. If upload fails then will need to keep trying 
            if (Properties.Settings.Default.LastStockListsCreated < DateTime.Today)
            {
                FTP.CreateStockLists();
                Properties.Settings.Default.LastStockListsCreated = DateTime.Today;
                Properties.Settings.Default.Save();
            }

            foreach (Retailer r in BLL.RetailerLogic.ListRetailers(false))
            {
                if (!string.IsNullOrEmpty(r.FTPHost))
                {
                    FTP.PerformUpload(r);
                    FTP.PerformDownloadAndImport(r);
                }
            }
        }
    }
}
