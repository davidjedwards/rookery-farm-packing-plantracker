﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;

namespace BLL
{
    public static class LocationLogic
    {
        public static Location RetrieveLocationFromId(object locId)
        {
            if (locId == null)
                return null;
            else
            {
                int LocationId;
                if (int.TryParse(locId.ToString(), out LocationId))
                    return MethodClasses.LocationHandler.GetLocationById(LocationId);
                else
                    return null;
            }
        }

        public static List<Location> ListLocations(bool _includeInactive)
        {
            return MethodClasses.LocationHandler.GetLocationList(_includeInactive);
        }

        public static List<Location> DropDownList()
        {
            return MethodClasses.LocationHandler.GetLocationListForDropDown();
        }

        public static bool UpdateLocation(Location loc, int userId, out string returnMsg)
        {
            //Make sure Location has not been changed to match an already existing location
            string msg;
            if (IsLocationValid(loc, out msg))
            {
                User liUser = UserLogic.RetrieveUser(userId);
                loc.LastModifiedBy = liUser.Logon;
                loc.LastModifiedDate = DateTime.Now;
                if (MethodClasses.LocationHandler.Update(loc))
                {
                    returnMsg = string.Empty;
                    return true;
                }
                else
                {
                    returnMsg = string.Empty;
                    return false;
                }
            }
            else
            {
                returnMsg = msg;
                return false;
            }
        }

        public static int AddLocation(Location loc, int userId, out string returnMsg)
        {
            //Make sure Location is not an already existing Location
            string msg;
            if (IsLocationValid(loc, out msg))
            {
                User liUser = UserLogic.RetrieveUser(userId);
                loc.LastModifiedBy = liUser.Logon;
                loc.LastModifiedDate = DateTime.Now;
                loc.CreatedBy = liUser.Logon;
                loc.CreatedDate = loc.LastModifiedDate;
                int newId = MethodClasses.LocationHandler.Add(loc);
                if (newId != 0)
                {
                    returnMsg = string.Empty;
                    return newId;
                }
                else
                {
                    returnMsg = string.Empty;
                    return 0;
                }
            }
            else
            {
                returnMsg = msg;
                return 0;
            }
        }

        private static bool IsLocationValid(Location loc, out string returnMsg)
        {
            //Make sure location has not been changed to match an already existing component
            if (MethodClasses.LocationHandler.IsDuplicate(loc))
            {
                returnMsg = "There is already a location in the system with this code (Note: it may be set to inactive - click on Show Inactive to view).";
                return false;
            }
            else if ((loc.Code ?? string.Empty) == string.Empty)
            {
                returnMsg = "Please enter a code";
                return false;
            }
            else if ((loc.Description ?? string.Empty) == string.Empty)
            {
                returnMsg = "Please enter a description";
                return false;
            }
            else
            {
                returnMsg = string.Empty;
                return true;
            }
        }

        public static bool IsLocationUsed(int locationId)
        {
            return LocationHandler.IsUsed(locationId);
        }
    }
}
