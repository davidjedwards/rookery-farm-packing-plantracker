﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Windows.Forms;

namespace BLL
{
    public static class ReplenishListLogic
    {
        public static bool Update(ReplenishList rpl)
        {
            return ReplenishListHandler.Update(rpl);
        }

        public static BindingSource RetrieveReplenishLists(int RetailerId)
        {
            return ReplenishListHandler.GetReplenishLists(RetailerId);
        }

        public static BindingSource RetrieveReplenishListDetails(int replenishList)
        {
            return ReplenishListHandler.GetReplenishListDetails(replenishList);
        }

        public static ReplenishList RetrieveReplenishListById(int rplId)
        {
            return ReplenishListHandler.GetReplenishListById(rplId);
        }

        public static bool Delete(ReplenishList delRpl)
        {
            return ReplenishListHandler.Delete(delRpl);
        }

        public static List<ReplenishListDetail> RetrieveReplenishListDetailsList(int replenishList)
        {
            return ReplenishListHandler.GetReplenishListDetailsList(replenishList);
        }

        public static bool EnoughShippableStock(int replenId, out string returnMsg)
        {
            return ReplenishListHandler.CheckShippableQuantities(replenId, out returnMsg);
        }

        public static bool EnoughShippableStockForLine(int replenDetail, out string returnMsg)
        {
            return ReplenishListHandler.CheckShippableQtyForReplenDetailLine(replenDetail, out returnMsg);
        }

        public static void UpdateReplenishListStatus(int replenishmentList)
        {
            ReplenishListHandler.UpdateReplenishListStatus(replenishmentList);
        }
    }
}
