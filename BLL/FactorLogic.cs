﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using MethodClasses;

namespace BLL
{
    public static class FactorLogic
    {
        public static string FTPDownloadFolder(int environment)
        {
            return FactorsHandler.GetFTPDownloadFolder(environment);
        }

        public static string FTPUploadFolder(int environment)
        {
            return FactorsHandler.GetFTPUploadFolder(environment);
        }

        public static bool UploadError(int environment)
        {
            return FactorsHandler.ShowUploadError(environment);
        }

        public static bool DownloadError(int environment)
        {
            return FactorsHandler.ShowDownloadError(environment);
        }

        public static bool DownloadsAvailable(int environment)
        {
            return FactorsHandler.ShowDownloadsAvailable(environment);
        }

        public static Factor RetrieveFactor(int environment)
        {
            return FactorsHandler.GetFactorByEnvironment(environment);
        }

        public static void Update(Factor f)
        {
            FactorsHandler.Update(f);
        }

    }
}
