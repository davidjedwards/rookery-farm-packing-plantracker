﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Windows.Forms;

namespace BLL
{
    public static class StockLevelLogic
    {
        public static int Add(StockLevel newStockLevel, Globals.Action act, int userId, out string returnMsg)
        {
            int newId = MethodClasses.StockLevelHandler.AddStockLevel(newStockLevel, act, userId);
            returnMsg = string.Empty;
            return newId;
        }

        public static bool Update(StockLevel updStockLevel, Globals.Action act, int userId, int prevQuantity, int prevQuality, 
            int prevShippableWeek, int prevShippableYear, string chgReason, out string returnMsg)
        {
            returnMsg = string.Empty;
            return MethodClasses.StockLevelHandler.UpdateStockLevel(updStockLevel, act, userId, prevQuantity, prevQuality, prevShippableWeek, prevShippableYear,
                chgReason);
        }

        public static bool Delete(int stockLevelId)
        {
            return MethodClasses.StockLevelHandler.Delete(stockLevelId);
        }

        public static BindingSource RetrieveStockLevelList(Nullable<int> compId, Nullable<int> batchNo, Nullable<int> quality, bool ShowQueryOnly, int Retailer)
        {
            return MethodClasses.StockLevelHandler.GetStockLevelList(compId, batchNo, quality, ShowQueryOnly, Retailer);
        }

        public static BindingSource RetrieveStockLevelListForStockCheck()
        {
            return MethodClasses.StockLevelHandler.GetStockLevelsForStockCheck();
        }
        
        public static StockLevel RetrieveStockLevelFromId(int id)
        {
            return StockLevelHandler.GetStockLevelFromId(id);
        }

        public static StockLevel SetNewQuality(StockLevel updStockLevel, Globals.CropQuality cq, int userId, int qtyToProcess, int? replenDetailId)
        {
            return StockLevelHandler.SetNewQuality(updStockLevel, cq, userId, qtyToProcess, replenDetailId);
        }

        public static StockLevel Move(StockLevel updStockLevel, int newLocation, int userId, int qtyToProcess)
        {
            return StockLevelHandler.PerformMove(updStockLevel, newLocation, userId, qtyToProcess);
        }

        public static bool PickDispose(StockLevel updStockLevel, Globals.Action act, int userId, int qtyToProcess)
        {
            return StockLevelHandler.PickOrDispose(updStockLevel, act, userId, qtyToProcess);
        }

        public static List<StockLevel> RetrieveShippableStockLevels(int compId)
        {
            return StockLevelHandler.GetShippableStockLevels(compId);
        }

        public static List<StockLevel> RetrieveRetailerStockExportList(int retailer)
        {
            return StockLevelHandler.GetRetailerStockExportList(retailer);
        }

        public static bool AddStockLevelsForStockCheck(int compId)
        {
            return StockLevelHandler.AddStockLevelsForStockCheck(compId);
        }
    }
}
