﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Windows.Forms;

namespace BLL
{
    public static class ComponentLogic
    {
        public static BindingSource ListComponents(string _Description, int _Retailer, string _Code, int _PickFaceLocation, bool _includeInactive)
        {
            return MethodClasses.ComponentHandler.GetComponentList(_Description, _Retailer, _Code, _PickFaceLocation, _includeInactive);
        }

        public static bool UpdateComponent(Component comp, int userId, out string returnMsg)
        {
            //Make sure component has not been changed to match an already existing component
            string msg;
            if (IsComponentValid(comp, out msg))
            {
                User liUser = UserLogic.RetrieveUser(userId);
                comp.LastModifiedBy = liUser.Logon;
                comp.LastModifiedDate = DateTime.Now;
                if (MethodClasses.ComponentHandler.Update(comp))
                {
                    returnMsg = string.Empty;
                    return true;
                }
                else
                {
                    returnMsg = string.Empty;
                    return false;
                }
            }
            else
            {
                returnMsg = msg;
                return false;
            }
        }

        public static int AddComponent(Component comp, int userId, out string returnMsg)
        {
            //Make sure component has not been changed to match an already existing component
            string msg;
            if (IsComponentValid(comp, out msg))
            {
                User liUser = UserLogic.RetrieveUser(userId);
                comp.LastModifiedBy = liUser.Logon;
                comp.LastModifiedDate = DateTime.Now;
                comp.CreatedBy = liUser.Logon;
                comp.CreatedDate = comp.LastModifiedDate;
                int newId = MethodClasses.ComponentHandler.Add(comp);
                if (newId != 0)
                {
                    returnMsg = string.Empty;
                    return newId;
                }
                else
                {
                    returnMsg = string.Empty;
                    return 0;
                }
            }
            else
            {
                returnMsg = msg;
                return 0;
            }
        }

        private static bool IsComponentValid(Component comp, out string returnMsg)
        {
            //Make sure component has not been changed to match an already existing component
            if (MethodClasses.ComponentHandler.IsDuplicate(comp))
            {
                returnMsg = "There is already a component in the system which matches these details (Note: it may be set to inactive - click on Show Inactive to view)." +
                    " Unable to add.";
                return false;
            }
            else if ((comp.Description ?? string.Empty) == string.Empty)
            {
                returnMsg = "Please enter a description";
                return false;
            }
            else if ((comp.Retailer ?? 0) == 0)
            {
                returnMsg = "Please select a retailer";
                return false;
            }
            else if ((comp.RetailerStockCode ?? string.Empty) == string.Empty)
            {
                returnMsg = "Please enter a retailer component code";
                return false;
            }
            else
            {
                returnMsg = string.Empty;
                return true;
            }
        }

        public static int GetCurrentQuantity(int ComponentId)
        {
            return ComponentHandler.CurrentQuantity(ComponentId);
        }

        public static BindingSource BatchDropDownList(Component comp)
        {
            return ComponentHandler.GetBatchListFromComponent(comp);
        }

        public static Component RetrieveComponentById(int componentId)
        {
            return ComponentHandler.GetComponentById(componentId);
        }
    }
}
