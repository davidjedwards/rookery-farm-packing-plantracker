﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Data;
using System.Windows.Forms;

namespace BLL
{
    public static class RetailerLogic
    {
        public static Retailer RetrieveRetailer(object RetailerId)
        {
            int rtlId;
            if (RetailerId == null)
                return null;
            else if (int.TryParse(RetailerId.ToString(), out rtlId))
            {
                return MethodClasses.RetailerHandler.GetRetailerById(rtlId);
            }
            else
                return null;
        }

        public static List<Retailer> ListRetailers(bool includeInActive)
        {
            return MethodClasses.RetailerHandler.GetRetailerList(includeInActive);
        }

        public static BindingSource ListRetailerContacts(Retailer rtl)
        {
            return MethodClasses.RetailerHandler.GetRetailerContacts(rtl);
        }

        public static bool UpdateRetailer(Retailer rtl, int userId, out string returnMsg)
        {
            //Make sure that the retailer name is not a duplicate of another retailer
            if (MethodClasses.RetailerHandler.IsDuplicate(rtl))
            {
                returnMsg = "There is already a retailer in the system with this name (Note: it may be set to inactive - click on Show Inactive to view)." +
                    " Unable to perform the update.";
                return false;
            }
            else
            {
                User liUser = UserLogic.RetrieveUser(userId);
                rtl.LastModifiedBy = liUser.Logon;
                rtl.LastModifiedDate = DateTime.Now;
                if (MethodClasses.RetailerHandler.Update(rtl))
                {
                    returnMsg = string.Empty;
                    return true;
                }
                else
                {
                    returnMsg = string.Empty;
                    return false;
                }
            }
        }
        /// <summary>
        /// Adds a new retailer
        /// </summary>
        /// <param name="userId">Logged on user</param>
        /// <returns>Id of new retailer</returns>
        public static int AddRetailer(int userId)
        {
            User liUser = UserLogic.RetrieveUser(userId);
            Retailer rtl = new Retailer();
            rtl.Name = "New Retailer";
            rtl.CreatedBy = liUser.Logon;
            rtl.Active = true;
            rtl.CreatedDate = DateTime.Now;
            rtl.LastModifiedBy = liUser.Logon;
            rtl.LastModifiedDate = rtl.CreatedDate;
            if (MethodClasses.RetailerHandler.Add(rtl))
            {
                return rtl.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
