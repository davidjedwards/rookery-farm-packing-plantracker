﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Data;
using System.Windows.Forms;

namespace BLL
{
    public static class ConfigurationLogic
    {
        /// <summary>
        /// Return list of configuration groups
        /// </summary>
        /// <returns></returns>
        public static List<ConfigurationGroup> ListConfigGroups()
        {
            return ConfigurationHandler.GetConfigGroups();
        }

        public static BindingSource ListConfigurationItemsAsBindingSource(ConfigGroup configGroup, bool includeInActive)
        {
            return ConfigurationHandler.GetConfigurationItemsAsBindingSource(configGroup, includeInActive);
        }

        public static List<ConfigurationItem> ListConfigurationItemsAsList(ConfigGroup configGroup, bool includeInActive)
        {
            return ConfigurationHandler.GetConfigurationItemsAsList(configGroup, includeInActive);
        }

        /// <summary>
        /// Update configuration item, ensuring first it is not a duplicate
        /// </summary>
        /// <param name="cfi"></param>
        /// <param name="userId"></param>
        /// <param name="returnMsg"></param>
        /// <returns></returns>
        public static bool UpdateConfigurationItem(ConfigurationItem cfi, out string returnMsg)
        {
            //Make sure configuration item not a duplicate within the same configuration group
            if (ConfigurationHandler.IsDuplicate(cfi))
            {
                returnMsg = "There is already a configuration item within this configuration group with the same value (Note: it may be set to inactive - click on Show Inactive to view)." +
                    " Unable to perform update.";
                return false;
            }
            else
            {
                if (ConfigurationHandler.Update(cfi))
                {
                    returnMsg = string.Empty;
                    return true;
                }
                else
                {
                    returnMsg = string.Empty;
                    return false;
                }
            }
        }

        public static int AddConfigurationItem(ConfigurationItem cfi, out string returnMsg)
        {
            if (cfi.ItemValue == null || cfi.ItemValue == string.Empty)
            {
                returnMsg = "Value cannot be blank.";
                return 0;
            }
            else if (ConfigurationHandler.IsDuplicate(cfi))
            {
                returnMsg = "There is already a configuration item within this configuration group with the same value (Note: it may be set to inactive - click on Show Inactive to view)." +
                    " Unable to perform update.";
                return 0;
            }
            else
            {
                returnMsg = string.Empty;
                return ConfigurationHandler.Add(cfi);
            }
        }

        public static ConfigurationItem RetrieveConfigItemById(object ConfigItemId)
        {
            int cfiId;
            if (ConfigItemId == null)
                return null;
            else if (int.TryParse(ConfigItemId.ToString(), out cfiId))
                return ConfigurationHandler.GetItemById(cfiId);
            else
                return null;
        }



    }
}
