﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using MethodClasses;
using RFP;

namespace BLL
{
    public static class CheckInOut
    {
        /// <summary>
        /// Checks to see whether requested record from the entity is currently in use by another user.
        /// If it is then a message will be returned, otherwise the record will be checked out and
        /// will not be available to another user until checked in again.
        /// </summary>
        /// <param name="entity">Name of entity record belongs to</param>
        /// <param name="id">Unique Id of record</param>
        /// <param name="returnMsg">Reason why check out not possible</param>
        public static bool CheckOut(RFP.Globals.Entities entity, int id, int userId, out string returnMsg)
        {
            returnMsg = string.Empty;
            int? CheckedOutUser;

            Lock newLock = new Lock();
            newLock.LockTable = entity.ToString();
            newLock.LockRecId = id;
            newLock.LockUserId = userId;
            
            //Determine if record is already checked out
            if (CheckInOutHander.IsRecordCheckedOut(newLock, out CheckedOutUser))
            {
                //If checked out by this user then just send back that it is successfully checked out
                if (CheckedOutUser == userId)
                {
                    return true;
                }
                else
                {
                    //Return message to UI
                    if (CheckedOutUser != null)
                    {
                        //CheckedOutUser is the user id so get the user's full name
                        string userName = UserHandler.GetUserName((int)CheckedOutUser);
                        if (id == 0)
                            returnMsg = string.Format("This area is currently in use by {0} and cannot be edited at the moment", userName);
                        else
                            returnMsg = String.Format("This record is currently in use by {0} and cannot be edited at the moment", userName);
                    }
                    return false;
                }
            }
            else
            {
                //Check out record
                if (CheckInOutHander.Add(newLock))
                {
                    return true;
                }
                else
                {
                    if (id == 0)
                        returnMsg = "Unable to determine if this area is currently in use. As a precaution, the area is not editable at the moment.";
                    else
                        returnMsg = "Unable to determine if this record is currently in use. As a precaution, the record is not editable at the moment.";
                    return false;
                }
            }
        }

        public static bool CheckIn(RFP.Globals.Entities entity, int id, int userId, out string returnMsg)
        {
            returnMsg = string.Empty;
            Lock remLock = new Lock();
            remLock.LockTable = entity.ToString();
            remLock.LockRecId = id;
            remLock.LockUserId = userId;
            if (CheckInOutHander.Delete(remLock))
            {
                return true;
            }
            else
            {
                if (id == 0)
                    returnMsg = "There was a problem releasing this areafor others to use. It may be necessary to log off the system and ask an adminstrator to reset your locks";
                else
                    returnMsg = "There was a problem releasing this record for others to use. It may be necessary to log off the system and ask an adminstrator to reset your locks";
                return false;
            }
        }

        public static bool ClearUserLocks(int userId)
        {
            return CheckInOutHander.ClearUserLocks(userId);
        }
    }
}
