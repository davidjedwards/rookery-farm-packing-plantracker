﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Windows.Forms;

namespace BLL
{
    public static class BatchLogic
    {
        public static Batch RetrieveBatchFromId(object batchId)
        {
            if (batchId == null)
                return null;
            else
            {
                int Id;
                if (int.TryParse(batchId.ToString(), out Id))
                    return MethodClasses.BatchHandler.GetBatchById(Id);
                else
                    return null;
            }
        }

        public static BindingSource ListBatches(int compId, Nullable<DateTime> rangeStart, Nullable<DateTime> rangeEnd)
        {
            return MethodClasses.BatchHandler.GetBatchList(compId, rangeStart, rangeEnd);
        }

        public static bool UpdateBatch(Batch batch, int userId, out string returnMsg)
        {
            //Only batch date and number can be amended
            if (batch.BatchDate == null)
            {
                returnMsg = "Please enter a batch date";
                return false;
            }
            else if (batch.BatchNo == null)
            {
                returnMsg = "Please enter a batch number";
                return false;
            }
            else if (BatchHandler.IsBatchNoAlreadyUsed(batch))
            {
                returnMsg = "Batch number has already been used this year. Please enter another";
                return false;
            }
            else
            {
                User liUser = UserLogic.RetrieveUser(userId);
                batch.LastModifiedBy = liUser.Logon;
                batch.LastModifiedDate = DateTime.Now;
                if (MethodClasses.BatchHandler.Update(batch))
                {
                    returnMsg = string.Empty;
                    return true;
                }
                else
                {
                    returnMsg = string.Empty;
                    return false;
                }
            }
        }

        public static int AddBatch(Batch batch, int userId, out string returnMsg)
        {
            //Validate that all fields are entered and the batch number is not a duplicate
            if (batch.BatchDate == null)
            {
                returnMsg = "Please enter a batch date";
                return 0;
            }
            else if (batch.BatchNo == null)
            {
                returnMsg = "Please enter a batch number";
                return 0;
            }
            else if (BatchHandler.IsBatchNoAlreadyUsed(batch))
            {
                returnMsg = "Batch number has already been used this year. Please enter another";
                return 0;
            }
            else
            {
                User liUser = UserLogic.RetrieveUser(userId);
                batch.LastModifiedBy = liUser.Logon;
                batch.LastModifiedDate = DateTime.Now;
                batch.CreatedBy = liUser.Logon;
                batch.CreatedDate = batch.LastModifiedDate;
                int newId = MethodClasses.BatchHandler.Add(batch);
                if (newId != 0)
                {
                    returnMsg = string.Empty;
                    return newId;
                }
                else
                {
                    returnMsg = string.Empty;
                    return 0;
                }
            }
        }

        public static BindingSource RetrieveBatchHistory(int batchId)
        {
            return MethodClasses.BatchHandler.GetBatchHistoryList(batchId);
        }

        public static BatchHistory RetrieveBatchHistoryRecordFromId(int batchHistoryId)
        {
            BatchHistory bh = BatchHandler.GetBatchHistoryRecordFromId(batchHistoryId);
            return bh;
        }

        public static bool UndoBatchHistory(BatchHistory bh, int userId, out string returnMsg)
        {
            return BatchHandler.UndoBatchHistory(bh, userId, out returnMsg);
        }

        public static List<string> RetrieveReasonList()
        {
            return BatchHandler.ChangeReasonList();
        }
           
        public static List<BatchHistory> BatchHistoryListByReplenDetId(int replenId)
        {
            return BatchHandler.GetBatchHistoryListByReplenDetId(replenId);
        }

        public static List<BatchHistory> LatestBatchHistoriesToId(int batchId, int bhId)
        {
            return BatchHandler.GetLatestBatchHistoriesToId(batchId, bhId);
        }
    
    }
}
