﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using MethodClasses;
using System.Windows.Forms;

namespace BLL
{
    public static class ImportErrorLogic
    {
        public static BindingSource RetrieveImportErrorList()
        {
            return ImportErrorHandler.GetImportErrorList();
        }

        public static bool DeleteImportError(ImportError ie)
        {
            return ImportErrorHandler.Delete(ie);
        }

        public static ImportError RetrieveImportError(int Id)
        {
            return ImportErrorHandler.GetImportErrorById(Id);
        }

        public static void AddImportError(ImportError ie)
        {
            ImportErrorHandler.Add(ie);
        }
    }
}
