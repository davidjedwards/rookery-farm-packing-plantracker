﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using DAL.CommonClasses;

namespace BLL
{
    public static class BLLFunctions
    {
        public static bool ImportReplenList(string fileName, int Retailer)
        {
            return DAL.CommonClasses.Functions.ImportReplenishList(fileName, Retailer);
        }

        public static bool ExportStockList(Retailer r, string fileName)
        {
            return DAL.CommonClasses.Functions.ExportStockList(r, fileName);
        }
    }
}
