﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;

namespace BLL
{
    public static class ExceptionLogic
    {
        public static void HandleException(Exception ex1, string location, bool showMessage)
        {
            DAL.CommonClasses.Exceptions.HandleException(ex1, location, showMessage);
        }
    }
}
