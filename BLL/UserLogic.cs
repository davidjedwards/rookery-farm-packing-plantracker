﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using RFP;
using MethodClasses;
using System.Data;
using System.Windows.Forms;

namespace BLL
{
    public static class UserLogic
    {
        public static BindingSource ListUsers(bool includeInActive)
        {
            return MethodClasses.UserHandler.GetUserList(includeInActive);
        }

        public static bool UpdateUser(User usr, int loggedInUsr, out string returnMsg)
        {
            //Make sure the user name is not a duplicate
            if (MethodClasses.UserHandler.IsDuplicate(usr))
            {
                returnMsg = "There is already a user in the system with this logon (Note: the user may be set to inactive - click on Show Inactive to view)." +
                    "Unable to perform the update.";
                return false;
            }
            else if (usr.Logon == null)
            {
                returnMsg = "Please enter a logon name";
                return false;
            }
            else if (usr.FullName == null)
            {
                returnMsg = "Please enter the user's full name.";
                return false;
            }
            else if (usr.Permission == null)
            {
                returnMsg = "Please select the user's permission level.";
                return false;
            }
            else
            {
                User liUser = RetrieveUser(loggedInUsr);
                returnMsg = string.Empty;
                usr.LastModifiedBy = liUser.Logon;
                usr.LastModifiedDate = DateTime.Now;
                if (MethodClasses.UserHandler.Update(usr))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int AddUser(User usr, int loggedInUsr, out string returnMsg)
        {
            //Make sure the new user does not have a duplicate name
            if (MethodClasses.UserHandler.IsDuplicate(usr))
            {
                returnMsg = "There is already a user in the system with this logon (Note: the user may be set to inactive - click on Show Inactive to view)." +
                    "Unable to perform the update.";
                return 0;
            }
            else if (usr.Logon == null)
            {
                returnMsg = "Please enter a logon name";
                return 0;
            }
            else if (usr.FullName == null)
            {
                returnMsg = "Please enter the user's full name.";
                return 0;
            }
            else if (usr.Permission == null)
            {
                returnMsg = "Please select the user's permission level.";
                return 0;
            }
            else
            {
                returnMsg = string.Empty;
                User liUser = RetrieveUser(loggedInUsr);
                usr.LastModifiedBy = liUser.Logon;
                usr.CreatedBy = liUser.Logon;
                usr.LastModifiedDate = DateTime.Now;
                usr.CreatedDate = usr.LastModifiedDate;
                return MethodClasses.UserHandler.Add(usr);
            }
        }

        public static User RetrieveUser(int userId)
        {
            return MethodClasses.UserHandler.GetUser(userId);
        }

        public static User RetrieveUserFromLogon(string logon, out string returnMsg)
        {
            User thisUser = MethodClasses.UserHandler.GetUserFromLogon(logon);
            if (thisUser == null)
            {
                returnMsg = "Username entered has not been found in the system. Please try again or contact your supervisor";
            }
            else
            {
                returnMsg = string.Empty;
            }
            return thisUser;
        }

        public static Globals.Permission GetPermission(int userId)
        {
            User thisUser = MethodClasses.UserHandler.GetUser(userId);
            return (Globals.Permission)thisUser.Permission;
        }
    }
}
