﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using DAL;
using MethodClasses;
using System.Windows.Forms;

namespace BLL
{
    public static class ExportErrorLogic
    {
        public static BindingSource RetrieveExportErrorList()
        {
            return ExportErrorHandler.GetExportErrorList();
        }

        public static bool DeleteExportError(ExportError ee)
        {
            return ExportErrorHandler.Delete(ee);
        }

        public static ExportError RetrieveExportError(int Id)
        {
            return ExportErrorHandler.GetExportErrorById(Id);
        }

        public static void AddExportError(ExportError ee)
        {
            ExportErrorHandler.Add(ee);
        }
    }
}
