//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RFP
{
    using System;
    using System.Collections.Generic;
    
    public partial class TabletTransaction
    {
        public int Id { get; set; }
        public Nullable<int> StockLevelId { get; set; }
        public Nullable<int> Batch { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> Location { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> Quality { get; set; }
        public Nullable<int> Action { get; set; }
        public string ActionedBy { get; set; }
        public string Reason { get; set; }
        public Nullable<int> PrevQuantity { get; set; }
        public Nullable<int> PrevQuality { get; set; }
        public Nullable<int> MovedOutId { get; set; }
        public Nullable<int> PrevShippableWk { get; set; }
        public Nullable<int> PrevShippableYr { get; set; }
        public Nullable<int> ReplenDetailId { get; set; }
    
        public virtual TabletLocation TabletLocation { get; set; }
        public virtual TabletBatch TabletBatch { get; set; }
        public virtual TabletStockLevel TabletStockLevel { get; set; }
    }
}
