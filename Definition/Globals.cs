﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Security.Cryptography;
using Microsoft.Win32;
using System.Windows.Forms;

namespace RFP
{
    [Serializable] 
    public enum SystemEnvironment { Development = 1, Test, Live }
    [Serializable]
    public enum ConfigGroup { QueryType = 1, PickFaceLocation }

    public enum TransactionStatus { Unchanged, Changed, New, Deleted }

    public static class Globals
    {
        public enum LogType {Application, System}

        public enum Entities { Batch, BatchHistory, Component, ConfigurationItem, Location, Retailer, RetailerContact, StockLevel, User }

        public enum Permission { Read_Only, Full }

        public enum Action { Added, Disposed, MovedIn, MovedOut, Picked, QualityChange, QuantityChange, Other }

        public enum CropQuality { GrowingOn = 0, Shippable, Dispose, MoveOn, Pick }

        public enum ReplenishListStatus { New, Applied, PartPicked, Picked }

        public enum FTPProtocol { FTP, SFTP }

        public const string gSalt = "TSG3NCRYPTI0N";

        #region ExcelConstants

            public const int xlAutomatic = -4105;
            public const int xlCenter = -4108;
            public const int xlNone = -4142;
            public const int xlContinuous = 1;
            public const int xlThin = 2;
            public const int xlDiagonalDown = 5; 
            public const int xlDiagonalUp = 6;
            public const int xlEdgeLeft = 7;
            public const int xlEdgeTop = 8;
            public const int xlEdgeBottom = 9;
            public const int xlEdgeRight = 10;
            public const int xlInsideVertical = 11;
            public const int xlInsideHorizontal = 12;

        #endregion

        public static BindingSource FTPProtocolList()
        {
            Dictionary<int, string> protocols = new Dictionary<int, string>();
            protocols.Add(-1, string.Empty);
            foreach (FTPProtocol ftp in Enum.GetValues(typeof(FTPProtocol)))
            {
                protocols.Add((int)ftp, ftp.ToString());
            }
            return new BindingSource(protocols, null);
        }

        public static BindingSource CropQualityList()
        {
            Dictionary<int,string> qualities = new Dictionary<int,string>();
            qualities.Add(-1, string.Empty);
            foreach (CropQuality cq in Enum.GetValues(typeof(CropQuality)))
            {
                qualities.Add((int)cq, CropQualityScreenDesc(cq));
            }
            return new BindingSource(qualities, null);
        }

        public static String CropQualityScreenDesc(CropQuality cq)
        {
            switch (cq)
            {
                case CropQuality.GrowingOn:
                    return "Growing On";
                case CropQuality.MoveOn:
                    return "Move On";
                default:
                    return cq.ToString();
            }
        }

        public static String ConfigGroupDesc(ConfigGroup cg)
        {
            switch (cg)
            {
                case ConfigGroup.PickFaceLocation:
                    return "Pick Face Location";
                case ConfigGroup.QueryType:
                    return "Query Type";
                default:
                    return string.Empty;
            }
        }

        public static String ReplenListStatusScreenDesc(ReplenishListStatus s)
        {
            switch (s)
            {
                case ReplenishListStatus.PartPicked:
                    return "Part Picked";
                default:
                    return s.ToString();
            }
        }
               
        public static String ActionScreenDesc(Action act)
        {
            switch (act)
            {
                case Action.MovedIn:
                    return "Moved";
                case Action.MovedOut:
                    return "Moved";
                case Action.QualityChange:
                    return "Quality Change";
                case Action.QuantityChange:
                    return "Quantity Change";
                default:
                    return act.ToString();
            }
        }

        /// <summary>
        /// Equivalent of VB.Net IsNumeric function
        /// </summary>
        /// <param name="expression">Expression to test</param>
        /// <returns>Returns true if expression is a string that can be converted to a numeric</returns>
        public static bool IsNumeric(object expression)
        {
            bool isNum;
            double retNum;
            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        /// <summary>
        /// Return password having had MD5 algorithm applied to it
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string HashData(string inputString)
        {
            // Convert string to a byte array
            byte[] dataToHash = (new UnicodeEncoding()).GetBytes(inputString);
            // Compute the MD5 hash algorithm
            byte[] hashValue = new MD5CryptoServiceProvider().ComputeHash(dataToHash);

            return BitConverter.ToString(hashValue);
        }

        #region "Registry-Main Write and Read"

        static string strRegParentUser = "Tandem";
        static string strRegParentMachine = "wdqghp";
        static string strRegParent = "";

        public static void WriteRegistry(string valueName, string Value, bool User)
        {
            try
            {
                RegistryKey oReg;

                if (User)
                {
                    strRegParent = strRegParentUser;
                    oReg = Registry.CurrentUser.OpenSubKey("Software", true);
                }
                else
                {
                    strRegParent = strRegParentMachine;
                    oReg = Registry.LocalMachine.OpenSubKey("Software", true);
                }
                oReg = oReg.CreateSubKey(strRegParent);
                //oReg = Registry.CurrentUser.OpenSubKey("Software\\" & strRegParent, True)
                oReg = oReg.CreateSubKey(Application.ProductName);
                oReg.SetValue(valueName, Value);
            }
            catch (Exception)
            {
            }
        }

        public static string ReadRegistry(string  valueName, bool User)
        {
            try
            {
                RegistryKey oReg;
                string strValue;
                if (User)
                {
                    strRegParent = strRegParentUser;
                    oReg = Registry.CurrentUser.OpenSubKey("Software\\" + strRegParent + "\\" + Application.ProductName, false);
                }
                else
                {
                    strRegParent = strRegParentMachine;
                    oReg = Registry.LocalMachine.OpenSubKey("Software\\" + strRegParent + "\\" + Application.ProductName, false);
                }
                strValue = oReg.GetValue(valueName, "").ToString();
                oReg.Close();
                return strValue;
            }
            catch (Exception) 
            {
                return string.Empty;
            }
        }

        #endregion
    }
}
