﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RFP
{
    public class TabletStockLevelRow
    {
        public int StockLevelId { get; set; }
        public string ComponentDesc { get; set; }
        public int BatchNo { get; set; }
        public int Quantity { get; set; }
        public int Quality { get; set; }
        public string QualityDesc { get; set; }
        public int? ShippableWeek { get; set; }
        public int? ShippableYear { get; set; }
        public bool Query { get; set; }
        public int? QueryType { get; set; }
        public string FileName { get; set; }

        public TabletStockLevelRow()
        {
        }

        public TabletStockLevelRow(int id, string compDesc, int batchNo, int quantity, int quality,
            int? shippableWeek, int? shippableYear, bool query, int? queryType, string fileName)
        {
            StockLevelId = id;
            ComponentDesc = compDesc;
            BatchNo = batchNo;
            Quantity = quantity;
            Quality = quality;
            QualityDesc = Globals.CropQualityScreenDesc((Globals.CropQuality)quality);
            ShippableWeek = shippableWeek;
            ShippableYear = shippableYear;
            Query = query;
            QueryType = queryType;
            if ((fileName ?? string.Empty) != string.Empty)
                FileName = "Replen file: " + fileName;
        }
    }
}
