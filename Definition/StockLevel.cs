//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RFP
{
    using System;
    using System.Collections.Generic;
    
    public partial class StockLevel
    {
        public int Id { get; set; }
        public Nullable<int> Location { get; set; }
        public Nullable<int> Batch { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> Quality { get; set; }
        public Nullable<int> ShippableWeek { get; set; }
        public Nullable<int> ShippableYear { get; set; }
        public bool Query { get; set; }
        public Nullable<int> QueryType { get; set; }
        public Nullable<int> ReplenDetailId { get; set; }
        public bool StockTake { get; set; }
    
        public virtual Batch Batch1 { get; set; }
        public virtual ConfigurationItem ConfigurationItem { get; set; }
        public virtual Location Location1 { get; set; }
        public virtual ReplenishListDetail ReplenishListDetail { get; set; }
    }
}
