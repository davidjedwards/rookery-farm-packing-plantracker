﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Diagnostics;

namespace RFP
{
    [Serializable] 
    public enum SystemEnvironment { Development = 1, Test, Live }
    [Serializable]
    public enum CropQuality { GrowingOn, Shippable, Dispose, MoveOn }
    [Serializable]
    public enum ConfigGroup { QueryType = 1, PickFaceLocation }

    public static class Globals
    {
        public enum LogType {Application, System}

        public static string ConnectionString
        {
            get
            {
                string connString;
                switch (Properties.Settings.Default.SystemEnvironment)
                {
                    case SystemEnvironment.Development:
                        connString = RFP.Properties.Settings.Default.ConnString_Dev;
                        break;
                    case SystemEnvironment.Test:
                        connString = RFP.Properties.Settings.Default.ConnString_Test;
                        break;
                    case SystemEnvironment.Live:
                        connString = RFP.Properties.Settings.Default.ConnString_Live;
                        break;
                    default:
                        connString = RFP.Properties.Settings.Default.ConnString_Dev;
                        break;
                }
                EntityConnectionStringBuilder esb = new EntityConnectionStringBuilder();
                esb.Metadata = "res://*/RookeryFarmPacking.csdl|res://*/RookeryFarmPacking.ssdl|res://*/RookeryFarmPacking.msl";
                esb.Provider = "System.Data.SqlClient";
                esb.ProviderConnectionString = connString;
                return esb.ToString();
            }
        }

       

#region Methods
        /// <summary>
        /// Test that the credentials provided will connect to the datasource
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns>bool</returns>
        //public static bool TestConnection(string connectionString)
        //{
        //    bool result = true;
        //    dbs_RookeryFarmEntities ctx = new dbs_RookeryFarmEntities(connectionString);
        //    if (!ctx.Database.Exists())
        //    {
        //        result = false;
        //    }
            
        //    ctx.Dispose();
            
        //    return result;
        //}

        public static void HandleException(Exception ex1, string location, bool showMessage)
        {
        }

        /// <summary>
        /// Write Event Log
        /// </summary>
        /// <param name="entry">Entry to be displayed in Event Log</param>
        /// <param name="appName">Application Name</param>
        /// <param name="eventType">What type of event you wish to log</param>
        /// <param name="logLocation">Place event in what location</param>
        public static void WriteToEventLog(string entry, string appName, EventLogEntryType eventType, LogType logLocation)
        {
            EventLog eventLog = new EventLog();
            string logName;

            switch (logLocation)
            {
                case LogType.Application:
                    logName = "Application";
                    break;
                case LogType.System:
                    logName = "System";
                    break;
                default:
                    logName = "Application";
                    break;
            }

            try
            {
                if (!EventLog.SourceExists(appName))
                {
                    EventLog.CreateEventSource(appName, logName);
                }
                eventLog.Source = appName;
                eventLog.WriteEntry(entry, eventType);
            }
            catch
            {
            }
        }



 
#endregion
        
    }
}
