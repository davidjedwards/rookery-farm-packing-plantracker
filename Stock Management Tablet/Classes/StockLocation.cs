﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Stock_Management_Tablet.Classes
{
    class StockLocation
    {
        public string Location { get; set; }
        public string Component { get; set; }
        public int BatchNo { get; set; }
        public int Quantity { get; set; }
        public int Quality { get; set; }
        public bool Query { get; set; }
        public string QueryType { get; set; }
        public int QuantityToProcess { get; set; }
        public int ShippableYear { get; set; }
        public int ShippableWeek { get; set; }

        public StockLocation(string _location, string _component, int _batchNo, int _quantity, int _quality, bool _query, string _queryType, int _quantityToProcess, 
            int _shippableYear, int _shippableWeek)
        {
            Location = _location;
            Component = _component;
            BatchNo = _batchNo;
            Quantity = _quantity;
            Quality = _quality;
            Query = _query;
            QueryType = _queryType;
            QuantityToProcess = _quantityToProcess;
            ShippableYear = _shippableYear;
            ShippableWeek = _shippableWeek;
        }

    }
}
