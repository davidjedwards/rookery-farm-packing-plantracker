﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Diagnostics;

namespace Stock_Management_Tablet.Classes
{
    public static class Functions
    {
        public static System.Diagnostics.Process virtualKeyboard;

        public static void FormatButton(Button pBtn)
        {
            pBtn.FlatAppearance.BorderColor = Properties.Settings.Default.CompanyColour;
            pBtn.BackColor = Color.White;
            pBtn.FlatAppearance.BorderSize = 2;
            pBtn.ForeColor = Properties.Settings.Default.MenuTextColour;
        }

        public static void StyleDataGridView(DataGridView dg)
        {
            dg.EnableHeadersVisualStyles = false;
            dg.RowsDefaultCellStyle.BackColor = Properties.Settings.Default.DGRow1Colour;
            dg.RowsDefaultCellStyle.SelectionBackColor = Properties.Settings.Default.DGRow1Colour;
            dg.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dg.AlternatingRowsDefaultCellStyle.SelectionBackColor = Properties.Settings.Default.DGRow2Colour;
            dg.AlternatingRowsDefaultCellStyle.BackColor = Properties.Settings.Default.DGRow2Colour;
            dg.AlternatingRowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dg.AutoGenerateColumns = false;
            dg.ColumnHeadersDefaultCellStyle.BackColor = Properties.Settings.Default.FieldBackground;
            dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            dg.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dg.ColumnHeadersDefaultCellStyle.Font = Properties.Settings.Default.FieldFont;
            dg.RowHeadersDefaultCellStyle.BackColor = Properties.Settings.Default.FieldBackground;
            dg.RowHeadersDefaultCellStyle.ForeColor = Color.Black;
            dg.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dg.BackgroundColor = Properties.Settings.Default.FormBackground;
            dg.MultiSelect = false;
        }


        public static void OpenOnScreenKeyboard()
        {
            if (virtualKeyboard == null)
            {
                virtualKeyboard = new System.Diagnostics.Process();
            }
            string windir = Environment.GetEnvironmentVariable("WINDIR");
            string osk = null;

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "sysnative"), "osk.exe");
                if (!File.Exists(osk))
                    osk = null;
            }

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "system32"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
                osk = "osk.exe";

            virtualKeyboard = Process.Start(osk);
        }

        public static void CloseOnScreenKeyboard()
        {
            if (virtualKeyboard != null)
            {
                virtualKeyboard.Kill();
            }
        }

        public static string GetRunningVersion()
        {
            string version = Application.ProductVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                version = ad.CurrentVersion.ToString();
            }
            return version;
        }
    }
 }
