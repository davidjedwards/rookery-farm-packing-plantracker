﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using RFP;
using Tablet_BLL;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmMainMdi : Form
    {
        frmMainForm frmMain;

        public bool DownloadPerformedThisSession = false;

        public frmMainMdi()
        {
            InitializeComponent();
        }

        private void frmMainMdi_Load(object sender, EventArgs e)
        {
            frmMain = new frmMainForm();
            frmMain.MdiParent = this;
            frmMain.Show();
        }

        private void frmMainMdi_Shown(object sender, EventArgs e)
        {
            frmLogon frmL = new frmLogon();
            frmL.ShowDialog();
            if (frmL.DialogResult == System.Windows.Forms.DialogResult.Cancel)
            {
                frmMain.Hide();
                Application.Exit();
            }
            frmL.Dispose();
            if (TransactionLogic.AnyTransactionsToUpload())
            {
                if (MessageBox.Show("There are outstanding transactions that have not yet been uploaded to the main database. Do you want to upload these now?",
                        Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    bool IsNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
                    if (!IsNetworkAvailable)
                    {
                        MessageBox.Show("You are not currently connected to the wireless network. Please move into range and use the Upload Data option",
                            Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        frmMain.DoUpdate();
                    }
                }
            }
        }
    }
}
