﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Stock_Management_Tablet.Classes;
using System.Net.NetworkInformation;
using System.Threading;
using Tablet_BLL;
using RFP;  

namespace Stock_Management_Tablet.Forms
{
    public partial class frmMainForm : Form
    {
        string CommProcess; //"Upload" or "Download"

       
        #region Network Status

                delegate void SetEnabledCallback(bool visibility);

                private void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
                {
                    SetEnabled(NetworkInterface.GetIsNetworkAvailable());
                }

                private void SetEnabled(bool enable)
                {
                    // InvokeRequired required compares the thread ID of the 
                    // calling thread to the thread ID of the creating thread. 
                    // If these threads are different, it returns true. 
                    if (this.btnDownload.InvokeRequired || this.btnUpdate.InvokeRequired)
                    {
                        SetEnabledCallback d = new SetEnabledCallback(SetEnabled);
                        this.Invoke(d, new object[] { enable });
                    }
                    else
                    {
                        this.btnDownload.Enabled = enable;
                        this.btnUpdate.Enabled = enable;
                    }
                    if (enable)
                    {
                        //Upload any user password changes if any took place while the tablet was out of range
                        GlobalLogic.UploadUserPWChange();
                    }
                }
    
        #endregion       

        public frmMainForm()
        {
            InitializeComponent();

            InitializeBackgroundWorker();
        }

        private void InitializeBackgroundWorker()
        {
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
        }

        private void frmMainForm_Load(object sender, EventArgs e)
        {
            // Set up the network status event
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(NetworkChange_NetworkAvailabilityChanged);
            bool IsNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
            SetEnabled(IsNetworkAvailable);

            if (IsNetworkAvailable && Properties.Settings.Default.Environment > 1)
            {
                //Refresh User Information
                TabletUserLogic.RefreshUserData();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private bool CheckDownloadDone()
        {
            // If the last thing done on the tablet was an upload then force the user to do a download before doing anything else
            if (Tablet_BLL.TabletFactorLogic.UploadHasBeenDone())
            {
                MessageBox.Show("A download has not been done since the last upload. Please perform a download before continuing",
                        Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
            {
                frmMainMdi frm = (frmMainMdi)this.MdiParent;
                if (!frm.DownloadPerformedThisSession)
                {
                    if (MessageBox.Show("You have not downloaded data from the main database so far this session. Are you sure you want to continue?",
                            Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        return true;
                    else
                        return false;
                }
                else
                    return true;
            }
        }

        private void btnCropWalk_Click(object sender, EventArgs e)
        {
            if (CheckDownloadDone())
            {
                if (TabletStockLevelLogic.AnyLinesToProcess())
                {
                    frmManageLocation frm = new frmManageLocation();
                    frm.MdiParent = this.MdiParent;
                    frm.Show();
                }
                else
                    MessageBox.Show("No stock lines exist on the tablet. Have you done a download of data?", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            //The transaction table is cleared following a successful upload. Therefore, if there are any records in the
            //transaction table then these should be uploaded before allowing a download of data.
            if (TransactionLogic.AnyTransactionsToUpload())
            {
                if (MessageBox.Show("There are transactions on this system that have not yet been uploaded. If you continue these transactions will be lost. Do you want to continue?",
                    Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
            }
            DownloadProgress.Visible = true;
            if (SystemLogic.ClearTables())
            {
                CommProcess = "Download";
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Tablet_BLL.TabletFactorLogic.UploadHasBeenDone())
            {
                MessageBox.Show("Upload has already been done on this machine for the latest changes.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                DoUpdate();
            }
        }

        public void DoUpdate()
        {
            UploadProgress.Visible = true;
            CommProcess = "Upload";
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message,Properties.Settings.Default.SystemName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (e.Result != null && (bool)e.Result == true)
                    MessageBox.Show(CommProcess + " complete", Properties.Settings.Default.SystemName,MessageBoxButtons.OK,MessageBoxIcon.Information);
                else
                    MessageBox.Show(CommProcess + " failed. Please try again or contact support.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                switch (CommProcess)
                {
                    case "Upload":
                        this.UploadProgress.Visible = false;
                        Tablet_BLL.TabletFactorLogic.SetUploadDone();
                        break;
                    case "Download":
                        this.DownloadProgress.Visible = false;
                        frmMainMdi frm = (frmMainMdi)this.MdiParent;
                        frm.DownloadPerformedThisSession = true;
                        Tablet_BLL.TabletFactorLogic.ResetUploadDone();
                        break;
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (CommProcess)
            {
                case "Upload":
                    this.UploadProgress.Value = e.ProgressPercentage;
                    break;
                case "Download":
                    this.DownloadProgress.Value = e.ProgressPercentage;
                    break;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            switch (CommProcess)
            {
                case "Upload":
                    if (GlobalLogic.UploadToDesktop(worker))
                        e.Result = true;
                    else
                        e.Result = false;
                    break;
                case "Download":
                    if (GlobalLogic.DownloadToTablet(worker))
                        e.Result = true;
                    else
                        e.Result = false;
                    break;
            }
        }

        private void btnStockCheck_Click(object sender, EventArgs e)
        {
            if (CheckDownloadDone())
            {
                if (TabletStockLevelLogic.AnyStockCheckLinesToProcess())
                {
                    frmManageLocation frm = new frmManageLocation();
                    frm.StockCheck = true;
                    frm.MdiParent = this.MdiParent;
                    frm.Show();
                }
                else
                    MessageBox.Show("No stock lines set up for stock check", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDisposals_Click(object sender, EventArgs e)
        {
            if (CheckDownloadDone())
            {
                if (TabletStockLevelLogic.AnyStockLinesToProcess(Globals.CropQuality.Dispose))
                {
                    frmProcess frm = new frmProcess();
                    frm.Quality = Globals.CropQuality.Dispose;
                    frm.MdiParent = this.MdiParent;
                    frm.Show();
                }
                else
                    MessageBox.Show("No stock lines set up for disposal", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnMoves_Click(object sender, EventArgs e)
        {
            if (CheckDownloadDone())
            {
                if (TabletStockLevelLogic.AnyStockLinesToProcess(Globals.CropQuality.MoveOn))
                {
                    frmProcess frm = new frmProcess();
                    frm.Quality = Globals.CropQuality.MoveOn;
                    frm.MdiParent = this.MdiParent;
                    frm.Show();
                }
                else
                    MessageBox.Show("No stock lines set up for moving on", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnPicking_Click(object sender, EventArgs e)
        {
            if (CheckDownloadDone())
            {
                if (TabletStockLevelLogic.AnyStockLinesToProcess(Globals.CropQuality.Pick))
                {
                    frmProcess frm = new frmProcess();
                    frm.Quality = Globals.CropQuality.Pick;
                    frm.MdiParent = this.MdiParent;
                    frm.Show();
                }
                else
                    MessageBox.Show("No stock lines set up for picking", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAddBatch_Click(object sender, EventArgs e)
        {
            if (CheckDownloadDone())
            {
                frmBatchAdd frm = new frmBatchAdd();
                frm.Show();
            }
        }


    }
}
