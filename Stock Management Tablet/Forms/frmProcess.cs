﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using Tablet_BLL;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmProcess : Stock_Management_Tablet.Forms.frmBase
    {
        LinkedList<TabletLocation> locList;
        LinkedListNode<TabletLocation> locNode;

        private Globals.CropQuality _quality;
        public Globals.CropQuality Quality
        {
            get { return _quality; }
            set { _quality = value; }
        }

        public frmProcess()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmProcess_Load(object sender, EventArgs e)
        {

            locList = TabletLocationLogic.GetLocationsInOrderForProcess(Quality);
            locNode = locList.First;
            btnPrev.Enabled = false;
            if (locNode.Next == null)
                btnNext.Enabled = false;

            lblComponent.DataBindings.Add(new Binding("Text", drMain.DataSource, "ComponentDesc"));
            lblBatch.DataBindings.Add(new Binding("Text", drMain.DataSource, "BatchNo"));
            lblQuantity.DataBindings.Add(new Binding("Text", drMain.DataSource, "Quantity"));
            lblReplenishFileName.DataBindings.Add(new Binding("Text", drMain.DataSource, "FileName"));
            LoadDataRepeater();
            switch (Quality)
            {
                case Globals.CropQuality.Dispose:
                    lblProcess.Text = "Disposals";
                    break;
                case Globals.CropQuality.MoveOn:
                    lblProcess.Text = "Moves";
                    break;
                case Globals.CropQuality.Pick:
                    lblProcess.Text = "Picking";
                    break;
            }
        }

        private void LoadDataRepeater()
        {
            loading = true;
            TabletLocation thisLoc = locNode.Value;
            lblLocation.Text = thisLoc.Code;
            drMain.DataSource = TabletStockLevelLogic.RetrieveStockLevelsForLocationForProcess(thisLoc.Id, Quality);
            btnPrev.Enabled = (locNode.Previous != null);
            btnNext.Enabled = (locNode.Next != null);
            loading = false;
        }

        private void drMain_DrawItem(object sender, Microsoft.VisualBasic.PowerPacks.DataRepeaterItemEventArgs e)
        {
            if (e.DataRepeaterItem.ItemIndex % 2 == 0)
            {
                e.DataRepeaterItem.BackColor = Properties.Settings.Default.DGRow1Colour;
            }
            else
            {
                e.DataRepeaterItem.BackColor = Properties.Settings.Default.DGRow2Colour;
            }
            if (Quality != Globals.CropQuality.Pick)
            {
                Label lbl1 = (Label)e.DataRepeaterItem.Controls.Find("lblReplenishFile", false)[0];
                Label lbl2 = (Label)e.DataRepeaterItem.Controls.Find("lblReplenishFileName", false)[0];
                lbl1.Visible = false;
                lbl2.Visible = false;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            locNode = locNode.Next;
            LoadDataRepeater();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            locNode = locNode.Previous;
            LoadDataRepeater();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)btn.Parent;
            Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)btn.Parent.Parent;
            TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
            TabletStockLevel tsl = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
            Label CurrQuantity = (Label)drMain.CurrentItem.Controls["lblQuantity"];
            Label ComponentDesc = (Label)drMain.CurrentItem.Controls["lblComponent"];

            if (Quality == Globals.CropQuality.Dispose || Quality == Globals.CropQuality.Pick)
            {
                Globals.Action act;
                if (Quality == Globals.CropQuality.Dispose)
                    act = Globals.Action.Disposed;
                else
                    act = Globals.Action.Picked;

                if (!TabletStockLevelLogic.PerformPickOrDispose(tsl, act, Properties.Settings.Default.UserId, int.Parse(CurrQuantity.Text)))
                {
                    MessageBox.Show("There was a system error trying to perform this action. Please try again and if it continues to happen contact support",
                        Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    LoadDataRepeater();
            }
            else //moving
            {
                frmChooseLocation frm = new frmChooseLocation();
                frm.info = string.Format("Move {0} of component {1} from location {2}", CurrQuantity.Text, ComponentDesc.Text, lblLocation.Text);
                frm.ShowDialog();
                if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    int newLocation = frm.newLocation;
                    if (newLocation != 0)
                    {
                        //Now prompt for quality
                        frmSelectQuality frmSQ = new frmSelectQuality();
                        frmSQ.ShowDialog();
                        int newQuality = frmSQ.NewQuality;
                        frmSQ.Dispose();

                        if (!TabletStockLevelLogic.Move(tsl, newLocation, newQuality, Properties.Settings.Default.UserId))
                        {
                            MessageBox.Show("There was a system error trying to perform this action. Please try again and if it continues to happen contact support",
                                Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                            LoadDataRepeater();
                    }
                }
                frm.Dispose();
            }
        }

    }
}
