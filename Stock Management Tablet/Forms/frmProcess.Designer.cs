﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.drMain = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
            this.lblReplenishFileName = new System.Windows.Forms.Label();
            this.lblReplenishFile = new System.Windows.Forms.Label();
            this.lblBatch = new System.Windows.Forms.Label();
            this.lblComponent = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.btnDone = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblProcess = new System.Windows.Forms.Label();
            this.drMain.ItemTemplate.SuspendLayout();
            this.drMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Stock_Management_Tablet.Properties.Settings.Default.TitleFont;
            this.label1.ForeColor = global::Stock_Management_Tablet.Properties.Settings.Default.MenuTextColour;
            this.label1.Location = new System.Drawing.Point(515, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Location:";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Font = global::Stock_Management_Tablet.Properties.Settings.Default.TitleFont;
            this.lblLocation.ForeColor = System.Drawing.Color.Black;
            this.lblLocation.Location = new System.Drawing.Point(661, 9);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(0, 35);
            this.lblLocation.TabIndex = 1;
            // 
            // drMain
            // 
            this.drMain.AllowUserToAddItems = false;
            this.drMain.AllowUserToDeleteItems = false;
            // 
            // drMain.ItemTemplate
            // 
            this.drMain.ItemTemplate.Controls.Add(this.lblReplenishFileName);
            this.drMain.ItemTemplate.Controls.Add(this.lblReplenishFile);
            this.drMain.ItemTemplate.Controls.Add(this.lblBatch);
            this.drMain.ItemTemplate.Controls.Add(this.lblComponent);
            this.drMain.ItemTemplate.Controls.Add(this.lblQuantity);
            this.drMain.ItemTemplate.Controls.Add(this.btnDone);
            this.drMain.ItemTemplate.Controls.Add(this.label4);
            this.drMain.ItemTemplate.Controls.Add(this.label9);
            this.drMain.ItemTemplate.Controls.Add(this.label2);
            this.drMain.ItemTemplate.Size = new System.Drawing.Size(1331, 70);
            this.drMain.Location = new System.Drawing.Point(8, 54);
            this.drMain.Name = "drMain";
            this.drMain.Size = new System.Drawing.Size(1339, 584);
            this.drMain.TabIndex = 4;
            this.drMain.Text = "dataRepeater1";
            this.drMain.DrawItem += new Microsoft.VisualBasic.PowerPacks.DataRepeaterItemEventHandler(this.drMain_DrawItem);
            // 
            // lblReplenishFileName
            // 
            this.lblReplenishFileName.Location = new System.Drawing.Point(786, 34);
            this.lblReplenishFileName.Name = "lblReplenishFileName";
            this.lblReplenishFileName.Size = new System.Drawing.Size(372, 23);
            this.lblReplenishFileName.TabIndex = 35;
            this.lblReplenishFileName.Text = "label8";
            // 
            // lblReplenishFile
            // 
            this.lblReplenishFile.AutoSize = true;
            this.lblReplenishFile.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblReplenishFile.ForeColor = System.Drawing.Color.Black;
            this.lblReplenishFile.Location = new System.Drawing.Point(785, 0);
            this.lblReplenishFile.Name = "lblReplenishFile";
            this.lblReplenishFile.Size = new System.Drawing.Size(141, 27);
            this.lblReplenishFile.TabIndex = 34;
            this.lblReplenishFile.Text = "Replenish File";
            // 
            // lblBatch
            // 
            this.lblBatch.AutoSize = true;
            this.lblBatch.Location = new System.Drawing.Point(560, 34);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(59, 23);
            this.lblBatch.TabIndex = 25;
            this.lblBatch.Text = "label8";
            // 
            // lblComponent
            // 
            this.lblComponent.AutoSize = true;
            this.lblComponent.Location = new System.Drawing.Point(4, 34);
            this.lblComponent.Name = "lblComponent";
            this.lblComponent.Size = new System.Drawing.Size(59, 23);
            this.lblComponent.TabIndex = 27;
            this.lblComponent.Text = "label8";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(684, 34);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(59, 23);
            this.lblQuantity.TabIndex = 33;
            this.lblQuantity.Text = "label8";
            // 
            // btnDone
            // 
            this.btnDone.FlatAppearance.BorderSize = 0;
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDone.Image = global::Stock_Management_Tablet.Properties.Resources.DoneButton;
            this.btnDone.Location = new System.Drawing.Point(1164, 16);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(121, 41);
            this.btnDone.TabIndex = 21;
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(677, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 27);
            this.label4.TabIndex = 10;
            this.label4.Text = "Quantity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(557, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 27);
            this.label9.TabIndex = 8;
            this.label9.Text = "Batch";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Component";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::Stock_Management_Tablet.Properties.Resources.FormExitButton;
            this.btnExit.Location = new System.Drawing.Point(1223, 644);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(118, 54);
            this.btnExit.TabIndex = 3;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.FlatAppearance.BorderSize = 0;
            this.btnPrev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrev.Image = global::Stock_Management_Tablet.Properties.Resources.PreviousButton;
            this.btnPrev.Location = new System.Drawing.Point(15, 649);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(157, 49);
            this.btnPrev.TabIndex = 22;
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnNext
            // 
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Image = global::Stock_Management_Tablet.Properties.Resources.NextButton;
            this.btnNext.Location = new System.Drawing.Point(187, 649);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(157, 49);
            this.btnNext.TabIndex = 23;
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblProcess
            // 
            this.lblProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProcess.AutoSize = true;
            this.lblProcess.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblProcess.ForeColor = System.Drawing.Color.Black;
            this.lblProcess.Location = new System.Drawing.Point(1205, 14);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(83, 27);
            this.lblProcess.TabIndex = 24;
            this.lblProcess.Text = "Process";
            // 
            // frmProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FormBackground;
            this.ClientSize = new System.Drawing.Size(1353, 708);
            this.Controls.Add(this.lblProcess);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.drMain);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.label1);
            this.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FormFont;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmProcess";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.frmProcess_Load);
            this.drMain.ItemTemplate.ResumeLayout(false);
            this.drMain.ItemTemplate.PerformLayout();
            this.drMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Button btnExit;
        private Microsoft.VisualBasic.PowerPacks.DataRepeater drMain;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.Label lblComponent;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Label lblReplenishFileName;
        private System.Windows.Forms.Label lblReplenishFile;
    }
}