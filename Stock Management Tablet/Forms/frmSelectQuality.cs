﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tablet_BLL;
using RFP;

namespace Stock_Management_Tablet.Forms
{

    public partial class frmSelectQuality : Form
    {
        public int NewQuality { get; set; }

        public frmSelectQuality()
        {
            InitializeComponent();
        }

        private void btnGrowingOn_Click(object sender, EventArgs e)
        {
            NewQuality = (int)Globals.CropQuality.GrowingOn;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnShippable_Click(object sender, EventArgs e)
        {
            NewQuality = (int)Globals.CropQuality.Shippable;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
