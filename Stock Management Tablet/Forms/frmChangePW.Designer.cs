﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmChangePW
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangePW));
            this.lblOldPW = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtConfPW = new System.Windows.Forms.TextBox();
            this.txtNewPW = new System.Windows.Forms.TextBox();
            this.txtOldPW = new System.Windows.Forms.TextBox();
            this.imgCrossConfPW = new System.Windows.Forms.PictureBox();
            this.imgTickConfPW = new System.Windows.Forms.PictureBox();
            this.imgCrossOldPW = new System.Windows.Forms.PictureBox();
            this.imgTickOldPW = new System.Windows.Forms.PictureBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgCrossConfPW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTickConfPW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCrossOldPW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTickOldPW)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOldPW
            // 
            this.lblOldPW.AutoSize = true;
            this.lblOldPW.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblOldPW.Location = new System.Drawing.Point(68, 21);
            this.lblOldPW.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblOldPW.Name = "lblOldPW";
            this.lblOldPW.Size = new System.Drawing.Size(147, 27);
            this.lblOldPW.TabIndex = 20;
            this.lblOldPW.Text = "Old Password:";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.Label4.Location = new System.Drawing.Point(60, 64);
            this.Label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(155, 27);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "New Password:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.Label3.Location = new System.Drawing.Point(26, 104);
            this.Label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(189, 27);
            this.Label3.TabIndex = 18;
            this.Label3.Text = "Confirm Password:";
            // 
            // txtConfPW
            // 
            this.txtConfPW.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtConfPW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfPW.Enabled = false;
            this.txtConfPW.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtConfPW.Location = new System.Drawing.Point(228, 101);
            this.txtConfPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtConfPW.Name = "txtConfPW";
            this.txtConfPW.PasswordChar = '*';
            this.txtConfPW.Size = new System.Drawing.Size(201, 32);
            this.txtConfPW.TabIndex = 17;
            this.txtConfPW.TextChanged += new System.EventHandler(this.txtConfPW_TextChanged);
            // 
            // txtNewPW
            // 
            this.txtNewPW.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtNewPW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewPW.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtNewPW.Location = new System.Drawing.Point(228, 58);
            this.txtNewPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtNewPW.Name = "txtNewPW";
            this.txtNewPW.PasswordChar = '*';
            this.txtNewPW.Size = new System.Drawing.Size(201, 32);
            this.txtNewPW.TabIndex = 16;
            this.txtNewPW.TextChanged += new System.EventHandler(this.txtNewPW_TextChanged);
            // 
            // txtOldPW
            // 
            this.txtOldPW.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtOldPW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOldPW.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtOldPW.Location = new System.Drawing.Point(228, 18);
            this.txtOldPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtOldPW.Name = "txtOldPW";
            this.txtOldPW.PasswordChar = '*';
            this.txtOldPW.Size = new System.Drawing.Size(201, 32);
            this.txtOldPW.TabIndex = 15;
            this.txtOldPW.TextChanged += new System.EventHandler(this.txtOldPW_TextChanged);
            // 
            // imgCrossConfPW
            // 
            this.imgCrossConfPW.Image = global::Stock_Management_Tablet.Properties.Resources.cross;
            this.imgCrossConfPW.Location = new System.Drawing.Point(468, 109);
            this.imgCrossConfPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.imgCrossConfPW.Name = "imgCrossConfPW";
            this.imgCrossConfPW.Size = new System.Drawing.Size(16, 16);
            this.imgCrossConfPW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCrossConfPW.TabIndex = 24;
            this.imgCrossConfPW.TabStop = false;
            this.imgCrossConfPW.Visible = false;
            // 
            // imgTickConfPW
            // 
            this.imgTickConfPW.Image = global::Stock_Management_Tablet.Properties.Resources.tick;
            this.imgTickConfPW.Location = new System.Drawing.Point(468, 109);
            this.imgTickConfPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.imgTickConfPW.Name = "imgTickConfPW";
            this.imgTickConfPW.Size = new System.Drawing.Size(16, 16);
            this.imgTickConfPW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgTickConfPW.TabIndex = 23;
            this.imgTickConfPW.TabStop = false;
            this.imgTickConfPW.Visible = false;
            // 
            // imgCrossOldPW
            // 
            this.imgCrossOldPW.Image = global::Stock_Management_Tablet.Properties.Resources.cross;
            this.imgCrossOldPW.Location = new System.Drawing.Point(468, 26);
            this.imgCrossOldPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.imgCrossOldPW.Name = "imgCrossOldPW";
            this.imgCrossOldPW.Size = new System.Drawing.Size(16, 16);
            this.imgCrossOldPW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCrossOldPW.TabIndex = 22;
            this.imgCrossOldPW.TabStop = false;
            this.imgCrossOldPW.Visible = false;
            // 
            // imgTickOldPW
            // 
            this.imgTickOldPW.Image = global::Stock_Management_Tablet.Properties.Resources.tick;
            this.imgTickOldPW.Location = new System.Drawing.Point(468, 26);
            this.imgTickOldPW.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.imgTickOldPW.Name = "imgTickOldPW";
            this.imgTickOldPW.Size = new System.Drawing.Size(16, 16);
            this.imgTickOldPW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgTickOldPW.TabIndex = 21;
            this.imgTickOldPW.TabStop = false;
            this.imgTickOldPW.Visible = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Image = global::Stock_Management_Tablet.Properties.Resources.SaveButton;
            this.btnOK.Location = new System.Drawing.Point(408, 158);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(121, 40);
            this.btnOK.TabIndex = 26;
            this.btnOK.TabStop = false;
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Tablet.Properties.Resources.CancelButton;
            this.btnCancel.Location = new System.Drawing.Point(204, 158);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 40);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.TabStop = false;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmChangePW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FormBackground;
            this.ClientSize = new System.Drawing.Size(538, 216);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.imgCrossConfPW);
            this.Controls.Add(this.imgTickConfPW);
            this.Controls.Add(this.imgCrossOldPW);
            this.Controls.Add(this.imgTickOldPW);
            this.Controls.Add(this.lblOldPW);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtConfPW);
            this.Controls.Add(this.txtNewPW);
            this.Controls.Add(this.txtOldPW);
            this.DoubleBuffered = true;
            this.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FormFont;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "frmChangePW";
            this.Text = "Change Password";
            this.Load += new System.EventHandler(this.frmChangePW_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCrossConfPW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTickConfPW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCrossOldPW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTickOldPW)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PictureBox imgCrossConfPW;
        internal System.Windows.Forms.PictureBox imgTickConfPW;
        internal System.Windows.Forms.PictureBox imgCrossOldPW;
        internal System.Windows.Forms.PictureBox imgTickOldPW;
        internal System.Windows.Forms.Label lblOldPW;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtConfPW;
        internal System.Windows.Forms.TextBox txtNewPW;
        internal System.Windows.Forms.TextBox txtOldPW;
        public System.Windows.Forms.Button btnOK;
        public System.Windows.Forms.Button btnCancel;
    }
}