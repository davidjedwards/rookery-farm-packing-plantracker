﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using Tablet_BLL;
using Stock_Management_Tablet.Classes;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmLogon : Form
    {
        public frmLogon()
        {
            InitializeComponent();
        }

        private void frmLogon_Load(object sender, EventArgs e)
        {
            //Place version details in lblVersion
            lblVersion.Text = string.Format("Version: {0} ({1})", Functions.GetRunningVersion(), TabletGlobals.gReleaseDate);

            //Set User ID for app back to 0
            Properties.Settings.Default.UserId = 0;

            Functions.FormatButton(btnCancel);

            txtPW.GotFocus += txtPW_GotFocus;
            txtPW.LostFocus += txtPW_LostFocus;
            txtUserName.GotFocus += txtUserName_GotFocus;
            txtUserName.LostFocus += txtUserName_LostFocus;

            //Get last successful username for app
            txtUserName.Text = Globals.ReadRegistry("Username", true);
            if (txtUserName.Text != string.Empty)
            {
                txtUserName.ForeColor = Color.Black;
                txtPW.Focus();
            }
            else
            {
                txtUserName.Text = "username";
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string Password = string.Empty;
            bool PasswordOK = false;
            string msg;

            frmProgress frmP = new frmProgress();
            frmP.lblProgress.Text = "Please wait...";
            frmP.Show();
            frmP.Refresh();

            //Get user details for logon name entered
            TabletUser thisUser = TabletUserLogic.RetrieveUserFromLogon(txtUserName.Text, out msg);

            frmP.Hide();
            frmP.Dispose();

            if (thisUser == null && msg != string.Empty) //no user record found for logon name
            {
                MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else //Check password entered
            {
                Password = thisUser.Password;
                if (Password == null)
                    Password = string.Empty;

                if (((txtPW.Text == string.Empty || txtPW.Text == "password") && Password == string.Empty) || string.Equals(Globals.HashData(txtPW.Text), Password))
                {
                    PasswordOK = true;
                }
                else
                {
                    PasswordOK = false;
                    MessageBox.Show("Password incorrect. Please try again or ask an administrator to reset it.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                if (PasswordOK)
                {
                    Properties.Settings.Default.UserId = thisUser.Id;
                    Globals.WriteRegistry("Username", txtUserName.Text, true);
                    //If password is blank force user to change password
                    if (txtPW.Text == string.Empty || txtPW.Text == "password")
                    {
                        MessageBox.Show("Please create a password", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        frmChangePW frm = new frmChangePW();
                        frm.ForceChange = true;
                        frm.ShowDialog();
                        frm.Dispose();
                    }
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    if (txtPW.Focused)
                        txtPW.Text = string.Empty;
                    else
                    {
                        txtPW.Text = "password";
                        txtPW.ForeColor = Color.Gray;
                        txtPW.PasswordChar = Convert.ToChar(" ");
                    }
                }
            }
        }

        private void txtPW_GotFocus(object sender, System.EventArgs e)
        {
            if (txtPW.Text == "password" || txtPW.Text == string.Empty)
            {
                txtPW.PasswordChar = Convert.ToChar("*");
                txtPW.ForeColor = Color.Black;
                txtPW.Text = string.Empty;
            }
        }

        private void txtPW_LostFocus(object sender, System.EventArgs e)
        {
            if (txtPW.Text == string.Empty)
            {
                txtPW.PasswordChar = Convert.ToChar(" ");
                txtPW.ForeColor = Color.Gray;
                txtPW.Text = "password";
            }
        }

        private void txtPW_MouseClick(object sender, MouseEventArgs e)
        {
            txtPW_Enter(sender, e);
        }

        private void txtPW_Enter(object sender, EventArgs e)
        {
            txtPW.SelectAll();
        }

        private void txtUserName_GotFocus(object sender, System.EventArgs e)
        {
            if (txtUserName.Text == "username")
            {
                txtUserName.ForeColor = Color.Black;
                txtUserName.Text = string.Empty;
            }
        }

        private void txtUserName_LostFocus(object sender, System.EventArgs e)
        {
            if (txtUserName.Text == string.Empty)
            {
                txtUserName.ForeColor = Color.Gray;
                txtUserName.Text = "username";
            }
        }

        private void txtUserName_MouseClick(object sender, MouseEventArgs e)
        {
            txtUserName_Enter(sender, e);
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            txtUserName.SelectAll();
        }
        
       


    }
}
