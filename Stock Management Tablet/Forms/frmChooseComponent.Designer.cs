﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmChooseComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.dgvComponentList = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetailerStockCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnFilter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComponentList)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Component Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label2.Location = new System.Drawing.Point(712, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "Code";
            // 
            // txtDesc
            // 
            this.txtDesc.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesc.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtDesc.Location = new System.Drawing.Point(248, 15);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(432, 32);
            this.txtDesc.TabIndex = 2;
            // 
            // txtCode
            // 
            this.txtCode.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCode.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtCode.Location = new System.Drawing.Point(777, 15);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(293, 32);
            this.txtCode.TabIndex = 3;
            // 
            // dgvComponentList
            // 
            this.dgvComponentList.AllowUserToAddRows = false;
            this.dgvComponentList.AllowUserToDeleteRows = false;
            this.dgvComponentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComponentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.RetailerStockCodeColumn,
            this.DescriptionColumn});
            this.dgvComponentList.Location = new System.Drawing.Point(8, 57);
            this.dgvComponentList.Name = "dgvComponentList";
            this.dgvComponentList.ReadOnly = true;
            this.dgvComponentList.RowHeadersVisible = false;
            this.dgvComponentList.RowTemplate.Height = 35;
            this.dgvComponentList.Size = new System.Drawing.Size(1339, 584);
            this.dgvComponentList.TabIndex = 30;
            this.dgvComponentList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComponentList_CellClick);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // RetailerStockCodeColumn
            // 
            this.RetailerStockCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RetailerStockCodeColumn.DataPropertyName = "RetailerStockCode";
            this.RetailerStockCodeColumn.HeaderText = "Retailer Code";
            this.RetailerStockCodeColumn.MinimumWidth = 150;
            this.RetailerStockCodeColumn.Name = "RetailerStockCodeColumn";
            this.RetailerStockCodeColumn.ReadOnly = true;
            this.RetailerStockCodeColumn.Width = 150;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.DataPropertyName = "Description";
            this.DescriptionColumn.HeaderText = "Component Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.ReadOnly = true;
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Tablet.Properties.Resources.CancelButton;
            this.btnCancel.Location = new System.Drawing.Point(615, 654);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(124, 45);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.FlatAppearance.BorderSize = 0;
            this.btnFilter.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilter.Image = global::Stock_Management_Tablet.Properties.Resources.FilterButton;
            this.btnFilter.Location = new System.Drawing.Point(1089, 11);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(119, 39);
            this.btnFilter.TabIndex = 29;
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // frmChooseComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 708);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.dgvComponentList);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmChooseComponent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.frmChooseComponent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvComponentList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridView dgvComponentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetailerStockCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.Button btnCancel;
    }
}