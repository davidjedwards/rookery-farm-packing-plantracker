﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainForm));
            this.btnStockCheck = new System.Windows.Forms.Button();
            this.UploadProgress = new System.Windows.Forms.ProgressBar();
            this.DownloadProgress = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDisposals = new System.Windows.Forms.Button();
            this.btnAddBatch = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCropWalk = new System.Windows.Forms.Button();
            this.btnPicking = new System.Windows.Forms.Button();
            this.btnMoves = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStockCheck
            // 
            this.btnStockCheck.FlatAppearance.BorderSize = 0;
            this.btnStockCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockCheck.Image = global::Stock_Management_Tablet.Properties.Resources.StockCheckButton;
            this.btnStockCheck.Location = new System.Drawing.Point(419, 354);
            this.btnStockCheck.Name = "btnStockCheck";
            this.btnStockCheck.Size = new System.Drawing.Size(237, 48);
            this.btnStockCheck.TabIndex = 8;
            this.btnStockCheck.UseVisualStyleBackColor = true;
            this.btnStockCheck.Click += new System.EventHandler(this.btnStockCheck_Click);
            // 
            // UploadProgress
            // 
            this.UploadProgress.Location = new System.Drawing.Point(419, 315);
            this.UploadProgress.Name = "UploadProgress";
            this.UploadProgress.Size = new System.Drawing.Size(237, 23);
            this.UploadProgress.TabIndex = 7;
            this.UploadProgress.Visible = false;
            // 
            // DownloadProgress
            // 
            this.DownloadProgress.Location = new System.Drawing.Point(419, 216);
            this.DownloadProgress.Name = "DownloadProgress";
            this.DownloadProgress.Size = new System.Drawing.Size(237, 23);
            this.DownloadProgress.TabIndex = 1;
            this.DownloadProgress.Visible = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            // 
            // btnDownload
            // 
            this.btnDownload.FlatAppearance.BorderSize = 0;
            this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownload.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.Location = new System.Drawing.Point(419, 162);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(237, 48);
            this.btnDownload.TabIndex = 0;
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnExit.Image = global::Stock_Management_Tablet.Properties.Resources.ExitPlantrackerButton1;
            this.btnExit.Location = new System.Drawing.Point(559, 574);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(237, 48);
            this.btnExit.TabIndex = 2;
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDisposals
            // 
            this.btnDisposals.FlatAppearance.BorderSize = 0;
            this.btnDisposals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisposals.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnDisposals.Image = ((System.Drawing.Image)(resources.GetObject("btnDisposals.Image")));
            this.btnDisposals.Location = new System.Drawing.Point(697, 261);
            this.btnDisposals.Name = "btnDisposals";
            this.btnDisposals.Size = new System.Drawing.Size(237, 48);
            this.btnDisposals.TabIndex = 5;
            this.btnDisposals.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDisposals.UseVisualStyleBackColor = true;
            this.btnDisposals.Click += new System.EventHandler(this.btnDisposals_Click);
            // 
            // btnAddBatch
            // 
            this.btnAddBatch.FlatAppearance.BorderSize = 0;
            this.btnAddBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBatch.Image = ((System.Drawing.Image)(resources.GetObject("btnAddBatch.Image")));
            this.btnAddBatch.Location = new System.Drawing.Point(419, 454);
            this.btnAddBatch.Name = "btnAddBatch";
            this.btnAddBatch.Size = new System.Drawing.Size(237, 48);
            this.btnAddBatch.TabIndex = 9;
            this.btnAddBatch.UseVisualStyleBackColor = true;
            this.btnAddBatch.Click += new System.EventHandler(this.btnAddBatch_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(419, 261);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(237, 48);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCropWalk
            // 
            this.btnCropWalk.FlatAppearance.BorderSize = 0;
            this.btnCropWalk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCropWalk.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnCropWalk.Image = ((System.Drawing.Image)(resources.GetObject("btnCropWalk.Image")));
            this.btnCropWalk.Location = new System.Drawing.Point(697, 162);
            this.btnCropWalk.Name = "btnCropWalk";
            this.btnCropWalk.Size = new System.Drawing.Size(237, 48);
            this.btnCropWalk.TabIndex = 6;
            this.btnCropWalk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCropWalk.UseVisualStyleBackColor = true;
            this.btnCropWalk.Click += new System.EventHandler(this.btnCropWalk_Click);
            // 
            // btnPicking
            // 
            this.btnPicking.FlatAppearance.BorderSize = 0;
            this.btnPicking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPicking.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnPicking.Image = ((System.Drawing.Image)(resources.GetObject("btnPicking.Image")));
            this.btnPicking.Location = new System.Drawing.Point(697, 454);
            this.btnPicking.Name = "btnPicking";
            this.btnPicking.Size = new System.Drawing.Size(237, 48);
            this.btnPicking.TabIndex = 3;
            this.btnPicking.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPicking.UseVisualStyleBackColor = true;
            this.btnPicking.Click += new System.EventHandler(this.btnPicking_Click);
            // 
            // btnMoves
            // 
            this.btnMoves.FlatAppearance.BorderSize = 0;
            this.btnMoves.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoves.Font = global::Stock_Management_Tablet.Properties.Settings.Default.MenuFont;
            this.btnMoves.Image = ((System.Drawing.Image)(resources.GetObject("btnMoves.Image")));
            this.btnMoves.Location = new System.Drawing.Point(697, 354);
            this.btnMoves.Name = "btnMoves";
            this.btnMoves.Size = new System.Drawing.Size(237, 48);
            this.btnMoves.TabIndex = 4;
            this.btnMoves.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMoves.UseVisualStyleBackColor = true;
            this.btnMoves.Click += new System.EventHandler(this.btnMoves_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Verdana", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(557, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 42);
            this.label1.TabIndex = 10;
            this.label1.Text = "Plantracker";
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.BackgroundImage = global::Stock_Management_Tablet.Properties.Resources.HomeScreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1353, 708);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddBatch);
            this.Controls.Add(this.btnStockCheck);
            this.Controls.Add(this.UploadProgress);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.DownloadProgress);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnCropWalk);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDisposals);
            this.Controls.Add(this.btnPicking);
            this.Controls.Add(this.btnMoves);
            this.Font = new System.Drawing.Font("Tahoma", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMainForm";
            this.Load += new System.EventHandler(this.frmMainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDisposals;
        private System.Windows.Forms.Button btnMoves;
        private System.Windows.Forms.Button btnPicking;
        private System.Windows.Forms.Button btnCropWalk;
        private System.Windows.Forms.ProgressBar DownloadProgress;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar UploadProgress;
        private System.Windows.Forms.Button btnAddBatch;
        private System.Windows.Forms.Button btnStockCheck;
        private System.Windows.Forms.Label label1;


    }
}