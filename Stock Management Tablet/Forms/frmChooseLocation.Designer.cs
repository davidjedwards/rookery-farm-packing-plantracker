﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmChooseLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInformation = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlLocations = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblInformation.Location = new System.Drawing.Point(13, 13);
            this.lblInformation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(0, 27);
            this.lblInformation.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label1.Location = new System.Drawing.Point(13, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Click on location to move stock to";
            // 
            // pnlLocations
            // 
            this.pnlLocations.AutoScroll = true;
            this.pnlLocations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLocations.Location = new System.Drawing.Point(12, 76);
            this.pnlLocations.Margin = new System.Windows.Forms.Padding(4);
            this.pnlLocations.Name = "pnlLocations";
            this.pnlLocations.Size = new System.Drawing.Size(1339, 562);
            this.pnlLocations.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Tablet.Properties.Resources.CancelButton;
            this.btnCancel.Location = new System.Drawing.Point(598, 647);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(157, 49);
            this.btnCancel.TabIndex = 24;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmChooseLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 708);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlLocations);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblInformation);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "frmChooseLocation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmChooseLocation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInformation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlLocations;
        private System.Windows.Forms.Button btnCancel;
    }
}