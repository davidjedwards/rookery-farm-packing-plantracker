﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using RFP;
using Tablet_BLL;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmBatchAdd : Stock_Management_Tablet.Forms.frmBase
    {
        int componentId;
        int locationId;

        public frmBatchAdd()
        {
            InitializeComponent();
        }

        private void frmBatchAdd_Load(object sender, EventArgs e)
        {
            loading = true;

            //Defaults
            lblShippableYear.Text = DateTime.Today.Year.ToString();
            cboQuality.DataSource = Globals.CropQualityList();
            cboQuality.DisplayMember = "Value";
            cboQuality.ValueMember = "Key";

            //Batch Date
            DataTable months = new DataTable();
            months.Columns.Add("MonthNum", typeof(int));
            months.Columns.Add("MonthName", typeof(string));
            DataRow month;
            for (int i = 1; i <= 12; i++)
            {
                month = months.NewRow();
                month["MonthNum"] = i;
                switch (i)
                {
                    case 1:
                        month["MonthName"] = "January";
                        break;
                    case 2:
                        month["MonthName"] = "February";
                        break;
                    case 3:
                        month["MonthName"] = "March";
                        break;
                    case 4:
                        month["MonthName"] = "April";
                        break;
                    case 5:
                        month["MonthName"] = "May";
                        break;
                    case 6:
                        month["MonthName"] = "June";
                        break;
                    case 7:
                        month["MonthName"] = "July";
                        break;
                    case 8:
                        month["MonthName"] = "August";
                        break;
                    case 9:
                        month["MonthName"] = "September";
                        break;
                    case 10:
                        month["MonthName"] = "October";
                        break;
                    case 11:
                        month["MonthName"] = "November";
                        break;
                    case 12:
                        month["MonthName"] = "December";
                        break;
                    default:
                        break;
                }
                months.Rows.Add(month);
            }
            cboBatchDateMonth.DataSource = months;
            cboBatchDateMonth.ValueMember = "MonthNum";
            cboBatchDateMonth.DisplayMember = "MonthName";
            cboBatchDateMonth.SelectedValue = DateTime.Today.Month;
            lblBatchDateYear.Text = DateTime.Today.Year.ToString();
            txtBatchDateDay.Text = DateTime.Today.Day.ToString();

            loading = false;
        }

        private void ShowNumericKeypad(object sender, EventArgs e)
        {
            frmNumericKeypad frm = new frmNumericKeypad();
            TextBox ThisTextBox = (TextBox)sender;
            int prevValue;
            if (!int.TryParse(ThisTextBox.Text, out prevValue))
                prevValue = 0;

            frm.txtEntry = ThisTextBox;
            int yPos = pnlBatch.Top + ThisTextBox.Top;
            if ((yPos + frm.Height) > (this.Height - 50))
            {
                yPos = yPos - ((yPos + frm.Height) - (this.Height - 50));
            }

            frm.Location = new Point(pnlBatch.Left + ThisTextBox.Left + ThisTextBox.Width + 50, yPos);
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Ensure a component has been selected
            if (lblComponentCode.Text == string.Empty)
            {
                MessageBox.Show( "Please select a component", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //ensure batch number is an integer
            int BatchNo;
            if (!int.TryParse(txtBatchNo.Text, out BatchNo))
            {
                MessageBox.Show("Batch number must be an integer", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //ensure quantity is an integer
            int qty;
            if (!int.TryParse(txtQuantity.Text, out qty))
            {
                MessageBox.Show("Quantity must be an integer", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //ensure location has a non-zero value
            if (locationId == 0)
            {
                MessageBox.Show("Please select a location", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //ensure batch day is valid
            int batchDay;
            if (!int.TryParse(txtBatchDateDay.Text, out batchDay))
            {
                MessageBox.Show("Batch date day must be an integer", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfi.Calendar;
            int NumWeeks = cal.GetWeekOfYear(new DateTime(int.Parse(lblShippableYear.Text), 12, 31), dfi.CalendarWeekRule, dfi.FirstDayOfWeek);

            int ShippableWeek;
            if (txtShippableWeek.Text != string.Empty)
            {
                if (!int.TryParse(txtShippableWeek.Text, out ShippableWeek))
                {
                    MessageBox.Show("Shippable week must be an integer", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else if (ShippableWeek > NumWeeks || ShippableWeek < 0)
                {
                    MessageBox.Show("Shippable week is invalid. Please enter a number between 0 and " + NumWeeks.ToString(), Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }

            TabletBatch newBatch = new TabletBatch();
            newBatch.Component = componentId;
            newBatch.BatchNo = BatchNo;
            newBatch.BatchDate = new DateTime(int.Parse(lblBatchDateYear.Text), (int)cboBatchDateMonth.SelectedValue, int.Parse(txtBatchDateDay.Text));
            TabletUser tuser = TabletUserLogic.RetrieveUser(Properties.Settings.Default.UserId);
            newBatch.CreatedBy = tuser.Logon;
            string msg;

            int newBatchId = TabletBatchLogic.AddBatch(newBatch, Properties.Settings.Default.UserId, out msg);
            if (newBatchId == 0 && msg != string.Empty)
            {
                MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            //Create new stock level record
            TabletStockLevel sl = new TabletStockLevel();
            sl.Location = locationId;
            sl.Batch = newBatchId;
            sl.Quantity = qty;
            sl.Quality = (int)cboQuality.SelectedValue;
            if (txtShippableWeek.Text != string.Empty)
                sl.ShippableWeek = int.Parse(txtShippableWeek.Text);
            sl.ShippableYear = int.Parse(lblShippableYear.Text);

            int newSL = TabletStockLevelLogic.AddStockLevel(sl, Globals.Action.Added, Properties.Settings.Default.UserId);
            if (newSL != 0 && msg != string.Empty)
                MessageBox.Show("There was a problem adding the initial stock location. Please contact support", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                MessageBox.Show("Batch added successfully", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }

        private void btnSelectLocation_Click(object sender, EventArgs e)
        {
            frmChooseLocation frm = new frmChooseLocation();
            frm.info = "Select initial location of new batch";
            frm.ShowDialog();
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                locationId = frm.newLocation;
                TabletLocation loc = TabletLocationLogic.RetrieveLocationById(locationId);
                lblLocation.Text = loc.Code + " (" + loc.Description + ")";
            }
            frm.Dispose();

        }

        private void btnShippableYear_Click(object sender, EventArgs e)
        {
            Button shipBtn = sender as Button;
            if (shipBtn.Name == "btnPlus")
                lblShippableYear.Text = (int.Parse(lblShippableYear.Text) + 1).ToString();
            else
                lblShippableYear.Text = (int.Parse(lblShippableYear.Text) - 1).ToString();
        }

        private void btnSearchComponent_Click(object sender, EventArgs e)
        {
            frmChooseComponent frm = new frmChooseComponent();
            frm.ShowDialog();
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                componentId = frm.selectedComponent;
                TabletComponent comp = TabletComponentLogic.RetrieveComponentById(componentId);
                lblComponentCode.Text = comp.RetailerStockCode + " (" + comp.Description + ")";
            }
        }

        private void btnBatchDateYear_Click(object sender, EventArgs e)
        {
            Button batchYearBtn = sender as Button;
            if (batchYearBtn.Name == "btnBatchDateYearPlus")
                lblBatchDateYear.Text = (int.Parse(lblBatchDateYear.Text) + 1).ToString();
            else
                lblBatchDateYear.Text = (int.Parse(lblBatchDateYear.Text) - 1).ToString();

            SetBatchDateIfInvalid();
        }

        private void txtBatchDateDay_Leave(object sender, EventArgs e)
        {
            SetBatchDateIfInvalid();
        }

        private void SetBatchDateIfInvalid()
        {
            //Most likely cause of invalid date is when the month changes and the day is greater than the number of days in that month.
            //Change day to be last day of selected month
            try
            {
                DateTime BatchDate = new DateTime(int.Parse(lblBatchDateYear.Text), (int)cboBatchDateMonth.SelectedValue, int.Parse(txtBatchDateDay.Text));
            }
            catch
            {
                MessageBox.Show("Invalid date. Date amended to a valid date", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                int nextMonth = (int)cboBatchDateMonth.SelectedValue + 1;
                int year = int.Parse(lblBatchDateYear.Text);
                if (nextMonth == 13)
                {
                    nextMonth = 1;
                    year += 1;
                }
                //Get first day of next month
                DateTime dtFirstOfNext = new DateTime(year, nextMonth, 1);
                DateTime dtLastOfThis = dtFirstOfNext.AddDays(-1);
                txtBatchDateDay.Text = dtLastOfThis.Day.ToString();
            }
        }

        private void cboBatchDateMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
                SetBatchDateIfInvalid();
        }
    }
}
