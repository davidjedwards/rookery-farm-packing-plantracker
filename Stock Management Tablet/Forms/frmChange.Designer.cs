﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInformation = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboReasonForChange = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblInformation.Location = new System.Drawing.Point(12, 53);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(69, 27);
            this.lblInformation.TabIndex = 0;
            this.lblInformation.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "Reason for change:";
            // 
            // cboReasonForChange
            // 
            this.cboReasonForChange.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.cboReasonForChange.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.cboReasonForChange.FormattingEnabled = true;
            this.cboReasonForChange.Location = new System.Drawing.Point(12, 117);
            this.cboReasonForChange.Name = "cboReasonForChange";
            this.cboReasonForChange.Size = new System.Drawing.Size(861, 35);
            this.cboReasonForChange.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::Stock_Management_Tablet.Properties.Resources.SaveButton;
            this.btnSave.Location = new System.Drawing.Point(752, 163);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(121, 47);
            this.btnSave.TabIndex = 4;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Tablet.Properties.Resources.CancelButton;
            this.btnCancel.Location = new System.Drawing.Point(376, 163);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(151, 47);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = global::Stock_Management_Tablet.Properties.Settings.Default.TitleFont;
            this.label2.ForeColor = global::Stock_Management_Tablet.Properties.Settings.Default.MenuTextColour;
            this.label2.Location = new System.Drawing.Point(305, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(298, 35);
            this.label2.TabIndex = 5;
            this.label2.Text = "Change of Quantity";
            // 
            // frmChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FormBackground;
            this.ClientSize = new System.Drawing.Size(907, 222);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cboReasonForChange);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblInformation);
            this.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FormFont;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmChange";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmChange_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Label lblInformation;
        public System.Windows.Forms.ComboBox cboReasonForChange;
        private System.Windows.Forms.Label label2;
    }
}