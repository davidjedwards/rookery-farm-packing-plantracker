﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tablet_BLL;
using RFP;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmChange : Form
    {
        public string reason { get; set; }
        
        public frmChange()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cboReasonForChange.Text == string.Empty)
            {
                MessageBox.Show("Please enter a reason for the change", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                reason = cboReasonForChange.Text;
                TabletReasonsForChange trc = new TabletReasonsForChange();
                trc.Reason = reason;
                TabletReasonsForChangeLogic.Add(trc);

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void frmChange_Load(object sender, EventArgs e)
        {
            //Load combo
            cboReasonForChange.DataSource = TabletReasonsForChangeLogic.RetrieveReasonsforChange();
        }
    }
}
