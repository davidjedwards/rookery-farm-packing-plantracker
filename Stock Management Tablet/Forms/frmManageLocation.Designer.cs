﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmManageLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManageLocation));
            this.label1 = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.drMain = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
            this.btnGrowingOn = new System.Windows.Forms.Button();
            this.lblQuality = new System.Windows.Forms.Label();
            this.txtShippableWeek = new System.Windows.Forms.TextBox();
            this.lblShippableYear = new System.Windows.Forms.Label();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btnPlus = new System.Windows.Forms.Button();
            this.lblBatch = new System.Windows.Forms.Label();
            this.lblComponent = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.txtQuantityToProcess = new System.Windows.Forms.TextBox();
            this.chkQuery = new System.Windows.Forms.CheckBox();
            this.cboQueryType = new System.Windows.Forms.ComboBox();
            this.btnShippable = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.btnDispose = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblQualityId = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblProcess = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.drMain.ItemTemplate.SuspendLayout();
            this.drMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Stock_Management_Tablet.Properties.Settings.Default.TitleFont;
            this.label1.ForeColor = global::Stock_Management_Tablet.Properties.Settings.Default.MenuTextColour;
            this.label1.Location = new System.Drawing.Point(515, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Location:";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Font = global::Stock_Management_Tablet.Properties.Settings.Default.TitleFont;
            this.lblLocation.ForeColor = System.Drawing.Color.Black;
            this.lblLocation.Location = new System.Drawing.Point(661, 9);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(0, 35);
            this.lblLocation.TabIndex = 1;
            // 
            // drMain
            // 
            this.drMain.AllowUserToAddItems = false;
            this.drMain.AllowUserToDeleteItems = false;
            // 
            // drMain.ItemTemplate
            // 
            this.drMain.ItemTemplate.Controls.Add(this.lblFileName);
            this.drMain.ItemTemplate.Controls.Add(this.btnGrowingOn);
            this.drMain.ItemTemplate.Controls.Add(this.lblQuality);
            this.drMain.ItemTemplate.Controls.Add(this.txtShippableWeek);
            this.drMain.ItemTemplate.Controls.Add(this.lblShippableYear);
            this.drMain.ItemTemplate.Controls.Add(this.btnMinus);
            this.drMain.ItemTemplate.Controls.Add(this.btnPlus);
            this.drMain.ItemTemplate.Controls.Add(this.lblBatch);
            this.drMain.ItemTemplate.Controls.Add(this.lblComponent);
            this.drMain.ItemTemplate.Controls.Add(this.lblQuantity);
            this.drMain.ItemTemplate.Controls.Add(this.txtQuantityToProcess);
            this.drMain.ItemTemplate.Controls.Add(this.chkQuery);
            this.drMain.ItemTemplate.Controls.Add(this.cboQueryType);
            this.drMain.ItemTemplate.Controls.Add(this.btnShippable);
            this.drMain.ItemTemplate.Controls.Add(this.btnMove);
            this.drMain.ItemTemplate.Controls.Add(this.btnDispose);
            this.drMain.ItemTemplate.Controls.Add(this.btnChange);
            this.drMain.ItemTemplate.Controls.Add(this.label6);
            this.drMain.ItemTemplate.Controls.Add(this.label7);
            this.drMain.ItemTemplate.Controls.Add(this.label5);
            this.drMain.ItemTemplate.Controls.Add(this.label4);
            this.drMain.ItemTemplate.Controls.Add(this.label3);
            this.drMain.ItemTemplate.Controls.Add(this.label9);
            this.drMain.ItemTemplate.Controls.Add(this.label2);
            this.drMain.ItemTemplate.Controls.Add(this.lblQualityId);
            this.drMain.ItemTemplate.Size = new System.Drawing.Size(1331, 134);
            this.drMain.Location = new System.Drawing.Point(8, 54);
            this.drMain.Name = "drMain";
            this.drMain.Size = new System.Drawing.Size(1339, 584);
            this.drMain.TabIndex = 4;
            this.drMain.Text = "dataRepeater1";
            this.drMain.ItemCloned += new Microsoft.VisualBasic.PowerPacks.DataRepeaterItemEventHandler(this.drMain_ItemCloned);
            this.drMain.DrawItem += new Microsoft.VisualBasic.PowerPacks.DataRepeaterItemEventHandler(this.drMain_DrawItem);
            // 
            // btnGrowingOn
            // 
            this.btnGrowingOn.FlatAppearance.BorderSize = 0;
            this.btnGrowingOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrowingOn.Image = global::Stock_Management_Tablet.Properties.Resources.GrowingOn;
            this.btnGrowingOn.Location = new System.Drawing.Point(911, 75);
            this.btnGrowingOn.Name = "btnGrowingOn";
            this.btnGrowingOn.Size = new System.Drawing.Size(230, 41);
            this.btnGrowingOn.TabIndex = 48;
            this.btnGrowingOn.UseVisualStyleBackColor = true;
            this.btnGrowingOn.Visible = false;
            this.btnGrowingOn.Click += new System.EventHandler(this.btnSetNewQuality_Click);
            // 
            // lblQuality
            // 
            this.lblQuality.AutoSize = true;
            this.lblQuality.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblQuality.Location = new System.Drawing.Point(496, 35);
            this.lblQuality.Name = "lblQuality";
            this.lblQuality.Size = new System.Drawing.Size(69, 27);
            this.lblQuality.TabIndex = 46;
            this.lblQuality.Text = "label8";
            // 
            // txtShippableWeek
            // 
            this.txtShippableWeek.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtShippableWeek.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtShippableWeek.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtShippableWeek.Location = new System.Drawing.Point(358, 82);
            this.txtShippableWeek.Name = "txtShippableWeek";
            this.txtShippableWeek.Size = new System.Drawing.Size(66, 32);
            this.txtShippableWeek.TabIndex = 45;
            this.txtShippableWeek.Click += new System.EventHandler(this.ShowNumericKeypad);
            // 
            // lblShippableYear
            // 
            this.lblShippableYear.AutoSize = true;
            this.lblShippableYear.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblShippableYear.Location = new System.Drawing.Point(171, 85);
            this.lblShippableYear.Name = "lblShippableYear";
            this.lblShippableYear.Size = new System.Drawing.Size(69, 27);
            this.lblShippableYear.TabIndex = 44;
            this.lblShippableYear.Text = "label8";
            // 
            // btnMinus
            // 
            this.btnMinus.FlatAppearance.BorderSize = 0;
            this.btnMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinus.Image = global::Stock_Management_Tablet.Properties.Resources.MinusButton;
            this.btnMinus.Location = new System.Drawing.Point(300, 73);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(51, 47);
            this.btnMinus.TabIndex = 43;
            this.btnMinus.UseVisualStyleBackColor = true;
            this.btnMinus.Click += new System.EventHandler(this.shippableButton_Click);
            // 
            // btnPlus
            // 
            this.btnPlus.FlatAppearance.BorderSize = 0;
            this.btnPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlus.Image = global::Stock_Management_Tablet.Properties.Resources.PlusButton;
            this.btnPlus.Location = new System.Drawing.Point(243, 73);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(51, 47);
            this.btnPlus.TabIndex = 42;
            this.btnPlus.UseVisualStyleBackColor = true;
            this.btnPlus.Click += new System.EventHandler(this.shippableButton_Click);
            // 
            // lblBatch
            // 
            this.lblBatch.AutoSize = true;
            this.lblBatch.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblBatch.Location = new System.Drawing.Point(412, 35);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(69, 27);
            this.lblBatch.TabIndex = 25;
            this.lblBatch.Text = "label8";
            // 
            // lblComponent
            // 
            this.lblComponent.AutoSize = true;
            this.lblComponent.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblComponent.Location = new System.Drawing.Point(4, 35);
            this.lblComponent.Name = "lblComponent";
            this.lblComponent.Size = new System.Drawing.Size(69, 27);
            this.lblComponent.TabIndex = 27;
            this.lblComponent.Text = "label8";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblQuantity.Location = new System.Drawing.Point(662, 35);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(69, 27);
            this.lblQuantity.TabIndex = 33;
            this.lblQuantity.Text = "label8";
            // 
            // txtQuantityToProcess
            // 
            this.txtQuantityToProcess.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtQuantityToProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuantityToProcess.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtQuantityToProcess.Location = new System.Drawing.Point(667, 81);
            this.txtQuantityToProcess.Name = "txtQuantityToProcess";
            this.txtQuantityToProcess.Size = new System.Drawing.Size(96, 32);
            this.txtQuantityToProcess.TabIndex = 35;
            this.txtQuantityToProcess.Click += new System.EventHandler(this.ShowNumericKeypad);
            // 
            // chkQuery
            // 
            this.chkQuery.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkQuery.Location = new System.Drawing.Point(842, 3);
            this.chkQuery.Name = "chkQuery";
            this.chkQuery.Size = new System.Drawing.Size(24, 24);
            this.chkQuery.TabIndex = 37;
            this.chkQuery.UseVisualStyleBackColor = true;
            this.chkQuery.CheckedChanged += new System.EventHandler(this.chkQuery_CheckedChanged);
            // 
            // cboQueryType
            // 
            this.cboQueryType.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.cboQueryType.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.cboQueryType.ForeColor = System.Drawing.Color.Black;
            this.cboQueryType.FormattingEnabled = true;
            this.cboQueryType.Items.AddRange(new object[] {
            "Wrong plant"});
            this.cboQueryType.Location = new System.Drawing.Point(782, 32);
            this.cboQueryType.Name = "cboQueryType";
            this.cboQueryType.Size = new System.Drawing.Size(508, 35);
            this.cboQueryType.TabIndex = 39;
            this.cboQueryType.SelectedIndexChanged += new System.EventHandler(this.cboQueryType_SelectedIndexChanged);
            this.cboQueryType.Leave += new System.EventHandler(this.cboQueryType_Leave);
            // 
            // btnShippable
            // 
            this.btnShippable.FlatAppearance.BorderSize = 0;
            this.btnShippable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShippable.Image = global::Stock_Management_Tablet.Properties.Resources.ShippableButton;
            this.btnShippable.Location = new System.Drawing.Point(1169, 75);
            this.btnShippable.Name = "btnShippable";
            this.btnShippable.Size = new System.Drawing.Size(121, 41);
            this.btnShippable.TabIndex = 24;
            this.btnShippable.UseVisualStyleBackColor = true;
            this.btnShippable.Click += new System.EventHandler(this.btnSetNewQuality_Click);
            // 
            // btnMove
            // 
            this.btnMove.FlatAppearance.BorderSize = 0;
            this.btnMove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMove.Image = global::Stock_Management_Tablet.Properties.Resources.MoveButton;
            this.btnMove.Location = new System.Drawing.Point(1040, 75);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(121, 41);
            this.btnMove.TabIndex = 23;
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnSetNewQuality_Click);
            // 
            // btnDispose
            // 
            this.btnDispose.FlatAppearance.BorderSize = 0;
            this.btnDispose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDispose.Image = global::Stock_Management_Tablet.Properties.Resources.DisposeButton;
            this.btnDispose.Location = new System.Drawing.Point(911, 75);
            this.btnDispose.Name = "btnDispose";
            this.btnDispose.Size = new System.Drawing.Size(121, 41);
            this.btnDispose.TabIndex = 22;
            this.btnDispose.UseVisualStyleBackColor = true;
            this.btnDispose.Click += new System.EventHandler(this.btnSetNewQuality_Click);
            // 
            // btnChange
            // 
            this.btnChange.FlatAppearance.BorderSize = 0;
            this.btnChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChange.Image = ((System.Drawing.Image)(resources.GetObject("btnChange.Image")));
            this.btnChange.Location = new System.Drawing.Point(782, 75);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(121, 41);
            this.btnChange.TabIndex = 21;
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(468, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(193, 27);
            this.label6.TabIndex = 13;
            this.label6.Text = "Quantity to Process";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 27);
            this.label7.TabIndex = 12;
            this.label7.Text = "Shippable Week";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(777, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 27);
            this.label5.TabIndex = 11;
            this.label5.Text = "Query";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(662, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 27);
            this.label4.TabIndex = 10;
            this.label4.Text = "Quantity";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(496, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 27);
            this.label3.TabIndex = 9;
            this.label3.Text = "Quality";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(412, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 27);
            this.label9.TabIndex = 8;
            this.label9.Text = "Batch";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Component";
            // 
            // lblQualityId
            // 
            this.lblQualityId.AutoSize = true;
            this.lblQualityId.Location = new System.Drawing.Point(469, 86);
            this.lblQualityId.Name = "lblQualityId";
            this.lblQualityId.Size = new System.Drawing.Size(59, 23);
            this.lblQualityId.TabIndex = 47;
            this.lblQualityId.Text = "label8";
            this.lblQualityId.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = global::Stock_Management_Tablet.Properties.Resources.FormExitButton;
            this.btnExit.Location = new System.Drawing.Point(1223, 644);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(118, 54);
            this.btnExit.TabIndex = 3;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblProcess
            // 
            this.lblProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProcess.AutoSize = true;
            this.lblProcess.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblProcess.ForeColor = System.Drawing.Color.Black;
            this.lblProcess.Location = new System.Drawing.Point(1183, 15);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(83, 27);
            this.lblProcess.TabIndex = 24;
            this.lblProcess.Text = "Process";
            // 
            // btnNext
            // 
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Image = global::Stock_Management_Tablet.Properties.Resources.NextButton;
            this.btnNext.Location = new System.Drawing.Point(187, 649);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(157, 49);
            this.btnNext.TabIndex = 23;
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.FlatAppearance.BorderSize = 0;
            this.btnPrev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrev.Image = global::Stock_Management_Tablet.Properties.Resources.PreviousButton;
            this.btnPrev.Location = new System.Drawing.Point(15, 649);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(157, 49);
            this.btnPrev.TabIndex = 22;
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // lblFileName
            // 
            this.lblFileName.BackColor = System.Drawing.Color.Transparent;
            this.lblFileName.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblFileName.ForeColor = System.Drawing.Color.Black;
            this.lblFileName.Location = new System.Drawing.Point(906, 0);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(384, 27);
            this.lblFileName.TabIndex = 49;
            this.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmManageLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.ClientSize = new System.Drawing.Size(1353, 708);
            this.Controls.Add(this.lblProcess);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.drMain);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmManageLocation";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.frmManageLocation_Load);
            this.drMain.ItemTemplate.ResumeLayout(false);
            this.drMain.ItemTemplate.PerformLayout();
            this.drMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Button btnExit;
        private Microsoft.VisualBasic.PowerPacks.DataRepeater drMain;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShippable;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Button btnDispose;
        private System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.Label lblComponent;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.TextBox txtQuantityToProcess;
        private System.Windows.Forms.CheckBox chkQuery;
        private System.Windows.Forms.ComboBox cboQueryType;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button btnMinus;
        private System.Windows.Forms.TextBox txtShippableWeek;
        private System.Windows.Forms.Label lblShippableYear;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblQuality;
        private System.Windows.Forms.Label lblQualityId;
        private System.Windows.Forms.Button btnGrowingOn;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Label lblFileName;
    }
}