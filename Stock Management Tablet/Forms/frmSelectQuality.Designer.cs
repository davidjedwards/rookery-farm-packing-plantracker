﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmSelectQuality
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.btnShippable = new System.Windows.Forms.Button();
            this.btnGrowingOn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = global::Stock_Management_Tablet.Properties.Settings.Default.TitleFont;
            this.label2.ForeColor = global::Stock_Management_Tablet.Properties.Settings.Default.MenuTextColour;
            this.label2.Location = new System.Drawing.Point(136, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 35);
            this.label2.TabIndex = 6;
            this.label2.Text = "Select Quality";
            // 
            // btnShippable
            // 
            this.btnShippable.FlatAppearance.BorderSize = 0;
            this.btnShippable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShippable.Image = global::Stock_Management_Tablet.Properties.Resources.ShippableButton;
            this.btnShippable.Location = new System.Drawing.Point(300, 86);
            this.btnShippable.Name = "btnShippable";
            this.btnShippable.Size = new System.Drawing.Size(124, 41);
            this.btnShippable.TabIndex = 8;
            this.btnShippable.UseVisualStyleBackColor = true;
            this.btnShippable.Click += new System.EventHandler(this.btnShippable_Click);
            // 
            // btnGrowingOn
            // 
            this.btnGrowingOn.FlatAppearance.BorderSize = 0;
            this.btnGrowingOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrowingOn.Image = global::Stock_Management_Tablet.Properties.Resources.GrowingOnSel;
            this.btnGrowingOn.Location = new System.Drawing.Point(55, 86);
            this.btnGrowingOn.Name = "btnGrowingOn";
            this.btnGrowingOn.Size = new System.Drawing.Size(130, 41);
            this.btnGrowingOn.TabIndex = 7;
            this.btnGrowingOn.UseVisualStyleBackColor = true;
            this.btnGrowingOn.Click += new System.EventHandler(this.btnGrowingOn_Click);
            // 
            // frmSelectQuality
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FormBackground;
            this.ClientSize = new System.Drawing.Size(489, 169);
            this.ControlBox = false;
            this.Controls.Add(this.btnShippable);
            this.Controls.Add(this.btnGrowingOn);
            this.Controls.Add(this.label2);
            this.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FormFont;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmSelectQuality";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGrowingOn;
        private System.Windows.Forms.Button btnShippable;
    }
}