﻿namespace Stock_Management_Tablet.Forms
{
    partial class frmBatchAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBatch = new System.Windows.Forms.Panel();
            this.lblBatchDateYear = new System.Windows.Forms.Label();
            this.btnBatchDateYearMinus = new System.Windows.Forms.Button();
            this.btnBatchDateYearPlus = new System.Windows.Forms.Button();
            this.txtBatchDateDay = new System.Windows.Forms.TextBox();
            this.cboBatchDateMonth = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboQuality = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtShippableWeek = new System.Windows.Forms.TextBox();
            this.lblShippableYear = new System.Windows.Forms.Label();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btnPlus = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSelectLocation = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblLocation = new System.Windows.Forms.Label();
            this.btnSearchComponent = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblComponentCode = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtBatchNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProcess = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlBatch.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBatch
            // 
            this.pnlBatch.BackColor = System.Drawing.Color.White;
            this.pnlBatch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBatch.Controls.Add(this.lblBatchDateYear);
            this.pnlBatch.Controls.Add(this.btnBatchDateYearMinus);
            this.pnlBatch.Controls.Add(this.btnBatchDateYearPlus);
            this.pnlBatch.Controls.Add(this.txtBatchDateDay);
            this.pnlBatch.Controls.Add(this.cboBatchDateMonth);
            this.pnlBatch.Controls.Add(this.label2);
            this.pnlBatch.Controls.Add(this.cboQuality);
            this.pnlBatch.Controls.Add(this.label10);
            this.pnlBatch.Controls.Add(this.label9);
            this.pnlBatch.Controls.Add(this.label8);
            this.pnlBatch.Controls.Add(this.label6);
            this.pnlBatch.Controls.Add(this.txtShippableWeek);
            this.pnlBatch.Controls.Add(this.lblShippableYear);
            this.pnlBatch.Controls.Add(this.btnMinus);
            this.pnlBatch.Controls.Add(this.btnPlus);
            this.pnlBatch.Controls.Add(this.label7);
            this.pnlBatch.Controls.Add(this.btnSelectLocation);
            this.pnlBatch.Controls.Add(this.panel2);
            this.pnlBatch.Controls.Add(this.btnSearchComponent);
            this.pnlBatch.Controls.Add(this.panel3);
            this.pnlBatch.Controls.Add(this.txtQuantity);
            this.pnlBatch.Controls.Add(this.txtBatchNo);
            this.pnlBatch.Controls.Add(this.label1);
            this.pnlBatch.Location = new System.Drawing.Point(8, 57);
            this.pnlBatch.Name = "pnlBatch";
            this.pnlBatch.Size = new System.Drawing.Size(1339, 584);
            this.pnlBatch.TabIndex = 26;
            // 
            // lblBatchDateYear
            // 
            this.lblBatchDateYear.AutoSize = true;
            this.lblBatchDateYear.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblBatchDateYear.Location = new System.Drawing.Point(490, 160);
            this.lblBatchDateYear.Name = "lblBatchDateYear";
            this.lblBatchDateYear.Size = new System.Drawing.Size(0, 27);
            this.lblBatchDateYear.TabIndex = 61;
            // 
            // btnBatchDateYearMinus
            // 
            this.btnBatchDateYearMinus.FlatAppearance.BorderSize = 0;
            this.btnBatchDateYearMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBatchDateYearMinus.Image = global::Stock_Management_Tablet.Properties.Resources.MinusButton;
            this.btnBatchDateYearMinus.Location = new System.Drawing.Point(604, 150);
            this.btnBatchDateYearMinus.Name = "btnBatchDateYearMinus";
            this.btnBatchDateYearMinus.Size = new System.Drawing.Size(51, 47);
            this.btnBatchDateYearMinus.TabIndex = 60;
            this.btnBatchDateYearMinus.UseVisualStyleBackColor = true;
            this.btnBatchDateYearMinus.Click += new System.EventHandler(this.btnBatchDateYear_Click);
            // 
            // btnBatchDateYearPlus
            // 
            this.btnBatchDateYearPlus.FlatAppearance.BorderSize = 0;
            this.btnBatchDateYearPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBatchDateYearPlus.Image = global::Stock_Management_Tablet.Properties.Resources.PlusButton;
            this.btnBatchDateYearPlus.Location = new System.Drawing.Point(550, 150);
            this.btnBatchDateYearPlus.Name = "btnBatchDateYearPlus";
            this.btnBatchDateYearPlus.Size = new System.Drawing.Size(51, 47);
            this.btnBatchDateYearPlus.TabIndex = 59;
            this.btnBatchDateYearPlus.UseVisualStyleBackColor = true;
            this.btnBatchDateYearPlus.Click += new System.EventHandler(this.btnBatchDateYear_Click);
            // 
            // txtBatchDateDay
            // 
            this.txtBatchDateDay.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtBatchDateDay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBatchDateDay.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtBatchDateDay.Location = new System.Drawing.Point(404, 159);
            this.txtBatchDateDay.Name = "txtBatchDateDay";
            this.txtBatchDateDay.Size = new System.Drawing.Size(70, 32);
            this.txtBatchDateDay.TabIndex = 58;
            this.txtBatchDateDay.Click += new System.EventHandler(this.ShowNumericKeypad);
            this.txtBatchDateDay.Leave += new System.EventHandler(this.txtBatchDateDay_Leave);
            // 
            // cboBatchDateMonth
            // 
            this.cboBatchDateMonth.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.cboBatchDateMonth.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.cboBatchDateMonth.FormattingEnabled = true;
            this.cboBatchDateMonth.Location = new System.Drawing.Point(261, 158);
            this.cboBatchDateMonth.Name = "cboBatchDateMonth";
            this.cboBatchDateMonth.Size = new System.Drawing.Size(128, 35);
            this.cboBatchDateMonth.TabIndex = 57;
            this.cboBatchDateMonth.SelectedIndexChanged += new System.EventHandler(this.cboBatchDateMonth_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(57, 336);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 27);
            this.label2.TabIndex = 56;
            this.label2.Text = "Quality";
            // 
            // cboQuality
            // 
            this.cboQuality.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.cboQuality.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.cboQuality.FormattingEnabled = true;
            this.cboQuality.Location = new System.Drawing.Point(261, 338);
            this.cboQuality.Name = "cboQuality";
            this.cboQuality.Size = new System.Drawing.Size(377, 35);
            this.cboQuality.TabIndex = 55;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(57, 276);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 27);
            this.label10.TabIndex = 54;
            this.label10.Text = "Quantity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(57, 216);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 27);
            this.label9.TabIndex = 53;
            this.label9.Text = "Initial Location";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(57, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 27);
            this.label8.TabIndex = 52;
            this.label8.Text = "Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(57, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 27);
            this.label6.TabIndex = 51;
            this.label6.Text = "Batch Number";
            // 
            // txtShippableWeek
            // 
            this.txtShippableWeek.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtShippableWeek.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtShippableWeek.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtShippableWeek.Location = new System.Drawing.Point(429, 398);
            this.txtShippableWeek.Name = "txtShippableWeek";
            this.txtShippableWeek.Size = new System.Drawing.Size(66, 32);
            this.txtShippableWeek.TabIndex = 50;
            this.txtShippableWeek.Click += new System.EventHandler(this.ShowNumericKeypad);
            // 
            // lblShippableYear
            // 
            this.lblShippableYear.AutoSize = true;
            this.lblShippableYear.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblShippableYear.Location = new System.Drawing.Point(257, 401);
            this.lblShippableYear.Name = "lblShippableYear";
            this.lblShippableYear.Size = new System.Drawing.Size(0, 27);
            this.lblShippableYear.TabIndex = 49;
            // 
            // btnMinus
            // 
            this.btnMinus.FlatAppearance.BorderSize = 0;
            this.btnMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinus.Image = global::Stock_Management_Tablet.Properties.Resources.MinusButton;
            this.btnMinus.Location = new System.Drawing.Point(371, 391);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(51, 47);
            this.btnMinus.TabIndex = 48;
            this.btnMinus.UseVisualStyleBackColor = true;
            this.btnMinus.Click += new System.EventHandler(this.btnShippableYear_Click);
            // 
            // btnPlus
            // 
            this.btnPlus.FlatAppearance.BorderSize = 0;
            this.btnPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlus.Image = global::Stock_Management_Tablet.Properties.Resources.PlusButton;
            this.btnPlus.Location = new System.Drawing.Point(317, 391);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(51, 47);
            this.btnPlus.TabIndex = 47;
            this.btnPlus.UseVisualStyleBackColor = true;
            this.btnPlus.Click += new System.EventHandler(this.btnShippableYear_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(57, 396);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 27);
            this.label7.TabIndex = 46;
            this.label7.Text = "Shippable Week";
            // 
            // btnSelectLocation
            // 
            this.btnSelectLocation.FlatAppearance.BorderSize = 0;
            this.btnSelectLocation.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSelectLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectLocation.Image = global::Stock_Management_Tablet.Properties.Resources.SelectButton;
            this.btnSelectLocation.Location = new System.Drawing.Point(1035, 210);
            this.btnSelectLocation.Name = "btnSelectLocation";
            this.btnSelectLocation.Size = new System.Drawing.Size(112, 40);
            this.btnSelectLocation.TabIndex = 28;
            this.btnSelectLocation.UseVisualStyleBackColor = true;
            this.btnSelectLocation.Click += new System.EventHandler(this.btnSelectLocation_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblLocation);
            this.panel2.Location = new System.Drawing.Point(261, 212);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(768, 37);
            this.panel2.TabIndex = 29;
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(3, 6);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(0, 23);
            this.lblLocation.TabIndex = 8;
            // 
            // btnSearchComponent
            // 
            this.btnSearchComponent.FlatAppearance.BorderSize = 0;
            this.btnSearchComponent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSearchComponent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchComponent.Image = global::Stock_Management_Tablet.Properties.Resources.SelectButton;
            this.btnSearchComponent.Location = new System.Drawing.Point(1035, 31);
            this.btnSearchComponent.Name = "btnSearchComponent";
            this.btnSearchComponent.Size = new System.Drawing.Size(112, 40);
            this.btnSearchComponent.TabIndex = 18;
            this.btnSearchComponent.UseVisualStyleBackColor = true;
            this.btnSearchComponent.Click += new System.EventHandler(this.btnSearchComponent_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblComponentCode);
            this.panel3.Location = new System.Drawing.Point(261, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(768, 37);
            this.panel3.TabIndex = 27;
            // 
            // lblComponentCode
            // 
            this.lblComponentCode.AutoSize = true;
            this.lblComponentCode.Location = new System.Drawing.Point(3, 6);
            this.lblComponentCode.Name = "lblComponentCode";
            this.lblComponentCode.Size = new System.Drawing.Size(0, 23);
            this.lblComponentCode.TabIndex = 8;
            // 
            // txtQuantity
            // 
            this.txtQuantity.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuantity.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtQuantity.Location = new System.Drawing.Point(261, 278);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(70, 32);
            this.txtQuantity.TabIndex = 25;
            this.txtQuantity.Click += new System.EventHandler(this.ShowNumericKeypad);
            // 
            // txtBatchNo
            // 
            this.txtBatchNo.BackColor = global::Stock_Management_Tablet.Properties.Settings.Default.FieldBackground;
            this.txtBatchNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBatchNo.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.txtBatchNo.Location = new System.Drawing.Point(261, 98);
            this.txtBatchNo.Name = "txtBatchNo";
            this.txtBatchNo.Size = new System.Drawing.Size(70, 32);
            this.txtBatchNo.TabIndex = 19;
            this.txtBatchNo.Click += new System.EventHandler(this.ShowNumericKeypad);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(57, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 27);
            this.label1.TabIndex = 20;
            this.label1.Text = "Component";
            // 
            // lblProcess
            // 
            this.lblProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProcess.AutoSize = true;
            this.lblProcess.Font = global::Stock_Management_Tablet.Properties.Settings.Default.FieldFont;
            this.lblProcess.ForeColor = System.Drawing.Color.Black;
            this.lblProcess.Location = new System.Drawing.Point(1205, 14);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(106, 27);
            this.lblProcess.TabIndex = 25;
            this.lblProcess.Text = "Add Batch";
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::Stock_Management_Tablet.Properties.Resources.SaveButton;
            this.btnSave.Location = new System.Drawing.Point(1190, 647);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(157, 49);
            this.btnSave.TabIndex = 28;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Tablet.Properties.Resources.CancelButton;
            this.btnCancel.Location = new System.Drawing.Point(596, 647);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(157, 49);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmBatchAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 708);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlBatch);
            this.Controls.Add(this.lblProcess);
            this.Name = "frmBatchAdd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.frmBatchAdd_Load);
            this.pnlBatch.ResumeLayout(false);
            this.pnlBatch.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Panel pnlBatch;
        private System.Windows.Forms.Button btnSelectLocation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Button btnSearchComponent;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblComponentCode;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtBatchNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtShippableWeek;
        private System.Windows.Forms.Label lblShippableYear;
        private System.Windows.Forms.Button btnMinus;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboQuality;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblBatchDateYear;
        private System.Windows.Forms.Button btnBatchDateYearMinus;
        private System.Windows.Forms.Button btnBatchDateYearPlus;
        private System.Windows.Forms.TextBox txtBatchDateDay;
        private System.Windows.Forms.ComboBox cboBatchDateMonth;
    }
}