﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmNumericKeypad : Form
    {
        public frmNumericKeypad()
        {
            InitializeComponent();
        }

        public TextBox txtEntry { get; set; }

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            try
            {
                int i = int.Parse(btn.Text);
                txtEntry.Text += i.ToString();
            }
            catch (FormatException)
            {
                //btn pressed is CLR 
                if (btn.Tag.ToString() == "Backspace")
                {
                    if (txtEntry.TextLength > 0)
                        txtEntry.Text = txtEntry.Text.Substring(0, txtEntry.Text.Length - 1);
                }
                else
                {
                    //exit button pressed
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }
    }
}
