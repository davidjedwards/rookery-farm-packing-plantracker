﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using Tablet_BLL;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmChooseLocation : Stock_Management_Tablet.Forms.frmBase
    {
        public frmChooseLocation()
        {
            InitializeComponent();
        }

        public int newLocation { get; set; }
        public string info { get; set; }

        private void frmChooseLocation_Load(object sender, EventArgs e)
        {
            loading = true;

            newLocation = 0;
            lblInformation.Text = info;
            CreateLocationButtons();

            loading = false;
        }

        private void CreateLocationButtons()
        {
            int firstRow = 15;
            int nextRow = 70;
            int firstCol = 12;
            int nextCol = 170;
            int numCols = 7;
            Size btnSize = new Size(150, 50); 
            int intRow = 0;
            int intCol = 0;
            Button btn;

            List<TabletLocation> locList = Tablet_BLL.TabletLocationLogic.LocationList();
            foreach (TabletLocation loc in locList)
            {
                btn = new Button();
                btn.Size = btnSize;
                btn.FlatStyle = FlatStyle.Flat;
                btn.Top = firstRow + (nextRow * intRow);
                btn.Left = firstCol + (nextCol * intCol);
                if (intCol == numCols - 1) //intcol is zero-based
                {
                    intCol = 0;
                    intRow += 1;
                }
                else
                    intCol += 1;
                btn.Text = loc.Code;
                btn.Tag = loc.Id;
                btn.Visible = true;
                btn.Click += LocationButton_Click;
                btn.FlatAppearance.BorderSize = 0;
                btn.BackColor = Properties.Settings.Default.TabletGreen;
                btn.ForeColor = Color.White;
                pnlLocations.Controls.Add(btn);
            }
        }

        private void LocationButton_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            newLocation = (int)btn.Tag;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

    }
}
