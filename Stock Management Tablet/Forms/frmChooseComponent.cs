﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using Tablet_BLL;
using Stock_Management_Tablet.Classes;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmChooseComponent : Stock_Management_Tablet.Forms.frmBase
    {
        public int selectedComponent;

        public frmChooseComponent()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void frmChooseComponent_Load(object sender, EventArgs e)
        {
            Functions.StyleDataGridView(dgvComponentList);
            LoadGrid();
        }

        private void LoadGrid()
        {
            dgvComponentList.DataSource = TabletComponentLogic.RetrieveComponentList(txtDesc.Text, txtCode.Text);
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }

        private void dgvComponentList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                selectedComponent = (int)dgvComponentList.Rows[e.RowIndex].Cells["IdColumn"].Value;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}
