﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using Tablet_BLL;
using Microsoft.VisualBasic.PowerPacks;

namespace Stock_Management_Tablet.Forms
{
    public partial class frmManageLocation : Stock_Management_Tablet.Forms.frmBase
    {
        LinkedList<TabletLocation> locList;
        LinkedListNode<TabletLocation> locNode;

        private bool _stockCheck = false;

        public bool StockCheck
        {
            get { return _stockCheck; }
            set { _stockCheck = value; }
        }
        
        public frmManageLocation()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmManageLocation_Load(object sender, EventArgs e)
        {

            locList = TabletLocationLogic.GetLocationsInOrder(StockCheck);
            locNode = locList.First;
            btnPrev.Enabled = false;
            if (locNode.Next == null)
                btnNext.Enabled = false;

            if (_stockCheck)
                lblProcess.Text = "Stock Check";
            else
                lblProcess.Text = "Crop Walk";
            
            lblComponent.DataBindings.Add(new Binding("Text", drMain.DataSource, "ComponentDesc"));
            lblBatch.DataBindings.Add(new Binding("Text", drMain.DataSource, "BatchNo"));
            lblQuality.DataBindings.Add(new Binding("Text", drMain.DataSource, "QualityDesc"));
            chkQuery.DataBindings.Add(new Binding("Checked", drMain.DataBindings, "Query"));
            lblShippableYear.DataBindings.Add(new Binding("Text", drMain.DataBindings, "ShippableYear"));
            txtShippableWeek.DataBindings.Add(new Binding("Text", drMain.DataBindings, "ShippableWeek"));
            lblQuantity.DataBindings.Add(new Binding("Text", drMain.DataSource, "Quantity"));
            txtQuantityToProcess.DataBindings.Add(new Binding("Text", drMain.DataBindings, "Quantity"));
            lblQualityId.DataBindings.Add(new Binding("Text", drMain.DataBindings, "Quality"));
            lblFileName.DataBindings.Add(new Binding("Text", drMain.DataBindings, "FileName"));
            LoadDataRepeater();
        }

        private void LoadDataRepeater()
        {
            loading = true;
            TabletLocation thisLoc = locNode.Value;
            lblLocation.Text = thisLoc.Code;
            drMain.DataSource = TabletStockLevelLogic.RetrieveStockLevelsForLocation(thisLoc.Id, _stockCheck);
            btnPrev.Enabled = (locNode.Previous != null);
            btnNext.Enabled = (locNode.Next != null);
            loading = false;
        }

        private void drMain_ItemCloned(object sender, Microsoft.VisualBasic.PowerPacks.DataRepeaterItemEventArgs e)
        {
            //Set the datasource in here. If we do it for the whole datarepeater then a change to the selected item on one datarepeater item changes them all.
            PopulateQueryTypeCombo(e.DataRepeaterItem);
        }

        private void PopulateQueryTypeCombo(DataRepeaterItem dataRepeaterItem, int selValue = 0)
        {
            //selValue will be set if this is a new query type)
            ComboBox cbo = (ComboBox)dataRepeaterItem.Controls.Find("cboQueryType", false)[0];
            int? prevSel = 0;
            if (selValue == 0)
                prevSel = (int?)cbo.SelectedValue;
            else
                prevSel = selValue;

            cbo.DataSource = TabletConfigurationLogic.ListConfigurationItemsAsList(ConfigGroup.QueryType);
            cbo.ValueMember = "Id";
            cbo.DisplayMember = "ItemValue";
            if (prevSel != null)
                cbo.SelectedValue = prevSel;
        }

        private void drMain_DrawItem(object sender, Microsoft.VisualBasic.PowerPacks.DataRepeaterItemEventArgs e)
        {
            if (e.DataRepeaterItem.ItemIndex % 2 == 0)
            {
                e.DataRepeaterItem.BackColor = Properties.Settings.Default.DGRow1Colour;
            }
            else
            {
                e.DataRepeaterItem.BackColor = Properties.Settings.Default.DGRow2Colour;
            }
            //Set selected value of combobox - pain, but we have to do it this way!
            Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)sender;
            ComboBox cbo = (ComboBox)e.DataRepeaterItem.Controls.Find("cboQueryType", false)[0];
            if (((List<TabletStockLevelRow>)dataRepeater.DataSource)[e.DataRepeaterItem.ItemIndex].QueryType != null)
                cbo.SelectedValue = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[e.DataRepeaterItem.ItemIndex].QueryType;

            //Only show Query Type combo if Query ticked
            CheckBox chk = (CheckBox)e.DataRepeaterItem.Controls.Find("chkQuery", false)[0];
            cbo.Visible = chk.Checked;

            TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[e.DataRepeaterItem.ItemIndex];
            Button btnD = (Button)e.DataRepeaterItem.Controls.Find("btnDispose", false)[0];
            Button btnM = (Button)e.DataRepeaterItem.Controls.Find("btnMove", false)[0];
            Button btnS = (Button)e.DataRepeaterItem.Controls.Find("btnShippable", false)[0];
            Button btnG = (Button)e.DataRepeaterItem.Controls.Find("btnGrowingOn", false)[0];
            bool ShowBackButton;
            if (source.Quality == (int)Globals.CropQuality.GrowingOn)
            {
                ShowBackButton = false;
            }
            else
            {
                ShowBackButton = true;
            }
            btnD.Visible = !ShowBackButton;
            btnM.Visible = !ShowBackButton;
            btnS.Visible = !ShowBackButton;
            btnG.Visible = ShowBackButton;
        }

        private void cboQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                //Save combobox value back to datasource. Nightmare!!
                ComboBox cbo = (ComboBox)sender;
                Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)cbo.Parent;
                Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)cbo.Parent.Parent;
                var source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
                source.QueryType = (int?)cbo.SelectedValue;
            }
        }

        private void shippableButton_Click(object sender, EventArgs e)
        {
            Button shipBtn = (Button)sender;
            Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)shipBtn.Parent;
            Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)shipBtn.Parent.Parent;
            TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
            Label thisLabel = (Label)dataRepeaterItem.Controls["lblShippableYear"];
            if (shipBtn.Name == "btnPlus")
                thisLabel.Text = (int.Parse(thisLabel.Text) + 1).ToString();
            else
                thisLabel.Text = (int.Parse(thisLabel.Text) - 1).ToString();

            //Update stock level record
            TabletStockLevel stockLevel = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
            int? prevShippableYear = stockLevel.ShippableYear;
            stockLevel.ShippableYear = int.Parse(thisLabel.Text);
            TabletStockLevelLogic.UpdateStockLevel(stockLevel, Globals.Action.Other, Properties.Settings.Default.UserId, (int)stockLevel.Quantity, (int)stockLevel.Quality,
                (int)stockLevel.ShippableWeek, (int)stockLevel.ShippableYear);
        }

        private void ShowNumericKeypad(object sender, EventArgs e)
        {
            frmNumericKeypad frm = new frmNumericKeypad();
            TextBox ThisTextBox = (TextBox)sender;
            int prevValue;
            if (!int.TryParse(ThisTextBox.Text, out prevValue))
                prevValue = 0;

            frm.txtEntry = ThisTextBox;
            frm.Location = new Point(drMain.Left + ThisTextBox.Left + ThisTextBox.Width + 50, drMain.Top + ThisTextBox.Top);
            frm.ShowDialog();
            frm.Dispose();

            if (ThisTextBox.Name == "txtShippableWeek")
            {
                int newValue = int.Parse(ThisTextBox.Text);
                if (newValue != prevValue)
                {
                    //Save new shippable week value back to the database
                    Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)ThisTextBox.Parent;
                    Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)ThisTextBox.Parent.Parent;
                    TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
                    TabletStockLevel stockLevel = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
                    Label Quantity = (Label)drMain.CurrentItem.Controls["lblQuantity"];
                    Label Quality = (Label)drMain.CurrentItem.Controls["lblQualityId"];
                    stockLevel.ShippableWeek = newValue;
                    int? prevShippableYear = stockLevel.ShippableYear;
                    TabletStockLevelLogic.UpdateStockLevel(stockLevel, Globals.Action.Other, Properties.Settings.Default.UserId, int.Parse(Quantity.Text), int.Parse(Quality.Text),
                            int.Parse(ThisTextBox.Text), prevShippableYear);
                }
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            TextBox ThisQuantityToProcess = (TextBox)drMain.CurrentItem.Controls["txtQuantityToProcess"];
            Label CurrQuantity = (Label)drMain.CurrentItem.Controls["lblQuantity"];
            if (ThisQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                int qtyToProcess;
                if (!int.TryParse(ThisQuantityToProcess.Text, out qtyToProcess))
                {
                    MessageBox.Show("Please enter a quantity to process - must be an integer", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    int qtyCurrent = int.Parse(((Label)drMain.CurrentItem.Controls["lblQuantity"]).Text);

                    if (qtyToProcess == qtyCurrent)
                    {
                        MessageBox.Show("Quantity to process is the same as the in stock quantity. Please use this button only if there is a change in the quantity",
                            Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        frmChange frm = new frmChange();
                        if (qtyToProcess < qtyCurrent)
                            frm.lblInformation.Text = string.Format("Decrease in quantity from {0} to {1}", qtyCurrent.ToString(), qtyToProcess.ToString());
                        else
                            frm.lblInformation.Text = string.Format("Increase in quantity from {0} to {1}", qtyCurrent.ToString(), qtyToProcess.ToString());

                        frm.ShowDialog();
                        if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                        {
                            //Save change to quantity in stock level and create transaction record
                            Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)ThisQuantityToProcess.Parent;
                            Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)ThisQuantityToProcess.Parent.Parent;
                            TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
                            TabletStockLevel stockLevel = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
                            int? prevQuantity = stockLevel.Quantity;
                            stockLevel.Quantity = qtyToProcess;
                            int? prevShippableYear = stockLevel.ShippableYear;
                            TabletStockLevelLogic.UpdateStockLevel(stockLevel, Globals.Action.QuantityChange, Properties.Settings.Default.UserId, (int)prevQuantity, (int)stockLevel.Quality,
                                    (int)stockLevel.ShippableWeek, (int)stockLevel.ShippableYear, frm.cboReasonForChange.Text);
                            ThisQuantityToProcess.Text = qtyToProcess.ToString();
                            CurrQuantity.Text = qtyToProcess.ToString();
                        }
                        else
                        {
                            ThisQuantityToProcess.Text = qtyCurrent.ToString();
                        }
                    }
                }
            }
        }

        private void chkQuery_CheckedChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                CheckBox chk = (CheckBox)sender;
                Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)chk.Parent;
                Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)chk.Parent.Parent;
                if (dataRepeater != null)
                {
                    ComboBox cboQ = (ComboBox)dataRepeaterItem.Controls.Find("cboQueryType", false)[0];
                    TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
                    cboQ.Visible = chk.Checked;
                    //Update stock level record
                    TabletStockLevel stockLevel = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
                    Label Quantity = (Label)drMain.CurrentItem.Controls["lblQuantity"];
                    Label Quality = (Label)drMain.CurrentItem.Controls["lblQualityId"];
                    TextBox ThisShippableWeek = (TextBox)drMain.CurrentItem.Controls["txtShippableWeek"];
                    stockLevel.Query = chk.Checked;
                    if (!chk.Checked)
                        stockLevel.QueryType = null;
                    int? prevShippableYear = stockLevel.ShippableYear;
                    TabletStockLevelLogic.UpdateStockLevel(stockLevel, Globals.Action.Other, Properties.Settings.Default.UserId, int.Parse(Quantity.Text), int.Parse(Quality.Text),
                        int.Parse(ThisShippableWeek.Text), prevShippableYear);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            locNode = locNode.Next;
            LoadDataRepeater();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            locNode = locNode.Previous;
            LoadDataRepeater();
        }

        private void cboQueryType_Leave(object sender, EventArgs e)
        {
            if (!loading)
            {
                ComboBox cbo = (ComboBox)sender;
                Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)cbo.Parent;
                Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)cbo.Parent.Parent;
                TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];

                bool reloadCombo = false;
                int? configItem;
                if (cbo.SelectedValue == null && cbo.Text != string.Empty)
                {
                    TabletConfigurationItem cfi = new TabletConfigurationItem();
                    cfi.ItemValue = cbo.Text;
                    cfi.ConfigurationGroup = (int?)ConfigGroup.QueryType;
                    string msg;
                    configItem = TabletConfigurationLogic.AddConfigurationItem(cfi, out msg);
                    if (configItem == 0)
                    {
                        MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        cbo.Text = string.Empty;
                        cbo.SelectedValue = -1;
                        cbo.DroppedDown = true;
                        return;
                    }
                    else
                        reloadCombo = true;
                }
                else
                {
                    if ((int)cbo.SelectedValue == -1)
                        configItem = null;
                    else
                        configItem = (int)cbo.SelectedValue;
                }

                //Update stock level record
                TabletStockLevel stockLevel = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
                stockLevel.QueryType = configItem;
                int? prevShippableYear = stockLevel.ShippableYear;
                TabletStockLevelLogic.UpdateStockLevel(stockLevel, Globals.Action.Other, Properties.Settings.Default.UserId, (int)stockLevel.Quantity, (int)stockLevel.Quality,
                    (int)stockLevel.ShippableWeek, (int)stockLevel.ShippableYear);
                if (reloadCombo)
                {
                    foreach (DataRepeaterItem dri in drMain.Controls)
                    {
                        if (dri == dataRepeaterItem && configItem != null)
                            PopulateQueryTypeCombo(dri, (int)configItem);
                        else
                            PopulateQueryTypeCombo(dri);
                    }
                }
            }
        }

        private void btnSetNewQuality_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            Microsoft.VisualBasic.PowerPacks.DataRepeaterItem dataRepeaterItem = (Microsoft.VisualBasic.PowerPacks.DataRepeaterItem)btn.Parent;
            Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater = (Microsoft.VisualBasic.PowerPacks.DataRepeater)btn.Parent.Parent;
            TabletStockLevelRow source = ((List<TabletStockLevelRow>)dataRepeater.DataSource)[dataRepeaterItem.ItemIndex];
            TabletStockLevel stockLevel = TabletStockLevelLogic.RetrieveStockLevelFromId(source.StockLevelId);
            TextBox ThisQuantityToProcess = (TextBox)drMain.CurrentItem.Controls["txtQuantityToProcess"];
            int qtyToProcess = int.Parse(ThisQuantityToProcess.Text);
            switch (btn.Name)
            {
                case "btnDispose":
                    {
                        TabletStockLevelLogic.SetNewQuality(stockLevel, Globals.CropQuality.Dispose, Properties.Settings.Default.UserId, qtyToProcess);
                        break;
                    }
                case "btnMove":
                    {
                        TabletStockLevelLogic.SetNewQuality(stockLevel, Globals.CropQuality.MoveOn, Properties.Settings.Default.UserId, qtyToProcess);
                        break;
                    }
                case "btnShippable":
                    {
                        TabletStockLevelLogic.SetNewQuality(stockLevel, Globals.CropQuality.Shippable, Properties.Settings.Default.UserId, qtyToProcess);
                        break;
                    }
                case "btnGrowingOn":
                    {
                        TabletStockLevelLogic.SetNewQuality(stockLevel, Globals.CropQuality.GrowingOn, Properties.Settings.Default.UserId, qtyToProcess);
                        break;
                    }
            }
            int currIndex = drMain.CurrentItemIndex;
            LoadDataRepeater();
            drMain.ScrollItemIntoView(currIndex, true);
        }


    }
}
