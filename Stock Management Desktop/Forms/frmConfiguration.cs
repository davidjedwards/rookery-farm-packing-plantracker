﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;
using RFP;
using Stock_Management_Desktop.Classes;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmConfiguration : Stock_Management_Desktop.Forms.frmBase
    {
        Globals.Permission perm;

        public frmConfiguration()
        {
            InitializeComponent();
        }

        private void frmConfiguration_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;

            dgvConfigItems.AutoGenerateColumns = false;
            Functions.FormatButton(btnClose);
            Functions.StyleDataGridView(dgvConfigItems);

            DataTable cfgdt = new DataTable();
            cfgdt.Columns.Add("Id", typeof(int));
            cfgdt.Columns.Add("GroupName", typeof(string));
            DataRow cfgr = cfgdt.NewRow();
            cfgr["Id"] = 0;
            cfgr["GroupName"] = string.Empty;
            cfgdt.Rows.Add(cfgr);

            List<ConfigurationGroup> configGroups = ConfigurationLogic.ListConfigGroups();
            foreach (ConfigurationGroup cfg in configGroups)
            {
                cfgr = cfgdt.NewRow();
                cfgr["Id"] = cfg.Id;
                cfgr["GroupName"] = cfg.GroupName;
                cfgdt.Rows.Add(cfgr);
            }

            cboConfigGroup.DataSource = cfgdt;
            cboConfigGroup.ValueMember = "Id";
            cboConfigGroup.DisplayMember = "GroupName";

            perm = UserLogic.GetPermission(Properties.Settings.Default.UserId);

            if (perm == Globals.Permission.Read_Only)
            {
                dgvConfigItems.ReadOnly = true;
            }
            loading = false;
        }

        private void cboConfigGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
                RefreshGrid();   
        }

        private void RefreshGrid()
        {
            gridLoading = true;

            if ((int)cboConfigGroup.SelectedValue == 0)
            {
                dgvConfigItems.AllowUserToAddRows = false;
                dgvConfigItems.DataSource = null;
            }
            else
            {
                dgvConfigItems.DataSource = ConfigurationLogic.ListConfigurationItemsAsBindingSource(
                    (ConfigGroup)cboConfigGroup.SelectedValue, chkShowInactive.Checked);
                if (perm == Globals.Permission.Full)
                {
                    dgvConfigItems.AllowUserToAddRows = true;
                }
            }

            gridLoading = false;
        }

        private void chkShowInactive_CheckedChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void dgvConfigItems_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && dgvConfigItems.CurrentRow.Index >= 0 && !dgvConfigItems.CurrentRow.IsNewRow &&
                dgvConfigItems.CurrentRow.Cells["IdColumn"].Value != null)
            {
                ConfigurationItem thisItem = (ConfigurationItem)dgvConfigItems.CurrentRow.DataBoundItem;
                string msg = string.Empty;
                if (thisItem.Id == 0)
                {
                    //new record
                    thisItem.ConfigurationGroup = (int)cboConfigGroup.SelectedValue;
                    int thisId = ConfigurationLogic.AddConfigurationItem(thisItem, out msg);
                    if (thisId == 0) //Valiation failed
                    {
                        MessageBox.Show("New record failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dgvConfigItems.CurrentRow.ErrorText = msg;
                        dgvConfigItems.CurrentCell.Selected = false;
                        GridIsValid = false;
                        e.Cancel = true;
                    }
                    else
                    {
                        dgvConfigItems.CurrentRow.ErrorText = string.Empty;
                        GridIsValid = true;
                        thisItem.Id = thisId;
                    }
                }
                else //update existing record
                {
                    if (!ConfigurationLogic.UpdateConfigurationItem(thisItem, out msg))
                    {
                        MessageBox.Show("Changes failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dgvConfigItems.CurrentRow.ErrorText = msg;
                        dgvConfigItems.CurrentCell.Selected = false;
                        GridIsValid = false;
                        e.Cancel = true;
                    }
                    else
                    {
                        dgvConfigItems.CurrentRow.ErrorText = string.Empty;
                        GridIsValid = true;
                    }
                }
            }
        }

        private void dgvConfigItems_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvConfigItems.Columns["DeleteColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    if (dgvConfigItems.Rows[e.RowIndex].IsNewRow || (bool)dgvConfigItems.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                    {
                        e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                    else
                    {
                        e.Graphics.DrawImage(Properties.Resources.refresh, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                }
            }
        }
 
        private void dgvConfigItems_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if ((dgvConfigItems.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewTextBoxCell) ||
                    dgvConfigItems.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewComboBoxCell)) &&
                    !dgvConfigItems.Rows[e.RowIndex].IsNewRow &&
                    !(bool)dgvConfigItems.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                {
                    dgvConfigItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Properties.Settings.Default.InactiveColour;
                    dgvConfigItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Properties.Settings.Default.InactiveColour;
                }
                else
                {
                    dgvConfigItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                    dgvConfigItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Black;
                }
            }
        }

        private void dgvConfigItems_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Deal with click on the Delete button
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvConfigItems.Columns["DeleteColumn"].Index)
                {
                    if (!dgvConfigItems.Rows[e.RowIndex].IsNewRow)
                    {
                        ConfigurationItem thisItem = (ConfigurationItem)dgvConfigItems.CurrentRow.DataBoundItem;
                        if (thisItem.Active)
                        {
                            if (MessageBox.Show("Are you sure you want to set this item to inactive?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            {
                                thisItem.Active = false;
                                dgvConfigItems.Rows[e.RowIndex].Cells["ActiveColumn"].Value = false;
                            }
                        }
                        else
                        {
                            thisItem.Active = true;
                            dgvConfigItems.Rows[e.RowIndex].Cells["ActiveColumn"].Value = true;
                        }
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
