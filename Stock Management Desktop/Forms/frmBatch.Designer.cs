﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmBatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearchComponent = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblComponentCode = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvBatches = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComponentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComponentCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvBatchHistory = new System.Windows.Forms.DataGridView();
            this.HistoryIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocationCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReasonColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionedByColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUndoLast = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.pnlFilter.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchHistory)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnAdd);
            this.pnlFooter.Location = new System.Drawing.Point(0, 443);
            this.pnlFooter.Size = new System.Drawing.Size(990, 44);
            // 
            // pnlFilter
            // 
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFilter.Controls.Add(this.btnReset);
            this.pnlFilter.Controls.Add(this.btnSearchComponent);
            this.pnlFilter.Controls.Add(this.panel2);
            this.pnlFilter.Controls.Add(this.dtpTo);
            this.pnlFilter.Controls.Add(this.dtpFrom);
            this.pnlFilter.Controls.Add(this.label3);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(990, 42);
            this.pnlFilter.TabIndex = 3;
            // 
            // btnReset
            // 
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::Stock_Management_Desktop.Properties.Resources.refresh;
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(738, 3);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(81, 34);
            this.btnReset.TabIndex = 11;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearchComponent
            // 
            this.btnSearchComponent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchComponent.Image = global::Stock_Management_Desktop.Properties.Resources.search;
            this.btnSearchComponent.Location = new System.Drawing.Point(293, 6);
            this.btnSearchComponent.Name = "btnSearchComponent";
            this.btnSearchComponent.Size = new System.Drawing.Size(34, 29);
            this.btnSearchComponent.TabIndex = 10;
            this.btnSearchComponent.UseVisualStyleBackColor = true;
            this.btnSearchComponent.Click += new System.EventHandler(this.btnSearchComponent_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblComponentCode);
            this.panel2.Location = new System.Drawing.Point(87, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 25);
            this.panel2.TabIndex = 9;
            // 
            // lblComponentCode
            // 
            this.lblComponentCode.AutoSize = true;
            this.lblComponentCode.Location = new System.Drawing.Point(3, 3);
            this.lblComponentCode.Name = "lblComponentCode";
            this.lblComponentCode.Size = new System.Drawing.Size(0, 16);
            this.lblComponentCode.TabIndex = 8;
            // 
            // dtpTo
            // 
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpTo.Location = new System.Drawing.Point(629, 9);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(91, 23);
            this.dtpTo.TabIndex = 7;
            this.dtpTo.ValueChanged += new System.EventHandler(this.dtpRange_ValueChanged);
            // 
            // dtpFrom
            // 
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFrom.Location = new System.Drawing.Point(497, 9);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(91, 23);
            this.dtpFrom.TabIndex = 6;
            this.dtpFrom.ValueChanged += new System.EventHandler(this.dtpRange_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(594, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "and";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(363, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Batch Dates between";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Component";
            // 
            // dgvBatches
            // 
            this.dgvBatches.AllowUserToAddRows = false;
            this.dgvBatches.AllowUserToDeleteRows = false;
            this.dgvBatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBatches.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.ComponentColumn,
            this.ComponentCodeColumn,
            this.DescriptionColumn,
            this.BatchDateColumn,
            this.BatchNoColumn});
            this.dgvBatches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBatches.Location = new System.Drawing.Point(0, 0);
            this.dgvBatches.Name = "dgvBatches";
            this.dgvBatches.ReadOnly = true;
            this.dgvBatches.RowHeadersVisible = false;
            this.dgvBatches.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBatches.Size = new System.Drawing.Size(546, 401);
            this.dgvBatches.TabIndex = 4;
            this.dgvBatches.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvBatches_CellBeginEdit);
            this.dgvBatches.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatches_RowEnter);
            this.dgvBatches.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatches_RowValidated);
            this.dgvBatches.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvBatches_RowValidating);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // ComponentColumn
            // 
            this.ComponentColumn.DataPropertyName = "Component";
            this.ComponentColumn.HeaderText = "";
            this.ComponentColumn.Name = "ComponentColumn";
            this.ComponentColumn.ReadOnly = true;
            this.ComponentColumn.Visible = false;
            this.ComponentColumn.Width = 98;
            // 
            // ComponentCodeColumn
            // 
            this.ComponentCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ComponentCodeColumn.DataPropertyName = "RetailerStockCode";
            this.ComponentCodeColumn.HeaderText = "Component";
            this.ComponentCodeColumn.Name = "ComponentCodeColumn";
            this.ComponentCodeColumn.ReadOnly = true;
            this.ComponentCodeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ComponentCodeColumn.Width = 98;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.DataPropertyName = "Description";
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.ReadOnly = true;
            this.DescriptionColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // BatchDateColumn
            // 
            this.BatchDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BatchDateColumn.DataPropertyName = "BatchDate";
            dataGridViewCellStyle1.Format = "d";
            this.BatchDateColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.BatchDateColumn.HeaderText = "Batch Date";
            this.BatchDateColumn.Name = "BatchDateColumn";
            this.BatchDateColumn.ReadOnly = true;
            this.BatchDateColumn.Width = 94;
            // 
            // BatchNoColumn
            // 
            this.BatchNoColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BatchNoColumn.DataPropertyName = "BatchNo";
            this.BatchNoColumn.HeaderText = "Batch Number";
            this.BatchNoColumn.Name = "BatchNoColumn";
            this.BatchNoColumn.ReadOnly = true;
            this.BatchNoColumn.Width = 113;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvBatches);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvBatchHistory);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(990, 401);
            this.splitContainer1.SplitterDistance = 546;
            this.splitContainer1.TabIndex = 5;
            // 
            // dgvBatchHistory
            // 
            this.dgvBatchHistory.AllowUserToAddRows = false;
            this.dgvBatchHistory.AllowUserToDeleteRows = false;
            this.dgvBatchHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBatchHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HistoryIdColumn,
            this.ActionDateColumn,
            this.LocationCodeColumn,
            this.QuantityColumn,
            this.QualityColumn,
            this.ActionColumn,
            this.ReasonColumn,
            this.ActionedByColumn});
            this.dgvBatchHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBatchHistory.Location = new System.Drawing.Point(0, 42);
            this.dgvBatchHistory.Name = "dgvBatchHistory";
            this.dgvBatchHistory.ReadOnly = true;
            this.dgvBatchHistory.RowHeadersVisible = false;
            this.dgvBatchHistory.Size = new System.Drawing.Size(440, 359);
            this.dgvBatchHistory.TabIndex = 1;
            // 
            // HistoryIdColumn
            // 
            this.HistoryIdColumn.DataPropertyName = "BatchHistoryId";
            this.HistoryIdColumn.HeaderText = "";
            this.HistoryIdColumn.Name = "HistoryIdColumn";
            this.HistoryIdColumn.ReadOnly = true;
            this.HistoryIdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.HistoryIdColumn.Visible = false;
            // 
            // ActionDateColumn
            // 
            this.ActionDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ActionDateColumn.DataPropertyName = "ActionDate";
            dataGridViewCellStyle2.Format = "d";
            this.ActionDateColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.ActionDateColumn.HeaderText = "Date";
            this.ActionDateColumn.Name = "ActionDateColumn";
            this.ActionDateColumn.ReadOnly = true;
            this.ActionDateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActionDateColumn.Width = 40;
            // 
            // LocationCodeColumn
            // 
            this.LocationCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LocationCodeColumn.DataPropertyName = "LocationCode";
            this.LocationCodeColumn.HeaderText = "Location";
            this.LocationCodeColumn.Name = "LocationCodeColumn";
            this.LocationCodeColumn.ReadOnly = true;
            this.LocationCodeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LocationCodeColumn.Width = 61;
            // 
            // QuantityColumn
            // 
            this.QuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QuantityColumn.DataPropertyName = "Quantity";
            this.QuantityColumn.HeaderText = "Quantity";
            this.QuantityColumn.Name = "QuantityColumn";
            this.QuantityColumn.ReadOnly = true;
            this.QuantityColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QuantityColumn.Width = 61;
            // 
            // QualityColumn
            // 
            this.QualityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QualityColumn.DataPropertyName = "Quality";
            this.QualityColumn.HeaderText = "Quality";
            this.QualityColumn.Name = "QualityColumn";
            this.QualityColumn.ReadOnly = true;
            this.QualityColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QualityColumn.Width = 53;
            // 
            // ActionColumn
            // 
            this.ActionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ActionColumn.DataPropertyName = "Action";
            this.ActionColumn.HeaderText = "Action";
            this.ActionColumn.Name = "ActionColumn";
            this.ActionColumn.ReadOnly = true;
            this.ActionColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActionColumn.Width = 49;
            // 
            // ReasonColumn
            // 
            this.ReasonColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ReasonColumn.DataPropertyName = "Reason";
            this.ReasonColumn.HeaderText = "Reason";
            this.ReasonColumn.MinimumWidth = 73;
            this.ReasonColumn.Name = "ReasonColumn";
            this.ReasonColumn.ReadOnly = true;
            this.ReasonColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ActionedByColumn
            // 
            this.ActionedByColumn.DataPropertyName = "ActionedBy";
            this.ActionedByColumn.HeaderText = "User";
            this.ActionedByColumn.Name = "ActionedByColumn";
            this.ActionedByColumn.ReadOnly = true;
            this.ActionedByColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnUndoLast);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(440, 42);
            this.panel1.TabIndex = 0;
            // 
            // btnUndoLast
            // 
            this.btnUndoLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndoLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUndoLast.Image = global::Stock_Management_Desktop.Properties.Resources.refresh;
            this.btnUndoLast.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndoLast.Location = new System.Drawing.Point(331, 4);
            this.btnUndoLast.Name = "btnUndoLast";
            this.btnUndoLast.Size = new System.Drawing.Size(103, 34);
            this.btnUndoLast.TabIndex = 1;
            this.btnUndoLast.Text = "Undo Last";
            this.btnUndoLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUndoLast.UseVisualStyleBackColor = true;
            this.btnUndoLast.Click += new System.EventHandler(this.btnUndoLast_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(188, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Batch History";
            // 
            // btnAdd
            // 
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Image = global::Stock_Management_Desktop.Properties.Resources.add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(17, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(101, 30);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add Batch";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // frmBatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(990, 487);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmBatch";
            this.Text = "Batch Maintenance";
            this.Load += new System.EventHandler(this.frmBatch_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatches)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchHistory)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvBatches;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvBatchHistory;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSearchComponent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblComponentCode;
        private System.Windows.Forms.Button btnUndoLast;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComponentColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComponentCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchNoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocationCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReasonColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionedByColumn;
        private System.Windows.Forms.Button btnReset;
    }
}
