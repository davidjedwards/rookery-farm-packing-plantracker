﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmBatch : Stock_Management_Desktop.Forms.frmBase
    {
        RFP.Component selectedComponent;
        User loggedInUser; 

        public frmBatch()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmBatchAdd frm = new frmBatchAdd();
            frm.openingForm = this;
            frm.ShowDialog();
        }

        private void btnSearchComponent_Click(object sender, EventArgs e)
        {
            frmComponentMaintenance frm = new frmComponentMaintenance();
            frm.SelectMode = true;
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                selectedComponent = frm.selectedComponent;
                lblComponentCode.Text = selectedComponent.RetailerStockCode;
                LoadBatchGrid();
            }
            frm.Dispose();
        }
        
        private void frmBatch_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;

            Functions.FormatButton(btnAdd);
            Functions.FormatButton(btnSearchComponent);
            Functions.FormatButton(btnUndoLast);
            Functions.StyleDataGridView(dgvBatches);
            Functions.StyleDataGridView(dgvBatchHistory);

            loggedInUser = UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
            EntityName = Globals.Entities.Batch;

            //Override style for dgvBatchHistory so that selected cells not shown as selected
            dgvBatchHistory.RowsDefaultCellStyle.SelectionBackColor = dgvBatchHistory.RowsDefaultCellStyle.BackColor;
            dgvBatchHistory.RowsDefaultCellStyle.SelectionForeColor = Color.Black;

            SetPermissions();

            //Default range to this year
            dtpFrom.Value = DateTime.Parse("01/01/" + DateTime.Today.Year.ToString());
            dtpTo.Value = DateTime.Parse("31/12/" + DateTime.Today.Year.ToString());
            selectedComponent = null;
            LoadBatchGrid();

            loading = false;
        }

        private void SetPermissions()
        {
            Globals.Permission perm = (Globals.Permission)loggedInUser.Permission;
            if (perm == Globals.Permission.Full)
                IsReadOnly = false;
            else
                IsReadOnly = true;

            dgvBatches.ReadOnly = IsReadOnly;
            btnAdd.Visible = !IsReadOnly;
            btnUndoLast.Visible = !IsReadOnly;
        }

        public void LoadBatchGrid(int selectedId = 0)
        {
            gridLoading = true;
            int CurrTopRow = dgvBatches.FirstDisplayedScrollingRowIndex;
            if (CurrTopRow < 0)
                CurrTopRow = 0;

            int selectedComponentId = 0;
            if (selectedComponent != null)
                selectedComponentId = selectedComponent.Id;

            dgvBatches.DataSource = BLL.BatchLogic.ListBatches(selectedComponentId, dtpFrom.Value, dtpTo.Value);

            if (selectedId != 0)
            {
                dgvBatches.FirstDisplayedScrollingRowIndex = CurrTopRow;
                int RowOfSelItem = 0;
                foreach (DataGridViewRow r in dgvBatches.Rows)
                {
                    if (((Nullable<int>)r.Cells["IdColumn"].Value ?? 0) == selectedId)
                        RowOfSelItem = r.Index;
                }
                dgvBatches.CurrentCell = dgvBatches.Rows[RowOfSelItem].Cells["BatchDateColumn"];
            }
            if (dgvBatches.RowCount > 0)
                recordId = (int)dgvBatches.CurrentRow.Cells["IdColumn"].Value;
            else
                recordId = 0;

            gridLoading = false;
            GridIsValid = true;
        }

        private void dgvBatches_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && e.RowIndex >= 0)
            {
                Batch thisBatch = BatchLogic.RetrieveBatchFromId((int)dgvBatches.Rows[e.RowIndex].Cells["IdColumn"].Value);
                string msg;
                if (dgvBatches.IsCurrentRowDirty)
                {
                    if (!BatchLogic.UpdateBatch(thisBatch, Properties.Settings.Default.UserId, out msg))
                    {
                        MessageBox.Show("Changes failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dgvBatches.CurrentRow.ErrorText = msg;
                        dgvBatches.CurrentCell.Selected = false;
                        GridIsValid = false;
                        e.Cancel = true;
                    }
                    else
                    {
                        dgvBatches.CurrentRow.ErrorText = string.Empty;
                        GridIsValid = true;
                    }
                }
            }
        }

        private void dgvBatches_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            recordId = (int)dgvBatches.Rows[e.RowIndex].Cells["IdColumn"].Value;
            if (!IsReadOnly)
                dgv_RowEnter(sender, e);

            //Display batch history for this batch
            dgvBatchHistory.DataSource = BLL.BatchLogic.RetrieveBatchHistory(recordId);
        }

        private void dgvBatches_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            recordId = (int)dgvBatches.Rows[e.RowIndex].Cells["IdColumn"].Value;
            dgv_CellBeginEdit(sender, e);
        }

        private void dgvBatches_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            dgv_RowValidated(sender, e);
        }

        private void dtpRange_ValueChanged(object sender, EventArgs e)
        {
            LoadBatchGrid();
        }

        private void btnUndoLast_Click(object sender, EventArgs e)
        {
            string msg;
            string question;
            if (dgvBatchHistory.Rows.Count == 0)
            {
                MessageBox.Show("Nothing to undo", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                int batchHistoryId = (int)dgvBatchHistory.Rows[0].Cells["HistoryIdColumn"].Value;
                BatchHistory bh = BatchLogic.RetrieveBatchHistoryRecordFromId(batchHistoryId);
                switch ((Globals.Action)bh.Action)
                {
                    case Globals.Action.Added:
                        question = "This will result in the batch being deleted. Do you want to continue?";
                        break;
                    default:
                        question = "Are you sure you want to undo the last transaction?";
                        break;
                }
                if (MessageBox.Show(question, Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == 
                            System.Windows.Forms.DialogResult.Yes)
                {
                    int selectedBatch = (int)bh.Batch;
                    if (!BatchLogic.UndoBatchHistory(bh, Properties.Settings.Default.UserId, out msg))
                        MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    
                    LoadBatchGrid(selectedBatch);
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            dtpFrom.Value = DateTime.Parse("01/01/" + DateTime.Today.Year.ToString());
            dtpTo.Value = DateTime.Parse("31/12/" + DateTime.Today.Year.ToString());
            selectedComponent = null;
            LoadBatchGrid();
        }
    }
}
