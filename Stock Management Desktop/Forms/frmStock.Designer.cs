﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblQuality = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblBatchDate = new System.Windows.Forms.Label();
            this.lblBatchNo = new System.Windows.Forms.Label();
            this.lblComponent = new System.Windows.Forms.Label();
            this.txtQuantityToProcess = new System.Windows.Forms.TextBox();
            this.chkQuery = new System.Windows.Forms.CheckBox();
            this.cboQueryType = new System.Windows.Forms.ComboBox();
            this.nudShippableYear = new System.Windows.Forms.NumericUpDown();
            this.nudShippableWeek = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlMark = new System.Windows.Forms.Panel();
            this.btnShippable = new System.Windows.Forms.Button();
            this.btnForDisposal = new System.Windows.Forms.Button();
            this.btnForMoving = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnDispose = new System.Windows.Forms.Button();
            this.btnPick = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.btnChangeQty = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlGrowingOn = new System.Windows.Forms.Panel();
            this.btnMarkAsGrowingOn = new System.Windows.Forms.Button();
            this.pnlShippable = new System.Windows.Forms.Panel();
            this.btnForPicking = new System.Windows.Forms.Button();
            this.btnMarkAsGrowingOn1 = new System.Windows.Forms.Button();
            this.pnlPick = new System.Windows.Forms.Panel();
            this.btnBackToShippable = new System.Windows.Forms.Button();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableWeek)).BeginInit();
            this.pnlMark.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlGrowingOn.SuspendLayout();
            this.pnlShippable.SuspendLayout();
            this.pnlPick.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnClose);
            this.pnlFooter.Location = new System.Drawing.Point(0, 365);
            this.pnlFooter.Size = new System.Drawing.Size(512, 44);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Location";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Component";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Batch No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(167, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Batch Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Quantity to process";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(167, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Quality";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(87, 330);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = "Query Type";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(91, 9);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(0, 16);
            this.lblLocation.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblQuality);
            this.panel1.Controls.Add(this.lblQuantity);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lblBatchDate);
            this.panel1.Controls.Add(this.lblBatchNo);
            this.panel1.Controls.Add(this.lblComponent);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblLocation);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(512, 139);
            this.panel1.TabIndex = 10;
            // 
            // lblQuality
            // 
            this.lblQuality.AutoSize = true;
            this.lblQuality.Location = new System.Drawing.Point(246, 105);
            this.lblQuality.Name = "lblQuality";
            this.lblQuality.Size = new System.Drawing.Size(0, 16);
            this.lblQuality.TabIndex = 15;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(91, 105);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(0, 16);
            this.lblQuantity.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Quantity";
            // 
            // lblBatchDate
            // 
            this.lblBatchDate.AutoSize = true;
            this.lblBatchDate.Location = new System.Drawing.Point(246, 73);
            this.lblBatchDate.Name = "lblBatchDate";
            this.lblBatchDate.Size = new System.Drawing.Size(0, 16);
            this.lblBatchDate.TabIndex = 12;
            // 
            // lblBatchNo
            // 
            this.lblBatchNo.AutoSize = true;
            this.lblBatchNo.Location = new System.Drawing.Point(91, 73);
            this.lblBatchNo.Name = "lblBatchNo";
            this.lblBatchNo.Size = new System.Drawing.Size(0, 16);
            this.lblBatchNo.TabIndex = 11;
            // 
            // lblComponent
            // 
            this.lblComponent.AutoSize = true;
            this.lblComponent.Location = new System.Drawing.Point(91, 41);
            this.lblComponent.Name = "lblComponent";
            this.lblComponent.Size = new System.Drawing.Size(0, 16);
            this.lblComponent.TabIndex = 10;
            // 
            // txtQuantityToProcess
            // 
            this.txtQuantityToProcess.CausesValidation = false;
            this.txtQuantityToProcess.Location = new System.Drawing.Point(132, 155);
            this.txtQuantityToProcess.Name = "txtQuantityToProcess";
            this.txtQuantityToProcess.Size = new System.Drawing.Size(100, 23);
            this.txtQuantityToProcess.TabIndex = 11;
            // 
            // chkQuery
            // 
            this.chkQuery.AutoSize = true;
            this.chkQuery.Location = new System.Drawing.Point(8, 329);
            this.chkQuery.Name = "chkQuery";
            this.chkQuery.Size = new System.Drawing.Size(61, 20);
            this.chkQuery.TabIndex = 16;
            this.chkQuery.Text = "Query";
            this.chkQuery.UseVisualStyleBackColor = true;
            this.chkQuery.CheckedChanged += new System.EventHandler(this.chkQuery_CheckedChanged);
            // 
            // cboQueryType
            // 
            this.cboQueryType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboQueryType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboQueryType.Enabled = false;
            this.cboQueryType.FormattingEnabled = true;
            this.cboQueryType.Location = new System.Drawing.Point(171, 327);
            this.cboQueryType.Name = "cboQueryType";
            this.cboQueryType.Size = new System.Drawing.Size(321, 24);
            this.cboQueryType.TabIndex = 17;
            // 
            // nudShippableYear
            // 
            this.nudShippableYear.Location = new System.Drawing.Point(171, 297);
            this.nudShippableYear.Maximum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.nudShippableYear.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.nudShippableYear.Name = "nudShippableYear";
            this.nudShippableYear.Size = new System.Drawing.Size(57, 23);
            this.nudShippableYear.TabIndex = 28;
            this.nudShippableYear.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            // 
            // nudShippableWeek
            // 
            this.nudShippableWeek.Location = new System.Drawing.Point(115, 297);
            this.nudShippableWeek.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudShippableWeek.Name = "nudShippableWeek";
            this.nudShippableWeek.Size = new System.Drawing.Size(51, 23);
            this.nudShippableWeek.TabIndex = 27;
            this.nudShippableWeek.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 299);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 16);
            this.label11.TabIndex = 26;
            this.label11.Text = "Shippable Week";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 16);
            this.label10.TabIndex = 31;
            this.label10.Text = "Mark";
            // 
            // pnlMark
            // 
            this.pnlMark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMark.Controls.Add(this.btnShippable);
            this.pnlMark.Controls.Add(this.btnForDisposal);
            this.pnlMark.Controls.Add(this.btnForMoving);
            this.pnlMark.Location = new System.Drawing.Point(76, 189);
            this.pnlMark.Name = "pnlMark";
            this.pnlMark.Size = new System.Drawing.Size(416, 41);
            this.pnlMark.TabIndex = 32;
            // 
            // btnShippable
            // 
            this.btnShippable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShippable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnShippable.Image = global::Stock_Management_Desktop.Properties.Resources.Flowers16x16;
            this.btnShippable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnShippable.Location = new System.Drawing.Point(282, 5);
            this.btnShippable.Name = "btnShippable";
            this.btnShippable.Size = new System.Drawing.Size(114, 29);
            this.btnShippable.TabIndex = 8;
            this.btnShippable.Text = "As Shippable";
            this.btnShippable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnShippable.UseVisualStyleBackColor = true;
            this.btnShippable.Click += new System.EventHandler(this.btnShippable_Click);
            // 
            // btnForDisposal
            // 
            this.btnForDisposal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForDisposal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnForDisposal.Image = global::Stock_Management_Desktop.Properties.Resources.trash;
            this.btnForDisposal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnForDisposal.Location = new System.Drawing.Point(150, 5);
            this.btnForDisposal.Name = "btnForDisposal";
            this.btnForDisposal.Size = new System.Drawing.Size(114, 29);
            this.btnForDisposal.TabIndex = 6;
            this.btnForDisposal.Text = "For Disposal";
            this.btnForDisposal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnForDisposal.UseVisualStyleBackColor = true;
            this.btnForDisposal.Click += new System.EventHandler(this.btnForDisposal_Click);
            // 
            // btnForMoving
            // 
            this.btnForMoving.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForMoving.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnForMoving.Image = global::Stock_Management_Desktop.Properties.Resources.rightarrow;
            this.btnForMoving.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnForMoving.Location = new System.Drawing.Point(8, 5);
            this.btnForMoving.Name = "btnForMoving";
            this.btnForMoving.Size = new System.Drawing.Size(124, 29);
            this.btnForMoving.TabIndex = 5;
            this.btnForMoving.Text = "For Moving On";
            this.btnForMoving.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnForMoving.UseVisualStyleBackColor = true;
            this.btnForMoving.Click += new System.EventHandler(this.btnForMoving_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 253);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 16);
            this.label12.TabIndex = 33;
            this.label12.Text = "Perform";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnDispose);
            this.panel3.Controls.Add(this.btnPick);
            this.panel3.Controls.Add(this.btnMove);
            this.panel3.Location = new System.Drawing.Point(76, 240);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(277, 41);
            this.panel3.TabIndex = 34;
            // 
            // btnDispose
            // 
            this.btnDispose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDispose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDispose.Image = global::Stock_Management_Desktop.Properties.Resources.trash;
            this.btnDispose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDispose.Location = new System.Drawing.Point(94, 5);
            this.btnDispose.Name = "btnDispose";
            this.btnDispose.Size = new System.Drawing.Size(85, 29);
            this.btnDispose.TabIndex = 1;
            this.btnDispose.Text = "Dispose";
            this.btnDispose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDispose.UseVisualStyleBackColor = true;
            this.btnDispose.Click += new System.EventHandler(this.btnPickDispose_Click);
            // 
            // btnPick
            // 
            this.btnPick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPick.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPick.Image = global::Stock_Management_Desktop.Properties.Resources.hand_property;
            this.btnPick.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPick.Location = new System.Drawing.Point(196, 5);
            this.btnPick.Name = "btnPick";
            this.btnPick.Size = new System.Drawing.Size(71, 29);
            this.btnPick.TabIndex = 2;
            this.btnPick.Text = "Pick";
            this.btnPick.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPick.UseVisualStyleBackColor = true;
            this.btnPick.Click += new System.EventHandler(this.btnPickDispose_Click);
            // 
            // btnMove
            // 
            this.btnMove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMove.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMove.Image = global::Stock_Management_Desktop.Properties.Resources.rightarrow;
            this.btnMove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMove.Location = new System.Drawing.Point(8, 5);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(71, 29);
            this.btnMove.TabIndex = 0;
            this.btnMove.Text = "Move";
            this.btnMove.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnChangeQty
            // 
            this.btnChangeQty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeQty.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnChangeQty.Image = global::Stock_Management_Desktop.Properties.Resources.Edit;
            this.btnChangeQty.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChangeQty.Location = new System.Drawing.Point(259, 152);
            this.btnChangeQty.Name = "btnChangeQty";
            this.btnChangeQty.Size = new System.Drawing.Size(134, 29);
            this.btnChangeQty.TabIndex = 30;
            this.btnChangeQty.Text = "Change Quantity";
            this.btnChangeQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnChangeQty.UseVisualStyleBackColor = true;
            this.btnChangeQty.Click += new System.EventHandler(this.btnChangeQty_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClose.Image = global::Stock_Management_Desktop.Properties.Resources.door_out;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(431, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(72, 29);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlGrowingOn
            // 
            this.pnlGrowingOn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGrowingOn.Controls.Add(this.btnMarkAsGrowingOn);
            this.pnlGrowingOn.Location = new System.Drawing.Point(76, 189);
            this.pnlGrowingOn.Name = "pnlGrowingOn";
            this.pnlGrowingOn.Size = new System.Drawing.Size(180, 41);
            this.pnlGrowingOn.TabIndex = 35;
            this.pnlGrowingOn.Visible = false;
            // 
            // btnMarkAsGrowingOn
            // 
            this.btnMarkAsGrowingOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarkAsGrowingOn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMarkAsGrowingOn.Image = global::Stock_Management_Desktop.Properties.Resources.Flowers16x16;
            this.btnMarkAsGrowingOn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMarkAsGrowingOn.Location = new System.Drawing.Point(8, 6);
            this.btnMarkAsGrowingOn.Name = "btnMarkAsGrowingOn";
            this.btnMarkAsGrowingOn.Size = new System.Drawing.Size(154, 29);
            this.btnMarkAsGrowingOn.TabIndex = 9;
            this.btnMarkAsGrowingOn.Text = "Back to Growing On";
            this.btnMarkAsGrowingOn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMarkAsGrowingOn.UseVisualStyleBackColor = true;
            this.btnMarkAsGrowingOn.Click += new System.EventHandler(this.btnMarkAsGrowingOn_Click);
            // 
            // pnlShippable
            // 
            this.pnlShippable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlShippable.Controls.Add(this.btnForPicking);
            this.pnlShippable.Controls.Add(this.btnMarkAsGrowingOn1);
            this.pnlShippable.Location = new System.Drawing.Point(76, 189);
            this.pnlShippable.Name = "pnlShippable";
            this.pnlShippable.Size = new System.Drawing.Size(296, 41);
            this.pnlShippable.TabIndex = 36;
            this.pnlShippable.Visible = false;
            // 
            // btnForPicking
            // 
            this.btnForPicking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForPicking.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnForPicking.Image = global::Stock_Management_Desktop.Properties.Resources.hand_property;
            this.btnForPicking.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnForPicking.Location = new System.Drawing.Point(175, 6);
            this.btnForPicking.Name = "btnForPicking";
            this.btnForPicking.Size = new System.Drawing.Size(102, 29);
            this.btnForPicking.TabIndex = 10;
            this.btnForPicking.Text = "For Picking";
            this.btnForPicking.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnForPicking.UseVisualStyleBackColor = true;
            this.btnForPicking.Click += new System.EventHandler(this.btnForPicking_Click);
            // 
            // btnMarkAsGrowingOn1
            // 
            this.btnMarkAsGrowingOn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarkAsGrowingOn1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMarkAsGrowingOn1.Image = global::Stock_Management_Desktop.Properties.Resources.Flowers16x16;
            this.btnMarkAsGrowingOn1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMarkAsGrowingOn1.Location = new System.Drawing.Point(8, 6);
            this.btnMarkAsGrowingOn1.Name = "btnMarkAsGrowingOn1";
            this.btnMarkAsGrowingOn1.Size = new System.Drawing.Size(154, 29);
            this.btnMarkAsGrowingOn1.TabIndex = 9;
            this.btnMarkAsGrowingOn1.Text = "Back to Growing On";
            this.btnMarkAsGrowingOn1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMarkAsGrowingOn1.UseVisualStyleBackColor = true;
            this.btnMarkAsGrowingOn1.Click += new System.EventHandler(this.btnMarkAsGrowingOn_Click);
            // 
            // pnlPick
            // 
            this.pnlPick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPick.Controls.Add(this.btnBackToShippable);
            this.pnlPick.Location = new System.Drawing.Point(76, 189);
            this.pnlPick.Name = "pnlPick";
            this.pnlPick.Size = new System.Drawing.Size(156, 41);
            this.pnlPick.TabIndex = 33;
            // 
            // btnBackToShippable
            // 
            this.btnBackToShippable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackToShippable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBackToShippable.Image = global::Stock_Management_Desktop.Properties.Resources.Flowers16x16;
            this.btnBackToShippable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBackToShippable.Location = new System.Drawing.Point(8, 5);
            this.btnBackToShippable.Name = "btnBackToShippable";
            this.btnBackToShippable.Size = new System.Drawing.Size(137, 29);
            this.btnBackToShippable.TabIndex = 8;
            this.btnBackToShippable.Text = "Back to Shippable";
            this.btnBackToShippable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBackToShippable.UseVisualStyleBackColor = true;
            this.btnBackToShippable.Click += new System.EventHandler(this.btnShippable_Click);
            // 
            // frmStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(512, 409);
            this.Controls.Add(this.pnlPick);
            this.Controls.Add(this.pnlShippable);
            this.Controls.Add(this.pnlGrowingOn);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pnlMark);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnChangeQty);
            this.Controls.Add(this.nudShippableYear);
            this.Controls.Add(this.nudShippableWeek);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cboQueryType);
            this.Controls.Add(this.chkQuery);
            this.Controls.Add(this.txtQuantityToProcess);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel3);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmStock";
            this.Text = "Stock Management";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmStock_Load);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.txtQuantityToProcess, 0);
            this.Controls.SetChildIndex(this.chkQuery, 0);
            this.Controls.SetChildIndex(this.cboQueryType, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.nudShippableWeek, 0);
            this.Controls.SetChildIndex(this.nudShippableYear, 0);
            this.Controls.SetChildIndex(this.btnChangeQty, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.pnlMark, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.pnlGrowingOn, 0);
            this.Controls.SetChildIndex(this.pnlShippable, 0);
            this.Controls.SetChildIndex(this.pnlPick, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableWeek)).EndInit();
            this.pnlMark.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlGrowingOn.ResumeLayout(false);
            this.pnlShippable.ResumeLayout(false);
            this.pnlPick.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPick;
        private System.Windows.Forms.Button btnDispose;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtQuantityToProcess;
        private System.Windows.Forms.CheckBox chkQuery;
        private System.Windows.Forms.ComboBox cboQueryType;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.NumericUpDown nudShippableYear;
        private System.Windows.Forms.NumericUpDown nudShippableWeek;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label lblBatchDate;
        private System.Windows.Forms.Label lblBatchNo;
        private System.Windows.Forms.Label lblComponent;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblQuality;
        private System.Windows.Forms.Button btnChangeQty;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pnlMark;
        private System.Windows.Forms.Button btnShippable;
        private System.Windows.Forms.Button btnForDisposal;
        private System.Windows.Forms.Button btnForMoving;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnlGrowingOn;
        private System.Windows.Forms.Button btnMarkAsGrowingOn;
        private System.Windows.Forms.Panel pnlShippable;
        private System.Windows.Forms.Button btnForPicking;
        private System.Windows.Forms.Button btnMarkAsGrowingOn1;
        private System.Windows.Forms.Panel pnlPick;
        private System.Windows.Forms.Button btnBackToShippable;
    }
}
