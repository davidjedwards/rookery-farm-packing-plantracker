﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Entity;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL;  

namespace Stock_Management_Desktop.Forms
{
    public partial class frmBase : Form
    {
        /// <summary>
        /// Set to form from which this form was opened
        /// </summary>
        public Form openingForm { get; set; }
        public List<clsFieldValidation> ValidationFields;
        public bool formIsClosing = false;
        public Globals.Entities EntityName { get; set; }
        public int recordId { get; set; }
        public bool loading { get; set; }
        public bool gridLoading { get; set; }
        public bool IsReadOnly { get; set; }
        public delegate void SaveChangesEventHandler();
        public event SaveChangesEventHandler SaveChanges;
        public delegate bool FurtherValidationEventHandler();
        public event FurtherValidationEventHandler FurtherValidation;

        private bool _suppress = false;
        /// <summary>
        /// Suppress the validation of mandatory items and data types
        /// </summary>
        public bool Suppress
        {
            get { return _suppress; }
            set { _suppress = value; }
        }

        private bool _dirty = false;
        /// <summary>
        /// Set when a value is changed on the screen (unless Suppress is true)
        /// </summary>
        public bool Dirty
        {
            get { return _dirty; }
            set
            {
                if (!Suppress)
                {
                    _dirty = value;
                }
                //if (_dirty)
                //{
                //    this.Text = this.Text.Replace("*", string.Empty);
                //    this.Text = this.Text.Trim() + " *";
                //}
                //else
                //{
                //    this.Text = this.Text.Replace("*", string.Empty);
                //}
            }
        }

        private bool _gridIsValid = true;
        /// <summary>
        /// Set when grid entry has been successfully validated
        /// </summary>
        public bool GridIsValid
        {
            get { return _gridIsValid; }
            set { _gridIsValid = value; }
        }

        private bool _isValid;
        /// <summary>
        /// Set to true is form validation has been successful
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (!formIsClosing)
                {
                    ValidateInput();
                }
                else
                {
                    _isValid = true;
                }
                return _isValid;
            }
            set { _isValid = value; }
        }

        public frmBase()
        {
            InitializeComponent();

        }

        private void frmBase_Load(object sender, EventArgs e)
        {
            ValidationFields = new List<clsFieldValidation>();
            this.DoubleBuffered = true;
            SetDirtyFlags(this);
        }
        
        public void dgv_RowEnter(System.Object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && !loading)
            {
                DataGridView dgv = (DataGridView)sender;
                if (dgv.Rows[e.RowIndex].ReadOnly)
                {
                    string msg;
                    if (CheckInOut.CheckOut(EntityName, recordId, Properties.Settings.Default.UserId, out msg))
                    {
                        dgv.Rows[e.RowIndex].ReadOnly = false;
                    }
                }
            }
        }

        public void dgv_CellBeginEdit(System.Object sender, System.Windows.Forms.DataGridViewCellCancelEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string msg;
            if (!CheckInOut.CheckOut(EntityName, recordId, Properties.Settings.Default.UserId, out msg))
            {
                MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                dgv.CurrentRow.ReadOnly = true;
            }
            else
            {
                dgv.CurrentRow.ReadOnly = false;
            }
        }

        public void dgv_RowValidated(System.Object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            string msg;
            if (!CheckInOut.CheckIn(EntityName, recordId, Properties.Settings.Default.UserId, out msg))
            {
                MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        

        public bool HasChanges(bool AllowClose = true)
        {
            if (!Suppress && Dirty)
            {
                DialogResult d = MessageBox.Show("Data has changed. Do you want to save your changes?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (d)
                {
                    case DialogResult.Yes:
                        if (IsValid)
                        {
                            SaveChanges();
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    case DialogResult.No:
                        formIsClosing = true;
                        Dirty = false;
                        IsValid = true;
                        return false;
                    default:
                        return true;
                }
            }
            else
            {
                return false;
            }
        }

        private void frmBase_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            e.Cancel = HasChanges() || !GridIsValid;
        }

        private void FieldValueChanged(object sender, EventArgs e)
        {
            if (!Suppress)
            {
                Dirty = true;
            }
        }

        private void SetDirtyFlags(Control ctl)
        {
            try
            {
                foreach (System.Windows.Forms.Control oCtl in ctl.Controls)
                {
                    if (oCtl.CausesValidation)
                    {
                        if (oCtl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)oCtl).TextChanged += new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(RichTextBox))
                        {
                            ((RichTextBox)oCtl).TextChanged += new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(ComboBox))
                        {
                            ((ComboBox)oCtl).TextChanged += new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(CheckBox))
                        {
                            ((CheckBox)oCtl).CheckedChanged += new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(DateTimePicker))
                        {
                            ((DateTimePicker)oCtl).ValueChanged += new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(RadioButton))
                        {
                            ((RadioButton)oCtl).CheckedChanged += new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(DataGridView))
                        {
                            ((DataGridView)oCtl).CurrentCellDirtyStateChanged += new EventHandler(FieldValueChanged);
                        }
                        if (oCtl.HasChildren)
                        {
                            SetDirtyFlags(oCtl);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void RemoveDirtyHandler(Control ctl)
        {
            try
            {
                foreach (System.Windows.Forms.Control oCtl in ctl.Controls)
                {
                    if (oCtl.CausesValidation)
                    {
                        if (oCtl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)oCtl).TextChanged -= new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(RichTextBox))
                        {
                            ((RichTextBox)oCtl).TextChanged -= new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(ComboBox))
                        {
                            ((ComboBox)oCtl).TextChanged -= new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(CheckBox))
                        {
                            ((CheckBox)oCtl).CheckedChanged -= new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(DateTimePicker))
                        {
                            ((DateTimePicker)oCtl).ValueChanged -= new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(RadioButton))
                        {
                            ((RadioButton)oCtl).CheckedChanged -= new EventHandler(FieldValueChanged);
                        }
                        else if (oCtl.GetType() == typeof(DataGridView))
                        {
                            ((DataGridView)oCtl).CurrentCellDirtyStateChanged -= new EventHandler(FieldValueChanged);
                        }
                        if (oCtl.HasChildren)
                        {
                            RemoveDirtyHandler(oCtl);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public void ResetValidationFlags()
        {
            foreach (clsFieldValidation fValidation in ValidationFields)
            {
                ep.SetError(fValidation.ControlToValidate, string.Empty);
                IsValid = true;
            }
        }

        public void ValidateInput()
        {
            IsValid = true;
            if (Dirty)
            {
                foreach (clsFieldValidation fValidation in ValidationFields)
                {
                    //Ensure that each field and its value is correct
                    if (fValidation.ControlToValidate.Enabled)
                    {
                        if (fValidation.ControlToValidate.GetType() == typeof(TextBox))
                        {
                            TextBox t = (TextBox)fValidation.ControlToValidate;
                            if (t.Text.Trim() == string.Empty && fValidation.Required)
                            {
                                ep.SetError(t, "Required field");
                                IsValid = false;
                            }
                            else
                            {
                                ep.SetError(t, string.Empty);
                            }
                            switch (fValidation.InputType)
                            {
                                case clsFieldValidation.InputTypes.Number:
                                    if (t.Text.Trim() != string.Empty)
                                    {
                                        if (!Globals.IsNumeric(t.Text))
                                        {
                                            ep.SetError(t, "Must be a number");
                                            IsValid = false;
                                        }
                                        else
                                        {
                                            ep.SetError(t, string.Empty);
                                        }
                                    }
                                    break;
                            }
                        }
                        else if (fValidation.ControlToValidate.GetType() == typeof(ComboBox))
                        {
                            ComboBox c = (ComboBox)fValidation.ControlToValidate;
                            if (c.SelectedValue.ToString() == string.Empty && fValidation.Required)
                            {
                                ep.SetError(c, "Required field");
                                IsValid = false;
                            }
                            else
                            {
                                ep.SetError(c, string.Empty);
                            }
                        }
                    }
                }
                if (!FurtherValidation())
                    IsValid = false;
            }
        }

        public void IdReqdFields()
        {
            Label asterix;
            Point loc;
            Label idLabel;

            foreach (clsFieldValidation fValidation in ValidationFields)
            {
                if (fValidation.Required)
                {
                    //Only add the label if it doesn't already exist
                    idLabel = CheckIdReqdLabelExists(this, fValidation.ControlToValidate.Name);
                    if (idLabel == null)
                    {
                        asterix = new System.Windows.Forms.Label();
                        fValidation.ControlToValidate.Parent.Controls.Add(asterix);
                        loc = fValidation.ControlToValidate.Location;
                        loc.Offset(fValidation.ControlToValidate.Width, 0);
                        asterix.Location = loc;
                        asterix.Name = "lbl_" + fValidation.ControlToValidate.Name;
                        asterix.Text = "*";
                        asterix.Width = 11;
                        asterix.ForeColor = Color.Red;
                        asterix.Visible = fValidation.ControlToValidate.Enabled;
                    }
                    else
                    {
                        idLabel.Visible = fValidation.ControlToValidate.Enabled;
                        if (idLabel.Visible)
                        {
                            loc = fValidation.ControlToValidate.Location;
                            loc.Offset(fValidation.ControlToValidate.Width, 0);
                            idLabel.Location = loc;
                        }
                    }
                }
            }
        }

        public void IdIndivReqdField(Control ctl)
        {
            Label lblAsterix;
            Point ptLoc;
            bool Exist = false;

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl.Name == "lbl_" + ctl.Name)
                {
                    Exist = true;
                    break;
                }
            }
            if (!Exist)
            {
                lblAsterix = new Label();
                ctl.Parent.Controls.Add(lblAsterix);
                ptLoc = ctl.Location;
                ptLoc.Offset(ctl.Width, 0);
                lblAsterix.Location = ptLoc;
                lblAsterix.Name = "lbl_" + ctl.Name;
                lblAsterix.Text = "*";
                lblAsterix.Width = 11;
                lblAsterix.ForeColor = Color.Red;
            }
        }

        public Label CheckIdReqdLabelExists(Control Parent, string ctlName)
        {
            Label IdLabel = null;
            foreach (Control ctl in Parent.Controls)
                if (ctl.Name == "lbl_" + ctlName)
                {
                    IdLabel = (Label)ctl;
                    break;
                }
                else
                {
                    if (ctl.HasChildren)
                    {
                        IdLabel = CheckIdReqdLabelExists(ctl, ctlName);
                        if (IdLabel != null)
                        {
                            break;
                        }
                    }
                }
                return IdLabel;
        }

        public void SetReadOnly(Control ctl)
        {
            foreach (Control oCtl in ctl.Controls)
            {
                if (oCtl.GetType() == typeof(TextBox))
                {
                    TextBox tb = (TextBox)oCtl;
                    tb.CausesValidation = false;
                    tb.ReadOnly = true;
                }
                else if (oCtl.GetType() == typeof(RichTextBox))
                {
                    RichTextBox rtb = (RichTextBox)oCtl;
                    rtb.CausesValidation = false;
                    rtb.ReadOnly = true;
                }
                else if (oCtl.GetType() == typeof(ComboBox))
                {
                    ComboBox cb = (ComboBox)oCtl;
                    cb.CausesValidation = false;
                    cb.Enabled = false;
                }
                else if (oCtl.GetType() == typeof(CheckBox))
                {
                    CheckBox chk = (CheckBox)oCtl;
                    chk.CausesValidation = false;
                    chk.Enabled = false;
                }
                else if (oCtl.GetType() == typeof(DateTimePicker))
                {
                    DateTimePicker dtp = (DateTimePicker)oCtl;
                    dtp.CausesValidation = false;
                    dtp.Enabled = false;
                }
                else if (oCtl.GetType() == typeof(RadioButton))
                {
                    RadioButton rb = (RadioButton)oCtl;
                    rb.CausesValidation = false;
                    rb.Enabled = false;
                }
                else if (oCtl.GetType() == typeof(DataGridView))
                {
                    DataGridView dgv = (DataGridView)oCtl;
                    dgv.CausesValidation = false;
                    dgv.ReadOnly = true;
                    dgv.AllowUserToAddRows = false;
                    dgv.AllowUserToDeleteRows = false;
                }
                else if (oCtl.GetType() == typeof(LinkLabel))
                {
                    LinkLabel lb = (LinkLabel)oCtl;
                    lb.Visible = false;
                }
                else if (oCtl.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown nud = (NumericUpDown)oCtl;
                    nud.CausesValidation = false;
                    nud.Enabled = false;
                }
                if (oCtl.HasChildren)
                {
                    SetReadOnly(oCtl);
                }
            }
        }

        public void UnsetReadOnly(Control ctl)
        {
            foreach (Control oCtl in ctl.Controls)
            {
                if (oCtl.GetType() == typeof(TextBox))
                {
                    TextBox tb = (TextBox)oCtl;
                    tb.CausesValidation = true;
                    tb.ReadOnly = false;
                }
                else if (oCtl.GetType() == typeof(RichTextBox))
                {
                    RichTextBox rtb = (RichTextBox)oCtl;
                    rtb.CausesValidation = true;
                    rtb.ReadOnly = false;
                }
                else if (oCtl.GetType() == typeof(ComboBox))
                {
                    ComboBox cb = (ComboBox)oCtl;
                    cb.CausesValidation = true;
                    cb.Enabled = true;
                }
                else if (oCtl.GetType() == typeof(CheckBox))
                {
                    CheckBox chk = (CheckBox)oCtl;
                    chk.CausesValidation = true;
                    chk.Enabled = true;
                }
                else if (oCtl.GetType() == typeof(DateTimePicker))
                {
                    DateTimePicker dtp = (DateTimePicker)oCtl;
                    dtp.CausesValidation = true;
                    dtp.Enabled = true;
                }
                else if (oCtl.GetType() == typeof(RadioButton))
                {
                    RadioButton rb = (RadioButton)oCtl;
                    rb.CausesValidation = true;
                    rb.Enabled = true;
                }
                else if (oCtl.GetType() == typeof(DataGridView))
                {
                    DataGridView dgv = (DataGridView)oCtl;
                    dgv.CausesValidation = true;
                    dgv.ReadOnly = false;
                    dgv.AllowUserToAddRows = true;
                    dgv.AllowUserToDeleteRows = true;
                }
                else if (oCtl.GetType() == typeof(LinkLabel))
                {
                    LinkLabel lb = (LinkLabel)oCtl;
                    lb.Visible = true;
                }
                else if (oCtl.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown nud = (NumericUpDown)oCtl;
                    nud.CausesValidation = true;
                    nud.Enabled = true;
                }
                if (oCtl.HasChildren)
                {
                    UnsetReadOnly(oCtl);
                }
            }
        }
    }
}
