﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using System.Globalization;
using RFP;
using BLL;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmBatchAdd : Stock_Management_Desktop.Forms.frmBase
    {
        RFP.Component selectedComponent;
        DateTimeFormatInfo dfi;
        Calendar cal;

        public int newBatchId { get; set; }
        

        public frmBatchAdd()
        {
            InitializeComponent();
        }

        private void frmBatchAdd_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;

            Functions.FormatButton(btnSave);
            Functions.FormatButton(btnCancel);

            //Set up validation fields
            ValidationFields.Add(new clsFieldValidation(txtBatchNo, clsFieldValidation.InputTypes.Number,true));
            ValidationFields.Add(new clsFieldValidation(dtpBatchDate, clsFieldValidation.InputTypes.DateTime, true));
            ValidationFields.Add(new clsFieldValidation(txtQuantity, clsFieldValidation.InputTypes.Number, true));
            ValidationFields.Add(new clsFieldValidation(cboQuality, clsFieldValidation.InputTypes.Number, true));
            IdReqdFields();
            IdIndivReqdField(btnSearchComponent);
            IdIndivReqdField(btnSelectLocation);

            FurtherValidation += new FurtherValidationEventHandler(FormSpecificValidation);

            //Fill combo
            //Qualities
            cboQuality.DataSource = Globals.CropQualityList();
            cboQuality.ValueMember = "Key";
            cboQuality.DisplayMember = "Value";

            //Locations
            cboLocation.DataSource = LocationLogic.DropDownList();
            cboLocation.ValueMember = "Id";
            cboLocation.DisplayMember = "Code";

            //Shippable details
            dfi = DateTimeFormatInfo.CurrentInfo;
            cal = dfi.Calendar;
           //Default year to current year
            nudShippableYear.Maximum = DateTime.Today.Year + 10;
            nudShippableYear.Value = DateTime.Today.Year;

            Suppress = false;
            loading = false;
            Dirty = true;
       }

        private bool FormSpecificValidation()
        {
            return true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Prevent save changes message appearing
            Dirty = false;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                //Further tests
                //Ensure a component has been selected
                if (selectedComponent == null)
                {
                    ep.SetError(btnSearchComponent, "Please select a component");
                    return;
                }
                else
                    ep.SetError(btnSearchComponent, string.Empty);

                //ensure batch number is an integer
                int BatchNo;
                if (!int.TryParse(txtBatchNo.Text, out BatchNo))
                {
                    ep.SetError(txtBatchNo, "Batch number must be an integer");
                    return;
                }
                else
                    ep.SetError(txtBatchNo, string.Empty);

                //ensure quantity is an integer
                int qty;
                if (!int.TryParse(txtQuantity.Text, out qty))
                {
                    ep.SetError(txtQuantity, "Quantity must be an integer");
                    return;
                }
                else
                    ep.SetError(txtQuantity, string.Empty);

                //ensure location has a non-zero value
                if ((int)cboLocation.SelectedValue == 0)
                {
                    ep.SetError(btnSelectLocation, "Please select a location");
                    return;
                }
                else
                    ep.SetError(btnSelectLocation, string.Empty);


                Batch newBatch = new Batch();
                newBatch.Component = selectedComponent.Id;
                newBatch.BatchNo = BatchNo;
                newBatch.BatchDate = dtpBatchDate.Value;
                string msg;

                newBatchId = BatchLogic.AddBatch(newBatch, Properties.Settings.Default.UserId, out msg);
                if (newBatchId == 0 && msg != string.Empty)
                {
                    MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                //Create new stock level record
                StockLevel sl = new StockLevel();
                sl.Location = (int)cboLocation.SelectedValue;
                sl.Batch = newBatchId;
                sl.Quantity = qty;
                sl.Quality = (int)cboQuality.SelectedValue;
                sl.ShippableWeek = (int)nudShippableWeek.Value;
                sl.ShippableYear = (int)nudShippableYear.Value;

                int newSL = StockLevelLogic.Add(sl, Globals.Action.Added, Properties.Settings.Default.UserId, out msg);
                if (newSL != 0 && msg != string.Empty)
                    MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                Dirty = false;
                frmBatch frm = (frmBatch)this.openingForm;
                frm.LoadBatchGrid();

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnSearchComponent_Click(object sender, EventArgs e)
        {
            frmComponentMaintenance frm = new frmComponentMaintenance();
            frm.SelectMode = true;
            frm.TopMost = true;
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                selectedComponent = frm.selectedComponent;
                lblComponentCode.Text = selectedComponent.RetailerStockCode;
            }
            frm.Dispose();

        }


        private void nudShippableYear_ValueChanged(object sender, EventArgs e)
        {
            nudShippableWeek.Maximum = cal.GetWeekOfYear(new DateTime((int)nudShippableYear.Value,12,31),dfi.CalendarWeekRule, 
                dfi.FirstDayOfWeek);
        }

        private void btnSelectLocation_Click(object sender, EventArgs e)
        {
            frmChooseLocation frm = new frmChooseLocation();
            frm.info = "Select initial location of new batch";
            frm.ShowDialog();
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                cboLocation.SelectedValue = frm.newLocation;
            }
            frm.Dispose();
        }
    }
}
