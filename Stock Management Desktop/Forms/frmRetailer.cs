﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL;
using System.Linq;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmRetailer : Stock_Management_Desktop.Forms.frmBase
    {
        Retailer currRetailer;
        bool systemPopulating = false;
        int currRow;
        bool SkipRowEnterEvent = false;
        User loggedInUser;
        Globals.Permission perm;

        public frmRetailer()
        {
            InitializeComponent();
        }

        private void frmRetailer_Load(object sender, EventArgs e)
        {
            Suppress = true; 
            loading = true;
            

            //Style form objects
            Functions.FormatButton(btnRetailer);
            Functions.StyleDataGridView(dgvRetailer);
            Functions.StyleDataGridView(dgvContacts);

            loggedInUser = UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
            SetPermissions();

            //Fill protocols combo
            cboFTPProtocol.DataSource = Globals.FTPProtocolList();
            cboFTPProtocol.ValueMember = "Key";
            cboFTPProtocol.DisplayMember = "Value";

            //Populate datagridview
            EntityName = Globals.Entities.Retailer;
            LoadRetailerGrid();


            //Set up field validation(s)
            ValidationFields.Add(new clsFieldValidation(txtName, clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new clsFieldValidation(txtFTPPort, clsFieldValidation.InputTypes.Number, false));
            IdReqdFields();
            SaveChanges += new SaveChangesEventHandler(Save);
            FurtherValidation += new FurtherValidationEventHandler(FormSpecificValidation);


            loading = false;
            Suppress = false;
        }
        
        private void LoadRetailerGrid(int SelectedId = 0)
        {
            SkipRowEnterEvent = true;

            int intCurrTopRow = dgvRetailer.FirstDisplayedScrollingRowIndex;
            if (intCurrTopRow < 0)
            {
                intCurrTopRow = 0;
            }

            dgvRetailer.DataSource = BLL.RetailerLogic.ListRetailers(chkShowDeleted.Checked);
            
            int intRowOfSelItem = 0;
            if (SelectedId != 0)
            {
                dgvRetailer.FirstDisplayedScrollingRowIndex = intCurrTopRow;
                foreach (DataGridViewRow r in dgvRetailer.Rows)
                {
                    if (int.Parse(r.Cells["IdColumn"].Value.ToString()) == SelectedId)
                    {
                        intRowOfSelItem = r.Index;
                    }
                }
                dgvRetailer.CurrentCell = dgvRetailer.Rows[intRowOfSelItem].Cells["NameColumn"];
                dgvRetailer.Rows[intRowOfSelItem].Selected = true;
            }
            if (dgvRetailer.RowCount > 0)
            {
                splitContainer1.Panel2.Enabled = true;
                currRetailer = (Retailer)dgvRetailer.Rows[intRowOfSelItem].DataBoundItem;
                PopulateFields(currRetailer);
            }
            SkipRowEnterEvent = false;
        }

        private void SetPermissions()
        {
            perm = (Globals.Permission)loggedInUser.Permission;
            if (perm == Globals.Permission.Full)
                IsReadOnly = false;
            else
                IsReadOnly = true;

            dgvContacts.ReadOnly = IsReadOnly;
            if (IsReadOnly)
                SetReadOnly(splitContainer1.Panel2);
            btnRetailer.Visible = !IsReadOnly;
            dgvRetailer.Columns["DeleteColumn"].Visible = !IsReadOnly;
            dgvContacts.Columns["DeleteContactColumn"].Visible = false;
        }

        //Display details of selected record
        private void PopulateFields(Retailer r)
        {
            systemPopulating = true;
            txtName.Text = r.Name;
            txtAddress1.Text = r.AddressLine1;
            txtAddress2.Text = r.AddressLine2;
            txtTown.Text = r.Town;
            txtCounty.Text = r.County;
            txtPostCode.Text = r.PostCode;
            txtFTPHost.Text = r.FTPHost;
            txtUserName.Text = r.FTPUserName;
            txtPassword.Text = r.FTPPassword;
            txtUploadFolder.Text = r.FTPInFolder;
            txtDownloadFolder.Text = r.FTPOutFolder;
            if (r.FTPProtocol == null)
                cboFTPProtocol.SelectedValue = -1;
            else
                cboFTPProtocol.SelectedValue = (int)r.FTPProtocol;
            txtFTPPort.Text = r.FTPPort.ToString();
            txtSSHHostKeyFingerprint.Text = r.FTPSshHostKeyFingerprint;
            chkActive.Checked = (bool)r.Active;
            chkActive.Visible = !chkActive.Checked;

            lblFingerprint.Visible = ((int)cboFTPProtocol.SelectedValue == (int)Globals.FTPProtocol.SFTP);
            txtSSHHostKeyFingerprint.Visible = lblFingerprint.Visible;
            
            systemPopulating = false;
            dgvContacts.DataSource = BLL.RetailerLogic.ListRetailerContacts(currRetailer);
            recordId = currRetailer.Id;

            //Check out the retailer if possible. If already checked out then make it read only - only do if not readonly anyway
            if (perm != Globals.Permission.Read_Only)
            {
                string msg;
                if (!CheckInOut.CheckOut(EntityName, recordId, Properties.Settings.Default.UserId, out msg))
                {
                    if (msg != string.Empty)
                        MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    SetReadOnly(this.splitContainer1.Panel2);
                    dgvContacts.Columns["DeleteContactColumn"].Visible = false;
                    IsReadOnly = true;
                }
                else
                {
                    UnsetReadOnly(this.splitContainer1.Panel2);
                    dgvContacts.Columns["DeleteContactColumn"].Visible = true;
                    IsReadOnly = false;
                }
                Dirty = false;
            }
        }


        private void dgvRetailer_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && dgvRetailer.Columns[e.ColumnIndex].Name == "DeleteColumn")
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                e.Handled = true;
            }
        }

        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            //Determine if current record has changed, and if it has, save it.
            if (CurrentRecordValid())
            {
                LoadRetailerGrid();
            }
        }

        private void Save()
        {
            currRetailer.Active = chkActive.Checked;
            currRetailer.AddressLine1 = txtAddress1.Text;
            currRetailer.AddressLine2 = txtAddress2.Text;
            currRetailer.County = txtCounty.Text;
            currRetailer.FTPHost = txtFTPHost.Text;
            currRetailer.FTPPassword = txtPassword.Text;
            currRetailer.FTPUserName = txtUserName.Text;
            currRetailer.Name = txtName.Text;
            currRetailer.PostCode = txtPostCode.Text;
            currRetailer.Town = txtTown.Text;
            currRetailer.FTPInFolder = txtUploadFolder.Text;
            currRetailer.FTPOutFolder = txtDownloadFolder.Text;
            if ((int)cboFTPProtocol.SelectedValue == -1)
                currRetailer.FTPProtocol = null;
            else
            { 
                //Cannot unbox directly to a short
                int protocol = (int)cboFTPProtocol.SelectedValue;
                currRetailer.FTPProtocol = (short?)protocol;
            }
            if (currRetailer.FTPProtocol == (short?)Globals.FTPProtocol.SFTP)
                currRetailer.FTPSshHostKeyFingerprint = txtSSHHostKeyFingerprint.Text;
            else
                currRetailer.FTPSshHostKeyFingerprint = null;

            if (txtFTPPort.Text != string.Empty)
                currRetailer.FTPPort = int.Parse(txtFTPPort.Text);

            string msg;
            if (!RetailerLogic.UpdateRetailer(currRetailer, Properties.Settings.Default.UserId, out msg) && msg != string.Empty)
                MessageBox.Show(msg, Properties.Settings.Default.SystemName,MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void dgvRetailer_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //If selection is being changed then make sure the current information is valid before moving on
            //Make sure it has indeed changed
            if (currRetailer != (Retailer)dgvRetailer.Rows[e.RowIndex].DataBoundItem && !formIsClosing)
            {
                if (!SkipRowEnterEvent && !loading)
                {
                    if (CurrentRecordValid())
                    {
                        currRetailer = (Retailer)dgvRetailer.Rows[e.RowIndex].DataBoundItem;
                        recordId = currRetailer.Id;
                        PopulateFields(currRetailer);
                        Dirty = false;
                    }
                    else
                    {
                        SkipRowEnterEvent = true;
                        dgvRetailer.CurrentRow.Selected = false;
                        dgvRetailer.Rows[currRow].Selected = true; //currRow saved in Validated event
                        SkipRowEnterEvent = false;
                    }
                }
            }
        }

        private bool CurrentRecordValid()
        {
            if (IsReadOnly) //No need to do checks
            {
                return true;
            }
            else 
            {
                if (Dirty)
                {
                    if (IsValid)
                    {
                        Save();
                    }
                    else
                        return false;
                }
                //Release lock of previous record
                string msg;
                if (!CheckInOut.CheckIn(EntityName, recordId, Properties.Settings.Default.UserId, out msg) && msg != string.Empty)
                    MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return true;
            }
        }

        private bool FormSpecificValidation()
        {
            return true;
        }
        
        private void dgvContacts_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvContacts.Columns["DeleteContactColumn"].Index)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                e.Handled = true;
            }
        }

        private void dgvRetailer_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            currRow = e.RowIndex;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            if (!systemPopulating)
            {
                currRetailer.Name = txtName.Text;
            }
        }

        private void frmRetailer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Release lock of last record
            if (perm != Globals.Permission.Read_Only)
            {
                string msg;
                if (!CheckInOut.CheckIn(EntityName, recordId, Properties.Settings.Default.UserId, out msg) && msg != string.Empty)
                {
                    MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void dgvRetailer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvRetailer.Columns["DeleteColumn"].Index)
            {
                if (IsReadOnly)
                {
                    MessageBox.Show("Retailer is read-only and cannot be set to inactive", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if (MessageBox.Show("Are you sure you want to set this retailer to inactive?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    Retailer rtl = (Retailer)dgvRetailer.Rows[e.RowIndex].DataBoundItem;
                    rtl.Active = false;
                    string msg;
                    if (!RetailerLogic.UpdateRetailer(rtl, Properties.Settings.Default.UserId, out msg) && msg!= string.Empty)
                        MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        if (e.RowIndex == 0)
                        {
                            LoadRetailerGrid();
                        }
                        else
                        {
                            LoadRetailerGrid((int)dgvRetailer.Rows[e.RowIndex - 1].Cells["IdColumn"].Value);
                        }
                    }
                }
            }

        }

        private void btnRetailer_Click(object sender, EventArgs e)
        {
            if (CurrentRecordValid())
            {
                int newRec = RetailerLogic.AddRetailer(Properties.Settings.Default.UserId);
                if (newRec != 0)
                {
                    LoadRetailerGrid(newRec);
                    txtName.Focus();
                    txtName.SelectionLength = txtName.Text.Length;
                }
            }
        }

        private void cboFTPProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                lblFingerprint.Visible = ((int)cboFTPProtocol.SelectedValue == (int)Globals.FTPProtocol.SFTP);
                txtSSHHostKeyFingerprint.Visible = lblFingerprint.Visible;
            }
        }
    }
}
