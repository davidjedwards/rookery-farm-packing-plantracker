﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmChangePW : Stock_Management_Desktop.Forms.frmBase
    {
        string oldPassword = string.Empty;
        string newPassword = string.Empty;
        User thisUser;

        public frmChangePW()
        {
            InitializeComponent();
        }

        public bool ForceChange { get; set; }

        private void txtOldPW_TextChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                //Check password against that stored and show cross or tick as appropriate
                bool Correct = false;
                if (string.Equals(Globals.HashData(txtOldPW.Text), oldPassword))
                    Correct = true;

                imgTickOldPW.Visible = Correct;
                imgCrossOldPW.Visible = !Correct;
            }
        }

        private void frmChangePW_Load(object sender, EventArgs e)
        {
            Suppress = true;
            loading = true;
            ValidationFields.Add(new Classes.clsFieldValidation(txtNewPW, Classes.clsFieldValidation.InputTypes.Text, true));
            ValidationFields.Add(new Classes.clsFieldValidation(txtConfPW, Classes.clsFieldValidation.InputTypes.Text, true));
            IdReqdFields();
            //Get old password
            thisUser = BLL.UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
            oldPassword = thisUser.Password ?? string.Empty;
            if (oldPassword == string.Empty)
            {
                imgTickOldPW.Visible = true;
                txtOldPW.Enabled = false;
                imgCrossOldPW.Visible = false;
            }
            if (ForceChange)
                btnCancel.Visible = false;

            loading = false;
        }

        private void txtConfPW_TextChanged(object sender, EventArgs e)
        {
            if (!loading) 
            {
                //Check confirmation password matches new one entered and show cross or tick as appropriate
                bool Correct = false;
                if (string.Equals(txtConfPW.Text, txtNewPW.Text))
                    Correct = true;

                imgTickConfPW.Visible = Correct;
                imgCrossConfPW.Visible = !Correct;
            }
        }

        private void txtNewPW_TextChanged(object sender, EventArgs e)
        {
            if (!loading)
            {
                if (txtNewPW.Text == string.Empty)
                    txtConfPW.Enabled = false;
                else
                    txtConfPW.Enabled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Dirty = true;
            Save();
        }

        private void Save()
        {
            if (IsValid)
            {
                //Validate password entry
                if (txtOldPW.Text != string.Empty && !string.Equals(Globals.HashData(txtOldPW.Text), oldPassword))
                {
                    MessageBox.Show("The old password is incorrect. Please try again.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ep.SetError(txtOldPW, "Old password is incorrect");
                    txtOldPW.Focus();
                }
                else if (!string.Equals(txtConfPW.Text, txtNewPW.Text))
                {
                    MessageBox.Show("The password confirmation does not match the new password. Please re-enter.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ep.SetError(txtOldPW, "Confirmation password is incorrect");
                    txtConfPW.Focus();
                }
                //If the above validation checks have been passed then save the details back to the user table
                thisUser.Password = Globals.HashData(txtNewPW.Text);
                string msg;
                bool Success = BLL.UserLogic.UpdateUser(thisUser, Properties.Settings.Default.UserId, out msg);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}
