﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.cboConfigGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkShowInactive = new System.Windows.Forms.CheckBox();
            this.dgvConfigItems = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfigurationGroupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiveColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfigItems)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnClose);
            this.pnlFooter.Location = new System.Drawing.Point(0, 612);
            this.pnlFooter.Size = new System.Drawing.Size(627, 44);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::Stock_Management_Desktop.Properties.Resources.door_out;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(545, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "&Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cboConfigGroup
            // 
            this.cboConfigGroup.DisplayMember = "GroupName";
            this.cboConfigGroup.FormattingEnabled = true;
            this.cboConfigGroup.Location = new System.Drawing.Point(182, 12);
            this.cboConfigGroup.Name = "cboConfigGroup";
            this.cboConfigGroup.Size = new System.Drawing.Size(286, 24);
            this.cboConfigGroup.TabIndex = 1;
            this.cboConfigGroup.ValueMember = "Id";
            this.cboConfigGroup.SelectedIndexChanged += new System.EventHandler(this.cboConfigGroup_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "List:";
            // 
            // chkShowInactive
            // 
            this.chkShowInactive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowInactive.AutoSize = true;
            this.chkShowInactive.Location = new System.Drawing.Point(493, 14);
            this.chkShowInactive.Name = "chkShowInactive";
            this.chkShowInactive.Size = new System.Drawing.Size(107, 20);
            this.chkShowInactive.TabIndex = 3;
            this.chkShowInactive.Text = "Show Inactive";
            this.chkShowInactive.UseVisualStyleBackColor = true;
            this.chkShowInactive.CheckedChanged += new System.EventHandler(this.chkShowInactive_CheckedChanged);
            // 
            // dgvConfigItems
            // 
            this.dgvConfigItems.AllowUserToAddRows = false;
            this.dgvConfigItems.AllowUserToDeleteRows = false;
            this.dgvConfigItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConfigItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConfigItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.ConfigurationGroupColumn,
            this.ItemValueColumn,
            this.ActiveColumn,
            this.DeleteColumn});
            this.dgvConfigItems.Location = new System.Drawing.Point(47, 52);
            this.dgvConfigItems.Name = "dgvConfigItems";
            this.dgvConfigItems.Size = new System.Drawing.Size(534, 535);
            this.dgvConfigItems.TabIndex = 4;
            this.dgvConfigItems.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConfigItems_CellClick);
            this.dgvConfigItems.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvConfigItems_CellFormatting);
            this.dgvConfigItems.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvConfigItems_CellPainting);
            this.dgvConfigItems.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvConfigItems_RowValidating);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // ConfigurationGroupColumn
            // 
            this.ConfigurationGroupColumn.DataPropertyName = "ConfigurationGroup";
            this.ConfigurationGroupColumn.HeaderText = "";
            this.ConfigurationGroupColumn.Name = "ConfigurationGroupColumn";
            this.ConfigurationGroupColumn.ReadOnly = true;
            this.ConfigurationGroupColumn.Visible = false;
            // 
            // ItemValueColumn
            // 
            this.ItemValueColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemValueColumn.DataPropertyName = "ItemValue";
            this.ItemValueColumn.HeaderText = "Value";
            this.ItemValueColumn.Name = "ItemValueColumn";
            // 
            // ActiveColumn
            // 
            this.ActiveColumn.DataPropertyName = "Active";
            this.ActiveColumn.HeaderText = "";
            this.ActiveColumn.Name = "ActiveColumn";
            this.ActiveColumn.ReadOnly = true;
            this.ActiveColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ActiveColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ActiveColumn.Visible = false;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Width = 25;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ConfigurationGroup";
            this.dataGridViewTextBoxColumn2.HeaderText = "";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ItemValue";
            this.dataGridViewTextBoxColumn3.HeaderText = "Value";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 656);
            this.Controls.Add(this.dgvConfigItems);
            this.Controls.Add(this.chkShowInactive);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboConfigGroup);
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(643, 330);
            this.Name = "frmConfiguration";
            this.Text = "Option Lists";
            this.Load += new System.EventHandler(this.frmConfiguration_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.cboConfigGroup, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.chkShowInactive, 0);
            this.Controls.SetChildIndex(this.dgvConfigItems, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfigItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cboConfigGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkShowInactive;
        private System.Windows.Forms.DataGridView dgvConfigItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfigurationGroupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemValueColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ActiveColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}