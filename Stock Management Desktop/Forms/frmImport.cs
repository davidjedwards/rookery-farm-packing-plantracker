﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using BLL;
using RFP;
using Stock_Management_Desktop.Classes;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmImport : Stock_Management_Desktop.Forms.frmBase
    {
        public frmImport()
        {
            InitializeComponent();
        }

        private void frmImport_Load(object sender, EventArgs e)
        {
            Suppress = true;
            //Set up retailer combo
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            Functions.FormatButton(btnCancel);
            Functions.FormatButton(btnImport);
            Functions.FormatButton(btnChooseFile);

            List<Retailer> rtlList = RetailerLogic.ListRetailers(false);
            foreach (Retailer rtl in rtlList)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = rtl.Id;
                dsRetailer["Name"] = rtl.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cboRetailer.DataSource = dtRetailer;
            cboRetailer.DisplayMember = "Name";
            cboRetailer.ValueMember = "Id";

            IdIndivReqdField(cboRetailer);
            IdIndivReqdField(btnChooseFile);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.LastImportFileLocation == null || Properties.Settings.Default.LastImportFileLocation == string.Empty)
                dlgOpenFile.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            else
                dlgOpenFile.InitialDirectory = Properties.Settings.Default.LastImportFileLocation;

            dlgOpenFile.Filter = "csv files (*.csv)|*.csv";
            dlgOpenFile.FilterIndex = 1;
            if (dlgOpenFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblFileName.Text = dlgOpenFile.FileName;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            //Ensure fields are populated
            ep.SetError(cboRetailer, string.Empty);
            ep.SetError(btnChooseFile, string.Empty);
            if ((int)cboRetailer.SelectedValue == 0)
            {
                ep.SetError(cboRetailer, "Please select a retailer");
            }
            else if (lblFileName.Text == string.Empty)
            {
                ep.SetError(btnChooseFile, "Please select a file to import");
            }
            else
            {
                this.Hide();
                frmProgress frmP = new frmProgress();
                frmP.lblProgress.Text = "Importing replenish list...";
                frmP.Show();
                frmP.Refresh();
                bool success = BLLFunctions.ImportReplenList(lblFileName.Text, (int)cboRetailer.SelectedValue);
                frmP.Close();
                frmP.Dispose();

                if (success)
                {
                    MessageBox.Show("Import successful. Go to Replenish Lists to create picking instructions", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("There was a problem with the import. Please check the import error list", Properties.Settings.Default.SystemName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                }
            }
        }

    }
}
