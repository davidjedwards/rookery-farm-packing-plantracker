﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmStockMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvStock = new System.Windows.Forms.DataGridView();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearchComponent = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblComponentCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboBatchNo = new System.Windows.Forms.ComboBox();
            this.chkShowQueriesOnly = new System.Windows.Forms.CheckBox();
            this.cboQuality = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocationCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComponentDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualityDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShippableWeekColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QueryColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.QueryTypeDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PickFaceLocationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).BeginInit();
            this.pnlFilter.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnExport);
            this.pnlFooter.Location = new System.Drawing.Point(0, 410);
            this.pnlFooter.Size = new System.Drawing.Size(1073, 44);
            this.pnlFooter.TabIndex = 1;
            // 
            // dgvStock
            // 
            this.dgvStock.AllowUserToAddRows = false;
            this.dgvStock.AllowUserToDeleteRows = false;
            this.dgvStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvStock.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.LocationCodeColumn,
            this.ComponentDescColumn,
            this.BatchNoColumn,
            this.BatchDateColumn,
            this.QuantityColumn,
            this.QualityDescColumn,
            this.ShippableWeekColumn,
            this.QueryColumn,
            this.QueryTypeDescColumn,
            this.FileNameColumn,
            this.PickFaceLocationColumn});
            this.dgvStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStock.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvStock.Location = new System.Drawing.Point(0, 0);
            this.dgvStock.Name = "dgvStock";
            this.dgvStock.ReadOnly = true;
            this.dgvStock.RowHeadersVisible = false;
            this.dgvStock.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStock.Size = new System.Drawing.Size(1073, 368);
            this.dgvStock.TabIndex = 0;
            this.dgvStock.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStock_CellDoubleClick);
            // 
            // pnlFilter
            // 
            this.pnlFilter.Controls.Add(this.btnClear);
            this.pnlFilter.Controls.Add(this.btnSearchComponent);
            this.pnlFilter.Controls.Add(this.panel2);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Controls.Add(this.cboBatchNo);
            this.pnlFilter.Controls.Add(this.chkShowQueriesOnly);
            this.pnlFilter.Controls.Add(this.cboQuality);
            this.pnlFilter.Controls.Add(this.label4);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(1073, 42);
            this.pnlFilter.TabIndex = 0;
            // 
            // btnClear
            // 
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Image = global::Stock_Management_Desktop.Properties.Resources.refresh;
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClear.Location = new System.Drawing.Point(926, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(76, 33);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearchComponent
            // 
            this.btnSearchComponent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchComponent.Image = global::Stock_Management_Desktop.Properties.Resources.search;
            this.btnSearchComponent.Location = new System.Drawing.Point(300, 6);
            this.btnSearchComponent.Name = "btnSearchComponent";
            this.btnSearchComponent.Size = new System.Drawing.Size(34, 29);
            this.btnSearchComponent.TabIndex = 0;
            this.btnSearchComponent.UseVisualStyleBackColor = true;
            this.btnSearchComponent.Click += new System.EventHandler(this.btnSearchComponent_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblComponentCode);
            this.panel2.Location = new System.Drawing.Point(94, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 25);
            this.panel2.TabIndex = 12;
            // 
            // lblComponentCode
            // 
            this.lblComponentCode.AutoSize = true;
            this.lblComponentCode.Location = new System.Drawing.Point(3, 3);
            this.lblComponentCode.Name = "lblComponentCode";
            this.lblComponentCode.Size = new System.Drawing.Size(0, 16);
            this.lblComponentCode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Component";
            // 
            // cboBatchNo
            // 
            this.cboBatchNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBatchNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBatchNo.Enabled = false;
            this.cboBatchNo.FormattingEnabled = true;
            this.cboBatchNo.Items.AddRange(new object[] {
            "3 (15/04/2014)",
            "2 (30/03/2014)",
            "1 (16/03/2014)",
            "3 (22/04/2013)",
            "2 (29/03/2013)",
            "1 (12/03/2013)"});
            this.cboBatchNo.Location = new System.Drawing.Point(445, 9);
            this.cboBatchNo.Name = "cboBatchNo";
            this.cboBatchNo.Size = new System.Drawing.Size(124, 24);
            this.cboBatchNo.TabIndex = 1;
            this.cboBatchNo.SelectedValueChanged += new System.EventHandler(this.cboBatchNo_SelectedValueChanged);
            // 
            // chkShowQueriesOnly
            // 
            this.chkShowQueriesOnly.AutoSize = true;
            this.chkShowQueriesOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowQueriesOnly.Location = new System.Drawing.Point(771, 13);
            this.chkShowQueriesOnly.Name = "chkShowQueriesOnly";
            this.chkShowQueriesOnly.Size = new System.Drawing.Size(136, 20);
            this.chkShowQueriesOnly.TabIndex = 3;
            this.chkShowQueriesOnly.Text = "Show Queries Only";
            this.chkShowQueriesOnly.UseVisualStyleBackColor = true;
            this.chkShowQueriesOnly.CheckedChanged += new System.EventHandler(this.chkShowQueriesOnly_CheckedChanged);
            // 
            // cboQuality
            // 
            this.cboQuality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboQuality.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboQuality.FormattingEnabled = true;
            this.cboQuality.Items.AddRange(new object[] {
            "Growing On",
            "Shippable",
            "Dispose",
            "Move On"});
            this.cboQuality.Location = new System.Drawing.Point(648, 10);
            this.cboQuality.Name = "cboQuality";
            this.cboQuality.Size = new System.Drawing.Size(117, 24);
            this.cboQuality.TabIndex = 2;
            this.cboQuality.SelectedValueChanged += new System.EventHandler(this.cboQuality_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(595, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Quality";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(351, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Batch Number";
            // 
            // pnlGrid
            // 
            this.pnlGrid.Controls.Add(this.dgvStock);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 42);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(1073, 368);
            this.pnlGrid.TabIndex = 3;
            // 
            // btnExport
            // 
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Image = global::Stock_Management_Desktop.Properties.Resources.excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(6, 5);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(136, 36);
            this.btnExport.TabIndex = 0;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "StockLevelId";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // LocationCodeColumn
            // 
            this.LocationCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LocationCodeColumn.DataPropertyName = "LocationCode";
            this.LocationCodeColumn.HeaderText = "Location";
            this.LocationCodeColumn.Name = "LocationCodeColumn";
            this.LocationCodeColumn.ReadOnly = true;
            this.LocationCodeColumn.Width = 80;
            // 
            // ComponentDescColumn
            // 
            this.ComponentDescColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ComponentDescColumn.DataPropertyName = "ComponentDesc";
            this.ComponentDescColumn.HeaderText = "Component";
            this.ComponentDescColumn.Name = "ComponentDescColumn";
            this.ComponentDescColumn.ReadOnly = true;
            // 
            // BatchNoColumn
            // 
            this.BatchNoColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BatchNoColumn.DataPropertyName = "BatchNo";
            this.BatchNoColumn.HeaderText = "Batch";
            this.BatchNoColumn.Name = "BatchNoColumn";
            this.BatchNoColumn.ReadOnly = true;
            this.BatchNoColumn.Width = 64;
            // 
            // BatchDateColumn
            // 
            this.BatchDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BatchDateColumn.DataPropertyName = "BatchDate";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.BatchDateColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.BatchDateColumn.HeaderText = "Batch Date";
            this.BatchDateColumn.Name = "BatchDateColumn";
            this.BatchDateColumn.ReadOnly = true;
            this.BatchDateColumn.Width = 94;
            // 
            // QuantityColumn
            // 
            this.QuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QuantityColumn.DataPropertyName = "Quantity";
            this.QuantityColumn.HeaderText = "Quantity";
            this.QuantityColumn.Name = "QuantityColumn";
            this.QuantityColumn.ReadOnly = true;
            this.QuantityColumn.Width = 80;
            // 
            // QualityDescColumn
            // 
            this.QualityDescColumn.DataPropertyName = "QualityDesc";
            this.QualityDescColumn.HeaderText = "Quality";
            this.QualityDescColumn.Name = "QualityDescColumn";
            this.QualityDescColumn.ReadOnly = true;
            // 
            // ShippableWeekColumn
            // 
            this.ShippableWeekColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ShippableWeekColumn.DataPropertyName = "ShippableWeek";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.ShippableWeekColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.ShippableWeekColumn.HeaderText = "Ship. Date";
            this.ShippableWeekColumn.Name = "ShippableWeekColumn";
            this.ShippableWeekColumn.ReadOnly = true;
            this.ShippableWeekColumn.Width = 92;
            // 
            // QueryColumn
            // 
            this.QueryColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.QueryColumn.DataPropertyName = "Query";
            this.QueryColumn.HeaderText = "Query";
            this.QueryColumn.Name = "QueryColumn";
            this.QueryColumn.ReadOnly = true;
            this.QueryColumn.Width = 48;
            // 
            // QueryTypeDescColumn
            // 
            this.QueryTypeDescColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QueryTypeDescColumn.DataPropertyName = "QueryTypeDesc";
            this.QueryTypeDescColumn.HeaderText = "Query Type";
            this.QueryTypeDescColumn.Name = "QueryTypeDescColumn";
            this.QueryTypeDescColumn.ReadOnly = true;
            this.QueryTypeDescColumn.Width = 99;
            // 
            // FileNameColumn
            // 
            this.FileNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.FileNameColumn.DataPropertyName = "FileName";
            this.FileNameColumn.HeaderText = "Replen. List";
            this.FileNameColumn.Name = "FileNameColumn";
            this.FileNameColumn.ReadOnly = true;
            this.FileNameColumn.Width = 99;
            // 
            // PickFaceLocationColumn
            // 
            this.PickFaceLocationColumn.DataPropertyName = "PickFaceLocation";
            this.PickFaceLocationColumn.HeaderText = "";
            this.PickFaceLocationColumn.Name = "PickFaceLocationColumn";
            this.PickFaceLocationColumn.ReadOnly = true;
            this.PickFaceLocationColumn.Visible = false;
            // 
            // frmStockMaintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(1073, 454);
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmStockMaintenance";
            this.Text = "Stock Maintenance";
            this.Load += new System.EventHandler(this.frmStockMaintenance_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.pnlGrid, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvStock;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.CheckBox chkShowQueriesOnly;
        private System.Windows.Forms.ComboBox cboQuality;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboBatchNo;
        private System.Windows.Forms.Button btnSearchComponent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblComponentCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocationCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComponentDescColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchNoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualityDescColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShippableWeekColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn QueryColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QueryTypeDescColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PickFaceLocationColumn;
    }
}
