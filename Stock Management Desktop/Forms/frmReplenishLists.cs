﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using BLL;
using Stock_Management_Desktop.Classes;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmReplenishLists : Stock_Management_Desktop.Forms.frmBase
    {
        int currReplenId;

        public frmReplenishLists()
        {
            InitializeComponent();
        }

        private void frmReplenishLists_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;

            //Set up retailer combo
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            List<Retailer> rtlList = RetailerLogic.ListRetailers(false);
            foreach (Retailer rtl in rtlList)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = rtl.Id;
                dsRetailer["Name"] = rtl.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cboRetailer.DataSource = dtRetailer;
            cboRetailer.DisplayMember = "Name";
            cboRetailer.ValueMember = "Id";

            dgvReplenishLists.AutoGenerateColumns = false;
            Functions.StyleDataGridView(dgvReplenishLists);
            Functions.StyleDataGridView(dgvPickingListContents);
            LoadGrid();

            loading = false;
        }

        public void LoadGrid(int selectedId = 0)
        {
            gridLoading = true;
            int intCurrTopRow = dgvReplenishLists.FirstDisplayedScrollingRowIndex;
            if (intCurrTopRow < 0)
            {
                intCurrTopRow = 0;
            }

            dgvReplenishLists.DataSource = BLL.ReplenishListLogic.RetrieveReplenishLists((int)cboRetailer.SelectedValue);

            int intRowOfSelItem = 0;
            if (selectedId != 0)
            {
                dgvReplenishLists.FirstDisplayedScrollingRowIndex = intCurrTopRow;
                foreach (DataGridViewRow r in dgvReplenishLists.Rows)
                {
                    if (int.Parse(r.Cells["IdColumn"].Value.ToString()) == selectedId)
                    {
                        intRowOfSelItem = r.Index;
                    }
                }
                dgvReplenishLists.CurrentCell = dgvReplenishLists.Rows[intRowOfSelItem].Cells["RetailerNameColumn"];
                dgvReplenishLists.Rows[intRowOfSelItem].Selected = true;
            }
            if (dgvReplenishLists.RowCount == 0)
                dgvPickingListContents.DataSource = null;

            gridLoading = false;
        }

        private void LoadPickingListContents(int replenId, int selectedId = 0)
        {
            int intCurrTopRow = dgvPickingListContents.FirstDisplayedScrollingRowIndex;
            if (intCurrTopRow < 0)
            {
                intCurrTopRow = 0;
            }

            dgvPickingListContents.DataSource = BLL.ReplenishListLogic.RetrieveReplenishListDetails(replenId);

            int intRowOfSelItem = 0;
            if (selectedId != 0)
            {
                dgvPickingListContents.FirstDisplayedScrollingRowIndex = intCurrTopRow;
                foreach (DataGridViewRow r in dgvPickingListContents.Rows)
                {
                    if (int.Parse(r.Cells["RLDIdColumn"].Value.ToString()) == selectedId)
                    {
                        intRowOfSelItem = r.Index;
                    }
                }
                dgvPickingListContents.CurrentCell = dgvPickingListContents.Rows[intRowOfSelItem].Cells["RLDComponentDescColumn"];
                dgvPickingListContents.Rows[intRowOfSelItem].Selected = true;
            }
            if (dgvPickingListContents.RowCount == 0)
                dgvPickingListContents.DataSource = null;
        }

        private void dgvReplenishLists_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvReplenishLists.Columns["ApplyColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    if ((int)dgvReplenishLists.Rows[e.RowIndex].Cells["StatusColumn"].Value == (int)Globals.ReplenishListStatus.New)
                    {
                        e.Graphics.DrawImage(Properties.Resources.hand_property, e.CellBounds.Left + 4, e.CellBounds.Top + 2, 16, 16);
                    }
                    else
                    {
                        e.Graphics.DrawImage(Properties.Resources.refresh, e.CellBounds.Left + 4, e.CellBounds.Top + 2, 16, 16);
                    }
                    e.Handled = true;
                }
                else if (e.ColumnIndex == dgvReplenishLists.Columns["DeleteColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.Left + 4, e.CellBounds.Top + 2, 16, 16);
                    e.Handled = true;
                }
            }
        }

        private void cboRetailer_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!loading)
                LoadGrid();
        }

        private void dgvReplenishLists_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((int)dgvReplenishLists.Rows[e.RowIndex].Cells["StatusColumn"].Value == (int)Globals.ReplenishListStatus.New)
            {
                dgvPickingListContents.Columns["AddColumn"].Visible = false;
            }
            else
            {
                dgvPickingListContents.Columns["AddColumn"].Visible = true;
            }
            currReplenId = (int)dgvReplenishLists.Rows[e.RowIndex].Cells["IdColumn"].Value;
            LoadPickingListContents(currReplenId);
        }

        private void dgvPickingListContents_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvPickingListContents.Columns["AddColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    if ((bool)dgvPickingListContents.Rows[e.RowIndex].Cells["ShowAddColumn"].Value == true)
                    {
                        e.Graphics.DrawImage(Properties.Resources.add, e.CellBounds.Left + 4, e.CellBounds.Top + 2, 16, 16);
                    }
                    else
                    {
                        e.Graphics.DrawImage(Properties.Resources.minus, e.CellBounds.Left + 4, e.CellBounds.Top + 2, 16, 16);
                    }
                    e.Handled = true;
                }
            }
        }

        private void dgvReplenishLists_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvReplenishLists.Columns["DeleteColumn"].Index)
                {
                    if ((int)dgvReplenishLists.Rows[e.RowIndex].Cells["StatusColumn"].Value != (int)Globals.ReplenishListStatus.New)
                    {
                        MessageBox.Show("This replenish list has been applied (i.e. pick instructions created) and so cannot be deleted. " +
                            "If it needs deleting then undo the application by clicking the undo button",
                            Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (MessageBox.Show("Are you sure you want to delete this replenish list?", Properties.Settings.Default.SystemName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            int refreshId;
                            //set id to refresh to to be id of next row unless we are deleting last row, in that case set to prev row
                            if (dgvReplenishLists.RowCount == e.RowIndex + 1)
                            {
                                if (e.RowIndex == 0)
                                {
                                    refreshId = 0;
                                }
                                else
                                {
                                    refreshId = (int)dgvReplenishLists.Rows[e.RowIndex - 1].Cells["IdColumn"].Value;
                                }
                            }
                            else
                            {
                                refreshId = (int)dgvReplenishLists.Rows[e.RowIndex + 1].Cells["IdColumn"].Value;
                            }

                            //Delete the replenish list
                            ReplenishList delRpl = ReplenishListLogic.RetrieveReplenishListById((int)dgvReplenishLists.Rows[e.RowIndex].Cells["IdColumn"].Value);
                            ReplenishListLogic.Delete(delRpl);
                            LoadGrid(refreshId);
                        }
                    }
                }
                else if (e.ColumnIndex == dgvReplenishLists.Columns["ApplyColumn"].Index)
                {
                    if ((int)dgvReplenishLists.Rows[e.RowIndex].Cells["StatusColumn"].Value == (int)Globals.ReplenishListStatus.New)
                    {
                        int replenId = (int)dgvReplenishLists.Rows[e.RowIndex].Cells["IdColumn"].Value;
                        //Ensure that we have enough shippable quantity:
                        string msg;
                        if (!ReplenishListLogic.EnoughShippableStock(replenId, out msg))
                        {
                            MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            //Create pick instructions
                            List<StockLevel> slList;
                            int remQty;
                            int qtyToProcess;
                            //Get details of replenish list
                            List<ReplenishListDetail> rplList = ReplenishListLogic.RetrieveReplenishListDetailsList(replenId);
                            foreach (ReplenishListDetail rplDet in rplList)
                            {
                                remQty = (int)rplDet.Quantity;
                                slList = StockLevelLogic.RetrieveShippableStockLevels((int)rplDet.Component);
                                foreach (StockLevel sl in slList)
                                {
                                    if (remQty > sl.Quantity)
                                    {
                                        qtyToProcess = (int)sl.Quantity;
                                        remQty -= (int)sl.Quantity;
                                    }
                                    else
                                    {
                                        qtyToProcess = remQty;
                                        remQty = 0;
                                    }
                                    StockLevelLogic.SetNewQuality(sl, Globals.CropQuality.Pick, Properties.Settings.Default.UserId, qtyToProcess, rplDet.Id);
                                    if (remQty == 0)
                                        break;
                                }
                            }
                            ReplenishList rpl = ReplenishListLogic.RetrieveReplenishListById(replenId);
                            rpl.Status = (int)Globals.ReplenishListStatus.Applied;
                            ReplenishListLogic.Update(rpl);
                            MessageBox.Show("Pick instructions now created against the replenish list", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LoadGrid(replenId);
                        }
                    }
                    else if ((int)dgvReplenishLists.Rows[e.RowIndex].Cells["StatusColumn"].Value >= (int)Globals.ReplenishListStatus.Applied)
                    {
                        if ((int)dgvReplenishLists.Rows[e.RowIndex].Cells["StatusColumn"].Value > (int)Globals.ReplenishListStatus.Applied)
                        {
                            if (MessageBox.Show("This will undo any picking done for this replenishment list. Note that it will also undo any subsequent changes " +
                                    "following the pick on the affected batches. Are you sure you want to continue?", Properties.Settings.Default.SystemName,
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                                return;
                        }
                        //Reset the quality on all affected stock level records back to "Shippable" by undo-ing the batch history records back to the quality change
                        //to picked
                        int replenId = (int)dgvReplenishLists.Rows[e.RowIndex].Cells["IdColumn"].Value;
                        List<BatchHistory> bhList;
                        List<BatchHistory> bhListFuture;
                        //Get details of replenish list
                        List<ReplenishListDetail> rplList = ReplenishListLogic.RetrieveReplenishListDetailsList(replenId);
                        string errMsg = string.Empty;
                        foreach (ReplenishListDetail rplDet in rplList)
                        {
                            //Get list of batch history records related to this replenishment detail item
                            bhList = BatchLogic.BatchHistoryListByReplenDetId((int)rplDet.Id);
                            foreach (BatchHistory bh in bhList)
                            {
                                //Need to undo these batch histories, but also, if there are subsequent batch history records for the batch, these need to be
                                //undone as well.
                                bhListFuture = BatchLogic.LatestBatchHistoriesToId((int)bh.Batch, (int)bh.Id);
                                foreach (BatchHistory bhF in bhListFuture)
                                {
                                    string msg;
                                    if (!BatchLogic.UndoBatchHistory(bhF, Properties.Settings.Default.UserId, out msg))
                                    {
                                        Batch b = BatchLogic.RetrieveBatchFromId(bhF.Batch);
                                        RFP.Component c = ComponentLogic.RetrieveComponentById((int)b.Component);
                                        errMsg += "\n" + "- Batch " + c.RetailerStockCode + "; No: " + b.BatchNo + " (" +
                                            string.Format("{0:dd/mm/yyyy}", b.BatchDate) + ") - " + Globals.ActionScreenDesc((Globals.Action)bhF.Action);
                                    }
                                }
                            }
                        }
                        if (errMsg != string.Empty)
                        {
                            errMsg = "Unable to undo changes below:" + errMsg + "\n Please undo these changes manually";
                            MessageBox.Show(errMsg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        ReplenishList rpl = ReplenishListLogic.RetrieveReplenishListById(replenId);
                        rpl.Status = (int)Globals.ReplenishListStatus.New;
                        ReplenishListLogic.Update(rpl);
                        MessageBox.Show("Pick instructions removed from system", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadGrid(replenId);
                    }
                }
            }
        }

        private void dgvPickingListContents_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvPickingListContents.Columns["AddColumn"].Index)
                {
                    if ((bool)dgvPickingListContents.Rows[e.RowIndex].Cells["ShowAddColumn"].Value == true)
                    {
                        e.ToolTipText = "Click to create picking instruction(s) for this line";
                    }
                    else
                    {
                        e.ToolTipText = "Click to remove picking instruction(s) for this line";
                    }
                }
            }

        }

        private void dgvPickingListContents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvPickingListContents.Columns["AddColumn"].Index)
                {
                    if ((bool)dgvPickingListContents.Rows[e.RowIndex].Cells["ShowAddColumn"].Value == true)
                    {
                        string msg;
                        if (!ReplenishListLogic.EnoughShippableStockForLine((int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDIdColumn"].Value, out msg))
                        {
                            MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            int qtyToProcess;
                            int remQty = (int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDQuantityColumn"].Value -
                                         (int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDPickedColumn"].Value;
                            List<StockLevel> slList = StockLevelLogic.RetrieveShippableStockLevels((int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDComponentColumn"].Value);
                            foreach (StockLevel sl in slList)
                            {
                                if (remQty > sl.Quantity)
                                {
                                    qtyToProcess = (int)sl.Quantity;
                                    remQty -= (int)sl.Quantity;
                                }
                                else
                                {
                                    qtyToProcess = remQty;
                                    remQty = 0;
                                }
                                StockLevelLogic.SetNewQuality(sl, Globals.CropQuality.Pick, Properties.Settings.Default.UserId, qtyToProcess, (int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDIdColumn"].Value);
                                if (remQty == 0)
                                    break;
                            }
                            LoadPickingListContents(currReplenId, (int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDIdColumn"].Value);
                        }
                    }
                    else
                    {
                        string prompt;
                        if ((int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDPickedColumn"].Value > 0)
                        {
                            prompt = "This will result in picked quantities going back into stock. Do you want to continue?";
                        }
                        else
                        {
                            prompt = "Are you sure you want to remove the picking instructions for this line?";
                        }
                        if (MessageBox.Show(prompt, Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            //Get list of batch history records related to this replenishment detail item
                            string errMsg = string.Empty;
                            List<BatchHistory> bhListFuture;
                            List<BatchHistory> bhList = BatchLogic.BatchHistoryListByReplenDetId((int)dgvPickingListContents.Rows[e.RowIndex].Cells["RLDIdColumn"].Value);
                            foreach (BatchHistory bh in bhList)
                            {
                                //Need to undo these batch histories, but also, if there are subsequent batch history records for the batch, these need to be
                                //undone as well.
                                bhListFuture = BatchLogic.LatestBatchHistoriesToId((int)bh.Batch, (int)bh.Id);
                                foreach (BatchHistory bhF in bhListFuture)
                                {
                                    string msg;
                                    if (!BatchLogic.UndoBatchHistory(bhF, Properties.Settings.Default.UserId, out msg))
                                    {
                                        Batch b = BatchLogic.RetrieveBatchFromId(bhF.Batch);
                                        RFP.Component c = ComponentLogic.RetrieveComponentById((int)b.Component);
                                        errMsg += "\n" + "- Batch " + c.RetailerStockCode + "; No: " + b.BatchNo + " (" +
                                            string.Format("{0:dd/mm/yyyy}", b.BatchDate) + ") - " + Globals.ActionScreenDesc((Globals.Action)bhF.Action);
                                    }
                                }
                                if (errMsg != string.Empty)
                                {
                                    errMsg = "Unable to undo changes below:" + errMsg + "\n Please undo these changes manually";
                                    MessageBox.Show(errMsg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                            }
                            LoadGrid(currReplenId);
                        }
                    }
                }
            }
        }

    //    private void btnExport_Click(object sender, EventArgs e)
    //    {

    //        //Export currently shown list to Excel
    //        frmProgress frmP = new frmProgress();
    //        frmP.lblProgress.Text = "Exporting replenish list...";
    //        frmP.Show();
    //        frmP.Refresh();

    //        //Create new Excel instance
    //        Type tp = Type.GetTypeFromProgID("Excel.Application");
    //        dynamic objExcel = Activator.CreateInstance(tp);
    //        dynamic objWorkBooks = objExcel.Workbooks;
    //        dynamic objWorkBook = objWorkBooks.Add();
    //        dynamic objSheet = objWorkBook.Worksheets(1);

    //        try
    //        {
    //            //Assign column headings
    //            objSheet.Range["A1"].Value = "Component";
    //            objSheet.Range["B1"].Value = "Location";
    //            objSheet.Range["C1"].Value = "Quantity";
    //            objSheet.Range["D1"].Value = "Pick Face Location";
    //            objSheet.Rows[1].Font.Bold = true;

    //            RFP.Component comp;
    //            ConfigurationItem cfi;
    //            int RowCount = dgvPickingListContents.Rows.Count;
    //            int FirstExcelRow = 2;
    //            for (int i = 0; i <= RowCount - 1; i++)
    //            {
    //                if (!dgvPickingListContents.Rows[i].IsNewRow)
    //                {
    //                    //Component description
    //                    objSheet.Range[string.Format("A{0}", (FirstExcelRow + i).ToString())].Value =
    //                        dgvPickingListContents.Rows[i].Cells["RLDComponentDescColumn"].Value.ToString();
    //                    //Quantity
    //                    objSheet.Range[string.Format("B{0}", (FirstExcelRow + i).ToString())].Value =
    //                        dgvPickingListContents.Rows[i].Cells["RLDQuantityColumn"].Value.ToString();


    //                    comp = ComponentLogic.RetrieveComponentById((int)dgvPickingListContents.Rows[i].Cells["RLDComponentColumn"].Value);
    //                    if (comp != null)
    //                    {
    //                        cfi = ConfigurationLogic.RetrieveConfigItemById(comp.PickFaceLocation);
    //                        if (cfi != null)
    //                        {
    //                            objSheet.Range[string.Format("C{0}", (FirstExcelRow + i).ToString())].Value = cfi.ItemValue;
    //                        }
    //                    }
    //                }
    //            }
    //            objSheet.Columns["A:C"].EntireColumn.Autofit();
    //            objExcel.Visible = true;
    //        }
    //        catch (Exception ex)
    //        {
    //            ExceptionLogic.HandleException(ex, this.Name + "_btnExport_Click", true);
    //        }

    //        finally
    //        {
    //            frmP.Close();
    //            frmP.Dispose();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(objSheet);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(objWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(objWorkBooks);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(objExcel);
    //        }
    //    }
    }
}
