﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLocation));
            this.dgvLocations = new Stock_Management_Desktop.Classes.DataGridViewPlus();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SortOrderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiveColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.chkShowInactive = new System.Windows.Forms.CheckBox();
            this.dgvLocationTemp = new Stock_Management_Desktop.Classes.DataGridViewPlus();
            this.TempIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempDescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempSortOrderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempActiveColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblInstructions = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocations)).BeginInit();
            this.pnlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationTemp)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Location = new System.Drawing.Point(0, 606);
            this.pnlFooter.Size = new System.Drawing.Size(1105, 44);
            // 
            // dgvLocations
            // 
            this.dgvLocations.AllowDrop = true;
            this.dgvLocations.AllowMultiRowDrag = true;
            this.dgvLocations.AllowUserToDeleteRows = false;
            this.dgvLocations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.CodeColumn,
            this.DescriptionColumn,
            this.SortOrderColumn,
            this.ActiveColumn,
            this.DeleteColumn});
            this.dgvLocations.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvLocations.dragBoxFromMouseDown = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.dgvLocations.Location = new System.Drawing.Point(0, 42);
            this.dgvLocations.Name = "dgvLocations";
            this.dgvLocations.RowCollectionType = typeof(System.Windows.Forms.DataGridViewRow[]);
            this.dgvLocations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLocations.Size = new System.Drawing.Size(486, 564);
            this.dgvLocations.TabIndex = 1;
            this.dgvLocations.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocations_CellClick);
            this.dgvLocations.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvLocations_CellFormatting);
            this.dgvLocations.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvLocations_CellPainting);
            this.dgvLocations.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLocations_RowValidating);
            this.dgvLocations.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvLocations_DragDrop);
            this.dgvLocations.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvLocations_DragEnter);
            this.dgvLocations.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvLocations_DragOver);
            this.dgvLocations.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvLocations_MouseDown);
            this.dgvLocations.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgvLocations_MouseMove);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // CodeColumn
            // 
            this.CodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CodeColumn.DataPropertyName = "Code";
            this.CodeColumn.HeaderText = "Location Code";
            this.CodeColumn.Name = "CodeColumn";
            this.CodeColumn.Width = 113;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.DataPropertyName = "Description";
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            // 
            // SortOrderColumn
            // 
            this.SortOrderColumn.DataPropertyName = "SortOrder";
            this.SortOrderColumn.HeaderText = "";
            this.SortOrderColumn.Name = "SortOrderColumn";
            this.SortOrderColumn.ReadOnly = true;
            this.SortOrderColumn.Visible = false;
            // 
            // ActiveColumn
            // 
            this.ActiveColumn.DataPropertyName = "Active";
            this.ActiveColumn.HeaderText = "";
            this.ActiveColumn.Name = "ActiveColumn";
            this.ActiveColumn.ReadOnly = true;
            this.ActiveColumn.Visible = false;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Width = 25;
            // 
            // pnlFilter
            // 
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFilter.Controls.Add(this.chkShowInactive);
            this.pnlFilter.Cursor = System.Windows.Forms.Cursors.Default;
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(1105, 42);
            this.pnlFilter.TabIndex = 3;
            // 
            // chkShowInactive
            // 
            this.chkShowInactive.AutoSize = true;
            this.chkShowInactive.Location = new System.Drawing.Point(11, 11);
            this.chkShowInactive.Name = "chkShowInactive";
            this.chkShowInactive.Size = new System.Drawing.Size(107, 20);
            this.chkShowInactive.TabIndex = 0;
            this.chkShowInactive.Text = "Show Inactive";
            this.chkShowInactive.UseVisualStyleBackColor = true;
            this.chkShowInactive.CheckedChanged += new System.EventHandler(this.chkShowInactive_CheckedChanged);
            // 
            // dgvLocationTemp
            // 
            this.dgvLocationTemp.AllowDrop = true;
            this.dgvLocationTemp.AllowMultiRowDrag = true;
            this.dgvLocationTemp.AllowUserToAddRows = false;
            this.dgvLocationTemp.AllowUserToDeleteRows = false;
            this.dgvLocationTemp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvLocationTemp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocationTemp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TempIdColumn,
            this.TempCodeColumn,
            this.TempDescriptionColumn,
            this.TempSortOrderColumn,
            this.TempActiveColumn});
            this.dgvLocationTemp.dragBoxFromMouseDown = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.dgvLocationTemp.Location = new System.Drawing.Point(570, 42);
            this.dgvLocationTemp.Name = "dgvLocationTemp";
            this.dgvLocationTemp.ReadOnly = true;
            this.dgvLocationTemp.RowCollectionType = typeof(System.Windows.Forms.DataGridViewRow[]);
            this.dgvLocationTemp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLocationTemp.Size = new System.Drawing.Size(471, 387);
            this.dgvLocationTemp.TabIndex = 4;
            this.dgvLocationTemp.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvLocationTemp_CellFormatting);
            this.dgvLocationTemp.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvLocationTemp_DragDrop);
            this.dgvLocationTemp.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvLocationTemp_DragEnter);
            this.dgvLocationTemp.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvLocationTemp_DragOver);
            this.dgvLocationTemp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvLocationTemp_MouseDown);
            this.dgvLocationTemp.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgvLocationTemp_MouseMove);
            // 
            // TempIdColumn
            // 
            this.TempIdColumn.DataPropertyName = "Id";
            this.TempIdColumn.HeaderText = "";
            this.TempIdColumn.Name = "TempIdColumn";
            this.TempIdColumn.ReadOnly = true;
            this.TempIdColumn.Visible = false;
            // 
            // TempCodeColumn
            // 
            this.TempCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TempCodeColumn.DataPropertyName = "Code";
            this.TempCodeColumn.HeaderText = "Location Code";
            this.TempCodeColumn.Name = "TempCodeColumn";
            this.TempCodeColumn.ReadOnly = true;
            this.TempCodeColumn.Width = 113;
            // 
            // TempDescriptionColumn
            // 
            this.TempDescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TempDescriptionColumn.DataPropertyName = "Description";
            this.TempDescriptionColumn.HeaderText = "Description";
            this.TempDescriptionColumn.Name = "TempDescriptionColumn";
            this.TempDescriptionColumn.ReadOnly = true;
            // 
            // TempSortOrderColumn
            // 
            this.TempSortOrderColumn.DataPropertyName = "SortOrder";
            this.TempSortOrderColumn.HeaderText = "";
            this.TempSortOrderColumn.Name = "TempSortOrderColumn";
            this.TempSortOrderColumn.ReadOnly = true;
            this.TempSortOrderColumn.Visible = false;
            // 
            // TempActiveColumn
            // 
            this.TempActiveColumn.DataPropertyName = "Active";
            this.TempActiveColumn.HeaderText = "";
            this.TempActiveColumn.Name = "TempActiveColumn";
            this.TempActiveColumn.ReadOnly = true;
            this.TempActiveColumn.Visible = false;
            // 
            // lblInstructions
            // 
            this.lblInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblInstructions.AutoSize = true;
            this.lblInstructions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInstructions.ForeColor = System.Drawing.Color.DarkRed;
            this.lblInstructions.Location = new System.Drawing.Point(513, 449);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(580, 146);
            this.lblInstructions.TabIndex = 5;
            this.lblInstructions.Text = resources.GetString("lblInstructions.Text");
            // 
            // frmLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(1105, 650);
            this.Controls.Add(this.lblInstructions);
            this.Controls.Add(this.dgvLocationTemp);
            this.Controls.Add(this.dgvLocations);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmLocation";
            this.Text = "Location Maintenance";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLocation_FormClosing);
            this.Load += new System.EventHandler(this.frmLocation_Load);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.dgvLocations, 0);
            this.Controls.SetChildIndex(this.dgvLocationTemp, 0);
            this.Controls.SetChildIndex(this.lblInstructions, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocations)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocationTemp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Classes.DataGridViewPlus dgvLocations;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.CheckBox chkShowInactive;
        private Classes.DataGridViewPlus dgvLocationTemp;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempDescriptionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempSortOrderColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn TempActiveColumn;
        private System.Windows.Forms.Label lblInstructions;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SortOrderColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ActiveColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
    }
}
