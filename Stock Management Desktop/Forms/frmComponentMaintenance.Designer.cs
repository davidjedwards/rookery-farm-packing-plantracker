﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmComponentMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvComponents = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetailerColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.RetailerStockCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PickFaceLocationColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ActiveColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CreatedByColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatedDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastModifiedByColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastModifiedDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.chkShowAudit = new System.Windows.Forms.CheckBox();
            this.cboFilterPickFace = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSearchCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboFilterRetailer = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearchDesc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComponents)).BeginInit();
            this.pnlFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnCancel);
            this.pnlFooter.Controls.Add(this.btnExport);
            this.pnlFooter.Location = new System.Drawing.Point(0, 437);
            this.pnlFooter.Size = new System.Drawing.Size(1224, 44);
            // 
            // dgvComponents
            // 
            this.dgvComponents.AllowUserToDeleteRows = false;
            this.dgvComponents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComponents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.DescriptionColumn,
            this.RetailerColumn,
            this.RetailerStockCodeColumn,
            this.PickFaceLocationColumn,
            this.ActiveColumn,
            this.CreatedByColumn,
            this.CreatedDateColumn,
            this.LastModifiedByColumn,
            this.LastModifiedDateColumn,
            this.DeleteColumn});
            this.dgvComponents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComponents.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvComponents.Location = new System.Drawing.Point(0, 76);
            this.dgvComponents.Name = "dgvComponents";
            this.dgvComponents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvComponents.Size = new System.Drawing.Size(1224, 361);
            this.dgvComponents.TabIndex = 1;
            this.dgvComponents.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvComponents_CellBeginEdit);
            this.dgvComponents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComponents_CellClick);
            this.dgvComponents.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvComponents_CellFormatting);
            this.dgvComponents.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvComponents_CellPainting);
            this.dgvComponents.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComponents_RowEnter);
            this.dgvComponents.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComponents_RowValidated);
            this.dgvComponents.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvComponents_RowValidating);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.DataPropertyName = "Description";
            this.DescriptionColumn.HeaderText = "Component Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            // 
            // RetailerColumn
            // 
            this.RetailerColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RetailerColumn.DataPropertyName = "Retailer";
            this.RetailerColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.RetailerColumn.HeaderText = "Retailer";
            this.RetailerColumn.Items.AddRange(new object[] {
            "Sarah Raven",
            "Fothergills"});
            this.RetailerColumn.Name = "RetailerColumn";
            this.RetailerColumn.Width = 58;
            // 
            // RetailerStockCodeColumn
            // 
            this.RetailerStockCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RetailerStockCodeColumn.DataPropertyName = "RetailerStockCode";
            this.RetailerStockCodeColumn.HeaderText = "Retailer Component Code";
            this.RetailerStockCodeColumn.MinimumWidth = 150;
            this.RetailerStockCodeColumn.Name = "RetailerStockCodeColumn";
            this.RetailerStockCodeColumn.Width = 150;
            // 
            // PickFaceLocationColumn
            // 
            this.PickFaceLocationColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PickFaceLocationColumn.DataPropertyName = "PickFaceLocation";
            this.PickFaceLocationColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PickFaceLocationColumn.HeaderText = "Pick Face Location";
            this.PickFaceLocationColumn.Name = "PickFaceLocationColumn";
            this.PickFaceLocationColumn.Width = 106;
            // 
            // ActiveColumn
            // 
            this.ActiveColumn.DataPropertyName = "Active";
            this.ActiveColumn.FalseValue = "False";
            this.ActiveColumn.HeaderText = "";
            this.ActiveColumn.IndeterminateValue = "False";
            this.ActiveColumn.Name = "ActiveColumn";
            this.ActiveColumn.ReadOnly = true;
            this.ActiveColumn.TrueValue = "True";
            this.ActiveColumn.Visible = false;
            // 
            // CreatedByColumn
            // 
            this.CreatedByColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CreatedByColumn.DataPropertyName = "CreatedBy";
            this.CreatedByColumn.HeaderText = "Created By";
            this.CreatedByColumn.Name = "CreatedByColumn";
            this.CreatedByColumn.ReadOnly = true;
            this.CreatedByColumn.Visible = false;
            // 
            // CreatedDateColumn
            // 
            this.CreatedDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CreatedDateColumn.DataPropertyName = "CreatedDate";
            dataGridViewCellStyle1.Format = "d";
            this.CreatedDateColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.CreatedDateColumn.HeaderText = "CreatedDate";
            this.CreatedDateColumn.Name = "CreatedDateColumn";
            this.CreatedDateColumn.ReadOnly = true;
            this.CreatedDateColumn.Visible = false;
            // 
            // LastModifiedByColumn
            // 
            this.LastModifiedByColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LastModifiedByColumn.DataPropertyName = "LastModifiedBy";
            this.LastModifiedByColumn.HeaderText = "Last Modified By";
            this.LastModifiedByColumn.Name = "LastModifiedByColumn";
            this.LastModifiedByColumn.ReadOnly = true;
            this.LastModifiedByColumn.Visible = false;
            // 
            // LastModifiedDateColumn
            // 
            this.LastModifiedDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LastModifiedDateColumn.DataPropertyName = "LastModifiedDate";
            dataGridViewCellStyle2.Format = "d";
            this.LastModifiedDateColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.LastModifiedDateColumn.HeaderText = "Last Modified Date";
            this.LastModifiedDateColumn.Name = "LastModifiedDateColumn";
            this.LastModifiedDateColumn.ReadOnly = true;
            this.LastModifiedDateColumn.Visible = false;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Width = 25;
            // 
            // pnlFilter
            // 
            this.pnlFilter.Controls.Add(this.chkShowAudit);
            this.pnlFilter.Controls.Add(this.cboFilterPickFace);
            this.pnlFilter.Controls.Add(this.label4);
            this.pnlFilter.Controls.Add(this.txtSearchCode);
            this.pnlFilter.Controls.Add(this.label3);
            this.pnlFilter.Controls.Add(this.cboFilterRetailer);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Controls.Add(this.txtSearchDesc);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Controls.Add(this.chkShowDeleted);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(1224, 76);
            this.pnlFilter.TabIndex = 3;
            // 
            // chkShowAudit
            // 
            this.chkShowAudit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowAudit.AutoSize = true;
            this.chkShowAudit.Location = new System.Drawing.Point(1078, 50);
            this.chkShowAudit.Name = "chkShowAudit";
            this.chkShowAudit.Size = new System.Drawing.Size(134, 20);
            this.chkShowAudit.TabIndex = 18;
            this.chkShowAudit.Text = "Show Audit Details";
            this.chkShowAudit.UseVisualStyleBackColor = true;
            this.chkShowAudit.CheckedChanged += new System.EventHandler(this.chkShowAudit_CheckedChanged);
            // 
            // cboFilterPickFace
            // 
            this.cboFilterPickFace.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterPickFace.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterPickFace.FormattingEnabled = true;
            this.cboFilterPickFace.Location = new System.Drawing.Point(983, 11);
            this.cboFilterPickFace.Name = "cboFilterPickFace";
            this.cboFilterPickFace.Size = new System.Drawing.Size(92, 24);
            this.cboFilterPickFace.TabIndex = 17;
            this.cboFilterPickFace.SelectedIndexChanged += new System.EventHandler(this.RefreshGrid);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(865, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Pick Face Location";
            // 
            // txtSearchCode
            // 
            this.txtSearchCode.Location = new System.Drawing.Point(738, 11);
            this.txtSearchCode.Name = "txtSearchCode";
            this.txtSearchCode.Size = new System.Drawing.Size(113, 23);
            this.txtSearchCode.TabIndex = 15;
            this.txtSearchCode.TextChanged += new System.EventHandler(this.RefreshGrid);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(695, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "Code";
            // 
            // cboFilterRetailer
            // 
            this.cboFilterRetailer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterRetailer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterRetailer.FormattingEnabled = true;
            this.cboFilterRetailer.Location = new System.Drawing.Point(432, 10);
            this.cboFilterRetailer.Name = "cboFilterRetailer";
            this.cboFilterRetailer.Size = new System.Drawing.Size(251, 24);
            this.cboFilterRetailer.TabIndex = 13;
            this.cboFilterRetailer.SelectedIndexChanged += new System.EventHandler(this.RefreshGrid);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "Retailer";
            // 
            // txtSearchDesc
            // 
            this.txtSearchDesc.Location = new System.Drawing.Point(113, 11);
            this.txtSearchDesc.Name = "txtSearchDesc";
            this.txtSearchDesc.Size = new System.Drawing.Size(255, 23);
            this.txtSearchDesc.TabIndex = 11;
            this.txtSearchDesc.TextChanged += new System.EventHandler(this.RefreshGrid);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Component Desc";
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(6, 50);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(107, 20);
            this.chkShowDeleted.TabIndex = 9;
            this.chkShowDeleted.Text = "Show Inactive";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.RefreshGrid);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn2.HeaderText = "Component Description";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "RetailerStockCode";
            this.dataGridViewTextBoxColumn3.HeaderText = "Retailer Component Code";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CreatedBy";
            this.dataGridViewTextBoxColumn4.HeaderText = "Created By";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CreatedDate";
            dataGridViewCellStyle3.Format = "d";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn5.HeaderText = "CreatedDate";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "LastModifiedBy";
            this.dataGridViewTextBoxColumn6.HeaderText = "Last Modified By";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "LastModifiedDate";
            dataGridViewCellStyle4.Format = "d";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn7.HeaderText = "Last Modified Date";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // btnExport
            // 
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Image = global::Stock_Management_Desktop.Properties.Resources.excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(6, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(136, 36);
            this.btnExport.TabIndex = 0;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Desktop.Properties.Resources.cross;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(571, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 36);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmComponentMaintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(1224, 481);
            this.ControlBox = false;
            this.Controls.Add(this.dgvComponents);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmComponentMaintenance";
            this.Text = "Component Maintenance";
            this.Load += new System.EventHandler(this.frmComponentMaintenance_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.dgvComponents, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComponents)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvComponents;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.CheckBox chkShowDeleted;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ComboBox cboFilterPickFace;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSearchCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboFilterRetailer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearchDesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkShowAudit;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn RetailerColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetailerStockCodeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn PickFaceLocationColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ActiveColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedByColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastModifiedByColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastModifiedDateColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Button btnCancel;

    }
}
