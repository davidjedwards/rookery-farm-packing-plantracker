﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.chkShowAudit = new System.Windows.Forms.CheckBox();
            this.chkShowInactive = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvUserList = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LogonColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PermissionColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.RetailerColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ActiveColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DesktopUserColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TabletUserColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CreatedByColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatedDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastModifiedByColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastModifiedDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResetLockColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ResetPasswordColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.pnlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnClose);
            this.pnlFooter.Location = new System.Drawing.Point(0, 507);
            this.pnlFooter.Size = new System.Drawing.Size(1180, 44);
            // 
            // pnlFilter
            // 
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFilter.Controls.Add(this.chkShowAudit);
            this.pnlFilter.Controls.Add(this.chkShowInactive);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(1180, 42);
            this.pnlFilter.TabIndex = 5;
            // 
            // chkShowAudit
            // 
            this.chkShowAudit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowAudit.AutoSize = true;
            this.chkShowAudit.Location = new System.Drawing.Point(1034, 12);
            this.chkShowAudit.Name = "chkShowAudit";
            this.chkShowAudit.Size = new System.Drawing.Size(132, 20);
            this.chkShowAudit.TabIndex = 10;
            this.chkShowAudit.Text = "Show audit details";
            this.chkShowAudit.UseVisualStyleBackColor = true;
            this.chkShowAudit.CheckedChanged += new System.EventHandler(this.chkShowAudit_CheckedChanged);
            // 
            // chkShowInactive
            // 
            this.chkShowInactive.AutoSize = true;
            this.chkShowInactive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowInactive.Location = new System.Drawing.Point(12, 12);
            this.chkShowInactive.Name = "chkShowInactive";
            this.chkShowInactive.Size = new System.Drawing.Size(107, 20);
            this.chkShowInactive.TabIndex = 9;
            this.chkShowInactive.Text = "Show Inactive";
            this.chkShowInactive.UseVisualStyleBackColor = true;
            this.chkShowInactive.CheckedChanged += new System.EventHandler(this.chkShowInactive_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::Stock_Management_Desktop.Properties.Resources.door_out;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(1100, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(67, 28);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvUserList
            // 
            this.dgvUserList.AllowUserToDeleteRows = false;
            this.dgvUserList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.LogonColumn,
            this.FullNameColumn,
            this.PermissionColumn,
            this.RetailerColumn,
            this.ActiveColumn,
            this.DesktopUserColumn,
            this.TabletUserColumn,
            this.CreatedByColumn,
            this.CreatedDateColumn,
            this.LastModifiedByColumn,
            this.LastModifiedDateColumn,
            this.ResetLockColumn,
            this.ResetPasswordColumn,
            this.DeleteColumn});
            this.dgvUserList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUserList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvUserList.Location = new System.Drawing.Point(0, 42);
            this.dgvUserList.Name = "dgvUserList";
            this.dgvUserList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUserList.Size = new System.Drawing.Size(1180, 465);
            this.dgvUserList.TabIndex = 6;
            this.dgvUserList.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvUserList_CellBeginEdit);
            this.dgvUserList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserList_CellClick);
            this.dgvUserList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvUserList_CellFormatting);
            this.dgvUserList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvUserList_CellPainting);
            this.dgvUserList.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dgvUserList_CellToolTipTextNeeded);
            this.dgvUserList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserList_RowEnter);
            this.dgvUserList.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserList_RowValidated);
            this.dgvUserList.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvUserList_RowValidating);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // LogonColumn
            // 
            this.LogonColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LogonColumn.DataPropertyName = "Logon";
            this.LogonColumn.HeaderText = "Logon";
            this.LogonColumn.Name = "LogonColumn";
            this.LogonColumn.Width = 67;
            // 
            // FullNameColumn
            // 
            this.FullNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FullNameColumn.DataPropertyName = "FullName";
            this.FullNameColumn.HeaderText = "Full Name";
            this.FullNameColumn.Name = "FullNameColumn";
            // 
            // PermissionColumn
            // 
            this.PermissionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PermissionColumn.DataPropertyName = "Permission";
            this.PermissionColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PermissionColumn.HeaderText = "Permission";
            this.PermissionColumn.Name = "PermissionColumn";
            this.PermissionColumn.Width = 76;
            // 
            // RetailerColumn
            // 
            this.RetailerColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RetailerColumn.DataPropertyName = "Retailer";
            this.RetailerColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.RetailerColumn.HeaderText = "Retailer";
            this.RetailerColumn.MinimumWidth = 100;
            this.RetailerColumn.Name = "RetailerColumn";
            // 
            // ActiveColumn
            // 
            this.ActiveColumn.DataPropertyName = "Active";
            this.ActiveColumn.HeaderText = "";
            this.ActiveColumn.Name = "ActiveColumn";
            this.ActiveColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ActiveColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ActiveColumn.Visible = false;
            // 
            // DesktopUserColumn
            // 
            this.DesktopUserColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DesktopUserColumn.DataPropertyName = "DesktopUser";
            this.DesktopUserColumn.HeaderText = "Desktop User";
            this.DesktopUserColumn.Name = "DesktopUserColumn";
            this.DesktopUserColumn.Width = 89;
            // 
            // TabletUserColumn
            // 
            this.TabletUserColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TabletUserColumn.DataPropertyName = "TabletUser";
            this.TabletUserColumn.HeaderText = "Tablet User";
            this.TabletUserColumn.Name = "TabletUserColumn";
            this.TabletUserColumn.Width = 80;
            // 
            // CreatedByColumn
            // 
            this.CreatedByColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CreatedByColumn.DataPropertyName = "CreatedBy";
            this.CreatedByColumn.HeaderText = "Created by";
            this.CreatedByColumn.Name = "CreatedByColumn";
            this.CreatedByColumn.ReadOnly = true;
            this.CreatedByColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CreatedByColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CreatedByColumn.Visible = false;
            this.CreatedByColumn.Width = 76;
            // 
            // CreatedDateColumn
            // 
            this.CreatedDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CreatedDateColumn.DataPropertyName = "CreatedDate";
            dataGridViewCellStyle1.Format = "d";
            this.CreatedDateColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.CreatedDateColumn.HeaderText = "Created Date";
            this.CreatedDateColumn.Name = "CreatedDateColumn";
            this.CreatedDateColumn.ReadOnly = true;
            this.CreatedDateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CreatedDateColumn.Visible = false;
            this.CreatedDateColumn.Width = 89;
            // 
            // LastModifiedByColumn
            // 
            this.LastModifiedByColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LastModifiedByColumn.DataPropertyName = "LastModifiedBy";
            this.LastModifiedByColumn.HeaderText = "Last Modified By";
            this.LastModifiedByColumn.Name = "LastModifiedByColumn";
            this.LastModifiedByColumn.ReadOnly = true;
            this.LastModifiedByColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LastModifiedByColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LastModifiedByColumn.Visible = false;
            this.LastModifiedByColumn.Width = 106;
            // 
            // LastModifiedDateColumn
            // 
            this.LastModifiedDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LastModifiedDateColumn.DataPropertyName = "LastModifiedDate";
            dataGridViewCellStyle2.Format = "d";
            this.LastModifiedDateColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.LastModifiedDateColumn.HeaderText = "Last Modified Date";
            this.LastModifiedDateColumn.Name = "LastModifiedDateColumn";
            this.LastModifiedDateColumn.ReadOnly = true;
            this.LastModifiedDateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LastModifiedDateColumn.Visible = false;
            this.LastModifiedDateColumn.Width = 119;
            // 
            // ResetLockColumn
            // 
            this.ResetLockColumn.HeaderText = "";
            this.ResetLockColumn.MinimumWidth = 25;
            this.ResetLockColumn.Name = "ResetLockColumn";
            this.ResetLockColumn.ToolTipText = "Clear locks";
            this.ResetLockColumn.Width = 25;
            // 
            // ResetPasswordColumn
            // 
            this.ResetPasswordColumn.HeaderText = "";
            this.ResetPasswordColumn.MinimumWidth = 25;
            this.ResetPasswordColumn.Name = "ResetPasswordColumn";
            this.ResetPasswordColumn.ToolTipText = "Reset Password";
            this.ResetPasswordColumn.Width = 25;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.ToolTipText = "Set user inactive";
            this.DeleteColumn.Width = 25;
            // 
            // frmUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 551);
            this.Controls.Add(this.dgvUserList);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmUser";
            this.Text = "User Maintenance";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmUser_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.dgvUserList, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.CheckBox chkShowInactive;
        private System.Windows.Forms.DataGridView dgvUserList;
        private System.Windows.Forms.CheckBox chkShowAudit;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LogonColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullNameColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn PermissionColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn RetailerColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ActiveColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DesktopUserColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn TabletUserColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedByColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastModifiedByColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastModifiedDateColumn;
        private System.Windows.Forms.DataGridViewButtonColumn ResetLockColumn;
        private System.Windows.Forms.DataGridViewButtonColumn ResetPasswordColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
    }
}