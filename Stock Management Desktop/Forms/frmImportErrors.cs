﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using BLL;
using Stock_Management_Desktop.Classes;


namespace Stock_Management_Desktop.Forms
{
    public partial class frmImportErrors : Stock_Management_Desktop.Forms.frmBase
    {
        public frmImportErrors()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmImportErrors_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;
            Functions.FormatButton(btnClear);
            Functions.FormatButton(btnClose);
            Functions.FormatButton(btnSelectAll);
            Functions.FormatButton(btnSelectNone);

            LoadGrid();

            loading = false;
        }

        private void LoadGrid()
        {
            //Fill datagridview
            dgvImportErrors.AutoGenerateColumns = false;
            dgvImportErrors.DataSource = BLL.ImportErrorLogic.RetrieveImportErrorList();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgv in dgvImportErrors.Rows)
            {
                if (dgv.Index >= 0)
                {
                    dgv.Cells["SelectedColumn"].Value = true;
                }
            }
        }

        private void btnSelectNone_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgv in dgvImportErrors.Rows)
            {
                if (dgv.Index >= 0)
                {
                    dgv.Cells["SelectedColumn"].Value = false;
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            bool success;
            foreach (DataGridViewRow dgv in dgvImportErrors.Rows)
            {
                if (dgv.Index >= 0 && (bool)(dgv.Cells["SelectedColumn"].Value ?? false) == true)
                {
                    ImportError ie = ImportErrorLogic.RetrieveImportError((int)dgv.Cells["IdColumn"].Value);
                    if (ie != null)
                        success = ImportErrorLogic.DeleteImportError(ie);
                }
            }
            LoadGrid();
        }
    }
}
