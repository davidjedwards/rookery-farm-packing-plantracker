﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainForm));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lblReplenishListsAvail = new System.Windows.Forms.Label();
            this.lblSystemName = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.optionListsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportStockDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importReplenishListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importErrorListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewExportErrorListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remoteSupportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionListsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.pnlOptions = new Stock_Management_Desktop.Controls.StackPanel(this.components);
            this.btnStockTake = new System.Windows.Forms.Button();
            this.btnReplenishLists = new System.Windows.Forms.Button();
            this.btnRetailers = new System.Windows.Forms.Button();
            this.btnBatches = new System.Windows.Forms.Button();
            this.btnLocations = new System.Windows.Forms.Button();
            this.btnComponents = new System.Windows.Forms.Button();
            this.btnStockMaintenance = new System.Windows.Forms.Button();
            this.btnNavDown = new System.Windows.Forms.Button();
            this.btnNavUp = new System.Windows.Forms.Button();
            this.timNavUp = new System.Windows.Forms.Timer(this.components);
            this.timNavDown = new System.Windows.Forms.Timer(this.components);
            this.timNotify = new System.Windows.Forms.Timer(this.components);
            this.lblDownloadsError = new System.Windows.Forms.Label();
            this.lblUploadErrors = new System.Windows.Forms.Label();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            this.pnlOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = global::Stock_Management_Desktop.Properties.Settings.Default.CompanyColour;
            this.pnlHeader.Controls.Add(this.lblUploadErrors);
            this.pnlHeader.Controls.Add(this.lblDownloadsError);
            this.pnlHeader.Controls.Add(this.lblReplenishListsAvail);
            this.pnlHeader.Controls.Add(this.lblSystemName);
            this.pnlHeader.Controls.Add(this.lblCompany);
            this.pnlHeader.Controls.Add(this.pbLogo);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 24);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1236, 93);
            this.pnlHeader.TabIndex = 0;
            // 
            // lblReplenishListsAvail
            // 
            this.lblReplenishListsAvail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReplenishListsAvail.AutoSize = true;
            this.lblReplenishListsAvail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblReplenishListsAvail.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReplenishListsAvail.ForeColor = global::Stock_Management_Desktop.Properties.Settings.Default.RFPGreen;
            this.lblReplenishListsAvail.Location = new System.Drawing.Point(834, 9);
            this.lblReplenishListsAvail.Name = "lblReplenishListsAvail";
            this.lblReplenishListsAvail.Size = new System.Drawing.Size(395, 19);
            this.lblReplenishListsAvail.TabIndex = 4;
            this.lblReplenishListsAvail.Text = "New replenish list(s) available - click to dismiss";
            this.lblReplenishListsAvail.Visible = false;
            this.lblReplenishListsAvail.Click += new System.EventHandler(this.lblReplenishListsAvail_Click);
            // 
            // lblSystemName
            // 
            this.lblSystemName.AutoSize = true;
            this.lblSystemName.Font = new System.Drawing.Font("Franklin Gothic Book", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemName.ForeColor = System.Drawing.Color.White;
            this.lblSystemName.Location = new System.Drawing.Point(523, 15);
            this.lblSystemName.Name = "lblSystemName";
            this.lblSystemName.Size = new System.Drawing.Size(259, 61);
            this.lblSystemName.TabIndex = 2;
            this.lblSystemName.Text = global::Stock_Management_Desktop.Properties.Settings.Default.SystemName;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Font = new System.Drawing.Font("Franklin Gothic Medium", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ForeColor = System.Drawing.Color.White;
            this.lblCompany.Location = new System.Drawing.Point(12, 15);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(530, 61);
            this.lblCompany.TabIndex = 1;
            this.lblCompany.Text = "Rookery Farm Packing |";
            // 
            // pbLogo
            // 
            this.pbLogo.Location = new System.Drawing.Point(0, 0);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(100, 50);
            this.pbLogo.TabIndex = 3;
            this.pbLogo.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.importExportToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1236, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changePasswordToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change &Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.logOutToolStripMenuItem.Text = "&Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(165, 6);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersToolStripMenuItem1,
            this.optionListsToolStripMenuItem1});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.configurationToolStripMenuItem.Text = "&Configuration";
            // 
            // usersToolStripMenuItem1
            // 
            this.usersToolStripMenuItem1.Name = "usersToolStripMenuItem1";
            this.usersToolStripMenuItem1.Size = new System.Drawing.Size(137, 22);
            this.usersToolStripMenuItem1.Text = "&Users";
            this.usersToolStripMenuItem1.Click += new System.EventHandler(this.usersToolStripMenuItem1_Click);
            // 
            // optionListsToolStripMenuItem1
            // 
            this.optionListsToolStripMenuItem1.Name = "optionListsToolStripMenuItem1";
            this.optionListsToolStripMenuItem1.Size = new System.Drawing.Size(137, 22);
            this.optionListsToolStripMenuItem1.Text = "&Option Lists";
            this.optionListsToolStripMenuItem1.Click += new System.EventHandler(this.optionListsToolStripMenuItem1_Click);
            // 
            // importExportToolStripMenuItem
            // 
            this.importExportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportStockDataToolStripMenuItem,
            this.importReplenishListToolStripMenuItem,
            this.importErrorListToolStripMenuItem,
            this.viewExportErrorListToolStripMenuItem});
            this.importExportToolStripMenuItem.Name = "importExportToolStripMenuItem";
            this.importExportToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.importExportToolStripMenuItem.Text = "&Import/Export";
            // 
            // exportStockDataToolStripMenuItem
            // 
            this.exportStockDataToolStripMenuItem.Name = "exportStockDataToolStripMenuItem";
            this.exportStockDataToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.exportStockDataToolStripMenuItem.Text = "E&xport Stock Data";
            this.exportStockDataToolStripMenuItem.Click += new System.EventHandler(this.exportStockDataToolStripMenuItem_Click);
            // 
            // importReplenishListToolStripMenuItem
            // 
            this.importReplenishListToolStripMenuItem.Name = "importReplenishListToolStripMenuItem";
            this.importReplenishListToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.importReplenishListToolStripMenuItem.Text = "&Import Replenish List";
            this.importReplenishListToolStripMenuItem.Click += new System.EventHandler(this.importReplenishListToolStripMenuItem_Click);
            // 
            // importErrorListToolStripMenuItem
            // 
            this.importErrorListToolStripMenuItem.Name = "importErrorListToolStripMenuItem";
            this.importErrorListToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.importErrorListToolStripMenuItem.Text = "View Import Error List";
            this.importErrorListToolStripMenuItem.Click += new System.EventHandler(this.importErrorListToolStripMenuItem_Click);
            // 
            // viewExportErrorListToolStripMenuItem
            // 
            this.viewExportErrorListToolStripMenuItem.Name = "viewExportErrorListToolStripMenuItem";
            this.viewExportErrorListToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.viewExportErrorListToolStripMenuItem.Text = "View Export Error List";
            this.viewExportErrorListToolStripMenuItem.Click += new System.EventHandler(this.viewExportErrorListToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.remoteSupportToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // remoteSupportToolStripMenuItem
            // 
            this.remoteSupportToolStripMenuItem.Name = "remoteSupportToolStripMenuItem";
            this.remoteSupportToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.remoteSupportToolStripMenuItem.Text = "&Remote Support";
            this.remoteSupportToolStripMenuItem.Click += new System.EventHandler(this.remoteSupportToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.usersToolStripMenuItem.Text = "&Users";
            // 
            // optionListsToolStripMenuItem
            // 
            this.optionListsToolStripMenuItem.Name = "optionListsToolStripMenuItem";
            this.optionListsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.optionListsToolStripMenuItem.Text = "&Option Lists";
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(157, 117);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1079, 670);
            this.pnlMain.TabIndex = 3;
            // 
            // pnlLeft
            // 
            this.pnlLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLeft.Controls.Add(this.pnlOptions);
            this.pnlLeft.Controls.Add(this.btnNavDown);
            this.pnlLeft.Controls.Add(this.btnNavUp);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 117);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(157, 670);
            this.pnlLeft.TabIndex = 4;
            this.pnlLeft.Visible = false;
            // 
            // pnlOptions
            // 
            this.pnlOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlOptions.AutoScroll = true;
            this.pnlOptions.BackColor = global::Stock_Management_Desktop.Properties.Settings.Default.FormBackground;
            this.pnlOptions.Controls.Add(this.btnStockTake);
            this.pnlOptions.Controls.Add(this.btnReplenishLists);
            this.pnlOptions.Controls.Add(this.btnRetailers);
            this.pnlOptions.Controls.Add(this.btnBatches);
            this.pnlOptions.Controls.Add(this.btnLocations);
            this.pnlOptions.Controls.Add(this.btnComponents);
            this.pnlOptions.Controls.Add(this.btnStockMaintenance);
            this.pnlOptions.Location = new System.Drawing.Point(0, 42);
            this.pnlOptions.Name = "pnlOptions";
            this.pnlOptions.Size = new System.Drawing.Size(174, 589);
            this.pnlOptions.StackFlowLayout = Stock_Management_Desktop.Controls.StackPanel.StackFlow.Vertical;
            this.pnlOptions.StackFullWidth = true;
            this.pnlOptions.TabIndex = 1;
            // 
            // btnStockTake
            // 
            this.btnStockTake.FlatAppearance.BorderSize = 0;
            this.btnStockTake.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnStockTake.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockTake.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStockTake.Image = global::Stock_Management_Desktop.Properties.Resources.Stock_Take;
            this.btnStockTake.Location = new System.Drawing.Point(25, 783);
            this.btnStockTake.Name = "btnStockTake";
            this.btnStockTake.Size = new System.Drawing.Size(105, 138);
            this.btnStockTake.TabIndex = 6;
            this.btnStockTake.UseVisualStyleBackColor = true;
            this.btnStockTake.Click += new System.EventHandler(this.button_Click);
            this.btnStockTake.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnStockTake.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnReplenishLists
            // 
            this.btnReplenishLists.FlatAppearance.BorderSize = 0;
            this.btnReplenishLists.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnReplenishLists.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplenishLists.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReplenishLists.Image = global::Stock_Management_Desktop.Properties.Resources.Replenish_Lists;
            this.btnReplenishLists.Location = new System.Drawing.Point(25, 649);
            this.btnReplenishLists.Name = "btnReplenishLists";
            this.btnReplenishLists.Size = new System.Drawing.Size(105, 138);
            this.btnReplenishLists.TabIndex = 5;
            this.btnReplenishLists.UseVisualStyleBackColor = true;
            this.btnReplenishLists.Click += new System.EventHandler(this.button_Click);
            this.btnReplenishLists.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnReplenishLists.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnRetailers
            // 
            this.btnRetailers.FlatAppearance.BorderSize = 0;
            this.btnRetailers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnRetailers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetailers.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetailers.Image = global::Stock_Management_Desktop.Properties.Resources.Retailers;
            this.btnRetailers.Location = new System.Drawing.Point(25, 525);
            this.btnRetailers.Name = "btnRetailers";
            this.btnRetailers.Size = new System.Drawing.Size(105, 138);
            this.btnRetailers.TabIndex = 4;
            this.btnRetailers.UseVisualStyleBackColor = true;
            this.btnRetailers.Click += new System.EventHandler(this.button_Click);
            this.btnRetailers.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnRetailers.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnBatches
            // 
            this.btnBatches.FlatAppearance.BorderSize = 0;
            this.btnBatches.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnBatches.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBatches.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBatches.Image = global::Stock_Management_Desktop.Properties.Resources.Batch_Maintenance;
            this.btnBatches.Location = new System.Drawing.Point(25, 396);
            this.btnBatches.Name = "btnBatches";
            this.btnBatches.Size = new System.Drawing.Size(105, 138);
            this.btnBatches.TabIndex = 3;
            this.btnBatches.UseVisualStyleBackColor = true;
            this.btnBatches.Click += new System.EventHandler(this.button_Click);
            this.btnBatches.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnBatches.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnLocations
            // 
            this.btnLocations.FlatAppearance.BorderSize = 0;
            this.btnLocations.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnLocations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocations.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocations.Image = global::Stock_Management_Desktop.Properties.Resources.Location_Maintenance;
            this.btnLocations.Location = new System.Drawing.Point(25, 267);
            this.btnLocations.Name = "btnLocations";
            this.btnLocations.Size = new System.Drawing.Size(105, 138);
            this.btnLocations.TabIndex = 2;
            this.btnLocations.UseVisualStyleBackColor = true;
            this.btnLocations.Click += new System.EventHandler(this.button_Click);
            this.btnLocations.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnLocations.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnComponents
            // 
            this.btnComponents.FlatAppearance.BorderSize = 0;
            this.btnComponents.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnComponents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComponents.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComponents.Image = global::Stock_Management_Desktop.Properties.Resources.Component_Maintenance;
            this.btnComponents.Location = new System.Drawing.Point(25, 138);
            this.btnComponents.Name = "btnComponents";
            this.btnComponents.Size = new System.Drawing.Size(105, 138);
            this.btnComponents.TabIndex = 1;
            this.btnComponents.UseVisualStyleBackColor = true;
            this.btnComponents.Click += new System.EventHandler(this.button_Click);
            this.btnComponents.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnComponents.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnStockMaintenance
            // 
            this.btnStockMaintenance.FlatAppearance.BorderSize = 0;
            this.btnStockMaintenance.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnStockMaintenance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockMaintenance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStockMaintenance.Image = global::Stock_Management_Desktop.Properties.Resources.Stock_Maintenance;
            this.btnStockMaintenance.Location = new System.Drawing.Point(25, 9);
            this.btnStockMaintenance.Name = "btnStockMaintenance";
            this.btnStockMaintenance.Size = new System.Drawing.Size(105, 138);
            this.btnStockMaintenance.TabIndex = 0;
            this.btnStockMaintenance.UseVisualStyleBackColor = true;
            this.btnStockMaintenance.Click += new System.EventHandler(this.button_Click);
            this.btnStockMaintenance.MouseEnter += new System.EventHandler(this.button_MouseEnter);
            this.btnStockMaintenance.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            // 
            // btnNavDown
            // 
            this.btnNavDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnNavDown.FlatAppearance.BorderSize = 0;
            this.btnNavDown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnNavDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavDown.Image = global::Stock_Management_Desktop.Properties.Resources.down_arrow;
            this.btnNavDown.Location = new System.Drawing.Point(0, 632);
            this.btnNavDown.Name = "btnNavDown";
            this.btnNavDown.Size = new System.Drawing.Size(155, 36);
            this.btnNavDown.TabIndex = 9;
            this.btnNavDown.UseVisualStyleBackColor = true;
            this.btnNavDown.MouseEnter += new System.EventHandler(this.btnNavDown_MouseEnter);
            this.btnNavDown.MouseLeave += new System.EventHandler(this.btnNavDown_MouseLeave);
            // 
            // btnNavUp
            // 
            this.btnNavUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNavUp.FlatAppearance.BorderSize = 0;
            this.btnNavUp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnNavUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavUp.Image = global::Stock_Management_Desktop.Properties.Resources.up_arrow;
            this.btnNavUp.Location = new System.Drawing.Point(0, 0);
            this.btnNavUp.Name = "btnNavUp";
            this.btnNavUp.Size = new System.Drawing.Size(155, 36);
            this.btnNavUp.TabIndex = 8;
            this.btnNavUp.UseVisualStyleBackColor = true;
            this.btnNavUp.MouseEnter += new System.EventHandler(this.btnNavUp_MouseEnter);
            this.btnNavUp.MouseLeave += new System.EventHandler(this.btnNavUp_MouseLeave);
            // 
            // timNavUp
            // 
            this.timNavUp.Interval = 10;
            this.timNavUp.Tick += new System.EventHandler(this.timNavUp_Tick);
            // 
            // timNavDown
            // 
            this.timNavDown.Interval = 10;
            this.timNavDown.Tick += new System.EventHandler(this.timNavDown_Tick);
            // 
            // timNotify
            // 
            this.timNotify.Enabled = true;
            this.timNotify.Interval = 10000;
            this.timNotify.Tick += new System.EventHandler(this.timNotify_Tick);
            // 
            // lblDownloadsError
            // 
            this.lblDownloadsError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDownloadsError.AutoSize = true;
            this.lblDownloadsError.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDownloadsError.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDownloadsError.ForeColor = System.Drawing.Color.LightCoral;
            this.lblDownloadsError.Location = new System.Drawing.Point(767, 32);
            this.lblDownloadsError.Name = "lblDownloadsError";
            this.lblDownloadsError.Size = new System.Drawing.Size(462, 19);
            this.lblDownloadsError.TabIndex = 5;
            this.lblDownloadsError.Text = "Download Errors - Check Event Viewer - click to dismiss";
            this.lblDownloadsError.Visible = false;
            this.lblDownloadsError.Click += new System.EventHandler(this.lblDownloadsError_Click);
            // 
            // lblUploadErrors
            // 
            this.lblUploadErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUploadErrors.AutoSize = true;
            this.lblUploadErrors.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblUploadErrors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUploadErrors.ForeColor = System.Drawing.Color.LightCoral;
            this.lblUploadErrors.Location = new System.Drawing.Point(791, 55);
            this.lblUploadErrors.Name = "lblUploadErrors";
            this.lblUploadErrors.Size = new System.Drawing.Size(438, 19);
            this.lblUploadErrors.TabIndex = 6;
            this.lblUploadErrors.Text = "Upload Errors - Check Event Viewer - click to dismiss";
            this.lblUploadErrors.Visible = false;
            this.lblUploadErrors.Click += new System.EventHandler(this.lblUploadErrors_Click);
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Stock_Management_Desktop.Properties.Settings.Default.FormBackground;
            this.ClientSize = new System.Drawing.Size(1236, 787);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.menuStrip1);
            this.Font = global::Stock_Management_Desktop.Properties.Settings.Default.FormFont;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmMainForm";
            this.Text = "Plantracker";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMainForm_FormClosing);
            this.Load += new System.EventHandler(this.frmMainForm_Load);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlLeft.ResumeLayout(false);
            this.pnlOptions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lblSystemName;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportStockDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importReplenishListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remoteSupportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button btnRetailers;
        private System.Windows.Forms.Button btnBatches;
        private System.Windows.Forms.Button btnLocations;
        private System.Windows.Forms.Button btnComponents;
        private System.Windows.Forms.Button btnStockMaintenance;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionListsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem optionListsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importErrorListToolStripMenuItem;
        private System.Windows.Forms.Button btnReplenishLists;
        private System.Windows.Forms.Button btnStockTake;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Button btnNavDown;
        private System.Windows.Forms.Button btnNavUp;
        private System.Windows.Forms.Timer timNavUp;
        private System.Windows.Forms.Timer timNavDown;
        public Controls.StackPanel pnlOptions;
        private System.Windows.Forms.ToolStripMenuItem viewExportErrorListToolStripMenuItem;
        private System.Windows.Forms.Timer timNotify;
        private System.Windows.Forms.Label lblReplenishListsAvail;
        private System.Windows.Forms.Label lblUploadErrors;
        private System.Windows.Forms.Label lblDownloadsError;
    }
}