﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using BLL;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmQtyChange : Stock_Management_Desktop.Forms.frmBase
    {
        public frmQtyChange()
        {
            InitializeComponent();
        }

        public string prompt { get; set; }
        public string reason { get; set; }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cboReason.Text == string.Empty)
            {
                MessageBox.Show("Please enter a reason for the change", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                reason = cboReason.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void frmQtyChange_Load(object sender, EventArgs e)
        {
            Suppress = true;
            lblInformation.Text = prompt;
            cboReason.DataSource = BatchLogic.RetrieveReasonList();
        }



    }
}
