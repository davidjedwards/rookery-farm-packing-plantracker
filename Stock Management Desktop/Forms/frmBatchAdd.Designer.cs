﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmBatchAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBatchNo = new System.Windows.Forms.TextBox();
            this.dtpBatchDate = new System.Windows.Forms.DateTimePicker();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.cboQuality = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSearchComponent = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblComponentCode = new System.Windows.Forms.Label();
            this.nudShippableWeek = new System.Windows.Forms.NumericUpDown();
            this.nudShippableYear = new System.Windows.Forms.NumericUpDown();
            this.btnSelectLocation = new System.Windows.Forms.Button();
            this.cboLocation = new System.Windows.Forms.ComboBox();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableYear)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnSave);
            this.pnlFooter.Controls.Add(this.btnCancel);
            this.pnlFooter.Size = new System.Drawing.Size(560, 44);
            this.pnlFooter.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Component";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Batch Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Initial Location";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Quantity";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Quality";
            // 
            // txtBatchNo
            // 
            this.txtBatchNo.Location = new System.Drawing.Point(147, 64);
            this.txtBatchNo.Name = "txtBatchNo";
            this.txtBatchNo.Size = new System.Drawing.Size(70, 23);
            this.txtBatchNo.TabIndex = 1;
            // 
            // dtpBatchDate
            // 
            this.dtpBatchDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBatchDate.Location = new System.Drawing.Point(147, 104);
            this.dtpBatchDate.Name = "dtpBatchDate";
            this.dtpBatchDate.Size = new System.Drawing.Size(112, 23);
            this.dtpBatchDate.TabIndex = 2;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(147, 190);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(70, 23);
            this.txtQuantity.TabIndex = 5;
            // 
            // cboQuality
            // 
            this.cboQuality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboQuality.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboQuality.FormattingEnabled = true;
            this.cboQuality.Location = new System.Drawing.Point(147, 232);
            this.cboQuality.Name = "cboQuality";
            this.cboQuality.Size = new System.Drawing.Size(129, 24);
            this.cboQuality.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(299, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Shippable Week";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::Stock_Management_Desktop.Properties.Resources.Save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(478, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::Stock_Management_Desktop.Properties.Resources.cross;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(241, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 30);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSearchComponent
            // 
            this.btnSearchComponent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchComponent.Image = global::Stock_Management_Desktop.Properties.Resources.search;
            this.btnSearchComponent.Location = new System.Drawing.Point(468, 19);
            this.btnSearchComponent.Name = "btnSearchComponent";
            this.btnSearchComponent.Size = new System.Drawing.Size(34, 29);
            this.btnSearchComponent.TabIndex = 0;
            this.btnSearchComponent.UseVisualStyleBackColor = true;
            this.btnSearchComponent.Click += new System.EventHandler(this.btnSearchComponent_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblComponentCode);
            this.panel2.Location = new System.Drawing.Point(148, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(310, 25);
            this.panel2.TabIndex = 15;
            // 
            // lblComponentCode
            // 
            this.lblComponentCode.AutoSize = true;
            this.lblComponentCode.Location = new System.Drawing.Point(3, 3);
            this.lblComponentCode.Name = "lblComponentCode";
            this.lblComponentCode.Size = new System.Drawing.Size(0, 16);
            this.lblComponentCode.TabIndex = 8;
            // 
            // nudShippableWeek
            // 
            this.nudShippableWeek.Location = new System.Drawing.Point(407, 233);
            this.nudShippableWeek.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudShippableWeek.Name = "nudShippableWeek";
            this.nudShippableWeek.Size = new System.Drawing.Size(51, 23);
            this.nudShippableWeek.TabIndex = 7;
            this.nudShippableWeek.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudShippableYear
            // 
            this.nudShippableYear.Location = new System.Drawing.Point(468, 233);
            this.nudShippableYear.Maximum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.nudShippableYear.Minimum = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.nudShippableYear.Name = "nudShippableYear";
            this.nudShippableYear.Size = new System.Drawing.Size(57, 23);
            this.nudShippableYear.TabIndex = 8;
            this.nudShippableYear.Value = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.nudShippableYear.ValueChanged += new System.EventHandler(this.nudShippableYear_ValueChanged);
            // 
            // btnSelectLocation
            // 
            this.btnSelectLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectLocation.Image = global::Stock_Management_Desktop.Properties.Resources.search;
            this.btnSelectLocation.Location = new System.Drawing.Point(285, 145);
            this.btnSelectLocation.Name = "btnSelectLocation";
            this.btnSelectLocation.Size = new System.Drawing.Size(34, 29);
            this.btnSelectLocation.TabIndex = 4;
            this.btnSelectLocation.UseVisualStyleBackColor = true;
            this.btnSelectLocation.Click += new System.EventHandler(this.btnSelectLocation_Click);
            // 
            // cboLocation
            // 
            this.cboLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLocation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLocation.FormattingEnabled = true;
            this.cboLocation.Location = new System.Drawing.Point(147, 148);
            this.cboLocation.Name = "cboLocation";
            this.cboLocation.Size = new System.Drawing.Size(129, 24);
            this.cboLocation.TabIndex = 3;
            // 
            // frmBatchAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(560, 345);
            this.ControlBox = false;
            this.Controls.Add(this.cboLocation);
            this.Controls.Add(this.btnSelectLocation);
            this.Controls.Add(this.nudShippableYear);
            this.Controls.Add(this.nudShippableWeek);
            this.Controls.Add(this.btnSearchComponent);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboQuality);
            this.Controls.Add(this.txtQuantity);
            this.Controls.Add(this.dtpBatchDate);
            this.Controls.Add(this.txtBatchNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmBatchAdd";
            this.Text = "Add Batch";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmBatchAdd_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.txtBatchNo, 0);
            this.Controls.SetChildIndex(this.dtpBatchDate, 0);
            this.Controls.SetChildIndex(this.txtQuantity, 0);
            this.Controls.SetChildIndex(this.cboQuality, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.btnSearchComponent, 0);
            this.Controls.SetChildIndex(this.nudShippableWeek, 0);
            this.Controls.SetChildIndex(this.nudShippableYear, 0);
            this.Controls.SetChildIndex(this.btnSelectLocation, 0);
            this.Controls.SetChildIndex(this.cboLocation, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShippableYear)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBatchNo;
        private System.Windows.Forms.DateTimePicker dtpBatchDate;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.ComboBox cboQuality;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSearchComponent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblComponentCode;
        private System.Windows.Forms.NumericUpDown nudShippableWeek;
        private System.Windows.Forms.NumericUpDown nudShippableYear;
        private System.Windows.Forms.Button btnSelectLocation;
        private System.Windows.Forms.ComboBox cboLocation;
    }
}
