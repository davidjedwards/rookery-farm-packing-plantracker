﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using BLL;
using Stock_Management_Desktop.Classes;
using System.IO;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmExport : Stock_Management_Desktop.Forms.frmBase
    {
        public frmExport()
        {
            InitializeComponent();
        }

        private void frmExport_Load(object sender, EventArgs e)
        {
            Suppress = true;
            //Set up retailer combo
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            Functions.FormatButton(btnCancel);
            Functions.FormatButton(btnExport);

            List<Retailer> rtlList = RetailerLogic.ListRetailers(false);
            foreach (Retailer rtl in rtlList)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = rtl.Id;
                dsRetailer["Name"] = rtl.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cboRetailer.DataSource = dtRetailer;
            cboRetailer.DisplayMember = "Name";
            cboRetailer.ValueMember = "Id";

            IdIndivReqdField(cboRetailer);

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //Ensure retailer selected
            ep.SetError(cboRetailer, string.Empty);
            if ((int)cboRetailer.SelectedValue == 0)
            {
                ep.SetError(cboRetailer, "Please select a retailer");
            }
            else
            {
                frmProgress frmP = new frmProgress();
                frmP.lblProgress.Text = "Exporting shippable stock list...";
                frmP.Show();
                frmP.Refresh();

                try
                {
                    string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    string fileName = cboRetailer.Text + " Stock List.csv";
                    string fullPath = Path.Combine(filePath, fileName);
                    Retailer r = RetailerLogic.RetrieveRetailer((int)cboRetailer.SelectedValue);
                    if (BLLFunctions.ExportStockList(r, fullPath))
                        MessageBox.Show("Stock list exported to: " + fullPath, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    frmP.Close();
                    frmP.Dispose();
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                catch (Exception ex)
                {
                    if (frmP != null)
                    {
                        frmP.Close();
                        frmP.Dispose();
                    }
                    ExceptionLogic.HandleException(ex, this.Name + "_btnExport_Click", true);
                }
            }
        }
    }
}
