﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmStockTake
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtStockCode = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboFilterRetailer = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvStockTake = new System.Windows.Forms.DataGridView();
            this.ComponentIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetailerStockCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddButtonColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvStockLevels = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockLevelIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocationCodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComponentDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QualityDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.pnlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockTake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockLevels)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Location = new System.Drawing.Point(0, 602);
            this.pnlFooter.Size = new System.Drawing.Size(1042, 44);
            // 
            // pnlFilter
            // 
            this.pnlFilter.Controls.Add(this.txtDescription);
            this.pnlFilter.Controls.Add(this.txtStockCode);
            this.pnlFilter.Controls.Add(this.btnSearch);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Controls.Add(this.cboFilterRetailer);
            this.pnlFilter.Controls.Add(this.label4);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(1042, 42);
            this.pnlFilter.TabIndex = 3;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(692, 11);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(228, 23);
            this.txtDescription.TabIndex = 16;
            // 
            // txtStockCode
            // 
            this.txtStockCode.Location = new System.Drawing.Point(447, 11);
            this.txtStockCode.Name = "txtStockCode";
            this.txtStockCode.Size = new System.Drawing.Size(109, 23);
            this.txtStockCode.TabIndex = 15;
            // 
            // btnSearch
            // 
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::Stock_Management_Desktop.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(936, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(76, 33);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Retailer";
            // 
            // cboFilterRetailer
            // 
            this.cboFilterRetailer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterRetailer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterRetailer.FormattingEnabled = true;
            this.cboFilterRetailer.Items.AddRange(new object[] {
            "3 (15/04/2014)",
            "2 (30/03/2014)",
            "1 (16/03/2014)",
            "3 (22/04/2013)",
            "2 (29/03/2013)",
            "1 (12/03/2013)"});
            this.cboFilterRetailer.Location = new System.Drawing.Point(70, 11);
            this.cboFilterRetailer.Name = "cboFilterRetailer";
            this.cboFilterRetailer.Size = new System.Drawing.Size(231, 24);
            this.cboFilterRetailer.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(562, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Description (or part)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(316, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stock Code (or part)";
            // 
            // dgvStockTake
            // 
            this.dgvStockTake.AllowUserToAddRows = false;
            this.dgvStockTake.AllowUserToDeleteRows = false;
            this.dgvStockTake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStockTake.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ComponentIdColumn,
            this.RetailerStockCodeColumn,
            this.DescriptionColumn,
            this.AddButtonColumn});
            this.dgvStockTake.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStockTake.Location = new System.Drawing.Point(0, 0);
            this.dgvStockTake.Name = "dgvStockTake";
            this.dgvStockTake.ReadOnly = true;
            this.dgvStockTake.RowHeadersVisible = false;
            this.dgvStockTake.Size = new System.Drawing.Size(513, 560);
            this.dgvStockTake.TabIndex = 4;
            this.dgvStockTake.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockTake_CellClick);
            this.dgvStockTake.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvStockTake_CellPainting);
            this.dgvStockTake.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dgvStockTake_CellToolTipTextNeeded);
            // 
            // ComponentIdColumn
            // 
            this.ComponentIdColumn.DataPropertyName = "Id";
            this.ComponentIdColumn.HeaderText = "";
            this.ComponentIdColumn.Name = "ComponentIdColumn";
            this.ComponentIdColumn.ReadOnly = true;
            this.ComponentIdColumn.Visible = false;
            // 
            // RetailerStockCodeColumn
            // 
            this.RetailerStockCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RetailerStockCodeColumn.DataPropertyName = "RetailerStockCode";
            this.RetailerStockCodeColumn.HeaderText = "Retailer Stock Code";
            this.RetailerStockCodeColumn.Name = "RetailerStockCodeColumn";
            this.RetailerStockCodeColumn.ReadOnly = true;
            this.RetailerStockCodeColumn.Width = 106;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.DataPropertyName = "Description";
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.ReadOnly = true;
            // 
            // AddButtonColumn
            // 
            this.AddButtonColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AddButtonColumn.HeaderText = "";
            this.AddButtonColumn.MinimumWidth = 25;
            this.AddButtonColumn.Name = "AddButtonColumn";
            this.AddButtonColumn.ReadOnly = true;
            this.AddButtonColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AddButtonColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AddButtonColumn.Width = 25;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvStockTake);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvStockLevels);
            this.splitContainer1.Size = new System.Drawing.Size(1042, 560);
            this.splitContainer1.SplitterDistance = 513;
            this.splitContainer1.TabIndex = 5;
            // 
            // dgvStockLevels
            // 
            this.dgvStockLevels.AllowUserToAddRows = false;
            this.dgvStockLevels.AllowUserToDeleteRows = false;
            this.dgvStockLevels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStockLevels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StockLevelIdColumn,
            this.LocationCodeColumn,
            this.ComponentDescColumn,
            this.QuantityColumn,
            this.QualityDescColumn,
            this.DeleteColumn});
            this.dgvStockLevels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStockLevels.Location = new System.Drawing.Point(0, 0);
            this.dgvStockLevels.Name = "dgvStockLevels";
            this.dgvStockLevels.ReadOnly = true;
            this.dgvStockLevels.RowHeadersVisible = false;
            this.dgvStockLevels.Size = new System.Drawing.Size(525, 560);
            this.dgvStockLevels.TabIndex = 0;
            this.dgvStockLevels.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStockLevels_CellClick);
            this.dgvStockLevels.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvStockLevels_CellPainting);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "RetailerStockCode";
            this.dataGridViewTextBoxColumn2.HeaderText = "Retailer Stock Code";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn3.HeaderText = "Description";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // StockLevelIdColumn
            // 
            this.StockLevelIdColumn.DataPropertyName = "StockLevelId";
            this.StockLevelIdColumn.HeaderText = "";
            this.StockLevelIdColumn.Name = "StockLevelIdColumn";
            this.StockLevelIdColumn.ReadOnly = true;
            this.StockLevelIdColumn.Visible = false;
            // 
            // LocationCodeColumn
            // 
            this.LocationCodeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LocationCodeColumn.DataPropertyName = "LocationCode";
            this.LocationCodeColumn.HeaderText = "Location";
            this.LocationCodeColumn.Name = "LocationCodeColumn";
            this.LocationCodeColumn.ReadOnly = true;
            this.LocationCodeColumn.Width = 80;
            // 
            // ComponentDescColumn
            // 
            this.ComponentDescColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ComponentDescColumn.DataPropertyName = "ComponentDesc";
            this.ComponentDescColumn.HeaderText = "Component";
            this.ComponentDescColumn.Name = "ComponentDescColumn";
            this.ComponentDescColumn.ReadOnly = true;
            // 
            // QuantityColumn
            // 
            this.QuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QuantityColumn.DataPropertyName = "Quantity";
            this.QuantityColumn.HeaderText = "Quantity";
            this.QuantityColumn.Name = "QuantityColumn";
            this.QuantityColumn.ReadOnly = true;
            this.QuantityColumn.Width = 80;
            // 
            // QualityDescColumn
            // 
            this.QualityDescColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.QualityDescColumn.DataPropertyName = "QualityDesc";
            this.QualityDescColumn.HeaderText = "Quality";
            this.QualityDescColumn.Name = "QualityDescColumn";
            this.QualityDescColumn.ReadOnly = true;
            this.QualityDescColumn.Width = 72;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.ReadOnly = true;
            this.DeleteColumn.Width = 25;
            // 
            // frmStockTake
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 646);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmStockTake";
            this.Text = "Stock Take";
            this.Load += new System.EventHandler(this.frmStockTake_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockTake)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockLevels)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboFilterRetailer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtStockCode;
        private System.Windows.Forms.DataGridView dgvStockTake;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComponentIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetailerStockCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.DataGridViewButtonColumn AddButtonColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dgvStockLevels;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockLevelIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocationCodeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComponentDescColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QualityDescColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
    }
}