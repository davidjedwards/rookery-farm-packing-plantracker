﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmReplenishLists
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.cboRetailer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvReplenishLists = new System.Windows.Forms.DataGridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvPickingListContents = new System.Windows.Forms.DataGridView();
            this.RLDIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RLDComponentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShowAddColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RLDComponentDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RLDQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RLDPickedColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetailerNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReplenishDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusDescColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApplyColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            this.pnlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReplenishLists)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickingListContents)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Location = new System.Drawing.Point(0, 543);
            this.pnlFooter.Size = new System.Drawing.Size(1191, 44);
            // 
            // pnlFilter
            // 
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFilter.Controls.Add(this.cboRetailer);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(577, 42);
            this.pnlFilter.TabIndex = 4;
            // 
            // cboRetailer
            // 
            this.cboRetailer.FormattingEnabled = true;
            this.cboRetailer.Location = new System.Drawing.Point(119, 10);
            this.cboRetailer.Name = "cboRetailer";
            this.cboRetailer.Size = new System.Drawing.Size(349, 24);
            this.cboRetailer.TabIndex = 1;
            this.cboRetailer.SelectedValueChanged += new System.EventHandler(this.cboRetailer_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Retailer";
            // 
            // dgvReplenishLists
            // 
            this.dgvReplenishLists.AllowUserToAddRows = false;
            this.dgvReplenishLists.AllowUserToDeleteRows = false;
            this.dgvReplenishLists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReplenishLists.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.StatusColumn,
            this.RetailerNameColumn,
            this.FileNameColumn,
            this.ReplenishDateColumn,
            this.StatusDescColumn,
            this.ApplyColumn,
            this.DeleteColumn});
            this.dgvReplenishLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReplenishLists.Location = new System.Drawing.Point(0, 42);
            this.dgvReplenishLists.Name = "dgvReplenishLists";
            this.dgvReplenishLists.ReadOnly = true;
            this.dgvReplenishLists.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReplenishLists.Size = new System.Drawing.Size(577, 501);
            this.dgvReplenishLists.TabIndex = 5;
            this.dgvReplenishLists.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReplenishLists_CellClick);
            this.dgvReplenishLists.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvReplenishLists_CellPainting);
            this.dgvReplenishLists.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReplenishLists_RowEnter);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvReplenishLists);
            this.splitContainer1.Panel1.Controls.Add(this.pnlFilter);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvPickingListContents);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1191, 543);
            this.splitContainer1.SplitterDistance = 577;
            this.splitContainer1.TabIndex = 6;
            // 
            // dgvPickingListContents
            // 
            this.dgvPickingListContents.AllowUserToAddRows = false;
            this.dgvPickingListContents.AllowUserToDeleteRows = false;
            this.dgvPickingListContents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPickingListContents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RLDIdColumn,
            this.RLDComponentColumn,
            this.ShowAddColumn,
            this.RLDComponentDescColumn,
            this.RLDQuantityColumn,
            this.RLDPickedColumn,
            this.AddColumn});
            this.dgvPickingListContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPickingListContents.Location = new System.Drawing.Point(0, 42);
            this.dgvPickingListContents.Name = "dgvPickingListContents";
            this.dgvPickingListContents.ReadOnly = true;
            this.dgvPickingListContents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPickingListContents.Size = new System.Drawing.Size(610, 501);
            this.dgvPickingListContents.TabIndex = 1;
            this.dgvPickingListContents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPickingListContents_CellClick);
            this.dgvPickingListContents.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvPickingListContents_CellPainting);
            this.dgvPickingListContents.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dgvPickingListContents_CellToolTipTextNeeded);
            // 
            // RLDIdColumn
            // 
            this.RLDIdColumn.DataPropertyName = "Id";
            this.RLDIdColumn.HeaderText = "";
            this.RLDIdColumn.Name = "RLDIdColumn";
            this.RLDIdColumn.ReadOnly = true;
            this.RLDIdColumn.Visible = false;
            // 
            // RLDComponentColumn
            // 
            this.RLDComponentColumn.DataPropertyName = "Component";
            this.RLDComponentColumn.HeaderText = "";
            this.RLDComponentColumn.Name = "RLDComponentColumn";
            this.RLDComponentColumn.ReadOnly = true;
            this.RLDComponentColumn.Visible = false;
            // 
            // ShowAddColumn
            // 
            this.ShowAddColumn.DataPropertyName = "ShowAdd";
            this.ShowAddColumn.HeaderText = "";
            this.ShowAddColumn.Name = "ShowAddColumn";
            this.ShowAddColumn.ReadOnly = true;
            this.ShowAddColumn.Visible = false;
            // 
            // RLDComponentDescColumn
            // 
            this.RLDComponentDescColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RLDComponentDescColumn.DataPropertyName = "ComponentDesc";
            this.RLDComponentDescColumn.HeaderText = "Component";
            this.RLDComponentDescColumn.Name = "RLDComponentDescColumn";
            this.RLDComponentDescColumn.ReadOnly = true;
            // 
            // RLDQuantityColumn
            // 
            this.RLDQuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RLDQuantityColumn.DataPropertyName = "Quantity";
            this.RLDQuantityColumn.HeaderText = "Quantity";
            this.RLDQuantityColumn.Name = "RLDQuantityColumn";
            this.RLDQuantityColumn.ReadOnly = true;
            this.RLDQuantityColumn.Width = 80;
            // 
            // RLDPickedColumn
            // 
            this.RLDPickedColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RLDPickedColumn.DataPropertyName = "PickedQuantity";
            this.RLDPickedColumn.HeaderText = "Picked";
            this.RLDPickedColumn.Name = "RLDPickedColumn";
            this.RLDPickedColumn.ReadOnly = true;
            this.RLDPickedColumn.Width = 69;
            // 
            // AddColumn
            // 
            this.AddColumn.HeaderText = "";
            this.AddColumn.MinimumWidth = 25;
            this.AddColumn.Name = "AddColumn";
            this.AddColumn.ReadOnly = true;
            this.AddColumn.Width = 25;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(610, 42);
            this.panel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Picking List Contents";
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // StatusColumn
            // 
            this.StatusColumn.DataPropertyName = "Status";
            this.StatusColumn.HeaderText = "";
            this.StatusColumn.Name = "StatusColumn";
            this.StatusColumn.ReadOnly = true;
            this.StatusColumn.Visible = false;
            // 
            // RetailerNameColumn
            // 
            this.RetailerNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RetailerNameColumn.DataPropertyName = "RetailerName";
            this.RetailerNameColumn.HeaderText = "Retailer";
            this.RetailerNameColumn.Name = "RetailerNameColumn";
            this.RetailerNameColumn.ReadOnly = true;
            this.RetailerNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RetailerNameColumn.Width = 58;
            // 
            // FileNameColumn
            // 
            this.FileNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FileNameColumn.DataPropertyName = "FileName";
            this.FileNameColumn.HeaderText = "File Name";
            this.FileNameColumn.Name = "FileNameColumn";
            this.FileNameColumn.ReadOnly = true;
            // 
            // ReplenishDateColumn
            // 
            this.ReplenishDateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ReplenishDateColumn.DataPropertyName = "ReplenishDate";
            dataGridViewCellStyle1.Format = "d";
            this.ReplenishDateColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.ReplenishDateColumn.HeaderText = "Replenish Date";
            this.ReplenishDateColumn.Name = "ReplenishDateColumn";
            this.ReplenishDateColumn.ReadOnly = true;
            this.ReplenishDateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReplenishDateColumn.Width = 99;
            // 
            // StatusDescColumn
            // 
            this.StatusDescColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.StatusDescColumn.DataPropertyName = "StatusDesc";
            this.StatusDescColumn.HeaderText = "Status";
            this.StatusDescColumn.Name = "StatusDescColumn";
            this.StatusDescColumn.ReadOnly = true;
            this.StatusDescColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StatusDescColumn.Width = 50;
            // 
            // ApplyColumn
            // 
            this.ApplyColumn.HeaderText = "";
            this.ApplyColumn.MinimumWidth = 25;
            this.ApplyColumn.Name = "ApplyColumn";
            this.ApplyColumn.ReadOnly = true;
            this.ApplyColumn.Width = 25;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.ReadOnly = true;
            this.DeleteColumn.Width = 25;
            // 
            // frmReplenishLists
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 587);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "frmReplenishLists";
            this.Text = "Replenish Lists";
            this.Load += new System.EventHandler(this.frmReplenishLists_Load);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReplenishLists)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickingListContents)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.ComboBox cboRetailer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvReplenishLists;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvPickingListContents;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RLDIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RLDComponentColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShowAddColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RLDComponentDescColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RLDQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RLDPickedColumn;
        private System.Windows.Forms.DataGridViewButtonColumn AddColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetailerNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReplenishDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusDescColumn;
        private System.Windows.Forms.DataGridViewButtonColumn ApplyColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
    }
}