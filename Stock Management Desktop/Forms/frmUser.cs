﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmUser : Stock_Management_Desktop.Forms.frmBase
    {
        public frmUser()
        {
            InitializeComponent();
        }

        private void frmUser_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;
            EntityName = Globals.Entities.User;
            Functions.FormatButton(btnClose);
            Functions.StyleDataGridView(dgvUserList);
            dgvUserList.AutoGenerateColumns = false;
            SetUpGridCombos();
            LoadGrid();
            loading = false;
        }

        private void LoadGrid(int selectedId = 0)
        {
            gridLoading = true;
            int CurrTopRow = dgvUserList.FirstDisplayedScrollingRowIndex;
            if (CurrTopRow < 0)
                CurrTopRow = 0;

            dgvUserList.DataSource = BLL.UserLogic.ListUsers(chkShowInactive.Checked);

            if (selectedId != 0)
            {
                dgvUserList.FirstDisplayedScrollingRowIndex = CurrTopRow;
                int RowOfSelItem = 0;
                foreach (DataGridViewRow r in dgvUserList.Rows)
                {
                    if (((Nullable<int>)r.Cells["IdColumn"].Value ?? 0) == selectedId)
                        RowOfSelItem = r.Index;
                }
                dgvUserList.CurrentCell = dgvUserList.Rows[RowOfSelItem].Cells["LogonColumn"];
            }
            recordId = (int)dgvUserList.CurrentRow.Cells["IdColumn"].Value;
            gridLoading = false;
            GridIsValid = true;
        }

        private void chkShowInactive_CheckedChanged(object sender, EventArgs e)
        {
            //Refresh grid
            LoadGrid();
        }

        private void SetUpGridCombos()
        {
            //Retailer combo
            DataGridViewComboBoxColumn cbr = (DataGridViewComboBoxColumn)dgvUserList.Columns["RetailerColumn"];
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            List<Retailer> retailers = RetailerLogic.ListRetailers(false);
            foreach (Retailer _retailer in retailers)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = _retailer.Id;
                dsRetailer["Name"] = _retailer.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cbr.DisplayMember = "Name";
            cbr.ValueMember = "Id";
            cbr.DataSource = dtRetailer;

            //Permission combo
            DataGridViewComboBoxColumn cbp = (DataGridViewComboBoxColumn)dgvUserList.Columns["PermissionColumn"];
            DataTable dtPermission = new DataTable();
            dtPermission.Columns.Add("Id", typeof(int));
            dtPermission.Columns.Add("Desc", typeof(string));
            DataRow dsPermission;
            int PermissionNum = 0;
            foreach (Globals.Permission permission in (Globals.Permission[])Enum.GetValues(typeof(Globals.Permission)))
            {
                dsPermission = dtPermission.NewRow();
                dsPermission["Id"] = PermissionNum;
                dsPermission["Desc"] = permission.ToString().Replace("_", " ");
                dtPermission.Rows.Add(dsPermission);
                PermissionNum += 1;
            }
            cbp.DisplayMember = "Desc";
            cbp.ValueMember = "Id";
            cbp.DataSource = dtPermission;
            
        }

        private void dgvUserList_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvUserList.Columns["ResetPasswordColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    e.Graphics.DrawImage(Properties.Resources.password, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                    e.Handled = true;
                }
                else if (e.ColumnIndex == dgvUserList.Columns["ResetLockColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    e.Graphics.DrawImage(Properties.Resources.lock_edit, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                    e.Handled = true;
                }
                else if (e.ColumnIndex == dgvUserList.Columns["DeleteColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    if (dgvUserList.Rows[e.RowIndex].IsNewRow || (bool)dgvUserList.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                    {
                        e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                    else
                    {
                        e.Graphics.DrawImage(Properties.Resources.refresh, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvUserList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if ((dgvUserList.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewTextBoxCell) ||
                    dgvUserList.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewComboBoxCell)) &&
                    !dgvUserList.Rows[e.RowIndex].IsNewRow &&
                    !(bool)dgvUserList.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                {
                    dgvUserList.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Properties.Settings.Default.InactiveColour;
                    dgvUserList.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Properties.Settings.Default.InactiveColour;
                }
                else
                {
                    dgvUserList.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                    dgvUserList.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Black;
                }
            }
        }

        private void dgvUserList_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && dgvUserList.CurrentRow.Index >= 0 && !dgvUserList.CurrentRow.IsNewRow  &&
                dgvUserList.CurrentRow.Cells["IdColumn"].Value != null)
            {
                User thisUser = (User)dgvUserList.CurrentRow.DataBoundItem;
                string msg = string.Empty;
                if (thisUser.Id == 0)
                {
                    //new record
                    int thisId = UserLogic.AddUser(thisUser, Properties.Settings.Default.UserId, out msg);
                    if (thisId == 0) //validation failed
                    {
                        MessageBox.Show("New record failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dgvUserList.CurrentRow.ErrorText = msg;
                        dgvUserList.CurrentCell.Selected = false;
                        GridIsValid = false;
                        e.Cancel = true;
                    }
                    else
                    {
                        dgvUserList.CurrentRow.ErrorText = string.Empty;
                        GridIsValid = true;
                        thisUser.Id = thisId;
                    }
                }
                else //update existing record (but only if amended)
                {
                    if (dgvUserList.IsCurrentRowDirty)
                    {
                        if (!UserLogic.UpdateUser(thisUser, Properties.Settings.Default.UserId, out msg))
                        {
                            MessageBox.Show("Changes failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            dgvUserList.CurrentRow.ErrorText = msg;
                            dgvUserList.CurrentCell.Selected = false;
                            GridIsValid = false;
                            e.Cancel = true;
                        }
                        else
                        {
                            dgvUserList.CurrentRow.ErrorText = string.Empty;
                            GridIsValid = true;
                        }
                    }
                }
            }

        }

        private void chkShowAudit_CheckedChanged(object sender, EventArgs e)
        {
            dgvUserList.Columns["CreatedByColumn"].Visible = chkShowAudit.Checked;
            dgvUserList.Columns["CreatedDateColumn"].Visible = chkShowAudit.Checked;
            dgvUserList.Columns["LastModifiedByColumn"].Visible = chkShowAudit.Checked;
            dgvUserList.Columns["LastModifiedDateColumn"].Visible = chkShowAudit.Checked;

        }

        private void dgvUserList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Deal with clicks on the buttons Delete / Change Password
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvUserList.Columns["DeleteColumn"].Index)
                {
                    User thisUser = (User)dgvUserList.CurrentRow.DataBoundItem;
                    if (thisUser.Active)
                    {
                        if (MessageBox.Show("Are you sure you want to set this user to inactive?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            thisUser.Active = false;
                            dgvUserList.Rows[e.RowIndex].Cells["ActiveColumn"].Value = false;
                        }
                    }
                    else
                    {
                        thisUser.Active = true;
                        dgvUserList.Rows[e.RowIndex].Cells["ActiveColumn"].Value = true;
                    }
                }
                else if (e.ColumnIndex == dgvUserList.Columns["ResetPasswordColumn"].Index)
                {
                    if (MessageBox.Show(string.Format("Are you sure you want to reset the password for {0}?", dgvUserList.Rows[e.RowIndex].Cells["FullNameColumn"].Value.ToString()),
                        Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        User thisUser = (User)dgvUserList.CurrentRow.DataBoundItem;
                        thisUser.Password = string.Empty;
                        string msg;
                        if (UserLogic.UpdateUser(thisUser, Properties.Settings.Default.UserId, out msg))
                        {
                            MessageBox.Show("Password reset. Ask " + dgvUserList.Rows[e.RowIndex].Cells["FullNameColumn"].Value.ToString() + " to log in leaving the password field blank. The system will present a screen for the entry of a new password.",
                                Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else if (e.ColumnIndex == dgvUserList.Columns["ResetLockColumn"].Index)
                {
                    if (MessageBox.Show(string.Format("Please make sure {0} is logged out of the system.", dgvUserList.Rows[e.RowIndex].Cells["FullNameColumn"].Value.ToString()),
                        Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (CheckInOut.ClearUserLocks((int)dgvUserList.Rows[e.RowIndex].Cells["IdColumn"].Value))
                        {
                            MessageBox.Show("Successfully cleared locks for " + dgvUserList.Rows[e.RowIndex].Cells["FullNameColumn"].Value.ToString() + ".",
                                Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("There was a problem clearing the locks for " + dgvUserList.Rows[e.RowIndex].Cells["FullNameColumn"].Value.ToString() + ". Please contact support.",
                                Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }

            }       
        }

        private void dgvUserList_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvUserList.Columns["ResetPasswordColumn"].Index ||
                    e.ColumnIndex == dgvUserList.Columns["DeleteColumn"].Index ||
                    e.ColumnIndex == dgvUserList.Columns["ResetLockColumn"].Index)
                {
                    e.ToolTipText = dgvUserList.Columns[e.ColumnIndex].ToolTipText;
                }
            }
        }

        private void dgvUserList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!gridLoading && !dgvUserList.Rows[e.RowIndex].IsNewRow)
            {
                recordId = (int)dgvUserList.Rows[e.RowIndex].Cells["IdColumn"].Value;
                dgv_RowEnter(sender, e);
            }
        }

        private void dgvUserList_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && !dgvUserList.Rows[e.RowIndex].IsNewRow)
            {
                recordId = (int)dgvUserList.Rows[e.RowIndex].Cells["IdColumn"].Value;
                dgv_CellBeginEdit(sender, e);
            }
        }

        private void dgvUserList_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            dgv_RowValidated(sender, e);
        }
    }
}
