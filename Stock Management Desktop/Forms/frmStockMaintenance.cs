﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL;  

namespace Stock_Management_Desktop.Forms
{
    public partial class frmStockMaintenance : Stock_Management_Desktop.Forms.frmBase
    {
        RFP.Component comp;
        Globals.Permission perm;
        User loggedInUser;
        int Retailer;

        public frmStockMaintenance()
        {
            InitializeComponent();
         }

        private void cboComponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lblComponentCode.Text != string.Empty)
            {
                cboBatchNo.Enabled = true;
            }
            else
            {
                cboBatchNo.Enabled = false;
            }
        }

        private void btnSearchComponent_Click(object sender, EventArgs e)
        {
            frmComponentMaintenance frm = new frmComponentMaintenance();
            frm.SelectMode = true;
            frm.ShowDialog();
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                comp = frm.selectedComponent;
                lblComponentCode.Text = comp.RetailerStockCode;
                cboBatchNo.DataSource = BLL.ComponentLogic.BatchDropDownList(comp);
                cboBatchNo.Enabled = true;
                //No need to reload grid here because selected value changed on batch drop down will be fired.
            }
        }

        private void dgvStock_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!IsReadOnly)
            {
                //Check whether stock level is locked
                recordId = (int)dgvStock.Rows[e.RowIndex].Cells["IdColumn"].Value;
                string msg;
                if (!CheckInOut.CheckOut(EntityName, recordId, Properties.Settings.Default.UserId, out msg))
                {
                    MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    StockLevel sl = StockLevelLogic.RetrieveStockLevelFromId(recordId);
                    frmStock frm = new frmStock();
                    frm.thisStockLevel = sl;
                    frm.ShowDialog();
                    //Check stock level back in
                    if (!CheckInOut.CheckIn(EntityName, recordId, Properties.Settings.Default.UserId, out msg))
                    {
                        MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    //Reload list
                    LoadGrid(sl.Id);
                }
            }
            else
                MessageBox.Show("You are not authorised to edit stock level records", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void frmStockMaintenance_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;

            Functions.StyleDataGridView(dgvStock);
            Functions.FormatButton(btnExport);
            cboBatchNo.ValueMember = "BatchId";
            cboBatchNo.DisplayMember = "Description";

            loggedInUser = UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
            if (loggedInUser.Retailer != null)
                Retailer = (int)loggedInUser.Retailer;
            else
                Retailer = 0;

            //Populate quality combo
            cboQuality.DataSource = Globals.CropQualityList();
            cboQuality.ValueMember = "Key";
            cboQuality.DisplayMember = "Value";

            EntityName = Globals.Entities.StockLevel;

            SetPermissions();

            LoadGrid();

            loading = false;
        }

        private void SetPermissions()
        {
            perm = (Globals.Permission)loggedInUser.Permission;
            if (perm == Globals.Permission.Full && Retailer == 0)
                IsReadOnly = false;
            else
                IsReadOnly = true;

        }

        private void LoadGrid(int selectedId = 0)
        {
            gridLoading = true;
            int CurrTopRow = dgvStock.FirstDisplayedScrollingRowIndex;
            if (CurrTopRow < 0)
                CurrTopRow = 0;

            Nullable<int> compId;
            if (comp == null)
                compId = null;
            else
                compId = comp.Id;

            dgvStock.DataSource = BLL.StockLevelLogic.RetrieveStockLevelList(compId, (Nullable<int>)cboBatchNo.SelectedValue,
                (Nullable<int>)cboQuality.SelectedValue, chkShowQueriesOnly.Checked, Retailer);

            if (selectedId != 0 && CurrTopRow > 0)
            {
                dgvStock.FirstDisplayedScrollingRowIndex = CurrTopRow;
                int RowOfSelItem = 0;
                foreach (DataGridViewRow r in dgvStock.Rows)
                {
                    if (((Nullable<int>)r.Cells["IdColumn"].Value ?? 0) == selectedId)
                        RowOfSelItem = r.Index;
                }
                dgvStock.CurrentCell = dgvStock.Rows[RowOfSelItem].Cells["LocationCodeColumn"];
            }
            if (dgvStock.RowCount > 0)
                recordId = (int)dgvStock.CurrentRow.Cells["IdColumn"].Value;

            gridLoading = false;
            GridIsValid = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lblComponentCode.Text = string.Empty;
            comp = null;
            cboBatchNo.DataSource = null;
            cboBatchNo.Enabled = false;
            cboQuality.SelectedValue = -1;
            chkShowQueriesOnly.Checked = false;
            LoadGrid();
        }

        private void cboBatchNo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!loading)
                LoadGrid();
        }

        private void cboQuality_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!loading)
                LoadGrid();
        }

        private void chkShowQueriesOnly_CheckedChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //Export currently shown list to Excel
            frmProgress frmP = new frmProgress();
            frmP.lblProgress.Text = "Exporting stock levels...";
            frmP.Show();
            frmP.Refresh();

            //Create new Excel instance
            Type tp = Type.GetTypeFromProgID("Excel.Application");
            dynamic objExcel = Activator.CreateInstance(tp);
            dynamic objWorkBooks = objExcel.Workbooks;
            dynamic objWorkBook = objWorkBooks.Add();
            dynamic objSheet = objWorkBook.Worksheets(1);

            try
            {
                //Assign column headings
                objSheet.Range["A1"].Value = "Location";
                objSheet.Range["B1"].Value = "Component";
                objSheet.Range["C1"].Value = "Batch No";
                objSheet.Range["D1"].Value = "Batch Date";
                objSheet.Range["E1"].Value = "Quantity";
                objSheet.Range["F1"].Value = "Quality";
                objSheet.Range["G1"].Value = "Shippable Week";
                objSheet.Range["H1"].Value = "Query";
                objSheet.Range["I1"].Value = "Pick Face Location";
                objSheet.Range["J1"].Value = "Replenish File";
                objSheet.Rows[1].Font.Bold = true;

                int RowCount = dgvStock.Rows.Count;
                int FirstExcelRow = 2;
                for (int i = 0; i <= RowCount - 1; i++)
                {
                    if (!dgvStock.Rows[i].IsNewRow)
                    {
                        //Location Code
                        objSheet.Range[string.Format("A{0}", (FirstExcelRow + i).ToString())].Value =
                            dgvStock.Rows[i].Cells["LocationCodeColumn"].Value.ToString();
                        //Component description
                        objSheet.Range[string.Format("B{0}", (FirstExcelRow + i).ToString())].Value =
                            dgvStock.Rows[i].Cells["ComponentDescColumn"].Value.ToString();
                        //Batch No
                        objSheet.Range[string.Format("C{0}", (FirstExcelRow + i).ToString())].Value =
                            dgvStock.Rows[i].Cells["BatchNoColumn"].Value.ToString();
                        //Batch Date
                        objSheet.Range[string.Format("D{0}", (FirstExcelRow + i).ToString())].Value =
                            string.Format("{0:dd/MM/yyyy}", dgvStock.Rows[i].Cells["BatchDateColumn"].Value);
                        objSheet.Range[string.Format("D{0}", (FirstExcelRow + i).ToString())].NumberFormat = "dd/mm/yyyy";
                        //Quantity
                        objSheet.Range[string.Format("E{0}", (FirstExcelRow + i).ToString())].Value =
                            dgvStock.Rows[i].Cells["QuantityColumn"].Value.ToString();
                        //Quality
                        objSheet.Range[string.Format("F{0}", (FirstExcelRow + i).ToString())].Value =
                            dgvStock.Rows[i].Cells["QualityDescColumn"].Value.ToString();
                        //Shippable Week
                        objSheet.Range[string.Format("G{0}", (FirstExcelRow + i).ToString())].Value = "'" + 
                            dgvStock.Rows[i].Cells["ShippableWeekColumn"].Value.ToString();
                        //Query Type
                        if (dgvStock.Rows[i].Cells["QueryTypeDescColumn"].Value != null)
                        {
                            objSheet.Range[string.Format("H{0}", (FirstExcelRow + i).ToString())].Value =
                                dgvStock.Rows[i].Cells["QueryTypeDescColumn"].Value.ToString();
                        }
                        //Pick face location
                        objSheet.Range[string.Format("I{0}", (FirstExcelRow + i).ToString())].Value = "'" +
                            dgvStock.Rows[i].Cells["PickFaceLocationColumn"].Value.ToString();
                        //Replenish List File
                        if (dgvStock.Rows[i].Cells["FileNameColumn"].Value != null)
                            objSheet.Range[string.Format("J{0}", (FirstExcelRow + i).ToString())].Value = "'" +
                                dgvStock.Rows[i].Cells["FileNameColumn"].Value.ToString();
                    }
                }
                objSheet.Columns["A:J"].EntireColumn.Autofit();
                objExcel.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "_btnExport_Click", true);
            }

            finally
            {
                frmP.Close();
                frmP.Dispose();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objWorkBooks);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objExcel);
            }
        }


    }
}
