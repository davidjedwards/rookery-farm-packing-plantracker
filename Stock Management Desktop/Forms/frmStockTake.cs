﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL; 


namespace Stock_Management_Desktop.Forms
{
    public partial class frmStockTake : Stock_Management_Desktop.Forms.frmBase
    {
        public frmStockTake()
        {
            InitializeComponent();
        }

        private void frmStockTake_Load(object sender, EventArgs e)
        {
            Suppress = true;

            //Retailer combo
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            List<Retailer> rtlList = RetailerLogic.ListRetailers(false);
            foreach (Retailer rtl in rtlList)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = rtl.Id;
                dsRetailer["Name"] = rtl.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cboFilterRetailer.DataSource = dtRetailer;
            cboFilterRetailer.DisplayMember = "Name";
            cboFilterRetailer.ValueMember = "Id";

            Functions.StyleDataGridView(dgvStockTake);
            dgvStockTake.RowsDefaultCellStyle.SelectionBackColor = dgvStockTake.RowsDefaultCellStyle.BackColor;
            dgvStockTake.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dgvStockTake.AlternatingRowsDefaultCellStyle.SelectionBackColor = dgvStockTake.AlternatingRowsDefaultCellStyle.BackColor;
            dgvStockTake.AlternatingRowsDefaultCellStyle.SelectionForeColor = Color.Black;
            Functions.StyleDataGridView(dgvStockLevels);
            dgvStockLevels.RowsDefaultCellStyle.SelectionBackColor = dgvStockLevels.RowsDefaultCellStyle.BackColor;
            dgvStockLevels.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dgvStockLevels.AlternatingRowsDefaultCellStyle.SelectionBackColor = dgvStockLevels.AlternatingRowsDefaultCellStyle.BackColor;
            dgvStockLevels.AlternatingRowsDefaultCellStyle.SelectionForeColor = Color.Black;
            Functions.FormatButton(btnSearch);

            LoadStockLevelsGrid();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgvStockTake.AutoGenerateColumns = false;
            BindingSource bs = ComponentLogic.ListComponents(txtDescription.Text, (int)cboFilterRetailer.SelectedValue, txtStockCode.Text, 0, false);
            dgvStockTake.DataSource = bs;
        }

        private void dgvStockTake_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvStockTake.Columns["AddButtonColumn"].Index)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                e.Graphics.DrawImage(Properties.Resources.add, e.CellBounds.Left + 3, e.CellBounds.Top + 3, 16, 16);
                e.Handled = true;
            }
        }

        private void dgvStockTake_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvStockTake.Columns["AddButtonColumn"].Index)
                e.ToolTipText = "Click to add to stock check";
        }

        private void dgvStockLevels_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvStockLevels.Columns["DeleteColumn"].Index)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.Left + 3, e.CellBounds.Top + 3, 16, 16);
                e.Handled = true;
            }
        }

        private void LoadStockLevelsGrid()
        {
            BindingSource bs = StockLevelLogic.RetrieveStockLevelListForStockCheck();
            dgvStockLevels.DataSource = bs;
        }

        private void dgvStockTake_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvStockTake.Columns["AddButtonColumn"].Index)
            {
                int compId = (int)dgvStockTake.Rows[e.RowIndex].Cells["ComponentIdColumn"].Value;
                if (StockLevelLogic.AddStockLevelsForStockCheck(compId))
                {
                    LoadStockLevelsGrid();
                }
            }
        }

        private void dgvStockLevels_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvStockLevels.Columns["DeleteColumn"].Index)
            {
                try
                {
                    int stockLevelId = (int)dgvStockLevels.Rows[e.RowIndex].Cells["StockLevelIdColumn"].Value;
                    StockLevel sl = StockLevelLogic.RetrieveStockLevelFromId(stockLevelId);
                    sl.StockTake = false;
                    string msg;
                    if (!StockLevelLogic.Update(sl, Globals.Action.Other, Properties.Settings.Default.UserId, (int)sl.Quantity, (int)sl.Quality,
                        (int)sl.ShippableWeek, (int)sl.ShippableYear, string.Empty, out msg))
                    {
                        MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    LoadStockLevelsGrid();
                }
                catch (Exception ex)
                {
                    ExceptionLogic.HandleException(ex, this.Name + "_dgvStockLevels_CellClick", true);
                }

            }
        }

    }
}
