﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using System.Reflection;
using RFP;
using BLL; 

namespace Stock_Management_Desktop.Forms
{
    public partial class frmComponentMaintenance : Stock_Management_Desktop.Forms.frmBase
    {
        private bool _selectMode = false;
        public bool SelectMode
        {
            get { return _selectMode; }
            set { _selectMode = value; }
        }
        int Retailer;
        User loggedInUser;

        public RFP.Component selectedComponent { get; set; }

        public frmComponentMaintenance()
        {
            InitializeComponent();
        }

        private void dgvComponents_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvComponents.Columns["DeleteColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    if (dgvComponents.Rows[e.RowIndex].IsNewRow || (bool)(dgvComponents.Rows[e.RowIndex].Cells["ActiveColumn"].Value ?? true))
                    {
                        e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                    else
                    {
                        e.Graphics.DrawImage(Properties.Resources.refresh, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                }
            }
        }

        private void frmComponentMaintenance_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;
            EntityName = Globals.Entities.Component;
            Functions.StyleDataGridView(dgvComponents);
            Functions.FormatButton(btnExport);
            Functions.FormatButton(btnCancel);
            dgvComponents.AutoGenerateColumns = false;
            SetUpGridCombos();

            loggedInUser = UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
            if (loggedInUser.Retailer != null)
                Retailer = (int)loggedInUser.Retailer;
            else
                Retailer = 0;

            //Retailer combo
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            List<Retailer> rtlList = RetailerLogic.ListRetailers(false);
            foreach (Retailer rtl in rtlList)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = rtl.Id;
                dsRetailer["Name"] = rtl.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cboFilterRetailer.DataSource = dtRetailer;
            cboFilterRetailer.DisplayMember = "Name";
            cboFilterRetailer.ValueMember = "Id";
            if (Retailer == 0)
                cboFilterRetailer.SelectedValue = 0;
            else
            {
                cboFilterRetailer.SelectedValue = Retailer;
                cboFilterRetailer.Enabled = false;
                dgvComponents.Columns["RetailerColumn"].Visible = false;
            }

            //Pick face combo
            DataTable dtPickFace = new DataTable();
            dtPickFace.Columns.Add("Id", typeof(int));
            dtPickFace.Columns.Add("ItemValue", typeof(string));
            DataRow drPickFace;

            List<ConfigurationItem> pfList = ConfigurationLogic.ListConfigurationItemsAsList(ConfigGroup.PickFaceLocation, false);
            foreach (ConfigurationItem rtl in pfList)
            {
                drPickFace = dtPickFace.NewRow();
                drPickFace["Id"] = rtl.Id;
                drPickFace["ItemValue"] = rtl.ItemValue;
                dtPickFace.Rows.Add(drPickFace);
            }
            cboFilterPickFace.DataSource = dtPickFace;
            cboFilterPickFace.DisplayMember = "ItemValue";
            cboFilterPickFace.ValueMember = "Id";
            cboFilterPickFace.SelectedValue = 0;

            //If the user is associated with a retailer then set the retailer combo value to the retailer and disable so it can't be changed.
            SetPermission();
            LoadGrid();
            if (_selectMode)
            {
                btnExport.Visible = false;
                btnCancel.Visible = true;
                this.Text = "Select Component";
                this.chkShowDeleted.Visible = false;
                dgvComponents.Columns["DeleteColumn"].Visible = false;
                dgvComponents.ReadOnly = true;
                dgvComponents.Cursor = Cursors.Hand;
                dgvComponents.AllowUserToAddRows = false;
            }
            loading = false;
        }

        private void LoadGrid(int selectedId = 0)
        {
            gridLoading = true;
            int CurrTopRow = dgvComponents.FirstDisplayedScrollingRowIndex;
            if (CurrTopRow < 0)
                CurrTopRow = 0;

            dgvComponents.DataSource = BLL.ComponentLogic.ListComponents(txtSearchDesc.Text, (int)cboFilterRetailer.SelectedValue, txtSearchCode.Text, 
                                                                        (int)cboFilterPickFace.SelectedValue, chkShowDeleted.Checked);

            if (selectedId != 0)
            {
                dgvComponents.FirstDisplayedScrollingRowIndex = CurrTopRow;
                int RowOfSelItem = 0;
                foreach (DataGridViewRow r in dgvComponents.Rows)
                {
                    if (((Nullable<int>)r.Cells["IdColumn"].Value ?? 0) == selectedId)
                        RowOfSelItem = r.Index;
                }
                dgvComponents.CurrentCell = dgvComponents.Rows[RowOfSelItem].Cells["LogonColumn"];
            }
            if (dgvComponents.Rows.Count > 1) //Always be at least one as the new row will be counted.
                recordId = (int)dgvComponents.CurrentRow.Cells["IdColumn"].Value;

            gridLoading = false;
            GridIsValid = true;
        }

        private void SetPermission()
        {
            Globals.Permission perm = (Globals.Permission)loggedInUser.Permission;
            if (perm == Globals.Permission.Full && Retailer == 0)
            {
                dgvComponents.Columns["DeleteColumn"].Visible = true;
                IsReadOnly = false;
            }
            else
            {
                dgvComponents.Columns["DeleteColumn"].Visible = false;
                IsReadOnly = true;
                dgvComponents.AllowUserToAddRows = false;
            }
            dgvComponents.ReadOnly = IsReadOnly;
        }

        private void SetUpGridCombos()
        {
            //Retailer combo
            DataGridViewComboBoxColumn cbr = (DataGridViewComboBoxColumn)dgvComponents.Columns["RetailerColumn"];
            DataTable dtRetailer = new DataTable();
            dtRetailer.Columns.Add("Id", typeof(int));
            dtRetailer.Columns.Add("Name", typeof(string));
            DataRow dsRetailer = dtRetailer.NewRow();
            dsRetailer["Id"] = 0;
            dsRetailer["Name"] = string.Empty;
            dtRetailer.Rows.Add(dsRetailer);

            List<Retailer> retailers = RetailerLogic.ListRetailers(false);
            foreach (Retailer _retailer in retailers)
            {
                dsRetailer = dtRetailer.NewRow();
                dsRetailer["Id"] = _retailer.Id;
                dsRetailer["Name"] = _retailer.Name;
                dtRetailer.Rows.Add(dsRetailer);
            }
            cbr.DisplayMember = "Name";
            cbr.ValueMember = "Id";
            cbr.DataSource = dtRetailer;

            //Pick face location combo
            DataGridViewComboBoxColumn cbp = (DataGridViewComboBoxColumn)dgvComponents.Columns["PickFaceLocationColumn"];
            DataTable dtPickFaceLoc = new DataTable();
            dtPickFaceLoc.Columns.Add("Id", typeof(int));
            dtPickFaceLoc.Columns.Add("ItemValue", typeof(string));
            DataRow dsPickFaceLoc = dtPickFaceLoc.NewRow();
            dsPickFaceLoc["Id"] = 0;
            dsPickFaceLoc["ItemValue"] = string.Empty;
            dtPickFaceLoc.Rows.Add(dsPickFaceLoc);

            List<ConfigurationItem> configItems = ConfigurationLogic.ListConfigurationItemsAsList(ConfigGroup.PickFaceLocation, false);
            foreach (ConfigurationItem cfi in configItems)
            {
                dsPickFaceLoc = dtPickFaceLoc.NewRow();
                dsPickFaceLoc["Id"] = cfi.Id;
                dsPickFaceLoc["ItemValue"] = cfi.ItemValue;
                dtPickFaceLoc.Rows.Add(dsPickFaceLoc);
            }
            cbp.DisplayMember = "ItemValue";
            cbp.ValueMember = "Id";
            cbp.DataSource = dtPickFaceLoc;
        }

        private void chkShowAudit_CheckedChanged(object sender, EventArgs e)
        {
            dgvComponents.Columns["CreatedByColumn"].Visible = chkShowAudit.Checked;
            dgvComponents.Columns["CreatedDateColumn"].Visible = chkShowAudit.Checked;
            dgvComponents.Columns["LastModifiedByColumn"].Visible = chkShowAudit.Checked;
            dgvComponents.Columns["LastModifiedDateColumn"].Visible = chkShowAudit.Checked;
        }

        private void RefreshGrid(object sender, System.EventArgs e)
        {
            if (!loading)
            {
                LoadGrid();
            }
        }

        private void dgvComponents_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if ((dgvComponents.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewTextBoxCell) ||
                    dgvComponents.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewComboBoxCell)) &&
                    !dgvComponents.Rows[e.RowIndex].IsNewRow &&
                    !(bool)dgvComponents.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                {
                    dgvComponents.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Properties.Settings.Default.InactiveColour;
                    dgvComponents.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Properties.Settings.Default.InactiveColour;
                }
                else
                {
                    dgvComponents.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                    dgvComponents.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Black;
                }
            }
        }

        private void dgvComponents_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && dgvComponents.CurrentRow.Index >= 0 && !dgvComponents.CurrentRow.IsNewRow &&
                 dgvComponents.CurrentRow.Cells["IdColumn"].Value != null)
            {
                RFP.Component thisComp = (RFP.Component)dgvComponents.CurrentRow.DataBoundItem;
                string msg = string.Empty;
                if (thisComp.Id == 0)
                {
                    //new record
                    int thisId = ComponentLogic.AddComponent(thisComp, Properties.Settings.Default.UserId, out msg);
                    if (thisId == 0) //validation failed
                    {
                        MessageBox.Show("New record failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dgvComponents.CurrentRow.ErrorText = msg;
                        dgvComponents.CurrentCell.Selected = false;
                        GridIsValid = false;
                        e.Cancel = true;
                    }
                    else
                    {
                        dgvComponents.CurrentRow.ErrorText = string.Empty;
                        GridIsValid = true;
                        thisComp.Id = thisId;
                    }
                }
                else //update existing record (but only if amended)
                {
                    if (dgvComponents.IsCurrentRowDirty)
                    {
                        if (!ComponentLogic.UpdateComponent(thisComp, Properties.Settings.Default.UserId, out msg))
                        {
                            MessageBox.Show("Changes failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            dgvComponents.CurrentRow.ErrorText = msg;
                            dgvComponents.CurrentCell.Selected = false;
                            GridIsValid = false;
                            e.Cancel = true;
                        }
                        else
                        {
                            dgvComponents.CurrentRow.ErrorText = string.Empty;
                            GridIsValid = true;
                        }
                    }
                }
            }
        }

        private void dgvComponents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Deal with clicks on delete button
            if (e.RowIndex >= 0)
            {
                if (_selectMode)
                {
                    selectedComponent = (RFP.Component)dgvComponents.Rows[e.RowIndex].DataBoundItem;
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else if (e.ColumnIndex == dgvComponents.Columns["DeleteColumn"].Index)
                {
                    RFP.Component thisComp = (RFP.Component)dgvComponents.CurrentRow.DataBoundItem;
                    if (thisComp.Active)
                    {
                        //Check to see if there is any quantity associated with this component - don't allow to set to inactive if there is
                        if (ComponentLogic.GetCurrentQuantity(thisComp.Id) > 0)
                        {
                            MessageBox.Show("There is existing quantity associated with this component and so it cannot be set to inactive", Properties.Settings.Default.SystemName,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (MessageBox.Show("Are you sure you want to set this component to inactive?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            thisComp.Active = false;
                            dgvComponents.Rows[e.RowIndex].Cells["ActiveColumn"].Value = false;
                        }
                    }
                    else
                    {
                        thisComp.Active = true;
                        dgvComponents.Rows[e.RowIndex].Cells["ActiveColumn"].Value = true;
                    }
                }
            }
        }

        private void dgvComponents_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!gridLoading && !dgvComponents.Rows[e.RowIndex].IsNewRow)
            {
                recordId = (int)dgvComponents.Rows[e.RowIndex].Cells["IdColumn"].Value;
                if (!IsReadOnly)
                    dgv_RowEnter(sender, e);
            }
        }

        private void dgvComponents_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && !dgvComponents.Rows[e.RowIndex].IsNewRow)
            {
                recordId = (int)dgvComponents.Rows[e.RowIndex].Cells["IdColumn"].Value;
                dgv_CellBeginEdit(sender, e);
            }
        }

        private void dgvComponents_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (!IsReadOnly)
                dgv_RowValidated(sender, e);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //Export currently shown list to Excel
            frmProgress frmP = new frmProgress();
            frmP.lblProgress.Text = "Exporting component list...";
            frmP.Show();
            frmP.Refresh();

            //Create new Excel instance
            Type tp = Type.GetTypeFromProgID("Excel.Application");
            dynamic objExcel  = Activator.CreateInstance(tp);
            dynamic objWorkBooks = objExcel.Workbooks;
            dynamic objWorkBook = objWorkBooks.Add();
            dynamic objSheet = objWorkBook.Worksheets(1);

            try
            {
                //Assign column headings
                objSheet.Range["A1"].Value = "Component Description";
                objSheet.Range["B1"].Value = "Retailer";
                objSheet.Range["C1"].Value = "Retailer Component Code";
                objSheet.Range["D1"].Value = "Pick Face Location";
                objSheet.Rows[1].Font.Bold = true;

                Retailer rtl;
                ConfigurationItem cfi;
                int RowCount = dgvComponents.Rows.Count;
                int FirstExcelRow = 2;
                for (int i = 0; i <= RowCount - 1; i++)
                {
                    if (!dgvComponents.Rows[i].IsNewRow)
                    {
                        //Component description
                        objSheet.Range[string.Format("A{0}",(FirstExcelRow + i).ToString())].Value = 
                            dgvComponents.Rows[i].Cells["DescriptionColumn"].Value.ToString();
                        //Retailer
                        rtl = RetailerLogic.RetrieveRetailer(dgvComponents.Rows[i].Cells["RetailerColumn"].Value);
                        if (rtl != null)
                        {
                            objSheet.Range[string.Format("B{0}",(FirstExcelRow + i).ToString())].Value = rtl.Name;
                        }
                        //Retailer Stock Code
                        objSheet.Range[string.Format("C{0}",(FirstExcelRow + i).ToString())].Value = 
                            dgvComponents.Rows[i].Cells["RetailerStockCodeColumn"].Value.ToString();
                        //Pick Face Location
                        cfi = ConfigurationLogic.RetrieveConfigItemById(dgvComponents.Rows[i].Cells["PickFaceLocationColumn"].Value);
                        if (cfi != null)
                        {
                            objSheet.Range[string.Format("D{0}",(FirstExcelRow + i).ToString())].Value = cfi.ItemValue;
                        }
                    }
                }
                objSheet.Columns["A:D"].EntireColumn.Autofit();
                objExcel.Visible = true;
	        }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "_btnExport_Click", true);
            }

	        finally
	        {
                frmP.Close();
                frmP.Dispose();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objWorkBooks);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objExcel);
	        }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
