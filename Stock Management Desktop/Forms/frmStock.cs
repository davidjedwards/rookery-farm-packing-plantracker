﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using BLL;
using RFP;
using System.Globalization;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmStock : Stock_Management_Desktop.Forms.frmBase
    {
        internal StockLevel thisStockLevel;
        DateTimeFormatInfo dfi;
        Calendar cal;

        public frmStock()
        {
            InitializeComponent();
        }

        private void frmStock_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;

            Functions.FormatButton(btnMove);
            Functions.FormatButton(btnDispose);
            Functions.FormatButton(btnPick);
            Functions.FormatButton(btnClose);
            Functions.FormatButton(btnBackToShippable);
            Functions.FormatButton(btnChangeQty);
            Functions.FormatButton(btnForDisposal);
            Functions.FormatButton(btnForMoving);
            Functions.FormatButton(btnForPicking);
            Functions.FormatButton(btnMarkAsGrowingOn);
            Functions.FormatButton(btnMarkAsGrowingOn1);
            Functions.FormatButton(btnShippable);

            //Setup combos
            cboQueryType.DataSource = ConfigurationLogic.ListConfigurationItemsAsList(ConfigGroup.QueryType, false);
            cboQueryType.ValueMember = "Id";
            cboQueryType.DisplayMember = "ItemValue";

            //Shippable details
            dfi = DateTimeFormatInfo.CurrentInfo;
            cal = dfi.Calendar;
            //Default year to current year
            nudShippableYear.Maximum = DateTime.Today.Year + 10;
            nudShippableYear.Value = DateTime.Today.Year;

            try
            {
                lblLocation.Text = thisStockLevel.Location1.Code;
                lblComponent.Text = thisStockLevel.Batch1.Component1.RetailerStockCode + " (" +
                    thisStockLevel.Batch1.Component1.Description + ")";
                lblBatchNo.Text = thisStockLevel.Batch1.BatchNo.ToString();
                lblBatchDate.Text = string.Format("{0:dd/MM/yyyy}", thisStockLevel.Batch1.BatchDate);
                lblQuantity.Text = thisStockLevel.Quantity.ToString();
                lblQuality.Text = Globals.CropQualityScreenDesc((Globals.CropQuality)thisStockLevel.Quality);
                //default quantity to process
                txtQuantityToProcess.Text = thisStockLevel.Quantity.ToString();
                nudShippableWeek.Value = (int)thisStockLevel.ShippableWeek;
                nudShippableYear.Value = (int)thisStockLevel.ShippableYear;
                chkQuery.Checked = (bool)thisStockLevel.Query;
                cboQueryType.Enabled = chkQuery.Checked;
                if (thisStockLevel.QueryType != null)
                    cboQueryType.SelectedValue = thisStockLevel.QueryType;
                else
                    cboQueryType.SelectedValue = 0;

                switch (thisStockLevel.Quality)
                {
                    case (int)Globals.CropQuality.GrowingOn:
                        {
                            pnlMark.Visible = true;
                            pnlGrowingOn.Visible = false;
                            pnlShippable.Visible = false;
                            pnlPick.Visible = false;    
                            break;
                        }
                    case (int)Globals.CropQuality.Shippable:
                        {
                            pnlMark.Visible = false;
                            pnlGrowingOn.Visible = false;
                            pnlShippable.Visible = true;
                            pnlPick.Visible = false;
                            break;
                        }
                    case (int)Globals.CropQuality.Pick:
                        {
                            pnlMark.Visible = false;
                            pnlGrowingOn.Visible = false;
                            pnlShippable.Visible = false;
                            pnlPick.Visible = true;
                            break;
                        }
                    default:
                        {
                            pnlMark.Visible = false;
                            pnlGrowingOn.Visible = true;
                            pnlShippable.Visible = false;
                            pnlPick.Visible = false;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, "frmStock_Load", true);
            }
            SaveChanges += new SaveChangesEventHandler(Save);
            FurtherValidation += new FurtherValidationEventHandler(FormSpecificValidation);
            loading = false;
            Suppress = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                Save();
                Dirty = false;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void Save()
        {
            string msg;
            thisStockLevel.ShippableWeek = (int)nudShippableWeek.Value;
            thisStockLevel.ShippableYear = (int)nudShippableYear.Value;
            thisStockLevel.Query = chkQuery.Checked;
            if (cboQueryType.Text != string.Empty && cboQueryType.SelectedValue == null)
            {
                //Add query type to configuration table
                ConfigurationItem cfi = new ConfigurationItem();
                cfi.ConfigurationGroup = (int)ConfigGroup.QueryType;
                cfi.ItemValue = cboQueryType.Text;
                cfi.Active = true;
                int newConfigItem = ConfigurationLogic.AddConfigurationItem(cfi, out msg);
                if (newConfigItem == 0)
                {
                    MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    thisStockLevel.QueryType = null;
                }
                else
                {
                    thisStockLevel.QueryType = newConfigItem;
                }
            }
            else
            {
                if ((int)cboQueryType.SelectedValue == 0)
                    thisStockLevel.QueryType = null;
                else
                    thisStockLevel.QueryType = (int)cboQueryType.SelectedValue;
            }
            if (!StockLevelLogic.Update(thisStockLevel, Globals.Action.Other, Properties.Settings.Default.UserId,
                    (int)thisStockLevel.Quantity, (int)thisStockLevel.Quality, (int)thisStockLevel.ShippableWeek,
                    (int)thisStockLevel.ShippableYear, null, out msg))
                MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private bool FormSpecificValidation()
        {
            //If query has been ticked then there should be a query type selected
            if (chkQuery.Checked && cboQueryType.Text == string.Empty)
            {
                ep.SetError(cboQueryType, "Please select a query type");
                return false;
            }
            else
                ep.SetError(cboQueryType, string.Empty);

            return true;
        }

        private void btnChangeQty_Click(object sender, EventArgs e)
        {
            if (txtQuantityToProcess.Text == lblQuantity.Text)
            {
                MessageBox.Show("This option requires that the quantity to process is different from the current quantity",
                    Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process",Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                int newQuantity;
                if (!int.TryParse(txtQuantityToProcess.Text,out newQuantity))
                {
                    MessageBox.Show("Please enter an integer for the quantity to process",Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    frmQtyChange frm = new frmQtyChange();
                    //"Increase in quantity from " + qtyCurrent.ToString() + " to " + qtyToProcess.ToString();
                    string qtyChgPrompt;
                    if (newQuantity < int.Parse(lblQuantity.Text))
                        qtyChgPrompt = string.Format("Decrease in quantity from {0} to {1}", lblQuantity.Text, newQuantity.ToString());
                    else
                        qtyChgPrompt = string.Format("Increase in quantity from {0} to {1}", lblQuantity.Text, newQuantity.ToString());

                    frm.prompt = qtyChgPrompt;
                    frm.ShowDialog();
                    if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                    {
                        //Save change to quantity in stock level and create batch history record
                        int prevQuantity = (int)thisStockLevel.Quantity;
                        int prevQuality = (int)thisStockLevel.Quality;
                        thisStockLevel.Quantity = newQuantity;
                        string msg;
                        if (!StockLevelLogic.Update(thisStockLevel, Globals.Action.QuantityChange, Properties.Settings.Default.UserId,
                             prevQuantity, prevQuality, (int)thisStockLevel.ShippableWeek, (int)thisStockLevel.ShippableYear, frm.reason, out msg))
                        {
                            MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            txtQuantityToProcess.Text = lblQuantity.Text;
                        }
                        else
                        {
                            lblQuantity.Text = newQuantity.ToString();
                        }
                    }
                    else
                    {
                        txtQuantityToProcess.Text = lblQuantity.Text;
                    }
                }
            }
        }

        private void chkQuery_CheckedChanged(object sender, EventArgs e)
        {
            if (chkQuery.Checked == false)
            {
                cboQueryType.SelectedValue = 0;
            }
            cboQueryType.Enabled = chkQuery.Checked;
        }

        private void btnForMoving_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process",Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else 
            {
                thisStockLevel = StockLevelLogic.SetNewQuality(thisStockLevel, Globals.CropQuality.MoveOn, Properties.Settings.Default.UserId, quantityToProcess, thisStockLevel.ReplenDetailId);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnForDisposal_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else 
            {
                thisStockLevel = StockLevelLogic.SetNewQuality(thisStockLevel, Globals.CropQuality.Dispose, Properties.Settings.Default.UserId, quantityToProcess, thisStockLevel.ReplenDetailId);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnShippable_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else 
            {
                thisStockLevel = StockLevelLogic.SetNewQuality(thisStockLevel, Globals.CropQuality.Shippable, Properties.Settings.Default.UserId, quantityToProcess, thisStockLevel.ReplenDetailId);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnMarkAsGrowingOn_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else 
            {
                thisStockLevel = StockLevelLogic.SetNewQuality(thisStockLevel, Globals.CropQuality.GrowingOn, Properties.Settings.Default.UserId, quantityToProcess, thisStockLevel.ReplenDetailId);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }

        }

        private void btnForPicking_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                thisStockLevel = StockLevelLogic.SetNewQuality(thisStockLevel, Globals.CropQuality.Pick, Properties.Settings.Default.UserId, quantityToProcess, thisStockLevel.ReplenDetailId);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                frmChooseLocation frm = new frmChooseLocation();
                frm.info = string.Format("Move {0} of component {1} from location {2}", quantityToProcess.ToString(), lblComponent.Text, lblLocation.Text);
                frm.ShowDialog();
                if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    int newLocation = frm.newLocation;
                    if (newLocation != 0)
                    {
                        thisStockLevel = StockLevelLogic.Move(thisStockLevel, newLocation, Properties.Settings.Default.UserId, quantityToProcess);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                }
                frm.Dispose();
            }
        }

        private void btnPickDispose_Click(object sender, EventArgs e)
        {
            int quantityToProcess;
            if (txtQuantityToProcess.Text == string.Empty)
            {
                MessageBox.Show("Please enter a quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!int.TryParse(txtQuantityToProcess.Text, out quantityToProcess))
            {
                MessageBox.Show("Please enter an integer for the quantity to process", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (quantityToProcess > thisStockLevel.Quantity)
            {
                MessageBox.Show("The quantity to process cannot be larger than the stock quantity", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Button btnPickDispose = (Button)sender;
                switch (btnPickDispose.Name)
                {
                    case "btnPick":
                        {
                            if (StockLevelLogic.PickDispose(thisStockLevel, Globals.Action.Picked, Properties.Settings.Default.UserId, quantityToProcess))
                            {
                                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                            }
                            break;
                        }
                    case "btnDispose":
                        {
                            if (StockLevelLogic.PickDispose(thisStockLevel, Globals.Action.Disposed, Properties.Settings.Default.UserId, quantityToProcess))
                            {
                                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                            }

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }

    }
}
