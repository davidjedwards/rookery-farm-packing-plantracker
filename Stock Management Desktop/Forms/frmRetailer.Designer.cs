﻿namespace Stock_Management_Desktop.Forms
{
    partial class frmRetailer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRetailer = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnRetailer = new System.Windows.Forms.Button();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.txtCounty = new System.Windows.Forms.TextBox();
            this.txtPostCode = new System.Windows.Forms.TextBox();
            this.dgvContacts = new System.Windows.Forms.DataGridView();
            this.ContactIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelephoneColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MobileColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmailColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiveColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteContactColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSSHHostKeyFingerprint = new System.Windows.Forms.TextBox();
            this.lblFingerprint = new System.Windows.Forms.Label();
            this.cboFTPProtocol = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDownloadFolder = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUploadFolder = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtFTPHost = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFTPPort = new System.Windows.Forms.TextBox();
            this.pnlFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRetailer)).BeginInit();
            this.pnlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContacts)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnRetailer);
            this.pnlFooter.Location = new System.Drawing.Point(0, 527);
            this.pnlFooter.Size = new System.Drawing.Size(1147, 44);
            // 
            // dgvRetailer
            // 
            this.dgvRetailer.AllowUserToAddRows = false;
            this.dgvRetailer.AllowUserToDeleteRows = false;
            this.dgvRetailer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRetailer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.NameColumn,
            this.DeleteColumn});
            this.dgvRetailer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRetailer.Location = new System.Drawing.Point(0, 0);
            this.dgvRetailer.MultiSelect = false;
            this.dgvRetailer.Name = "dgvRetailer";
            this.dgvRetailer.RowHeadersVisible = false;
            this.dgvRetailer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRetailer.Size = new System.Drawing.Size(276, 483);
            this.dgvRetailer.TabIndex = 1;
            this.dgvRetailer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRetailer_CellClick);
            this.dgvRetailer.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvRetailer_CellPainting);
            this.dgvRetailer.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRetailer_RowEnter);
            this.dgvRetailer.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRetailer_RowValidated);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            // 
            // NameColumn
            // 
            this.NameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameColumn.DataPropertyName = "Name";
            this.NameColumn.HeaderText = "Retailer";
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DeleteColumn.HeaderText = "";
            this.DeleteColumn.MinimumWidth = 25;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteColumn.Width = 25;
            // 
            // btnRetailer
            // 
            this.btnRetailer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetailer.Image = global::Stock_Management_Desktop.Properties.Resources.add;
            this.btnRetailer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRetailer.Location = new System.Drawing.Point(12, 7);
            this.btnRetailer.Name = "btnRetailer";
            this.btnRetailer.Size = new System.Drawing.Size(117, 29);
            this.btnRetailer.TabIndex = 2;
            this.btnRetailer.Text = "Add Retailer";
            this.btnRetailer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRetailer.UseVisualStyleBackColor = true;
            this.btnRetailer.Click += new System.EventHandler(this.btnRetailer_Click);
            // 
            // pnlFilter
            // 
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFilter.Controls.Add(this.chkShowDeleted);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(1147, 42);
            this.pnlFilter.TabIndex = 4;
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowDeleted.Location = new System.Drawing.Point(12, 12);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(107, 20);
            this.chkShowDeleted.TabIndex = 9;
            this.chkShowDeleted.Text = "Show Inactive";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "County";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 241);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Contacts";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Town";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Post Code";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(95, 15);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(266, 23);
            this.txtName.TabIndex = 0;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(95, 43);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(266, 23);
            this.txtAddress1.TabIndex = 1;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(95, 72);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(266, 23);
            this.txtAddress2.TabIndex = 2;
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(95, 101);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(266, 23);
            this.txtTown.TabIndex = 3;
            // 
            // txtCounty
            // 
            this.txtCounty.Location = new System.Drawing.Point(95, 130);
            this.txtCounty.Name = "txtCounty";
            this.txtCounty.Size = new System.Drawing.Size(266, 23);
            this.txtCounty.TabIndex = 4;
            // 
            // txtPostCode
            // 
            this.txtPostCode.Location = new System.Drawing.Point(95, 159);
            this.txtPostCode.Name = "txtPostCode";
            this.txtPostCode.Size = new System.Drawing.Size(103, 23);
            this.txtPostCode.TabIndex = 5;
            // 
            // dgvContacts
            // 
            this.dgvContacts.AllowUserToDeleteRows = false;
            this.dgvContacts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvContacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContacts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ContactIdColumn,
            this.ContactNameColumn,
            this.TelephoneColumn,
            this.MobileColumn,
            this.EmailColumn,
            this.ActiveColumn,
            this.DeleteContactColumn});
            this.dgvContacts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvContacts.Location = new System.Drawing.Point(25, 275);
            this.dgvContacts.Name = "dgvContacts";
            this.dgvContacts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContacts.Size = new System.Drawing.Size(827, 203);
            this.dgvContacts.TabIndex = 7;
            this.dgvContacts.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvContacts_CellPainting);
            // 
            // ContactIdColumn
            // 
            this.ContactIdColumn.DataPropertyName = "Id";
            this.ContactIdColumn.HeaderText = "";
            this.ContactIdColumn.Name = "ContactIdColumn";
            this.ContactIdColumn.ReadOnly = true;
            this.ContactIdColumn.Visible = false;
            // 
            // ContactNameColumn
            // 
            this.ContactNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ContactNameColumn.DataPropertyName = "Name";
            this.ContactNameColumn.HeaderText = "Name";
            this.ContactNameColumn.Name = "ContactNameColumn";
            this.ContactNameColumn.Width = 66;
            // 
            // TelephoneColumn
            // 
            this.TelephoneColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TelephoneColumn.DataPropertyName = "Telephone";
            this.TelephoneColumn.HeaderText = "Telephone";
            this.TelephoneColumn.Name = "TelephoneColumn";
            this.TelephoneColumn.Width = 93;
            // 
            // MobileColumn
            // 
            this.MobileColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.MobileColumn.DataPropertyName = "Mobile";
            this.MobileColumn.HeaderText = "Mobile";
            this.MobileColumn.Name = "MobileColumn";
            this.MobileColumn.Width = 70;
            // 
            // EmailColumn
            // 
            this.EmailColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmailColumn.DataPropertyName = "Email";
            this.EmailColumn.HeaderText = "Email Address";
            this.EmailColumn.Name = "EmailColumn";
            // 
            // ActiveColumn
            // 
            this.ActiveColumn.DataPropertyName = "Active";
            this.ActiveColumn.HeaderText = "";
            this.ActiveColumn.Name = "ActiveColumn";
            this.ActiveColumn.ReadOnly = true;
            this.ActiveColumn.Visible = false;
            // 
            // DeleteContactColumn
            // 
            this.DeleteContactColumn.HeaderText = "";
            this.DeleteContactColumn.MinimumWidth = 25;
            this.DeleteContactColumn.Name = "DeleteContactColumn";
            this.DeleteContactColumn.Width = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFTPPort);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtSSHHostKeyFingerprint);
            this.groupBox1.Controls.Add(this.lblFingerprint);
            this.groupBox1.Controls.Add(this.cboFTPProtocol);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtDownloadFolder);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtUploadFolder);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.txtFTPHost);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(393, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 251);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FTP Details";
            // 
            // txtSSHHostKeyFingerprint
            // 
            this.txtSSHHostKeyFingerprint.Location = new System.Drawing.Point(12, 216);
            this.txtSSHHostKeyFingerprint.Name = "txtSSHHostKeyFingerprint";
            this.txtSSHHostKeyFingerprint.Size = new System.Drawing.Size(441, 23);
            this.txtSSHHostKeyFingerprint.TabIndex = 7;
            this.txtSSHHostKeyFingerprint.Visible = false;
            // 
            // lblFingerprint
            // 
            this.lblFingerprint.AutoSize = true;
            this.lblFingerprint.Location = new System.Drawing.Point(9, 197);
            this.lblFingerprint.Name = "lblFingerprint";
            this.lblFingerprint.Size = new System.Drawing.Size(151, 16);
            this.lblFingerprint.TabIndex = 29;
            this.lblFingerprint.Text = "SSH Host Key Fingerprint";
            this.lblFingerprint.Visible = false;
            // 
            // cboFTPProtocol
            // 
            this.cboFTPProtocol.FormattingEnabled = true;
            this.cboFTPProtocol.Location = new System.Drawing.Point(111, 170);
            this.cboFTPProtocol.Name = "cboFTPProtocol";
            this.cboFTPProtocol.Size = new System.Drawing.Size(97, 24);
            this.cboFTPProtocol.TabIndex = 5;
            this.cboFTPProtocol.SelectedIndexChanged += new System.EventHandler(this.cboFTPProtocol_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 174);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 16);
            this.label12.TabIndex = 28;
            this.label12.Text = "FTP Protocol";
            // 
            // txtDownloadFolder
            // 
            this.txtDownloadFolder.Location = new System.Drawing.Point(111, 141);
            this.txtDownloadFolder.Name = "txtDownloadFolder";
            this.txtDownloadFolder.Size = new System.Drawing.Size(238, 23);
            this.txtDownloadFolder.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 16);
            this.label11.TabIndex = 27;
            this.label11.Text = "Download folder";
            // 
            // txtUploadFolder
            // 
            this.txtUploadFolder.Location = new System.Drawing.Point(111, 112);
            this.txtUploadFolder.Name = "txtUploadFolder";
            this.txtUploadFolder.Size = new System.Drawing.Size(238, 23);
            this.txtUploadFolder.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 25;
            this.label10.Text = "Upload folder";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(111, 83);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(238, 23);
            this.txtPassword.TabIndex = 2;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(111, 54);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(238, 23);
            this.txtUserName.TabIndex = 1;
            // 
            // txtFTPHost
            // 
            this.txtFTPHost.Location = new System.Drawing.Point(111, 25);
            this.txtFTPHost.Name = "txtFTPHost";
            this.txtFTPHost.Size = new System.Drawing.Size(238, 23);
            this.txtFTPHost.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 16);
            this.label7.TabIndex = 21;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "FTP Host";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "User Name";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvRetailer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chkActive);
            this.splitContainer1.Panel2.Controls.Add(this.dgvContacts);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.txtPostCode);
            this.splitContainer1.Panel2.Controls.Add(this.txtName);
            this.splitContainer1.Panel2.Controls.Add(this.txtCounty);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.txtTown);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.txtAddress2);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.txtAddress1);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.label6);
            this.splitContainer1.Panel2.Enabled = false;
            this.splitContainer1.Size = new System.Drawing.Size(1147, 485);
            this.splitContainer1.SplitterDistance = 278;
            this.splitContainer1.TabIndex = 21;
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Location = new System.Drawing.Point(265, 162);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(96, 20);
            this.chkActive.TabIndex = 6;
            this.chkActive.Text = "Undo Delete";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(223, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 16);
            this.label13.TabIndex = 30;
            this.label13.Text = "FTP Port";
            // 
            // txtFTPPort
            // 
            this.txtFTPPort.Location = new System.Drawing.Point(297, 171);
            this.txtFTPPort.Name = "txtFTPPort";
            this.txtFTPPort.Size = new System.Drawing.Size(52, 23);
            this.txtFTPPort.TabIndex = 6;
            // 
            // frmRetailer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(1147, 571);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pnlFilter);
            this.DoubleBuffered = true;
            this.Name = "frmRetailer";
            this.Text = "Retailer Maintenance";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRetailer_FormClosing);
            this.Load += new System.EventHandler(this.frmRetailer_Load);
            this.Controls.SetChildIndex(this.pnlFilter, 0);
            this.Controls.SetChildIndex(this.pnlFooter, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.pnlFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRetailer)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContacts)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRetailer;
        private System.Windows.Forms.Button btnRetailer;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.CheckBox chkShowDeleted;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.TextBox txtCounty;
        private System.Windows.Forms.TextBox txtPostCode;
        private System.Windows.Forms.DataGridView dgvContacts;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtFTPHost;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteColumn;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelephoneColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MobileColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmailColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ActiveColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteContactColumn;
        private System.Windows.Forms.TextBox txtDownloadFolder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtUploadFolder;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboFTPProtocol;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSSHHostKeyFingerprint;
        private System.Windows.Forms.Label lblFingerprint;
        private System.Windows.Forms.TextBox txtFTPPort;
        private System.Windows.Forms.Label label13;
    }
}
