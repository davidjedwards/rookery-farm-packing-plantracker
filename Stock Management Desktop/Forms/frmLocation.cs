﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Stock_Management_Desktop.Classes;
using RFP;
using BLL;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmLocation : Stock_Management_Desktop.Forms.frmBase
    {
        DataTable dtLocation;
        DataRow drLocation;
        DataView dvLocation;
        DataTable dtLocationTemp;
        DataRow drLocationTemp;
        DataView dvLocationTemp;
        int IdForRefresh;
        int TempIdForRefresh;

        public frmLocation()
        {
            InitializeComponent();
        }

        private void frmLocation_Load(object sender, EventArgs e)
        {
            loading = true;
            Suppress = true;
            Functions.StyleDataGridView(dgvLocations);
            Functions.StyleDataGridView(dgvLocationTemp);
            //Override multi-select setting set by above function
            dgvLocations.MultiSelect = true;
            dgvLocationTemp.MultiSelect = true;
            EntityName = Globals.Entities.Location;

            SetPermissions();
            InitialiseTables();
            //Do initial load of grid from database
            List<Location> locations = LocationLogic.ListLocations(true);
            //Fill data table with locations
            foreach (Location loc in locations)
            {
                drLocation = dtLocation.NewRow();
                drLocation["Id"] = loc.Id;
                drLocation["Code"] = loc.Code;
                drLocation["Description"] = loc.Description;
                drLocation["SortOrder"] = loc.SortOrder;
                drLocation["Active"] = loc.Active;
                drLocation["Visible"] = true;
                dtLocation.Rows.Add(drLocation);
            }
            dtLocation.AcceptChanges();
            dtLocation.DefaultView.RowFilter = "Active = true AND Visible = true";
            dtLocation.DefaultView.Sort = "SortOrder ASC";
            dvLocation = dtLocation.DefaultView;

            gridLoading = true;
            dgvLocations.DataSource = dvLocation;
            gridLoading = false;
            //By default the "Show Inactive" will be false so only load grid with records that are active
            loading = false;
        }

        private void SetPermissions()
        {
            IsReadOnly = false;
            Globals.Permission perm = UserLogic.GetPermission((int)Properties.Settings.Default.UserId);
            if (perm == Globals.Permission.Read_Only)
            {
                IsReadOnly = true;
            }
            dgvLocationTemp.Visible = !IsReadOnly;
            dgvLocations.ReadOnly = IsReadOnly;
            dgvLocations.Columns["DeleteColumn"].Visible = !IsReadOnly;
            lblInstructions.Visible = !IsReadOnly;
        }

        private void InitialiseTables()
        {
            dtLocation = new DataTable();
            dtLocation.Columns.Add("Id", typeof(int));
            dtLocation.Columns.Add("Code", typeof(string));
            dtLocation.Columns.Add("Description", typeof(string));
            dtLocation.Columns.Add("SortOrder", typeof(int));
            dtLocation.Columns.Add("Active", typeof(bool));
            dtLocation.Columns.Add("Visible", typeof(bool));
            DataColumn[] PrimaryKeys = new DataColumn[1];
            PrimaryKeys[0] = dtLocation.Columns["Id"];
            dtLocation.PrimaryKey = PrimaryKeys;
            dtLocation.Columns["Active"].DefaultValue = true;
            dtLocation.Columns["Visible"].DefaultValue = true;

            dtLocationTemp = dtLocation.Clone();
        }

        private void dgvLocations_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvLocations.Columns["DeleteColumn"].Index)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    if (dgvLocations.Rows[e.RowIndex].IsNewRow || (bool)(dgvLocations.Rows[e.RowIndex].Cells["ActiveColumn"].Value ?? true))
                    {
                        e.Graphics.DrawImage(Properties.Resources.cross, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                    else
                    {
                        e.Graphics.DrawImage(Properties.Resources.refresh, e.CellBounds.X + 3, e.CellBounds.Y + 3, 16, 16);
                        e.Handled = true;
                    }
                }
            }
        }

        private void dgvLocations_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if ((dgvLocations.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewTextBoxCell) ||
                    dgvLocations.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewComboBoxCell)) &&
                    !dgvLocations.Rows[e.RowIndex].IsNewRow &&
                    !(bool)dgvLocations.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                {
                    dgvLocations.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Properties.Settings.Default.InactiveColour;
                    dgvLocations.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Properties.Settings.Default.InactiveColour;
                }
                else
                {
                    dgvLocations.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                    dgvLocations.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Black;
                }
            }
        }

        private void dgvLocationTemp_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if ((dgvLocationTemp.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewTextBoxCell) ||
                    dgvLocationTemp.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewComboBoxCell)) &&
                    !dgvLocationTemp.Rows[e.RowIndex].IsNewRow &&
                    !(bool)dgvLocationTemp.Rows[e.RowIndex].Cells["TempActiveColumn"].Value)
                {
                    dgvLocationTemp.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Properties.Settings.Default.InactiveColour;
                    dgvLocationTemp.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Properties.Settings.Default.InactiveColour;
                }
                else
                {
                    dgvLocationTemp.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                    dgvLocationTemp.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Black;
                }
            }
        }

        private void chkShowInactive_CheckedChanged(object sender, EventArgs e)
        {
            //Refresh grid
            LoadLocationsGrid();
        }

        private void dgvLocations_MouseDown(object sender, MouseEventArgs e)
        {
            //Don't do this check if the shift or control key are pressed as the user is in the process of selecting the rows to move
            if (ModifierKeys != Keys.Shift && ModifierKeys != Keys.Control)
            {
                //Don't allow drag from button column
                int colIndex = dgvLocations.HitTest(e.X, e.Y).ColumnIndex;
                if (colIndex >= 0)
                {
                    //Get the index of the item the mouse is below
                    dgvLocations.rowIndexFromMouseDown = dgvLocations.HitTest(e.X, e.Y).RowIndex;
                    Type colType = dgvLocations.Columns[colIndex].GetType();

                    if (colType.Name != "DataGridViewButtonColumn")
                    {
                        if (dgvLocations.rowIndexFromMouseDown != -1)
                        {
                            //The DragSize indicates the size that the mouse can move before a drag event should be started
                            Size dragSize = SystemInformation.DragSize;
                            //Create a rectangle using the DragSize, with the mouse position being
                            // at the center of the rectangle.
                            dgvLocations.dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                            e.Y - (dragSize.Height / 2)),
                            dragSize);
                        }
                        else
                            // Reset the rectangle if the mouse is not over an item in the ListBox.
                            dgvLocations.dragBoxFromMouseDown = Rectangle.Empty;
                    }
                    else
                    {
                        dgvLocations.rowIndexFromMouseDown = -1;
                    }
                }
            }
        }

        private void dgvLocations_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left && (ModifierKeys != Keys.Shift && ModifierKeys != Keys.Control))
            {
                if (dgvLocations.rowIndexFromMouseDown >= 0 && !IsReadOnly && !dgvLocations.Rows[dgvLocations.rowIndexFromMouseDown].IsNewRow)
                {
                    //If the mouse moves outside the rectangle start the drag.
                    if (dgvLocations.dragBoxFromMouseDown != Rectangle.Empty && !dgvLocations.dragBoxFromMouseDown.Contains(e.X, e.Y))
                    {
                        if (!dgvLocations.SelectedRows.Contains(dgvLocations.Rows[dgvLocations.rowIndexFromMouseDown]))
                        {
                            dgvLocations.ClearSelection();
                            dgvLocations.Rows[dgvLocations.rowIndexFromMouseDown].Selected = true;
                        }
                        Queue<Location> dragLocs = new Queue<Location>();
                        Location dragLoc;
                        int TopRowIndex = 0;
                        //Ensure selected rows are ordered by original sort order
                        SortedList<int, DataGridViewRow> sortedRows = new SortedList<int, DataGridViewRow>();
                        foreach (DataGridViewRow dgvr in dgvLocations.SelectedRows)
                        {
                            sortedRows.Add((int)dgvr.Cells["SortOrderColumn"].Value, dgvr);
                        }
                        int locId;
                        DataGridViewRow dr;
                        foreach (int key in sortedRows.Keys)
                        {
                            dr = sortedRows[key];
                            if (!dr.IsNewRow)
                            {
                                if (TopRowIndex == 0 || TopRowIndex > dr.Index)
                                    TopRowIndex = dr.Index;

                                if (int.TryParse(dr.Cells["IdColumn"].Value.ToString(), out locId))
                                {
                                    dragLoc = LocationLogic.RetrieveLocationFromId(locId);
                                    dragLocs.Enqueue(dragLoc);
                                }
                            }
                        }
                        if (dragLocs.Count > 0)
                        {
                            //Get id of record above top selected row
                            if (TopRowIndex == 0 || dragLocs.Count == dgvLocations.Rows.Count)
                                IdForRefresh = 0;
                            else
                                IdForRefresh = (int)dgvLocations.Rows[TopRowIndex - 1].Cells["IdColumn"].Value;

                            DragDropEffects dropEffect = dgvLocations.DoDragDrop(dragLocs, DragDropEffects.Move);
                        }
                    }
                }
            }
        }

        private void dgvLocations_DragOver(object sender, DragEventArgs e)
        {
            if (!IsReadOnly)
            {
                if (e.Data.GetDataPresent(typeof(Queue<Location>)))
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;
            }
        }

        private void dgvLocations_DragEnter(object sender, DragEventArgs e)
        {
            if (!IsReadOnly)
            {
                if (e.Data.GetDataPresent(typeof(Queue<Location>)))
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;
            }
        }

        private void dgvLocations_DragDrop(object sender, DragEventArgs e)
        {
            if (!IsReadOnly)
            {
                if (e.Data.GetDataPresent(typeof(Queue<Location>)))
                {
                    Queue<Location> dropLocs = (Queue<Location>)e.Data.GetData(typeof(Queue<Location>));

                    //Determine the row the mouse pointer is on as this will determine where the locations get inserted
                    //The mouse locations are relative to the screen so must be converted to client coordinates
                    Point clientPoint = dgvLocations.PointToClient(new Point(e.X, e.Y));
                    //Get row index of row the mouse is on
                    int rowIndexOfItemUnderMouseToDrop = dgvLocations.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
                    //Create int to indicate if mouse above top row (-1), below bottom row (1), or in middle somewhere (0)
                    int aboveBelowMiddle = 0;
                    if (rowIndexOfItemUnderMouseToDrop == -1)
                    {
                        if (clientPoint.Y < dgvLocations.RowTemplate.Height)
                        {
                            aboveBelowMiddle = -1; //Above
                        }
                        else
                        {
                            aboveBelowMiddle = 1; //Below
                        }
                    }

                    int firstSortOrder;
                    //Locations will be inserted above the row the mouse is over or added to the end if aboveBelowMiddle = 1
                    if (aboveBelowMiddle <= 0)
                    {
                        if (aboveBelowMiddle == -1)
                            firstSortOrder = (int)dgvLocations.Rows[dgvLocations.FirstDisplayedScrollingRowIndex].Cells["SortOrderColumn"].Value;
                        else
                            firstSortOrder = (int)dgvLocations.Rows[rowIndexOfItemUnderMouseToDrop].Cells["SortOrderColumn"].Value;

                        //Need to update sort order on all subsequent rows
                        int numRowsToInsert = dropLocs.Count;
                        DataRow[] drArray = dtLocation.Select("Visible = true AND SortOrder >= " + firstSortOrder.ToString());
                        foreach (DataRow dr in drArray)
                        {
                            dr["SortOrder"] = (int)dr["SortOrder"] + numRowsToInsert;
                        }
                    }
                    else
                    {
                        firstSortOrder = (int)dtLocation.Compute("Max(SortOrder)", null) + 1;
                    }

                    int thisSortOrder = firstSortOrder;
                    int firstId = 0;
                    Location dropLoc;

                    while (dropLocs.Count > 0)
                    {
                        dropLoc = dropLocs.Dequeue();
                        if (firstId == 0)
                            firstId = dropLoc.Id;

                        drLocation = dtLocation.Rows.Find(dropLoc.Id);
                        drLocation["SortOrder"] = thisSortOrder;
                        drLocation["Visible"] = true;
                        thisSortOrder += 1;

                        //and remove from dgvLocationsTemp
                        if (dtLocationTemp.Rows.Contains(dropLoc.Id))
                        {
                            dtLocationTemp.Rows.Find(dropLoc.Id).Delete();
                        }
                    } 

                    ResetSortInLocationTable();
                    LoadLocationsTempGrid(TempIdForRefresh);
                    LoadLocationsGrid(firstId);

                    //If the holding grid is empty then do an update to the database
                    if (dtLocationTemp.Rows.Count == 0)
                    {
                        SaveLocationChangesAfterDragDrop();
                    }
                }
            }
        }

        private void SaveLocationChangesAfterDragDrop()
        {
            try
            {
                //This method is only for updates to the table following a drag drop. All other mods/adds/deletes are dealt with on row validation
                DataTable dtUpds = dtLocation.GetChanges(System.Data.DataRowState.Modified);
                if (dtUpds != null && dtUpds.Rows.Count > 0)
                {
                    Location loc;
                    foreach (DataRow dr in dtUpds.Rows)
                    {
                        loc = BLL.LocationLogic.RetrieveLocationFromId(dr["Id"]);
                        loc.SortOrder = (int)dr["SortOrder"];
                        string msg;
                        if (!BLL.LocationLogic.UpdateLocation(loc, Properties.Settings.Default.UserId, out msg))
                        {
                            MessageBox.Show(msg, Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    dtLocation.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                BLL.ExceptionLogic.HandleException(ex, "frmLocation_SaveLocationChangesAfterDragDrop", true);
            }
        }

        private void ResetSortInLocationTable()
        {
            string PrevFilter = dtLocation.DefaultView.RowFilter;
            dtLocation.DefaultView.RowFilter = string.Empty;
            dvLocation = dtLocation.DefaultView;
            DataRow dr;
            int SortOrder = 1;
            foreach (DataRowView drv in dvLocation)
            {
                dr = drv.Row;
                dr["SortOrder"] = SortOrder;
                SortOrder += 1;
            }
            dtLocation.DefaultView.RowFilter = PrevFilter;
        }

        private void dgvLocationTemp_DragEnter(object sender, DragEventArgs e)
        {
            if (!IsReadOnly)
            {
                if (e.Data.GetDataPresent(typeof(Queue<Location>)))
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;
            }
        }

        private void dgvLocationTemp_DragOver(object sender, DragEventArgs e)
        {
            if (!IsReadOnly)
            {
                if (e.Data.GetDataPresent(typeof(Queue<Location>)))
                    e.Effect = DragDropEffects.Move;
                else
                    e.Effect = DragDropEffects.None;
            }
        }

        private void dgvLocationTemp_DragDrop(object sender, DragEventArgs e)
        {
            if (!IsReadOnly)
            {
                if (e.Data.GetDataPresent(typeof(Queue<Location>)))
                {
                    //If location(s) already in this table then don't do anything (they've been picked up but not moved)
                    //Otherwise add to dgvLocationsTemp and remove from dgvLocations
                    Queue<Location> dropLocs = (Queue<Location>)e.Data.GetData(typeof(Queue<Location>));

                    int firstId = 0;
                    Location dropLoc;

                    while (dropLocs.Count > 0)
                    {
                        dropLoc = dropLocs.Dequeue();
                        if (firstId == 0)
                            firstId = dropLoc.Id;

                        if (!dtLocationTemp.Rows.Contains(dropLoc.Id))
                        {
                            drLocationTemp = dtLocationTemp.NewRow();
                            drLocationTemp["Id"] = dropLoc.Id;
                            drLocationTemp["Code"] = dropLoc.Code;
                            drLocationTemp["Description"] = dropLoc.Description;
                            drLocationTemp["SortOrder"] = dropLoc.SortOrder;
                            drLocationTemp["Active"] = dropLoc.Active;
                            dtLocationTemp.Rows.Add(drLocationTemp);
                        }
                        //and set invisible on dgvLocations
                        DataRow dr;
                        if (dtLocation.Rows.Contains(dropLoc.Id))
                        {
                            dr = dtLocation.Rows.Find(dropLoc.Id);
                            dr["Visible"] = false;
                        }
                    }

                    LoadLocationsGrid(IdForRefresh);
                    LoadLocationsTempGrid(firstId);
                }
            }
        }

        private void LoadLocationsGrid(int selectedId = 0)
        {
            if (chkShowInactive.Checked)
                dtLocation.DefaultView.RowFilter = "Visible = true";
            else
                dtLocation.DefaultView.RowFilter = "Active = true AND Visible = true";
            dtLocation.DefaultView.Sort = "SortOrder ASC";
            dvLocation = dtLocation.DefaultView;

            gridLoading = true;
            int CurrTopRow = dgvLocations.FirstDisplayedScrollingRowIndex;
            if (CurrTopRow < 0)
                CurrTopRow = 0;
            
            dgvLocations.DataSource = dvLocation;

            if (selectedId != 0)
            {
                dgvLocations.FirstDisplayedScrollingRowIndex = CurrTopRow;
                int RowOfSelItem = 0;
                int rowId;
                foreach (DataGridViewRow r in dgvLocations.Rows)
                {
                    if (r.Cells["IdColumn"].Value != null)
                    {
                        if (int.TryParse(r.Cells["IdColumn"].Value.ToString(), out rowId))
                        {
                            if (rowId == selectedId)
                                RowOfSelItem = r.Index;
                        }
                    }
                }
                dgvLocations.CurrentCell = dgvLocations.Rows[RowOfSelItem].Cells["CodeColumn"];
            }
            if (dgvLocations.Rows.Count > 1) //Always be at least one as the new row will be counted.
                recordId = (int)dgvLocations.CurrentRow.Cells["IdColumn"].Value;

            gridLoading = false;
        }

        private void LoadLocationsTempGrid(int selectedId = 0)
        {
            dtLocationTemp.DefaultView.Sort = "SortOrder ASC";
            dvLocationTemp = dtLocationTemp.DefaultView;

            gridLoading = true;
            int CurrTopRow = dgvLocationTemp.FirstDisplayedScrollingRowIndex;
            if (CurrTopRow < 0)
                CurrTopRow = 0;

            dgvLocationTemp.DataSource = dvLocationTemp;

            if (selectedId != 0)
            {
                dgvLocationTemp.FirstDisplayedScrollingRowIndex = CurrTopRow;
                int RowOfSelItem = 0;
                foreach (DataGridViewRow r in dgvLocationTemp.Rows)
                {
                    if (((Nullable<int>)r.Cells["TempIdColumn"].Value ?? 0) == selectedId)
                        RowOfSelItem = r.Index;
                }
                dgvLocationTemp.CurrentCell = dgvLocationTemp.Rows[RowOfSelItem].Cells["TempCodeColumn"];
            }

            gridLoading = false;
        }


        private void dgvLocationTemp_MouseDown(object sender, MouseEventArgs e)
        {
            
            //Don't do this check if the shift or control key are pressed as the user is in the process of selecting the rows to move
            if (ModifierKeys != Keys.Shift && ModifierKeys != Keys.Control)
            {
                //Get the index of the item the mouse is below
                dgvLocationTemp.rowIndexFromMouseDown = dgvLocationTemp.HitTest(e.X, e.Y).RowIndex;
                if (dgvLocationTemp.rowIndexFromMouseDown != -1)
                {
                    //The DragSize indicates the size that the mouse can move before a drag event should be started
                    Size dragSize = SystemInformation.DragSize;
                    //Create a rectangle using the DragSize, with the mouse position being
                    // at the center of the rectangle.
                    dgvLocationTemp.dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                    e.Y - (dragSize.Height / 2)),
                    dragSize);
                }
                else
                    // Reset the rectangle if the mouse is not over an item in the ListBox.
                    dgvLocationTemp.dragBoxFromMouseDown = Rectangle.Empty;

            }
        }

        private void dgvLocationTemp_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left && (ModifierKeys != Keys.Shift && ModifierKeys != Keys.Control))
            {
                if (dgvLocationTemp.rowIndexFromMouseDown >= 0 && !IsReadOnly)
                {
                    //If the mouse moves outside the rectangle start the drag.
                    if (dgvLocationTemp.dragBoxFromMouseDown != Rectangle.Empty && !dgvLocationTemp.dragBoxFromMouseDown.Contains(e.X, e.Y))
                    {
                        if (!dgvLocationTemp.SelectedRows.Contains(dgvLocationTemp.Rows[dgvLocationTemp.rowIndexFromMouseDown]))
                        {
                            dgvLocationTemp.ClearSelection();
                            dgvLocationTemp.Rows[dgvLocationTemp.rowIndexFromMouseDown].Selected = true;
                        }
                        Queue<Location> dragLocs = new Queue<Location>();
                        Location dragLoc;
                        int TopRowIndex = 0;
                        //Ensure selected rows are ordered by original sort order
                        SortedList<int, DataGridViewRow> sortedRows = new SortedList<int, DataGridViewRow>();
                        foreach (DataGridViewRow dgvr in dgvLocationTemp.SelectedRows)
                        {
                            sortedRows.Add((int)dgvr.Cells["TempSortOrderColumn"].Value, dgvr);
                        }
                        DataGridViewRow dr;
                        foreach (int key in sortedRows.Keys)
                        {
                            dr = sortedRows[key];
                            if (!dr.IsNewRow)
                            {
                                if (TopRowIndex == 0 || TopRowIndex > dr.Index)
                                    TopRowIndex = dr.Index;

                                dragLoc = LocationLogic.RetrieveLocationFromId((int)dr.Cells["TempIdColumn"].Value);
                                dragLocs.Enqueue(dragLoc);
                            }
                        }
                        if (dragLocs.Count > 0)
                        {
                            //Get id of record above top selected row
                            if (TopRowIndex == 0 || dragLocs.Count == dgvLocationTemp.Rows.Count)
                                TempIdForRefresh = 0;
                            else
                                TempIdForRefresh = (int)dgvLocationTemp.Rows[TopRowIndex - 1].Cells["TempIdColumn"].Value;

                            dgvLocationTemp.DoDragDrop(dragLocs, DragDropEffects.Move);
                        }
                    }
                }
            }
        }
        
        private void frmLocation_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Don't allow close if there are still items in the temp grid
            if (dgvLocationTemp.Rows.Count > 0)
            {
                MessageBox.Show("Please move the rows from the holding grid to the main grid before moving away from this screen",
                    Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.formIsClosing = false;
                e.Cancel = true;
            }
        }

        private void dgvLocations_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!gridLoading && e.RowIndex >= 0 && !dgvLocations.Rows[e.RowIndex].IsNewRow && dgvLocations.IsCurrentRowDirty)
            {
                int LocId;
                if (!int.TryParse(dgvLocations.Rows[e.RowIndex].Cells["IdColumn"].Value.ToString(), out LocId))
                {
                    //If no code or description present then don't bother!
                    if (dgvLocations.Rows[e.RowIndex].Cells["CodeColumn"].Value.ToString() == string.Empty &&
                        dgvLocations.Rows[e.RowIndex].Cells["DescriptionColumn"].Value.ToString() == string.Empty)
                    {
                        e.Cancel = true;
                        return;
                    }
                    //New record
                    Location newLoc = new Location();
                    newLoc.Code = dgvLocations.Rows[e.RowIndex].Cells["CodeColumn"].Value.ToString();
                    newLoc.Description = dgvLocations.Rows[e.RowIndex].Cells["DescriptionColumn"].Value.ToString();
                    newLoc.SortOrder = (int)dtLocation.Compute("Max(SortOrder)", null) + 1;
                    string msg;
                    int newLocId = LocationLogic.AddLocation(newLoc, Properties.Settings.Default.UserId, out msg);
                    if (newLocId == 0)
                    {
                        MessageBox.Show("New record failed validation. Please hover over error icon to see why", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dgvLocations.Rows[e.RowIndex].ErrorText = msg;
                        dgvLocations.CurrentCell.Selected = false;
                        GridIsValid = false;
                        e.Cancel = true;
                    }
                    else
                    {
                        dgvLocations.Rows[e.RowIndex].Cells["SortOrderColumn"].Value = newLoc.SortOrder;
                        dgvLocations.Rows[e.RowIndex].ErrorText = string.Empty;
                        GridIsValid = true;
                        dgvLocations.Rows[e.RowIndex].Cells["IdColumn"].Value = newLocId;
                    }
                }
                else //update existing record
                {
                    if (dgvLocations.IsCurrentRowDirty)
                    {
                        Location updLoc = LocationLogic.RetrieveLocationFromId((int)dgvLocations.Rows[e.RowIndex].Cells["IdColumn"].Value);
                        updLoc.Code = dgvLocations.Rows[e.RowIndex].Cells["CodeColumn"].Value.ToString();
                        updLoc.Description = dgvLocations.Rows[e.RowIndex].Cells["DescriptionColumn"].Value.ToString();
                        string msg;
                        if (!LocationLogic.UpdateLocation(updLoc, Properties.Settings.Default.UserId, out msg))
                        {
                            MessageBox.Show("Changes failed validation. Please hover over error icon to see why.", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            dgvLocations.Rows[e.RowIndex].ErrorText = msg;
                            dgvLocations.CurrentCell.Selected = false;
                            GridIsValid = false;
                            e.Cancel = true;
                        }
                        else
                        {
                            dgvLocations.Rows[e.RowIndex].ErrorText = string.Empty;
                            GridIsValid = true;
                        }
                    }
                }
            }
        }

        private void dgvLocations_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Deal with hit of delete key
            if (e.RowIndex >= 0 && e.ColumnIndex == dgvLocations.Columns["DeleteColumn"].Index)
            {
                if (e.ColumnIndex == dgvLocations.Columns["DeleteColumn"].Index)
                {
                    if ((bool)dgvLocations.Rows[e.RowIndex].Cells["ActiveColumn"].Value)
                    {
                        if (LocationLogic.IsLocationUsed((int)dgvLocations.Rows[e.RowIndex].Cells["IdColumn"].Value))
                        {
                            MessageBox.Show("This location is in use and cannot be set to inactive", Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else if (MessageBox.Show("Are you sure you want to set this location to inactive?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            dgvLocations.Rows[e.RowIndex].Cells["ActiveColumn"].Value = false;
                        }
                    }
                    else
                    {
                        dgvLocations.Rows[e.RowIndex].Cells["ActiveColumn"].Value = true;
                    }
                }
            }
        }
    }
}
