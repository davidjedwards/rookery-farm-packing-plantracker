﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFP;
using BLL;

namespace Stock_Management_Desktop.Forms
{
    public partial class frmMainForm : Form
    {
        private frmBase currentForm;
        private User LoggedInUser;
        private bool ExitingViaMenu;
        private Button selectedButton;
        private int navigation = 0;

        public frmMainForm()
        {
            InitializeComponent();
        }

        private void frmMainForm_Load(object sender, EventArgs e)
        {

            frmLogon frm = new frmLogon();
            frm.ShowDialog();
            if (frm.DialogResult == System.Windows.Forms.DialogResult.Cancel)
            {
                ExitingViaMenu = true;
                Application.Exit();
                frm.Dispose();
            }
            else
            {
                frm.Dispose();
                LoggedInUser = UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
                SetPermissions();
                pnlLeft.Visible = true;
                //Disable the horizontal scroll bar on navigation panel
                pnlOptions.VerticalScroll.Visible = true;
                if (ShowForm(new frmStockMaintenance()))
                {
                    btnStockMaintenance.Image = Properties.Resources.Stock_Maintenance_Rollover;
                    selectedButton = btnStockMaintenance;
                }
            }
        }

        private void SetPermissions()
        {
            Globals.Permission perm = (Globals.Permission)LoggedInUser.Permission;
            //What is visible to the user will depend on whether or not they are attached to a retailer
            bool IsRetailer = (LoggedInUser.Retailer != null);
            configurationToolStripMenuItem.Visible = !IsRetailer && perm == Globals.Permission.Full;
            importExportToolStripMenuItem.Visible = !IsRetailer;
            btnRetailers.Visible = !IsRetailer;
            btnBatches.Visible = !IsRetailer;
            btnLocations.Visible = !IsRetailer;
            btnReplenishLists.Visible = !IsRetailer;
            btnStockTake.Visible = !IsRetailer;
        }

       private bool ShowForm(frmBase frm)
       {
           try
           {
               bool formCloseSuccessful = true;
               if (currentForm == null || currentForm.Name != frm.Name)
               {
                   if (currentForm != null)
                   {
                       currentForm.formIsClosing = true;
                       currentForm.Close();
                       //If there was a problem closing the form the formIsClosing will have been reset to true by the form
                       if (!currentForm.formIsClosing)
                           formCloseSuccessful = false;
                       else
                       {
                           formCloseSuccessful = true;

                           pnlMain.Controls.Remove(currentForm);
                           currentForm.Dispose();
                       }
                   }

                   if (formCloseSuccessful)
                   {
                       currentForm = frm;
                       currentForm.TopLevel = false;
                       pnlMain.Controls.Add(currentForm);
                       pnlMain.Controls[0].BringToFront();
                       currentForm.Dock = DockStyle.Fill;
                       currentForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                       currentForm.Show();
                       return true;
                   }
                   else
                       return false;
               }
               else
                   return false;
           }
           catch (Exception ex)
           {
               ExceptionLogic.HandleException(ex, this.Name + "_ShowForm", true);
               return false;
           }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            FormatOptionButtons(btn);
        }

        private void FormatOptionButtons(Button btn)
        {
            bool showFormSuccess = false;
            switch (btn.Name)
            {
                case "btnStockMaintenance":
                    showFormSuccess = ShowForm(new frmStockMaintenance());
                    if (showFormSuccess)
                    {
                        btnStockMaintenance.Image = Properties.Resources.Stock_Maintenance_Rollover;
                        selectedButton = btnStockMaintenance;
                    }
                    break;
                case "btnBatches":
                    showFormSuccess = ShowForm(new frmBatch());
                    if (showFormSuccess)
                    {
                        btnBatches.Image = Properties.Resources.Batch_Maintenance_Rollover;
                        selectedButton = btnBatches;
                    }
                    break;
                case "btnComponents":
                    showFormSuccess = ShowForm(new frmComponentMaintenance());
                    if (showFormSuccess)
                    {
                        btnComponents.Image = Properties.Resources.Component_Maintenance_Rollover;
                        selectedButton = btnComponents;
                    }
                    break;
                case "btnLocations":
                    showFormSuccess = ShowForm(new frmLocation());
                    if (showFormSuccess)
                    {
                        btnLocations.Image = Properties.Resources.Location_Maintenance_Rollover;
                        selectedButton = btnLocations;
                    }
                    break;
                case "btnRetailers":
                    showFormSuccess = ShowForm(new frmRetailer());
                    if (showFormSuccess)
                    {
                        btnRetailers.Image = Properties.Resources.Retailers_Rollover;
                        selectedButton = btnRetailers;
                    }
                    break;
                case "btnReplenishLists":
                    showFormSuccess = ShowForm(new frmReplenishLists());
                    if (showFormSuccess)
                    {
                        btnReplenishLists.Image = Properties.Resources.Replenish_Lists_Rollover;
                        selectedButton = btnReplenishLists;
                    }
                    break;
                case "btnStockTake":
                    showFormSuccess = ShowForm(new frmStockTake());
                    if (showFormSuccess)
                    {
                        btnStockTake.Image = Properties.Resources.Stock_Take_Rollover;
                        selectedButton = btnStockTake;
                    }
                    break;
            }
            if (showFormSuccess)
            {
                ResetButtons();
            }
        }

        private void ResetButtons()
        {
            if (selectedButton.Name != "btnStockMaintenance")
                btnStockMaintenance.Image = Properties.Resources.Stock_Maintenance;
            if (selectedButton.Name != "btnBatches")
                btnBatches.Image = Properties.Resources.Batch_Maintenance;
            if (selectedButton.Name != "btnComponents")
                btnComponents.Image = Properties.Resources.Component_Maintenance;
            if (selectedButton.Name != "btnLocations")
                btnLocations.Image = Properties.Resources.Location_Maintenance;
            if (selectedButton.Name != "btnRetailers")
                btnRetailers.Image = Properties.Resources.Retailers;
            if (selectedButton.Name != "btnReplenishLists")
                btnReplenishLists.Image = Properties.Resources.Replenish_Lists;
            if (selectedButton.Name != "btnStockTake")
                btnStockTake.Image = Properties.Resources.Stock_Take;
        }


        private void usersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmUser frm = new frmUser();
            frm.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Are you sure you want to exit?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                ExitingViaMenu = true;
                Application.Exit();
            }
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmChangePW frm = new frmChangePW();
            frm.ShowDialog();
            frm.Close();
            frm.Dispose();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to log out?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                bool formCloseSuccessful = true;
                if (currentForm != null)
                {
                    currentForm.formIsClosing = true;
                    currentForm.Close();
                    //If there was a problem closing the form the formIsClosing will have been reset to true by the form
                    if (!currentForm.formIsClosing)
                        formCloseSuccessful = false;
                    else
                    {
                        formCloseSuccessful = true;

                        pnlMain.Controls.Remove(currentForm);
                        pnlLeft.Visible = false;
                        currentForm.Dispose();
                        currentForm = null;
                    }
                }
                if (formCloseSuccessful)
                {
                    frmLogon frm = new frmLogon();
                    frm.ShowDialog();
                    if (frm.DialogResult == System.Windows.Forms.DialogResult.Cancel)
                    {
                        ExitingViaMenu = true;
                        Application.Exit();
                        frm.Dispose();
                    }
                    else
                    {
                        frm.Dispose();
                        LoggedInUser = UserLogic.RetrieveUser(Properties.Settings.Default.UserId);
                        SetPermissions();
                        pnlLeft.Visible = true;
                        FormatOptionButtons(btnStockMaintenance);

                    }
                }
            }
        }


        private void frmMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ExitingViaMenu)
            {
                if (MessageBox.Show("Are you sure you want to exit?", Properties.Settings.Default.SystemName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void remoteSupportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Team Viewer will be downloaded onto your machine. When asked what action to perform on the download, click Run. " +
                            "When the Team Viewer window appears, tell the software support engineer the 9 digit code and the password.", 
                            Properties.Settings.Default.SystemName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            System.Diagnostics.Process.Start("http://www.totalsolutions.co.uk/downloads/TeamViewerQS.exe");
        }

        private void optionListsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConfiguration frm = new frmConfiguration();
            frm.Show();
        }

        private void importReplenishListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImport frm = new frmImport();
            frm.ShowDialog();
            if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                //if replenish form open then reload grid
                if (currentForm.Name == "frmReplenishLists")
                {
                    frmReplenishLists rplFrm = (frmReplenishLists)currentForm;
                    rplFrm.LoadGrid();
                }
            }
        }

        private void importErrorListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImportErrors frm = new frmImportErrors();
            frm.Show();
        }

        private void exportStockDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmExport frm = new frmExport();
            frm.ShowDialog();
        }

        private void viewExportErrorListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmExportErrors frm = new frmExportErrors();
            frm.Show();
        }
        
        private void button_MouseEnter(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn != selectedButton)
            {
                switch (btn.Name)
                {
                    case "btnStockMaintenance":
                        btnStockMaintenance.Image = Properties.Resources.Stock_Maintenance_Rollover;
                        break;
                    case "btnBatches":
                        btnBatches.Image = Properties.Resources.Batch_Maintenance_Rollover;
                        break;
                    case "btnComponents":
                        btnComponents.Image = Properties.Resources.Component_Maintenance_Rollover;
                        break;
                    case "btnLocations":
                        btnLocations.Image = Properties.Resources.Location_Maintenance_Rollover;
                        break;
                    case "btnRetailers":
                        btnRetailers.Image = Properties.Resources.Retailers_Rollover;
                        break;
                    case "btnReplenishLists":
                        btnReplenishLists.Image = Properties.Resources.Replenish_Lists_Rollover;
                        break;
                    case "btnStockTake":
                        btnStockTake.Image = Properties.Resources.Stock_Take_Rollover;
                        break;
                }
            }
        }


        private void button_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn != selectedButton)
            {
                switch (btn.Name)
                {
                    case "btnStockMaintenance":
                        btnStockMaintenance.Image = Properties.Resources.Stock_Maintenance;
                        break;
                    case "btnBatches":
                        btnBatches.Image = Properties.Resources.Batch_Maintenance;
                        break;
                    case "btnComponents":
                        btnComponents.Image = Properties.Resources.Component_Maintenance;
                        break;
                    case "btnLocations":
                        btnLocations.Image = Properties.Resources.Location_Maintenance;
                        break;
                    case "btnRetailers":
                        btnRetailers.Image = Properties.Resources.Retailers;
                        break;
                    case "btnReplenishLists":
                        btnReplenishLists.Image = Properties.Resources.Replenish_Lists;
                        break;
                    case "btnStockTake":
                        btnStockTake.Image = Properties.Resources.Stock_Take;
                        break;
                }
            }
        }

        private void btnNavDown_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                timNavDown.Enabled = true;
                btnNavDown.Image = Properties.Resources.down_arrow_rollover;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "btnNavDown_MouseEnter", true);
            }
        }

        private void btnNavDown_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                timNavDown.Enabled = false;
                btnNavDown.Image = Properties.Resources.down_arrow;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "btnNavDown_MouseLeave", true);
            }

        }

        private void btnNavUp_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                timNavUp.Enabled = true;
                btnNavUp.Image = Properties.Resources.up_arrow_rollover;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "btnNavUp_MouseEnter", true);
            }
        }

        private void btnNavUp_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                timNavUp.Enabled = false;
                btnNavUp.Image = Properties.Resources.up_arrow;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "btnNavUp_MouseLeave", true);
            }
        }

        private void timNavUp_Tick(object sender, EventArgs e)
        {
            try
            {
                if ((navigation - 10) <= this.pnlOptions.VerticalScroll.Minimum)
                    navigation = this.pnlOptions.VerticalScroll.Minimum;
                else
                    navigation -= 10;

                this.pnlOptions.VerticalScroll.Value = navigation;
                if (navigation == this.pnlOptions.VerticalScroll.Minimum)
                    timNavDown.Enabled = false;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "timNavDown_Tick", true);
            }
        }

        private void timNavDown_Tick(object sender, EventArgs e)
        {
            try
            {
                if ((navigation + 10) >= this.pnlOptions.VerticalScroll.Maximum)
                    navigation = this.pnlOptions.VerticalScroll.Maximum;
                else
                    navigation += 10;

                this.pnlOptions.VerticalScroll.Value = navigation;
                if (navigation == this.pnlOptions.VerticalScroll.Maximum)
                    timNavUp.Enabled = false;
            }
            catch (Exception ex)
            {
                ExceptionLogic.HandleException(ex, this.Name + "timNavDown_Tick", true);
            }
        }

        private void timNotify_Tick(object sender, EventArgs e)
        {
            int env = Properties.Settings.Default.Environment;

            lblReplenishListsAvail.Visible = FactorLogic.DownloadsAvailable(env);
            lblDownloadsError.Visible = FactorLogic.DownloadError(env);
            lblUploadErrors.Visible = FactorLogic.UploadError(env);
        }

        private void lblReplenishListsAvail_Click(object sender, EventArgs e)
        {
            int env = Properties.Settings.Default.Environment;

            Factor f = FactorLogic.RetrieveFactor(env);
            f.ShowDownloadsAvailable = false;
            FactorLogic.Update(f);
            lblReplenishListsAvail.Visible = false;
        }

        private void lblDownloadsError_Click(object sender, EventArgs e)
        {
            int env = Properties.Settings.Default.Environment;

            Factor f = FactorLogic.RetrieveFactor(env);
            f.ShowDownloadError = false;
            FactorLogic.Update(f);
            lblDownloadsError.Visible = false;
        }

        private void lblUploadErrors_Click(object sender, EventArgs e)
        {
            int env = Properties.Settings.Default.Environment;

            Factor f = FactorLogic.RetrieveFactor(env);
            f.ShowUploadError = false;
            FactorLogic.Update(f);
            lblUploadErrors.Visible = false;
        }


    }
}
