﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Stock_Management_Desktop.Classes
{
    public partial class DataGridViewPlus : DataGridView
    {
        private bool _allowMultiRowDrag = false;
        public bool AllowMultiRowDrag
        {
            get { return _allowMultiRowDrag; }
            set { _allowMultiRowDrag = value; }
        }

        private Type _rowCollectionType = typeof(DataGridViewRow[]);
        public Type RowCollectionType
        {
            get { return _rowCollectionType; }
            set { _rowCollectionType = value; }
        }

        private Rectangle _dragBoxFromMouseDown;
        public Rectangle dragBoxFromMouseDown
        {
            get { return _dragBoxFromMouseDown; }
            set { _dragBoxFromMouseDown = value; }
        }


        public int rowIndexFromMouseDown;
        public MouseEventArgs lastLeftMouseDownArgs;

        public DataGridViewPlus()
        {
            InitializeComponent();
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            //Don't do this for button columns as we want the cellclick event to fire
            int colIndex = this.HitTest(e.X, e.Y).ColumnIndex;
            if (colIndex >= 0)
            {
                Type colType = Columns[colIndex].GetType();

                if (colType.Name != "DataGridViewButtonColumn")
                {

                    if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                    {
                        rowIndexFromMouseDown = this.HitTest(e.X, e.Y).RowIndex;
                        if (rowIndexFromMouseDown != -1)
                        {
                            Size dragSize = SystemInformation.DragSize;
                            _dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                            lastLeftMouseDownArgs = e; //remember the MouseEventArgs

                            if (AllowMultiRowDrag)
                            {
                                //Don't call base function to keep the selection
                                return;
                            }
                        }
                        else
                        {
                            _dragBoxFromMouseDown = Rectangle.Empty;
                            lastLeftMouseDownArgs = null;
                        }
                    }
                }
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            //Don't do this for button columns
            int colIndex = this.HitTest(e.X, e.Y).ColumnIndex;
            if (colIndex >= 0)
            {
                Type colType = Columns[colIndex].GetType();

                if (colType.Name != "DataGridViewButtonColumn")
                {

                    if (lastLeftMouseDownArgs != null && (e.Button & MouseButtons.Left) == MouseButtons.Left)
                    {
                        //There has been a multiselect drag operation so catch up the MouseDown event
                        if (AllowMultiRowDrag)
                            base.OnMouseDown(lastLeftMouseDownArgs);
                    }
                }
            }
            base.OnMouseUp(e);
        }
    }
}
