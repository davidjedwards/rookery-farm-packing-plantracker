﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;

namespace Stock_Management_Desktop.Classes
{
    public class clsFieldValidation
    {
        public enum InputTypes { Text, Number, DateTime }

        private bool _exclude;

        public bool Exclude
        {
            get { return _exclude; }
            set { _exclude = value; }
        }

        private Control _controlToValidate;

        public Control ControlToValidate
        {
            get { return _controlToValidate; }
            set { _controlToValidate = value; }
        }

        private InputTypes _inputType;

        public InputTypes InputType
        {
            get { return _inputType; }
            set { _inputType = value; }
        }

        private bool _required;

        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        public clsFieldValidation(Control controlToValidate, InputTypes inputType, bool required, bool exclude = false)
        {
            _controlToValidate = controlToValidate;
            _inputType = inputType;
            _required = required;
            _exclude = exclude;
        }
    }
}
