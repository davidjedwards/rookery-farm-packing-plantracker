﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Deployment;
using System.Reflection;

namespace Stock_Management_Desktop.Classes
{
    public static class Functions
    {
        public static void FormatButton(Button pBtn)
        {
            pBtn.FlatAppearance.BorderColor = Properties.Settings.Default.CompanyColour;
            pBtn.BackColor = Properties.Settings.Default.FormBackground;
            pBtn.FlatAppearance.BorderSize = 1;
        }

        public static void StyleDataGridView(DataGridView dg)
        {
            dg.EnableHeadersVisualStyles = false;
            dg.RowsDefaultCellStyle.BackColor = Properties.Settings.Default.DGRow1Colour;
            dg.RowsDefaultCellStyle.SelectionBackColor = Properties.Settings.Default.OptionChosenColour;
            dg.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dg.AlternatingRowsDefaultCellStyle.SelectionBackColor = Properties.Settings.Default.OptionChosenColour;
            dg.AlternatingRowsDefaultCellStyle.BackColor = Properties.Settings.Default.DGRow2Colour;
            dg.AlternatingRowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dg.AutoGenerateColumns = false;
            dg.ColumnHeadersDefaultCellStyle.BackColor = Properties.Settings.Default.CompanyColour;
            dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dg.ColumnHeadersDefaultCellStyle.Font = Properties.Settings.Default.DGHdrFont;
            dg.RowHeadersDefaultCellStyle.BackColor = Properties.Settings.Default.CompanyColour;
            dg.RowHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dg.BackgroundColor = Properties.Settings.Default.FormBackground;
            dg.MultiSelect = false;
        }

        public static string GetRunningVersion()
        {
            string version = Application.ProductVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed) 
            {
              System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
              version = ad.CurrentVersion.ToString();
            }
            return version;
        }
    }
}
