﻿using System.Windows.Forms;
using System.Reflection;

namespace RFP
{
    public class DataGridViewChildRelationTextBoxCell : DataGridViewTextBoxCell
    {
        protected override object GetValue(int rowIndex)
        {
            try
            {
                BindingSource bs = (BindingSource)DataGridView.DataSource;
                DataGridViewChildRelationTextBoxColumn cl = (DataGridViewChildRelationTextBoxColumn)DataGridView.Columns[ColumnIndex];
                return getChildValue(bs.List[rowIndex], cl.DataPropertyName).ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        private object getChildValue(object dataSource, string childMember)
        {
            try
            {

                int nextPoint = childMember.IndexOf('.');
                if (nextPoint == -1)
                    return dataSource.GetType().GetProperty(childMember).GetValue(dataSource, null);
                string proName = childMember.Substring(0, nextPoint);
                object newDs = dataSource.GetType().GetProperty(proName).GetValue(dataSource, null);
                return getChildValue(newDs, childMember.Substring(nextPoint + 1));
            }
            catch
            {
                return null;
            }
        }
    }
}
