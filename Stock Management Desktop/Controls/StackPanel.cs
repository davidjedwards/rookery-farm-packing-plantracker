﻿using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Layout;
using System.ComponentModel;

namespace Stock_Management_Desktop.Controls
{
    public partial class StackPanel : Panel
    {
        public StackPanel()
        {
            InitializeComponent();
            base.AutoScroll = true;
        }

        public StackPanel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public new bool AutoScroll 
        {
            get
            {
                return base.AutoScroll;
            }
            set
            {
                base.AutoScroll = value;
            }
        }

        public enum StackFlow {Horizontal, Vertical}

        private StackFlow _stackFlowLayout;
	    public StackFlow StackFlowLayout
	    {
		    get { return _stackFlowLayout;}
		    set 
            {
                this.SuspendLayout();
                _stackFlowLayout = value;
                this.ResumeLayout(true);
                this.Update();
                this.Refresh();
            }
	    }
	
        private bool _stackFullWidth;
	    public bool StackFullWidth
	    {
		    get { return _stackFullWidth;}
		    set { _stackFullWidth = value;}
	    }
	
        public new LayoutEngine LayoutEngine
        {
            get { return StackLayout.Instance; }
        }

        private class StackLayout : LayoutEngine
        {
            public static StackLayout Instance;

            public StackLayout()
            {
                Instance = new StackLayout();
            }

            public override bool Layout(object container, LayoutEventArgs layoutEventArgs)
            {
                StackPanel stackPanel;
                if (container.GetType() == typeof(StackPanel))
                {
                    stackPanel = (StackPanel)container;
                }
                else
                {
                    return false;
                }
                Rectangle displayRectangle = stackPanel.DisplayRectangle;
                Point nextControlLocation = displayRectangle.Location;

                foreach (Control ctl in stackPanel.Controls)
                {
                    if (ctl.Visible)
                    {
                        nextControlLocation.Offset(ctl.Margin.Left, ctl.Margin.Top);
                        ctl.Location = nextControlLocation;

                        if (stackPanel.StackFlowLayout == StackFlow.Horizontal)
                        {
                            nextControlLocation.X = ctl.Width + ctl.Margin.Left;
                            nextControlLocation.Y += displayRectangle.Y;
                        }
                        else
                        {
                            if (stackPanel.StackFullWidth)
                            {
                                Size size = ctl.Size;
                                size.Width = displayRectangle.Width = ctl.Margin.Left - ctl.Margin.Right;
                                ctl.Size = size;
                            }
                            nextControlLocation.X = displayRectangle.X;
                            nextControlLocation.Y += ctl.Height + ctl.Margin.Bottom;
                        }
                    }
                }
                return false;
            }
        }
    }
}
