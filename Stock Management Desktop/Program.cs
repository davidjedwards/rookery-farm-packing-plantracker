﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Stock_Management_Desktop.Forms;

namespace Stock_Management_Desktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMainForm());
        }
    }
}
