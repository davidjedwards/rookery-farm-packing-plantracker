﻿CREATE TABLE [dbo].[TabletFactor] (
    [Id]         INT NOT NULL,
    [UploadDone] BIT DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

