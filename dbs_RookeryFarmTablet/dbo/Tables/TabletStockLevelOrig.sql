﻿CREATE TABLE [dbo].[TabletStockLevelOrig] (
    [Id]            INT           NOT NULL,
    [Location]      INT           NULL,
    [Batch]         INT           NULL,
    [Quantity]      INT           NULL,
    [Quality]       INT           NULL,
    [ShippableWeek] INT           NULL,
    [ShippableYear] INT           NULL,
    [Query]         BIT           NOT NULL,
    [QueryType]     INT           NULL,
    [QueryTypeDesc] NVARCHAR (50) NULL,
    CONSTRAINT [PK_TabletStockLevelOrig] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TabletStockLevelOrig_TabletBatch] FOREIGN KEY ([Batch]) REFERENCES [dbo].[TabletBatch] ([Id]),
    CONSTRAINT [FK_TabletStockLevelOrig_TabletLocation] FOREIGN KEY ([Location]) REFERENCES [dbo].[TabletLocation] ([Id]),
    CONSTRAINT [FK_TabletStockLevelOrig_TabletStockLevel] FOREIGN KEY ([Id]) REFERENCES [dbo].[TabletStockLevel] ([Id])
);

