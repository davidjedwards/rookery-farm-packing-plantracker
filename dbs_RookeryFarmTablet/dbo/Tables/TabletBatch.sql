﻿CREATE TABLE [dbo].[TabletBatch] (
    [Id]        INT           NOT NULL,
    [Component] INT           NULL,
    [BatchDate] DATETIME      NULL,
    [BatchNo]   INT           NULL,
    [New]       BIT           CONSTRAINT [DF_TabletBatch_New] DEFAULT ((0)) NOT NULL,
    [CreatedBy] NVARCHAR (15) NULL,
    CONSTRAINT [PK_TabletBatches] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_IX_Component_Batch] FOREIGN KEY ([Component]) REFERENCES [dbo].[TabletComponent] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_Component_Batch]
    ON [dbo].[TabletBatch]([Component] ASC);

