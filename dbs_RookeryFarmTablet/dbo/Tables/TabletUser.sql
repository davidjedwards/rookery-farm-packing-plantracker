﻿CREATE TABLE [dbo].[TabletUser] (
    [Id]               INT            NOT NULL,
    [Logon]            NVARCHAR (15)  NULL,
    [FullName]         NVARCHAR (128) NULL,
    [Password]         NVARCHAR (64)  NULL,
    [LastModifiedDate] DATETIME       NULL,
    [LastModifiedBy]   NVARCHAR (15)  NULL,
    CONSTRAINT [PK_TabletUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

