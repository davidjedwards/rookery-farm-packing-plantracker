﻿CREATE TABLE [dbo].[TabletReasonsForChange] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Reason] VARCHAR (255) NULL,
    CONSTRAINT [PK_TabletReasonsForChange] PRIMARY KEY CLUSTERED ([Id] ASC)
);

