﻿CREATE TABLE [dbo].[TabletStockLevel] (
    [Id]             INT            NOT NULL,
    [Location]       INT            NULL,
    [Batch]          INT            NULL,
    [Quantity]       INT            NULL,
    [Quality]        INT            NULL,
    [ShippableWeek]  INT            NULL,
    [ShippableYear]  INT            NULL,
    [Query]          BIT            NOT NULL,
    [QueryType]      INT            NULL,
    [QueryTypeDesc]  NVARCHAR (50)  NULL,
    [StockTake]      BIT            CONSTRAINT [DF_TabletStockLevel_StockTake] DEFAULT ((0)) NOT NULL,
    [ReplenDetailId] INT            NULL,
    [Status]         INT            CONSTRAINT [DF_TabletStockLevel_Status] DEFAULT ((0)) NOT NULL,
    [FileName]       NVARCHAR (255) NULL,
    CONSTRAINT [PK_TabletStockLevels] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_IX_Batch_StockLevel] FOREIGN KEY ([Batch]) REFERENCES [dbo].[TabletBatch] ([Id]),
    CONSTRAINT [FK_IX_ConfigurationItem_StockLevel] FOREIGN KEY ([QueryType]) REFERENCES [dbo].[TabletConfigurationItem] ([Id]),
    CONSTRAINT [FK_IX_Location_StockLevel] FOREIGN KEY ([Location]) REFERENCES [dbo].[TabletLocation] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_Batch_StockLevel]
    ON [dbo].[TabletStockLevel]([Batch] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_ConfigurationItem_StockLevel]
    ON [dbo].[TabletStockLevel]([QueryType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_Location_StockLevel]
    ON [dbo].[TabletStockLevel]([Location] ASC);

