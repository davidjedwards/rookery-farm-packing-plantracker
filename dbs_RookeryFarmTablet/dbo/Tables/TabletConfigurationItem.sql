﻿CREATE TABLE [dbo].[TabletConfigurationItem] (
    [Id]                 INT           NOT NULL,
    [ConfigurationGroup] INT           NULL,
    [ItemValue]          NVARCHAR (50) NULL,
    [New]                BIT           CONSTRAINT [DF_TabletConfigurationItem_New] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TabletConfigurationitems] PRIMARY KEY CLUSTERED ([Id] ASC)
);

