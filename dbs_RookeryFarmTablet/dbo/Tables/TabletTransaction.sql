﻿CREATE TABLE [dbo].[TabletTransaction] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [StockLevelId]    INT            NULL,
    [Batch]           INT            NULL,
    [ModifiedDate]    DATETIME       NULL,
    [Location]        INT            NULL,
    [Quantity]        INT            NULL,
    [Quality]         INT            NULL,
    [Action]          INT            NULL,
    [ActionedBy]      NVARCHAR (15)  NULL,
    [Reason]          NVARCHAR (255) NULL,
    [PrevQuantity]    INT            NULL,
    [PrevQuality]     INT            NULL,
    [MovedOutId]      INT            NULL,
    [PrevShippableWk] INT            NULL,
    [PrevShippableYr] INT            NULL,
    [ReplenDetailId]  INT            NULL,
    CONSTRAINT [PK_TabletTransactions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_IX_Batch_Transaction] FOREIGN KEY ([Batch]) REFERENCES [dbo].[TabletBatch] ([Id]),
    CONSTRAINT [FK_IX_Location_Transaction] FOREIGN KEY ([Location]) REFERENCES [dbo].[TabletLocation] ([Id]),
    CONSTRAINT [FK_TabletTransaction_TabletStockLevel] FOREIGN KEY ([StockLevelId]) REFERENCES [dbo].[TabletStockLevel] ([Id]) ON DELETE SET NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_Batch_Transaction]
    ON [dbo].[TabletTransaction]([Batch] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_Location_Transaction]
    ON [dbo].[TabletTransaction]([Location] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_IX_User_Transaction]
    ON [dbo].[TabletTransaction]([ActionedBy] ASC);

