﻿CREATE TABLE [dbo].[TabletComponent] (
    [Id]                INT            NOT NULL,
    [Description]       NVARCHAR (128) NULL,
    [RetailerStockCode] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_TabletComponents] PRIMARY KEY CLUSTERED ([Id] ASC)
);

