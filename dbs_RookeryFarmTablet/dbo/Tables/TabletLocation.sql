﻿CREATE TABLE [dbo].[TabletLocation] (
    [Id]          INT            NOT NULL,
    [Code]        NVARCHAR (50)  NULL,
    [Description] NVARCHAR (128) NULL,
    [SortOrder]   INT            NULL,
    CONSTRAINT [PK_TabletLocations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

