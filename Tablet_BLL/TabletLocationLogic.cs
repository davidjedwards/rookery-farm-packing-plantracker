﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tablet_DAL;
using RFP;
using Tablet_DAL.MethodClass;

namespace Tablet_BLL
{
    public static class TabletLocationLogic
    {
        public static LinkedList<TabletLocation> GetLocationsInOrder(bool stockTake = false)
        {
            return TabletLocationHandler.ListLocationsInOrder(stockTake);
        }

        public static LinkedList<TabletLocation> GetLocationsInOrderForProcess(Globals.CropQuality cq)
        {
            return TabletLocationHandler.ListLocationsInOrderForProcess(cq);
        }

        public static TabletLocation RetrieveLocationById(int locationId)
        {
            return TabletLocationHandler.GetLocationById(locationId);
        }

        public static List<TabletLocation> LocationList()
        {
            return TabletLocationHandler.GetLocationList();
        }
    }
}
