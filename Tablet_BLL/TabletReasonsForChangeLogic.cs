﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL;
using Tablet_DAL.MethodClass;

namespace Tablet_BLL
{
    public static class TabletReasonsForChangeLogic
    {
        public static List<string> RetrieveReasonsforChange()
        {
            return TabletReasonsForChangeHandler.GetReasonsForChangeList();
        }

        public static bool Add(TabletReasonsForChange trc)
        {
            return TabletReasonsForChangeHandler.Add(trc);
        }

    }

}
