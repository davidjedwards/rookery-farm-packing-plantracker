﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL;
using System.Windows.Forms;
using Tablet_DAL.MethodClass;

namespace Tablet_BLL
{
    public static class TabletStockLevelLogic
    {
        public static List<TabletStockLevelRow> RetrieveStockLevelsForLocation(int location, bool stockCheck)
        {
            return TabletStockLevelHandler.GetStockLevelsForLocation(location, stockCheck);
        }

        public static List<TabletStockLevelRow> RetrieveStockLevelsForLocationForProcess(int location, Globals.CropQuality cq)
        {
            return TabletStockLevelHandler.GetStockLevelsForLocationForProcess(location, cq);
        }

        public static TabletStockLevel RetrieveStockLevelFromId(int Id)
        {
            return TabletStockLevelHandler.GetStockLevelFromId(Id);
        }

        public static bool UpdateStockLevel(TabletStockLevel updStockLevel, Globals.Action act, int userId, int prevQuantity, int prevQuality, 
            int? prevShippableWeek, int? prevShippableYear, string chgReason = "")
        {
            return TabletStockLevelHandler.UpdateStockLevel(updStockLevel, act, userId, prevQuantity, prevQuality, prevShippableWeek, prevShippableYear, chgReason);
        }

        public static int AddStockLevel(TabletStockLevel newStockLevel, Globals.Action act, int userId)
        {
            return TabletStockLevelHandler.AddStockLevel(newStockLevel, act, userId);
        }

        public static TabletStockLevel SetNewQuality(TabletStockLevel updStockLevel, Globals.CropQuality cq, int userId, int qtyToProcess)
        {
            return TabletStockLevelHandler.SetNewQuality(updStockLevel, cq, userId, qtyToProcess);
        }

        public static bool AnyStockLinesToProcess(Globals.CropQuality cq)
        {
            return TabletStockLevelHandler.StockLinesToProcess(cq);
        }

        public static bool AnyStockCheckLinesToProcess()
        {
            return TabletStockLevelHandler.StockCheckLinesToProcess();
        }

        public static bool AnyLinesToProcess()
        {
            return TabletStockLevelHandler.CheckLinesToProcess();
        }

        public static bool PerformPickOrDispose(TabletStockLevel updStockLevel, Globals.Action act, int userId, int qtyToProcess)
        {
            return TabletStockLevelHandler.PickOrDispose(updStockLevel, act, userId, qtyToProcess);
        }

        public static bool Move(TabletStockLevel updStockLevel, int newLocation, int newQuality, int userId)
        {
            return TabletStockLevelHandler.PerformMove(updStockLevel, newLocation, newQuality, userId);
        }
    }
}
