﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tablet_DAL;
using RFP;
using Tablet_DAL.MethodClass;

namespace Tablet_BLL
{
    public static class TabletConfigurationLogic
    {
        public static List<TabletConfigurationItem> ListConfigurationItemsAsList(ConfigGroup configGroup)
        {
            return TabletConfigurationHandler.GetConfigurationItemsAsList(configGroup);
        }

        public static int AddConfigurationItem(TabletConfigurationItem cfi, out string returnMsg)
        {
            if (cfi.ItemValue == null || cfi.ItemValue == string.Empty)
            {
                returnMsg = "Value cannot be blank.";
                return 0;
            }
            else if (TabletConfigurationHandler.IsDuplicate(cfi))
            {
                string desc = Globals.ConfigGroupDesc((ConfigGroup)cfi.ConfigurationGroup);
                returnMsg = string.Format("There is already a {0} with this value. Please select it from the drop down list", desc);
                return 0;
            }
            else
            {
                returnMsg = string.Empty;
                return TabletConfigurationHandler.Add(cfi);
            }
        }


    }
}
