﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tablet_DAL;

namespace Tablet_BLL
{
    public static class TabletFactorLogic
    {
        public static void SetUploadDone()
        {
            Tablet_DAL.MethodClass.TabletFactorHandler.SetUploadDoneFlag();
        }

        public static void ResetUploadDone()
        {
            Tablet_DAL.MethodClass.TabletFactorHandler.ResetUploadDoneFlag();
        }

        public static bool UploadHasBeenDone()
        {
            return Tablet_DAL.MethodClass.TabletFactorHandler.ReturnUploadDoneFlag();
        }
    }
}
