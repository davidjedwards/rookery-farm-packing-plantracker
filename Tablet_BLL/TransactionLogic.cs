﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL;

namespace Tablet_BLL
{
    public static class TransactionLogic
    {
        public static bool AnyTransactionsToUpload()
        {
            return Tablet_DAL.MethodClass.TabletTransactionHandler.TransactionsExist();
        }
    }
}
