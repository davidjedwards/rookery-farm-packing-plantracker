﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tablet_DAL;
using RFP;
using Tablet_DAL.MethodClass;

namespace Tablet_BLL
{
    public static class TabletUserLogic
    {
        public static void RefreshUserData()
        {
            TabletUserHandler.RefreshUserData();
        }

        public static TabletUser RetrieveUser(int usrId)
        {
            return TabletUserHandler.GetUser(usrId);
        }

        public static bool UpdateUser(TabletUser usr, int loggedInUsr, out string returnMsg)
        {
            TabletUser liUser = RetrieveUser(loggedInUsr);
            returnMsg = string.Empty;
            usr.LastModifiedBy = liUser.Logon;
            usr.LastModifiedDate = DateTime.Now;
            if (Tablet_DAL.MethodClass.TabletUserHandler.Update(usr))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static TabletUser RetrieveUserFromLogon(string logon, out string returnMsg)
        {
            TabletUser thisUser = Tablet_DAL.MethodClass.TabletUserHandler.GetUserFromLogon(logon);
            if (thisUser == null)
            {
                returnMsg = "Username entered has not been found in the system. You may not be set up as a tablet user. Please try again or contact your supervisor";
            }
            else
            {
                returnMsg = string.Empty;
            }
            return thisUser;
        }

        public static string RetrieveUserName(int usrId)
        {
            return TabletUserHandler.GetUserName(usrId);
        }
    }
}
