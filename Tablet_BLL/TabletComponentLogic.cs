﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL;
using Tablet_DAL.MethodClass;
using System.Windows.Forms;

namespace Tablet_BLL
{
    public static class TabletComponentLogic
    {
        public static BindingSource RetrieveComponentList(string Desc, string Code)
        {
            return TabletComponentHandler.GetComponentList(Desc, Code);
        }

        public static TabletComponent RetrieveComponentById(int componentId)
        {
            return TabletComponentHandler.GetComponentById(componentId);
        }
    }
}
