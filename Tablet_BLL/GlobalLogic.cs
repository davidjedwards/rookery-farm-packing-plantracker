﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL;
using Tablet_DAL.MethodClass;
using System.ComponentModel;

namespace Tablet_BLL
{
    public static class GlobalLogic
    {
        public static bool DownloadToTablet(BackgroundWorker worker)
        {
            return GlobalHandler.DownloadData(worker);
        }
        public static bool UploadToDesktop(BackgroundWorker worker)
        {
            return GlobalHandler.UploadData(worker);
        }
        public static void UploadUserPWChange()
        {
            GlobalHandler.UploadUserPWChange();
        }
    }
}
