﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RFP;
using Tablet_DAL;
using Tablet_DAL.MethodClass;

namespace Tablet_BLL
{
    public static class TabletBatchLogic
    {
        public static int AddBatch(TabletBatch batch, int userId, out string returnMsg)
        {
            //Validate that all fields are entered and the batch number is not a duplicate
            if (batch.BatchDate == null)
            {
                returnMsg = "Please enter a batch date";
                return 0;
            }
            else if (batch.BatchNo == null)
            {
                returnMsg = "Please enter a batch number";
                return 0;
            }
            else if (TabletBatchHandler.IsBatchNoAlreadyUsed(batch))
            {
                returnMsg = "Batch number has already been used this year. Please enter another";
                return 0;
            }
            else
            {

                batch.Id = TabletBatchHandler.GetMaxId() + 1;
                batch.New = true;
                int newId = TabletBatchHandler.Add(batch);
                if (newId != 0)
                {
                    returnMsg = string.Empty;
                    return newId;
                }
                else
                {
                    returnMsg = string.Empty;
                    return 0;
                }
            }
        }
    }
}
